import json
from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils import timezone
from post_office import mail

import config
from main.models import Tenant


class Command(BaseCommand):
    help = 'Reminds the BA and affected tenants that their contract ends soon'

    def handle(self, *args, **options):
        date_puffer_until = timezone.now() + timedelta(days=3 * 31)
        tenants = Tenant.objects.filter(tenant_type='0',
                                        date_move_out__gte=timezone.now(),
                                        date_move_out__lt=date_puffer_until)

        ending_contracts = []
        for tenant in tenants:
            mail.send(
                tenant.person.user.email,
                config.EMAIL['EMAIL_ACCOUNT_BA'],
                template='move_out_reminder_tenants',
                context={'name': tenant.person.user.first_name,
                         'date_move_out': tenant.date_move_out},
            )
            ending_contracts = ending_contracts + [{
                "name": tenant.person.full_name(),
                'room': tenant.room.number,
                'date_move_out': tenant.date_move_out.strftime("%d.%m.%Y")
            }]

        mail.send(
            config.EMAIL['EMAIL_ACCOUNT_BA'],
            config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
            template='move_out_reminder_ba',
            context={'ending_contracts': json.dumps(ending_contracts, indent=4),
                     'date_puffer_until': date_puffer_until},
        )
