from datetime import timedelta

from django.core.management.base import BaseCommand
from django.db.models import Q
from django.utils import timezone
from post_office import mail
from webdav3.client import Client

import config
from main.models import Event
from workinggroups.models import WorkingGroup


class Command(BaseCommand):
    help = 'Check if protocols were uploaded for working group meetings'

    def handle(self, *args, **options):
        options = {
            'webdav_hostname': config.NEXTCLOUD['URL'] + "/remote.php/dav/files/" + config.NEXTCLOUD['USERNAME'],
            'webdav_login': config.NEXTCLOUD['USERNAME'],
            'webdav_password': config.NEXTCLOUD['PASSWORD'],
            'disable_check': True,
            'webdav_override_methods': {
                'check': "GET",
            }
        }
        client = Client(options)
        files = client.list("Hilton Dorm/Association/Protocols")
        files = [file.lower() for file in files]

        def exists(event, callback):
            date = str(event.time.date())
            for file in files:
                if file.startswith(date) and callback(file):
                    return True
            return False

        def send_mail(event, working_group: WorkingGroup):
            mail.send(
                working_group.email,
                config.EMAIL["EMAIL_ACCOUNT_VORSTAND"],
                cc=config.EMAIL["EMAIL_ACCOUNT_VORSTAND"],
                headers={'Reply-to': config.EMAIL["EMAIL_ACCOUNT_VORSTAND"]},
                template='protocol_missing',
                context={'event': event.name,
                         'date': event.time.date(),
                         'person': working_group.get_leader().person.user.first_name},
                priority='medium'
            )

        for event in Event.objects.filter(Q(name__icontains="versammlung") | Q(name__icontains="sitzung"),
                                          time__lte=timezone.now() - timedelta(days=4 * 7),
                                          time__gte=timezone.now() - timedelta(days=365)):
            lower_name = event.name.lower()

            if "netz" in lower_name:
                if not exists(event, lambda file: "netz" in file):
                    send_mail(event, WorkingGroup.objects.filter(name__icontains="network").first())
            elif "bar" in lower_name:
                if not exists(event, lambda file: "bar" in file):
                    send_mail(event, WorkingGroup.objects.filter(name__icontains="symposion").first())
            elif "ba" in lower_name or "belegung" in lower_name:
                if not exists(event, lambda file: ("ba" in file or "belegung" in file) and not "bar" in file):
                    send_mail(event, WorkingGroup.objects.filter(name__icontains="belegungsausschuss").first())
            elif "fitness" in lower_name:
                if not exists(event, lambda file: "fitness" in file):
                    send_mail(event, WorkingGroup.objects.filter(name__icontains="fitness").first())
            elif "senat" in lower_name:
                if not exists(event, lambda file: "senat" in file):
                    send_mail(event, WorkingGroup.objects.filter(name__icontains="haussprecher").first())
            elif "14" in lower_name:
                if not exists(event, lambda file: "14" in file):
                    send_mail(event, WorkingGroup.objects.filter(name__icontains="14").first())
            elif "mitglieder" in lower_name:
                if not exists(event, lambda file: "mv" in file or "mitglieder" in file):
                    send_mail(event, WorkingGroup.objects.filter(name__icontains="haussprecher").first())
