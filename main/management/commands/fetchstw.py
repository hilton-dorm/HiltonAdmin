from datetime import datetime
from datetime import timedelta
from enum import Enum

import requests
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError
from post_office import mail

import config
from common.util.usernamegenerator import UsernameGenerator
from main.models import Person, Tenant, Room


class Command(BaseCommand):
    help = 'Fetches new tenants from the STW API'

    def handle(self, *args, **options):
        # not sure why `%3F` is necessary but it does not work with `?`
        STW_URL = 'https://bewerberportal.stw.rwth-aachen.de/app.php/api/get%3Fkey='

        ''' Example JSON value
        val = {"id": 1234,
               "name": "Mustermann",
               "firstname": "Max",
               "dateofbirth": "23.04.1993",
               "email": "max@mustermann.de",
               "matr": "355555",
               "roomNumber": "0040",
               "moveInDate": "01.02.2018"}
        '''

        class ApiAttr(Enum):
            ID = 'id'
            LAST_NAME = 'name'
            FIRST_NAME = 'firstname'
            BIRTHDAY = 'dateofbirth'
            EMAIL = 'email'
            ROOM = 'roomNumber'
            MOVE_IN_DATE = 'moveInDate'

        if len(config.STW['key']) == 0:
            raise CommandError("STW key is not set in the config file")

        response = requests.get(STW_URL + config.STW['key'])
        response = response.json()

        for person in response:
            if Person.objects.filter(stw_id=person[ApiAttr.ID.value]).exists():
                continue
            if User.objects.filter(email=person[ApiAttr.EMAIL.value]).exists():
                continue
            existing_user = User.objects.filter(first_name=person[ApiAttr.FIRST_NAME.value],
                                                last_name=person[ApiAttr.LAST_NAME.value]).first()

            date_move_in = datetime.strptime(person[ApiAttr.MOVE_IN_DATE.value], "%d.%m.%Y")
            date_move_out = date_move_in + timedelta(days=3 * 365)

            if existing_user is not None:
                if Tenant.objects.filter(person__user=existing_user, date_move_in=date_move_in).exists():
                    continue

            user = User.objects.create_user(username=UsernameGenerator.generate(person[ApiAttr.FIRST_NAME.value],
                                                                                person[ApiAttr.LAST_NAME.value]),
                                            first_name=person[ApiAttr.FIRST_NAME.value],
                                            last_name=person[ApiAttr.LAST_NAME.value],
                                            is_staff=False,
                                            is_superuser=False,
                                            is_active=True,
                                            email=person[ApiAttr.EMAIL.value],
                                            password=BaseUserManager().make_random_password(45))

            user.save()

            try:
                tenant_room = Room.objects.get(number=int(person[ApiAttr.ROOM.value]))
            except Room.DoesNotExist:
                raise CommandError('Could not import person "%s" from the STW API. Room "%s" not found.' %
                                   (person[ApiAttr.LAST_NAME.value], person[ApiAttr.ROOM.value]))

            user_person, _ = Person.objects.get_or_create(
                user=user,
                birthday=datetime.strptime(person[ApiAttr.BIRTHDAY.value], "%d.%m.%Y").date(),
                stw_id=person[ApiAttr.ID.value],
                nationality='DE',
                cloud_quota='100 GB')

            person_tenant, _ = Tenant.objects.get_or_create(
                person=user_person,
                room=tenant_room,
                date_move_in=date_move_in,
                date_move_out=date_move_out)

            # Sends welcome mail regarding network access to the new tenant
            mail.send(
                user.email,
                config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                template='new_tenant_email',
                headers={'Reply-to': config.EMAIL['EMAIL_ACCOUNT_NETWORK']},
                context={'user': user,
                         'url': user.person.generate_initial_password_reset_url()},
            )

            # Sends notification to the admin
            mail.send(
                config.EMAIL['EMAIL_ACCOUNT_ADMIN'],
                config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                subject='New person imported',
                message='Hi there! %s imported. Move in date: %s' % (
                    user_person.full_name(), date_move_in),
                html_message='Hi there! <b>%s</b> imported. Move in date: <b>%s</b>' % (
                    user_person.full_name(), date_move_in))
