from django.core.management.base import BaseCommand
from django.utils import timezone
from post_office.models import Email


class Command(BaseCommand):
    help = 'Queues failed mails again. Only selects emails that are up to 10 minutes old'

    def handle(self, *args, **options):
        failed_emails = Email.objects.filter(status=1, created__gt=timezone.now() - timezone.timedelta(minutes=10))
        for mail in failed_emails:
            mail.status = 2
            mail.save()
