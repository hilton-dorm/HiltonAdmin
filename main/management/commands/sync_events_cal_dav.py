from datetime import datetime

from caldav.lib.error import PutError
from django.db.models import Q
from django.core.management.base import BaseCommand, CommandError

import config
from association.models import SocialService
from main.models import Event

import caldav
from caldav.elements import dav

from ics import Calendar, Event as VEvent


class Command(BaseCommand):
    help = 'Sync events with a CalDav Calendar'

    def add_arguments(self, parser):
        parser.add_argument('-id', '--event-id', type=int, help='Only sync a specific event to caldav', )

    def handle(self, *args, **options):
        event_id = options['event_id']
        VERWALTUNGS_EVENT_DOMAIN = "@verwaltung.hilton.rwth-aachen.de"

        if not config.CAL_DAV:
            raise CommandError("CAL_DAV config is missing in the config file!")
        if "<password>" in config.CAL_DAV['url'] or "<username>" in config.CAL_DAV['url']:
            raise CommandError("The username and password placeholders are not replaced in the URL!")

        url = config.CAL_DAV['url']
        client = caldav.DAVClient(url)
        principal = client.principal()
        calendars = principal.calendars()

        def get_cal_name(c):
            return c.get_properties([dav.DisplayName(), ])['{DAV:}displayname']
        calendar = False
        for c in calendars:
            if get_cal_name(c) == config.CAL_DAV['calendar']:
                calendar = c
                break

        if not calendar:
            cals = ", ".join(get_cal_name(c) for c in calendars)
            raise CommandError(
                "No calendar with name '" +
                config.CAL_DAV['calendar'] +
                "' found. Available calendars: " +
                cals)
        # first sync events caldav => verwaltung. Each event is wrapped inside its own calendar
        calendars_with_one_event = (Calendar(c.data) for c in calendar.date_search(datetime(2010, 5, 1)))
        if event_id is None:
            for c in calendars_with_one_event:
                for event in c.events:
                    # ignore events created in the Verwaltung, don't sync them back
                    if event.uid.endswith(VERWALTUNGS_EVENT_DOMAIN):
                        continue
                    # if the event is from the Symposion, don't create a event, only update events
                    bar_id = False
                    if event.uid.endswith("@symposion-aachen.com"):
                        bar_id = event.uid[0:event.uid.index("@")]
                        try:
                            e = Event.objects.get(external_identifier=bar_id)
                        except Event.DoesNotExist:
                            break  # wait for fetch_symposion_events
                    else:
                        e, _ = Event.objects.get_or_create(external_identifier=event.uid)
                    e.name = event.name
                    if not event.description:
                        # don't copy events without descriptions
                        continue
                    e.description = event.description
                    e.time = event.begin.datetime
                    if bar_id and not e.canceled and event.status == 'CANCELLED':  # it is a bar event and now cancelled
                        # delete social services for this event
                        SocialService.objects.filter(external_identifier=bar_id).delete()
                    e.canceled = event.status == 'CANCELLED'
                    e.save()

        def save_event(event):
            try:
                calendar.save_event(event.serialize())
            except PutError as e:
                # We currently get https://github.com/nextcloud/server/issues/19043, but the event is saved nevertheless
                if not e.url.startswith("500 Internal Server Error"):
                    raise
        # sync now verwaltung => caldav
        # if an event was created in the verwaltung it has no external identifier
        event_filter = Q(external_identifier="")
        if event_id is not None:
            event_filter &= Q(id=event_id)
        for event in Event.objects.filter(event_filter):
            # check if a event already exists in the caldav calendar
            uid = str(event.id) + VERWALTUNGS_EVENT_DOMAIN
            event_description = event.description.replace("\r", "")
            for cal in calendars_with_one_event:
                for e in cal.events:
                    if e.uid == uid:
                        # if we update always we get hundred thousand update events in nextcloud
                        needUpdate = False
                        if e.name != event.name:
                            e.name = event.name
                            needUpdate = True
                        if e.description != event_description:
                            e.description = event_description
                            needUpdate = True
                        if e.begin != event.time:
                            e.begin = event.time
                            needUpdate = True
                        newStatus = 'CANCELLED' if event.canceled else 'CONFIRMED'
                        if newStatus != e.status:
                            e.status = newStatus
                            needUpdate = True
                        if needUpdate:
                            save_event(e)
                        break
                else:  # no event found
                    continue
                break  # event found
            else:  # no event found
                # create event https://icspy.readthedocs.io/en/stable/#create-a-new-calendar-and-add-events
                c = Calendar()
                e = VEvent()
                # see https://icspy.readthedocs.io/en/stable/api.html#ics.event.Event for event properties
                e.name = event.name
                e.begin = event.time
                e.uid = uid
                e.description = event_description
                e.status = 'CANCELLED' if event.canceled else 'CONFIRMED'
                c.events.add(e)
                # https://pythonhosted.org/caldav/caldav/objects.html#caldav.objects.Calendar.add_event
                save_event(c)

        return
