import os
from datetime import timedelta

from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.utils import timezone

from association.models import Membership
from hiltonadmin import settings
from main.models import Person, Tenant


class Command(BaseCommand):
    help = 'Deletes people from the database permanently who do not live in the ' \
           'dorm for more than 14 days and are not a member of any association anymore'

    def handle(self, *args, **options):
        save_time_buffer = timezone.now() + timedelta(days=14)
        people = Person.objects.all()
        for person in people:
            is_member = len(Membership.objects.filter(person=person,
                                                      date_leave__gt=save_time_buffer)) > 0
            is_tenant = len(Tenant.objects.filter(person=person,
                                                  date_move_out__gt=save_time_buffer)) > 0
            if not is_member and not is_tenant:
                # Makes sure that no ip addresses get archived
                call_command('delete_old_ip_addresses')

                # Archives the user into a textfile
                file_tenant = os.path.join(settings.PATH_ARCHIVED_USERS,
                                           'person_' + str(person.id) + '-tenant_')

                tenant_ids = list(Tenant.objects.filter(person=person).values_list('id', flat=True))
                for id in tenant_ids:
                    with open(file_tenant + str(id) + '.json', 'w') as f:
                        call_command('dump_object', 'main.tenant', id, stdout=f)
                    f.close()

                # Deletes the user permanently
                person.user.delete()
