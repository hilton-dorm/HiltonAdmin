import datetime

import requests
from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

import config
from association.models import SocialService
from main.models import Event
from workinggroups.models import WorkingGroup


class Command(BaseCommand):
    help = 'Fetches new events from symposion orga'

    def handle(self, *args, **options):
        api_endpoint = config.SYMPOSION_ORGA['api_endpoint']

        if len(config.SYMPOSION_ORGA['password']) == 0:
            raise CommandError("Symposion orga password missing!")

        # First receive new access token
        data = {'name': config.SYMPOSION_ORGA['username'],
                'password': config.SYMPOSION_ORGA['password']}

        response = requests.post(url=api_endpoint + "/login", data=data)
        response = response.json()

        token = response["user"]["sessionID"]

        cookie = {'auth': token}

        response = requests.get(url=api_endpoint + "/bars", cookies=cookie)
        response = response.json()

        for event in response:
            date = timezone.make_aware(datetime.datetime.strptime(event["start"], "%Y-%m-%dT%H:%M:%S.%fZ"),
                                       timezone.get_current_timezone()) + timezone.get_current_timezone().utcoffset(
                timezone.datetime.utcnow())

            if not event["public"] or date <= timezone.now():
                continue

            if Event.objects.filter(external_identifier=event["id"]).exists() or SocialService.objects.filter(
                    external_identifier=event["id"]).exists():
                continue

            Event.objects.get_or_create(external_identifier=event["id"],
                                        time=date,
                                        name="Symposion - " + event["name"],
                                        description=event["description"])

            responsible_working_group = WorkingGroup.objects.filter(group_django__name="symposion")
            if responsible_working_group.exists():
                responsible_working_group = responsible_working_group.first()
            else:
                responsible_working_group = None

            social_service, _ = \
                SocialService.objects.get_or_create(external_identifier=event["id"],
                                                    time=date,
                                                    name="Symposion - " + event["name"],
                                                    description="Help us tapping beer and mixing cocktails. :)",
                                                    show_as_event=False,
                                                    people_limit=4,
                                                    responsible_working_group=responsible_working_group)
