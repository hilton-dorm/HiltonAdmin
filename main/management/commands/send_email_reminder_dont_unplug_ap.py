from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils import timezone
from post_office import mail

import config
from main.models import Tenant


class Command(BaseCommand):
    help = 'Send a reminder to tenants who are moving out not to unplug the AP.'

    def handle(self, *args, **options):
        now = timezone.now()
        tenants = Tenant.objects.filter(date_move_out__gte=now - timedelta(days=15),
                                        date_move_out__lte=now)
        for tenant in tenants:
            mail.send(
                tenant.person.user.email,
                config.EMAIL["EMAIL_ACCOUNT_NETWORK"],
                template='dont_unplug_ap',
                context={'first_name': tenant.person.user.first_name, 'room': tenant.room.number},
                priority='medium'
            )
