from django.core.management.base import BaseCommand
from django_telegrambot.apps import DjangoTelegramBot
from django.utils.timezone import localtime
import config

# Automatische, wöchentliche Umfrage wer zur NetzAG kommt.
# Wird per HiltonManagementBot and die NetzAG@Hilton Gruppe geschickt.
# Wird per Cron ausgeführt jeden Donnerstag um 17:00 Uhr ausgeführt."
# Manuell ausführen: "sudo docker exec verwaltung.hilton.rwth-aachen.de python manage.py send_netzag_poll"


class Command(BaseCommand):
    help = 'Sends the usual attendance poll into the NetzAG Telegram Group. Can be automated via cron.'

    def handle(self, *args, **options):
        chat = DjangoTelegramBot.dispatcher.bot.get_chat(chat_id=config.TELEGRAM_GROUP_NETZAG_AT_HILTON)
        chat.send_poll(
            question=localtime().strftime("Wer kommt heute (%d.%m.%y) zur NetzAG?"),
            options=["Ja, ab 20 Uhr", "Ja, ab 20:30 Uhr", "Ja, ab 21 Uhr", "Ja, aber später", "Eventuell", "Nein"],
            is_anonymous=False,
            type='regular',
            allows_multiple_answers=False,
            disable_notification=False,
        )
