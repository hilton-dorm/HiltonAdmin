from urllib.request import urlopen

from django.core.management.base import BaseCommand
from post_office import mail

import config
from main.models import TelegramGroup


class Command(BaseCommand):
    help = 'Checks if the telegram links are valid'

    def handle(self, *args, **options):
        expired = [g.title for g in TelegramGroup.objects.all() if Command.is_expired(g)]
        if expired:
            mail.send(
                config.EMAIL['EMAIL_ACCOUNT_ADMIN'],
                config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                template='broken_telegram_link',
                context={
                    'titles': expired
                }
            )

    @staticmethod
    def is_expired(group: TelegramGroup):
        with urlopen(group.telegram_link) as response:
            return "You are invited to a <strong>group chat</strong> on <strong>Telegram</strong>. Click to join:" \
                   in str(response.read())
