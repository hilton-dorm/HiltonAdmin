import requests
from django.core.management.base import BaseCommand

from hiltonadmin import settings


class Command(BaseCommand):
    help = 'Fetches new tenants from the STW API'

    def handle(self, *args, **options):
        if not settings.DEBUG:
            print("no")
            return

        FIDOR_URL = 'https://api.fidor.de/sepa_credit_transfers'

        ''' Example JSON value
        '''

        payload = "{}"
        headers = {
            'authorization': " Bearer edd86a9e72982c79e91ee5b2fa8d9896",
            'accept': " application/vnd.fidor.de; version=1,text/json",
            'content-type': " application/json"
        }

        response = requests.request("GET", FIDOR_URL, data=payload, headers=headers)

        print(response.text)
