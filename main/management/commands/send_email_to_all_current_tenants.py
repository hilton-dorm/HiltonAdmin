from django.core.management.base import BaseCommand
from django.utils import timezone
from post_office import mail

import config
from main.models import Tenant


class Command(BaseCommand):
    help = 'Reminder for social services. It does not get triggered in a cron job and has to be executed manually.'

    def handle(self, *args, **options):
        now = timezone.now()
        tenants = Tenant.objects.filter(date_move_in__lte=now,
                                        date_move_out__gte=now)
        for tenant in tenants:
            mail.send(
                tenant.person.user.email,
                config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                template='all_current_tenants',
                context={'first_name': tenant.person.user.first_name},
                priority='medium'
            )
