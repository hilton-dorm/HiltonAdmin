from django.contrib.auth.models import User
from django.core.management.base import BaseCommand
from django.db.models import Q, Count
from django.utils import timezone
from post_office import mail

import config
from main.models import Tenant, Room


class Command(BaseCommand):
    help = 'Checks the integrity of the data. E.g. if two people are assigned to the same room at the same time and so on.'

    def handle(self, *args, **options):
        errors = []
        errors.append(self.check_double_room_assignments())
        errors.append(self.check_user_duplicates())
        errors.append(self.check_empty_email_address())

        if errors.count:
            mail.send(
                config.EMAIL['EMAIL_ACCOUNT_BA'],
                config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                template='data_integrity_check',
                context={'errors': errors},
            )

    @staticmethod
    def check_double_room_assignments():
        ''' The following rules should hold:
        - In the same time frame, there cannot be two tenants in one room (there can be a subtenant tho)
        - The same tenant cannot be in two different rooms at the same time
        '''
        rooms = Room.objects.all()
        errors = []
        for room in rooms:
            room_tenants = Tenant.objects.filter(room=room)

            # Checks overlapping internationals/tenants
            move_in_out_dates = room_tenants.filter(
                Q(tenant_type=Tenant.TYPE_TENANT) or Q(tenant_type=Tenant.TYPE_INTERNATIONAL)) \
                .order_by('date_move_in'). \
                values_list('date_move_in', 'date_move_out')
            move_in_out_dates = list(move_in_out_dates)
            # Converts [(in,out), (in,out),...] to [in,out,in,out,...]
            move_in_out_dates_sorted = []
            for in_out_date in move_in_out_dates:
                move_in_out_dates_sorted.append(in_out_date[0])
                move_in_out_dates_sorted.append(in_out_date[1])

            if move_in_out_dates_sorted != sorted(move_in_out_dates_sorted):
                errors.append({'Overlapping time frames in room': room.number})

        return {'Error type': 'Room inconsistencies',
                'Possible solution': 'Correct the move in/out dates',
                'errors': errors}

    @staticmethod
    def check_user_duplicates():
        duplicate_users = User.objects.values('first_name', 'last_name') \
            .annotate(Count('id')) \
            .order_by() \
            .filter(id__count__gt=1)

        errors = []
        for user in duplicate_users:
            errors.append({'Multiple users with the same name': user["first_name"] + " " + user["last_name"]})

        return {'Error type': 'Duplicate Users',
                'Possible solution': 'Remove the user with the wrong or missing stw id. '
                                     'Check user transactions first and contact the corresponding person '
                                     'if both accounts have transactions.',
                'errors': errors}

    @staticmethod
    def check_empty_email_address():
        users = User.objects.all()

        errors = []
        for user in users:
            if len(user.email) == 0:
                errors.append({'User has no email address': user.person.full_name()})

            if user.person.birthday is not None and \
                    user.person.birthday >= timezone.localdate() - timezone.timedelta(days=365):
                errors.append({'User\'s birthday is most likely incorrect':
                               user.person.full_name() + ": " + str(user.person.birthday)})

        return {'Error type': 'Missing attributes',
                'Possible solution': 'Update information.',
                'errors': errors}
