from django.core.management.base import BaseCommand
from post_office import mail
from main.models import Room, Person

import config

# Möglichkeit eine E-Mail an bestimmte Bewohner via Zimmernummer zu senden.
# Verwendet das E-Mail Template 'tenant_selection_email' mit den Parametern 'name' und 'room'.


class Command(BaseCommand):
    help = 'Sends an Email to specified Tenants by room number.'

    def add_arguments(self, parser):
        parser.add_argument('room_numbers', nargs='+', type=int, help='Room numbers to send the email to.')

    def handle(self, *args, **options):
        if 'room_numbers' not in options:
            self.stdout.write("Please provide at least one room number.")
            return

        rooms = []
        for room_number in options['room_numbers']:
            try:
                room_number = int(room_number)
                if room_number < 1000 or room_number > 7092:
                    raise ValueError
                rooms.append(room_number)
            except ValueError:
                self.stdout.write(f"Invalid room number '{room_number}'")
                return

        rooms = list(set(rooms))  # remove duplicates and sort

        self.stdout.write(f"Sending email to the following rooms:\n{rooms}")
        ack = input(f"Send email to {len(rooms)} rooms? (y/n) ")

        if ack.lower() != 'y':
            print("Aborted.")
            return

        for room_number in rooms:
            try:
                room: Room = Room.objects.filter(number=room_number).first()
                person: Person = room.current_tenant().person  # will trow AttributeError if room == None (not found)
                main_email = person.user.email
                # bcc_email = config.EMAIL['EMAIL_ACCOUNT_ADMIN']
                name = person.full_name()

                mail.send(
                    recipients=main_email,
                    bcc=None,
                    sender=config.EMAIL['EMAIL_ACCOUNT_NETWORK'],
                    template='tenant_selection_email',
                    context={
                        'name': name,  # full name
                        'room': room_number,
                    }
                )
                print(f"Mail for AP in {room_number} has been send to user '{name}'")
            except Exception:
                self.stdout.write(f"Room {room_number} not found or no tenant assigned.")
