from django.contrib.auth import views as auth_views
from django.urls import path

from main import views

urlpatterns = [
    path('profile', views.profile, name='profile'),
    path('print', views.print, name='print'),
    path('job/<int:job_id>/delete', views.cancel_print, name='cancel_print'),
    path('logout_telegram', views.logout_telegram, name='logout_telegram'),
    path('tenant_list', views.tenant_list, name='tenant_list'),
    path('calendar', views.calendar, name='calendar'),
    path('calendar_remind/<int:is_social_service>/<int:event_id>/', views.calendar_remind, name='calendar_remind'),
    path('room_administration', views.room_administration, name='room_administration'),
    path('email', views.email, name='email'),
    path('no_permission', views.no_permission, name='no_permission'),
    path('telegram_groups', views.telegram_groups, name='telegram_groups'),
    path('login/', auth_views.LoginView.as_view(template_name='main/login.html'), name='login'),
    path('logout/', auth_views.LogoutView.as_view(template_name='main/logout.html'), name='logout'),
    path(
        'password_reset/',
        auth_views.PasswordResetView.as_view(template_name='main/password_reset_form.html'),
        name='password_reset',
    ),
    path(
        'password_reset/done/',
        auth_views.PasswordResetDoneView.as_view(template_name='main/password_reset_done.html'),
        name='password_reset_done',
    ),
    path(
        'reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(template_name='main/password_reset_confirm.html'),
        name='password_reset_confirm',
    ),
    path(
        'reset_initial/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(template_name='main/password_reset_confirm_initial.html'),
        name='password_reset_confirm_initial',
    ),
    path(
        'reset/done/',
        auth_views.PasswordResetCompleteView.as_view(template_name='main/password_reset_complete.html'),
        name='password_reset_complete',
    ),
]
