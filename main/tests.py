from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.test import TestCase
from django.utils import timezone

from main.models import Person, Tenant, Room


class TenantTestCase(TestCase):
    def setUp(self):
        user1 = User.objects.create(username="test1", first_name="test1", last_name="Testo", email="test1@tester.de")
        self.person1 = Person.objects.create(user=user1)
        self.room = Room.objects.create(number="2072", vlan_id=21, eth_port0=10, eth_port1=11)

    def create_tenant(self, date_move_in, date_move_out, tenant_type):
        t = Tenant(person=self.person1,
                   room=self.room,
                   date_move_in=timezone.datetime(*date_move_in),
                   date_move_out=timezone.datetime(*date_move_out),
                   tenant_type=tenant_type)
        t.full_clean()
        t.save()
        return t

    def test_conflicts(self):
        for tenant_type in (Tenant.TYPE_TENANT, Tenant.TYPE_SUBTENANT):
            self.create_tenant(date_move_in=(2020, 2, 1), date_move_out=(2020, 6, 30), tenant_type=tenant_type)
        for (tenant_type, _) in Tenant.TENANT_TYPES:
            # All kind of overlaps
            with self.assertRaises(ValidationError):
                self.create_tenant(date_move_in=(2020, 3, 1), date_move_out=(2020, 5, 30), tenant_type=tenant_type)
            with self.assertRaises(ValidationError):
                self.create_tenant(date_move_in=(2020, 2, 1), date_move_out=(2020, 6, 30), tenant_type=tenant_type)
            with self.assertRaises(ValidationError):
                self.create_tenant(date_move_in=(2020, 3, 1), date_move_out=(2020, 8, 30), tenant_type=tenant_type)
            with self.assertRaises(ValidationError):
                self.create_tenant(date_move_in=(2020, 1, 1), date_move_out=(2020, 8, 30), tenant_type=tenant_type)
            with self.assertRaises(ValidationError):
                self.create_tenant(date_move_in=(2020, 1, 1), date_move_out=(2020, 4, 1), tenant_type=tenant_type)
        # should work
        for tenant_type in (Tenant.TYPE_TENANT, Tenant.TYPE_SUBTENANT):
            self.create_tenant(date_move_in=(2020, 1, 1), date_move_out=(2020, 1, 31), tenant_type=tenant_type)
            self.create_tenant(date_move_in=(2020, 7, 1), date_move_out=(2020, 8, 1), tenant_type=tenant_type)

    def test_can_change(self):
        t = self.create_tenant(date_move_in=(2020, 2, 1), date_move_out=(2020, 6, 30), tenant_type=Tenant.TYPE_TENANT)
        t.date_move_in = timezone.datetime(2020, 1, 1)
        t.full_clean()
        t.save()
