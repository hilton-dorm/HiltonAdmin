from __future__ import unicode_literals

from random import randint
from threading import Thread
from typing import Union

from django.core.management import call_command
from auditlog.registry import auditlog
from django.contrib.auth.models import User
from django.contrib.auth.tokens import PasswordResetTokenGenerator
from django.db import models, connection
from django.core.exceptions import ValidationError
from django.db.models import OneToOneField, Q
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone, dateformat
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from django.utils.timezone import now
from django_countries.fields import CountryField
from django_telegrambot.apps import DjangoTelegramBot
from telegram.error import Unauthorized
from telegram.utils.types import ODVInput
from telegram.utils.helpers import DEFAULT_NONE
from telegram import ParseMode
from markdown import markdown
from post_office import mail
import cologne_phonetics
import config
from datetime import datetime, timedelta


def start_new_thread(function):
    def decorator(*args, **kwargs):
        t = Thread(target=function, args=args, kwargs=kwargs)
        t.daemon = True
        t.start()

    return decorator


class PersonManager(models.Manager):
    def get_queryset(self):
        # I think we need always the user when we request a person object
        return super().get_queryset().select_related('user')


def random_login_pin():
    return randint(pow(2, 53), pow(2, 63))


class Person(models.Model):
    objects = PersonManager()
    MALE = 'Male'
    FEMALE = 'Female'
    DIVERS = 'Divers'
    UNKNOWN = 'Unknown'
    GENDER_CHOICES = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
        (DIVERS, 'Divers'),
        (UNKNOWN, 'Unknown'),
    )

    user = OneToOneField(User, on_delete=models.CASCADE)
    name_cologne_phonetics = models.CharField(max_length=32, blank=True, default="")
    birthday = models.DateField(blank=True, null=True)
    stw_id = models.IntegerField(default=0)
    nationality = CountryField(null=True, blank=True)
    unfunded_account_warning_days_before = models.IntegerField(default=7, null=True, blank=True)
    # chat id: https://core.telegram.org/bots/api#chat
    # smaller than 52 bit, so we can create values > 53 bit and use them as login pin
    # we can not set the default to random_login_pin, because this would assign every person the same pin when migrating
    telegram_chat_id = models.BigIntegerField(null=True, unique=True, blank=True)
    got_fitnessroom_introduction = models.BooleanField("Got Fitnessroom Introduction", default=False)

    gender = models.CharField(max_length=10, choices=GENDER_CHOICES, default=UNKNOWN)
    cloud_quota = models.CharField(max_length=10, default='30 GB')
    email_distributor = models.BooleanField(default=False)
    signed_usage_data_stw = models.BooleanField(blank=False, null=False, default=False,
                                                verbose_name='Signed the privacy policy (STW)',
                                                help_text='This is mandatory for all active people '
                                                          'who work with data about the tenants. '
                                                          'Since we receive tenant data from the '
                                                          'Studierendenwerk we have to accept their privacy policy.')
    want_dns_adblocker = models.BooleanField("Pyhole", default=False)

    def __str__(self):
        return str(self.user.get_full_name())

    def full_name(self):
        return str(self.user.get_full_name())

    def get_last_room_number(self):
        return self.get_latest_tenant().room

    def get_earliest_move_in_date(self):
        return self.tenant_set.earliest('date_move_in').date_move_in

    def get_rooms(self):
        return [tenant.room for tenant in self.tenant_set.all()]

    def generate_initial_password_reset_url(self):
        token = PasswordResetTokenGenerator().make_token(self.user)
        uid = urlsafe_base64_encode(force_bytes(self.user.pk))
        return reverse('password_reset_confirm_initial', kwargs={'uidb64': uid, 'token': token})

    def attended_new_tenant_bar(self):
        return self.registration_set.filter(verified=True, social_service__is_new_tenant_bar=True).exists()

    def get_latest_tenant(self):
        return self.tenant_set.select_related('room').latest('date_move_out', 'date_move_in')

    def get_current_tenant(self) -> Union['Tenant', None]:
        today = timezone.localdate()
        return self.tenant_set.filter(date_move_in__lte=today, date_move_out__gte=today).select_related('room').first()

    def current_room(self):
        current = self.get_current_tenant()
        return current.room if current else "extern"

    def is_living_here(self):
        return self.tenant_set.filter(date_move_out__gte=now(), date_move_in__lte=now()).exists()

    def has_telegram_chat(self):
        return self.telegram_chat_id is not None and self.telegram_chat_id < pow(2, 52)

    def active_workinggroup_memberships(self):
        from workinggroups.models import GroupMembership
        return GroupMembership.objects.filter(
            Q(date_join__lte=timezone.now()) & (Q(date_leave__isnull=True) | Q(date_leave__gte=timezone.now())) & Q(
                person=self))

    def clean(self):
        if self.birthday and self.birthday >= datetime.now().date() - timedelta(days=3650):
            raise ValidationError('Please enter a valid year of birth.')

    @staticmethod
    def get_all_with_telegram():
        return Person.objects.filter(telegram_chat_id__lt=pow(2, 52))

    def get_telegram_login_pin(self):
        if self.has_telegram_chat():
            raise RuntimeError("The Person has no login pin, the person already has a chat.")
        if not self.telegram_chat_id:
            self.generate_and_save_login_pin()
        return str(self.telegram_chat_id)

    def generate_and_save_login_pin(self):
        self.telegram_chat_id = random_login_pin()
        self.save()

    def logout_telegram(self):
        self.generate_and_save_login_pin()

    def send_telegram_or_mail(self, message: str, parse_mode: ODVInput[str] = DEFAULT_NONE):
        if self.has_telegram_chat():
            try:
                DjangoTelegramBot.getBot(config.TELEGRAM_BOT_NAME).send_message(chat_id=self.telegram_chat_id, text=message, parse_mode=parse_mode)
            except Unauthorized as e:
                if "bot was blocked by the user" in e.message:
                    self.generate_and_save_login_pin()
        else:
            if parse_mode is None:
                message = message.replace('\n', '<br>')
            elif parse_mode is ParseMode.MARKDOWN or parse_mode is ParseMode.MARKDOWN_V2:
                message = markdown(message)

            message += "<br><br>You can also get this message via our telegram bot, see " \
                       "<a href='https://verwaltung.hilton.rwth-aachen.de/#telegram'>here</a>."
            mail.send(
                self.user.email,
                config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                html_message=message
            )

    @staticmethod
    def get_for_login_pin(pin):
        if pin < pow(2, 52):
            return None
        return Person.objects.filter(telegram_chat_id=pin).first()

    @staticmethod
    def get_for_chat_id(id):
        return Person.objects.filter(telegram_chat_id=id).first()

    class Meta:
        ordering = ('user__last_name',)


@receiver(pre_save, sender=Person)
def person_pre_save(sender, instance, **kwargs):
    if instance.user:
        instance.name_cologne_phonetics = ' ' + " ".join(
            [code for _, code in cologne_phonetics.encode(instance.user.get_full_name(), concat=True)]) + ' '


class Room(models.Model):
    number = models.CharField(max_length=4)
    eth_port0 = models.SmallIntegerField()
    eth_port1 = models.SmallIntegerField()
    renovated = models.BooleanField(default=False)
    comment = models.CharField(max_length=255, blank=True)
    vlan_id = models.SmallIntegerField(unique=True, null=True, blank=True)

    def __str__(self):
        return str(self.number.zfill(4))

    class Meta:
        ordering = ('number',)

    def current_tenant(self):
        tenant = None
        sub_tenant = None
        for t in self.tenant_set.filter(date_move_in__lte=datetime.now(), date_move_out__gte=datetime.now()):
            if t.tenant_type == Tenant.TYPE_TENANT or t.tenant_type == Tenant.TYPE_INTERNATIONAL:
                if tenant:
                    raise Exception(
                        f"Two tenants ({tenant} and {t}) are currently living in room {self}. This is forbidden.")
                tenant = t
            elif t.tenant_type == Tenant.TYPE_SUBTENANT:
                if sub_tenant:
                    raise Exception(
                        f"Two sub tenants ({sub_tenant} and {t}) are currently living in room {self}. This is forbidden.")
                sub_tenant = t
        if sub_tenant:
            return sub_tenant
        return tenant


class Tenant(models.Model):
    TYPE_TENANT = '0'
    TYPE_INTERNATIONAL = '1'
    TYPE_SUBTENANT = '2'
    TENANT_TYPES = (
        (TYPE_TENANT, 'Mieter'),
        (TYPE_INTERNATIONAL, 'International'),
        (TYPE_SUBTENANT, 'Untermieter'),
    )
    TENANT_TYPES_EN = (
        (TYPE_TENANT, 'Normal Tenant'),
        (TYPE_INTERNATIONAL, 'International Tenant'),
        (TYPE_SUBTENANT, 'Subtenant'),
    )

    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    room = models.ForeignKey(Room, on_delete=models.CASCADE)
    date_move_in = models.DateField()
    date_move_out = models.DateField()
    tenant_type = models.CharField(max_length=10, choices=TENANT_TYPES, default=TENANT_TYPES[0])
    comment = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return self.person.full_name()

    class Meta:
        ordering = ('room',)

    def clean(self):
        overlapping = Tenant.objects.filter(~Q(id=self.id), Q(room_id=self.room_id),
                                            Q(date_move_in__lte=self.date_move_out, date_move_out__gte=self.date_move_in))
        is_self_subtenant = self.tenant_type == Tenant.TYPE_SUBTENANT
        for t in overlapping:
            is_t_subtenant = t.tenant_type == Tenant.TYPE_SUBTENANT
            if is_self_subtenant == is_t_subtenant:
                raise ValidationError(f"This tenant ({self.date_move_in} - {self.date_move_out}) conflicts with the existing "
                                      f"tenant {t.id} ({t.date_move_in} - {t.date_move_out})")

    def tenant_status(self):
        if timezone.localdate() > self.date_move_out:
            return 'moved out recently'
        elif timezone.localdate() < self.date_move_in:
            return 'is moving in soon'
        else:
            return 'is living here'

    def get_latest_tenant(self):
        return self.person.get_latest_tenant()

    @staticmethod
    def current_tenants(time_point=timezone.now()):
        return Tenant.objects.filter(date_move_in__lte=time_point, date_move_out__gte=time_point)


class Event(models.Model):
    TYPE_SENATSSITZUNG = 'S'
    TYPE_MITGLIEDERVOLLVERSAMMLUNG = 'M'
    TYPE_SEMESTERSTART = 'I'
    TYPES = [(TYPE_SENATSSITZUNG, 'Senatssitzung'),
             (TYPE_MITGLIEDERVOLLVERSAMMLUNG, 'Mitgliedervollversammlung'),
             (TYPE_SEMESTERSTART, 'Semesterstart'),]
    name = models.CharField(max_length=255, default='')
    description = models.TextField(blank=True)
    time = models.DateTimeField(default=timezone.now)
    external_identifier = models.CharField(max_length=50, default='', blank=True, null=True)
    canceled = models.BooleanField(default=False)
    type = models.CharField(max_length=1, choices=TYPES, blank=True, null=True)

    def __str__(self):
        # For Date format see https://docs.djangoproject.com/en/4.2/ref/templates/builtins/#date
        return f'{self.name} {dateformat.format(timezone.localtime( self.time), "d.m.Y")}'


class TelegramGroup(models.Model):
    title = models.CharField(max_length=50)
    text = models.TextField(max_length=255)
    telegram_link = models.URLField()
    telegram_button_text = models.CharField(max_length=50, default='Join Telegram Group')
    discord_link = models.URLField(blank=True, null=True)
    card_header_text = models.CharField(max_length=50, blank=True, null=True)

    def __str__(self):
        return self.title


@receiver(post_save, sender=Event)
def event_saved(sender, instance, **kwargs):
    if not instance.external_identifier:
        update_caldav_event(instance.id)


@start_new_thread
def update_caldav_event(id):
    call_command('sync_events_cal_dav', event_id=id)
    # see https://stackoverflow.com/a/28913218/10162645
    connection.close()


def is_member_room_administration(user):
    return user.is_superuser or user.groups.filter(name='room_administration').exists()


auditlog.register(Person)
auditlog.register(Tenant)
auditlog.register(Room)
auditlog.register(Event)
