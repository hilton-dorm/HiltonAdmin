from django import template
from main.views import can_use_printer as view_can_use_printer

register = template.Library()


@register.filter(name='can_use_printer')
def can_use_printer(user):
    return view_can_use_printer(user)
