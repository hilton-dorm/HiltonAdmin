from itertools import chain

from cups import IPPError
from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import User
from django.db.models import Q
from django.shortcuts import render, redirect
from django.urls import reverse_lazy, reverse
from django.utils import timezone
from django.utils.html import format_html
from post_office import mail
from datetime import timedelta
from html2text import HTML2Text

from association.views import membership
from common.util.print import print_file, cancel_job, printer_jobs

import config
from association.models import Membership, SocialService, MembershipType
from common.util.usernamegenerator import UsernameGenerator
from config import BASE_URL
from finances_v2.models import Accounts
from main.forms import PersonForm, UserForm, EmailForm, NewPersonForm, NewUserForm, NewTenantForm, PrintForm
from main.models import Tenant, Person, Room, Event, is_member_room_administration, TelegramGroup
from networking.models import Mac
from workinggroups.models import WorkingGroup, GroupMembership


@login_required
def profile(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = PersonForm(request.POST, instance=request.user.person)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()

            messages.success(request, 'Your profile was successfully updated!')
            return redirect('profile')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        user_form = UserForm(instance=request.user)
        profile_form = PersonForm(instance=request.user.person)

    user_tenants = Tenant.objects.filter(person=request.user.person)
    macs = Mac.objects.filter(person=request.user.person)
    group_membership = GroupMembership.objects.filter(person=request.user.person)

    transactions = list(Accounts().NETWORK_EK.entries.filter(person=request.user.person).select_related('transaction')) + \
        list(Accounts().HOUSE_EK.entries.filter(person=request.user.person).select_related('transaction')) + \
        list(Accounts().NETWORK_FK.entries.filter(person=request.user.person, amount__gt=0).select_related('transaction'))

    return render(request, 'main/profile.html', {
        'title': 'Profile',
        'user_form': user_form,
        'profile_form': profile_form,
        'user': request.user,
        'user_tenants': user_tenants,
        'macs': macs,
        'group_membership': group_membership,
        'memberships': Membership.objects.filter(person=request.user.person),
        'transactions': transactions
    })


def can_use_printer(user):
    return user.is_superuser or user.groups.exists()


@login_required
@user_passes_test(can_use_printer, login_url=reverse_lazy('membership'))
def print(request):
    if request.method == 'POST':
        print_form = PrintForm(request.POST, request.FILES)
        if print_form.is_valid():
            print_file(print_form.cleaned_data['pdf'], print_form.cleaned_data['pages'], print_form.cleaned_data['num_copies'])
            messages.success(request, 'Printing...')
            return redirect('print')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        print_form = PrintForm()

    return render(request, 'main/print.html', {
        'title': 'Printer',
        'print_form': print_form,
        'jobs': printer_jobs(),
    })


@login_required
@user_passes_test(can_use_printer, login_url=reverse_lazy('membership'))
def cancel_print(request, job_id):
    try:
        cancel_job(job_id)
        messages.success(request, 'Job cancelled.')
    except IPPError as e:
        messages.error(request, f'Failed to cancel job {job_id}: {e}')
    return redirect('print')


def logout_telegram(request):
    request.user.person.logout_telegram()
    messages.info(request, 'You have been logged out from the telegram bot.')
    return redirect('profile')


@login_required
def tenant_list(request):
    date_60_days_before = timezone.now() - timezone.timedelta(days=60)
    tenants = Tenant.objects.filter(
        Q(date_move_out__gte=date_60_days_before) | Q(date_move_out__isnull=True)) \
        .select_related("person").prefetch_related("person__user").select_related("room")

    return render(request, 'main/tenant_list.html', {
        'title': 'Tenant List',
        'tenants': tenants
    })


@login_required
@user_passes_test(is_member_room_administration, login_url=reverse_lazy('no_permission'))
def room_administration(request):
    if request.method == 'POST':
        user_form = NewUserForm(request.POST)
        person_form = NewPersonForm(request.POST)
        tenant_form = NewTenantForm(request.POST)
        if user_form.is_valid() and person_form.is_valid() and tenant_form.is_valid():
            while True:
                try:
                    tenant_room = Room.objects.get(number=tenant_form.cleaned_data["room"])
                except Room.DoesNotExist:
                    messages.error(request, "Room not found.")
                    break

                new_first_name = user_form.cleaned_data["first_name"]
                new_last_name = user_form.cleaned_data["last_name"]
                existing_user = User.objects.filter(first_name=new_first_name,
                                                    last_name=new_last_name).first()
                if existing_user is not None:
                    messages.error(request,
                                   f"User with name {new_first_name} {new_last_name} already exists. " +
                                   f"See <a href='/admin/auth/user/{existing_user.id}/change/'>here</a>.")
                    break

                new_email = user_form.cleaned_data["email"]
                existing_user = User.objects.filter(email=new_email).first()
                if existing_user is not None:
                    messages.error(request,
                                   f"User with email {new_email} already exists. " +
                                   f"See <a href='/admin/auth/user/{existing_user.id}/change/'>here</a>")
                    break

                user = User.objects.create_user(
                    username=UsernameGenerator.generate(user_form.cleaned_data["first_name"],
                                                        user_form.cleaned_data["last_name"]),
                    first_name=user_form.cleaned_data["first_name"],
                    last_name=user_form.cleaned_data["last_name"],
                    email=user_form.cleaned_data["email"],
                    password=User.objects.make_random_password())

                person, _ = Person.objects.get_or_create(user=user,
                                                         birthday=person_form.cleaned_data["birthday"],
                                                         stw_id=person_form.cleaned_data["stw_id"],
                                                         nationality=person_form.cleaned_data["nationality"],
                                                         gender=person_form.cleaned_data["gender"])

                person_tenant, _ = Tenant.objects.get_or_create(
                    person=person,
                    room=tenant_room,
                    date_move_in=tenant_form.cleaned_data["date_move_in"],
                    date_move_out=tenant_form.cleaned_data["date_move_out"],
                    tenant_type=tenant_form.cleaned_data["tenant_type"])

                normal_membership = MembershipType.objects.get(name__icontains="normal")
                mail.send(
                    user.email,
                    config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                    template='new_tenant_email',
                    headers={'Reply-to': config.EMAIL['EMAIL_ACCOUNT_NETWORK']},
                    context={'user': user,
                             'url': user.person.generate_initial_password_reset_url(),
                             'house_fee': normal_membership.membership_fee,
                             'network_fee': normal_membership.membership_fee_network,
                             'total_fee': normal_membership.membership_fee + normal_membership.membership_fee_network,
                             'telegram_house': TelegramGroup.objects.get(title__iexact="hilton")},
                )
                messages.success(request, 'New User added!')
                break
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        user_form = NewUserForm()
        person_form = NewPersonForm()
        tenant_form = NewTenantForm()

    return render(request, 'main/room_administration.html', {
        'title': 'Room Administration Management',
        'user_form': user_form,
        'person_form': person_form,
        'tenant_form': tenant_form,
        'user': request.user
    })


def send_email(form, request):
    target_emails = []
    working_group_target_emails = 0
    for group in form.cleaned_data["to_them"]:
        # tenants
        for i in range(1, 8):
            if group == ("floor_" + str(i)):
                tens = Tenant.objects.filter(Q(room__number__startswith=str(i)) &
                                             Q(person__email_distributor=True) &
                                             (Q(date_move_out__gt=timezone.now()) | Q(date_move_out__isnull=True))
                                             )
                target_emails = target_emails + [ten.person.user.email for ten in tens]
                continue
        # members
        if group == 'members':
            members = Membership.current_memberships()
            target_emails = target_emails + [m.person.user.email for m in members]
        elif group == 'regular_members':
            members = Membership.current_memberships().filter(type__name__icontains='ordentlich')
            target_emails = target_emails + [m.person.user.email for m in members]
        elif group == 'tenants':
            tenants = Tenant.current_tenants()
            if not GroupMembership.current_members().filter(person=request.user.person).exists():
                tenants = tenants.filter(person__email_distributor=True)
            target_emails = target_emails + [t.person.user.email for t in tenants]
        elif group == 'moving_in_tenants':
            in_3_months = timezone.now() + timedelta(90)
            tenants = Tenant.objects.filter(Q(date_move_in__gte=timezone.now()) & Q(date_move_in__lte=in_3_months))
            target_emails = target_emails + [t.person.user.email for t in tenants]
        else:
            # working groups
            try:
                target_emails.append(WorkingGroup.objects.filter(name__icontains=group)[0].email)
                working_group_target_emails += 1
            except IndexError:
                pass

    # Make list of addresses distinct
    target_emails = list(set(target_emails))
    # docs: https://github.com/ui/django-post_office#mailsend
    attachments = None
    if request.FILES:
        file = request.FILES['attachement']
        attachments = {
            file.name: {'file': file.file, 'mimetype': file.content_type},
        }
    content = form.cleaned_data["content"]
    if working_group_target_emails == len(target_emails):
        # Die Mail geht nur an AGs, wir schreiben noch rein wer sie gesendet hat
        user_url = f"{BASE_URL}{reverse('admin:auth_user_change', args=[request.user.id])}"
        membership_url = f"{BASE_URL}{reverse('admin:association_membership_changelist')}?q={request.user.person}"
        content += f'\n<br><br>\nSend from {request.user.person}, Room {request.user.person.current_room()}, <a href="{user_url}">User Admin</a>, <a href="{membership_url}">Membership Admin</a>'
    mail.send(recipients=[],
              bcc=target_emails,
              sender=form.cleaned_data["reply_to"],
              template='email_distributor',
              context={'subject': form.cleaned_data["subject"],
                       'content': content},
              attachments=attachments)
    # we can not use mail.send_many(...) because it does not support attachments
    # https://github.com/ui/django-post_office#send_many
    return len(target_emails)


@login_required
def email(request):
    # list of tuples with (value, display_text), here (email, email)
    sender_choices = [(request.user.email, request.user.email)] + \
                     [(w.working_group.email, w.working_group.email) for w in GroupMembership.current_members().filter(
                         person=request.user.person).select_related("working_group")]
    if request.method == 'POST':
        form = EmailForm(request.POST)
        form.fields['reply_to'].choices = sender_choices
        if form.is_valid():
            amount_receivers = send_email(form, request)
            if amount_receivers:
                messages.success(request, str(amount_receivers) + ' email(s) successfully sent!')
            else:
                messages.warning(request, 'Unfortunately your email was not sent.')
            if form.cleaned_data['create_event']:
                h2t = HTML2Text()
                h2t.ignore_links = True
                h2t.ignore_emphasis = True
                h2t.ul_item_mark = '•'
                h2t.single_line_break = True
                h2t.body_width = 300
                h2t.unicode_snob = True
                event = Event(name=form.cleaned_data["event_name"], time=form.cleaned_data["event_date"], description=h2t.handle(form.cleaned_data["content"]))
                if "senat" in event.name.lower():
                    event.type = Event.TYPE_SENATSSITZUNG
                elif "voll" in event.name.lower():
                    event.type = Event.TYPE_MITGLIEDERVOLLVERSAMMLUNG
                event.save()
                link = reverse("admin:main_event_change", args=[event.id])
                text = format_html('Event <b><a href="{}">{}</a></b> created.', link, event.name)
                messages.success(request, text)
            return redirect('email')
        else:
            messages.warning(request, 'Please correct the error below.')
    else:
        form = EmailForm(initial={'reply_to': request.user.email, 'subject': request.GET.get('subject', '')})
        form.fields['reply_to'].choices = sender_choices

    if not GroupMembership.current_members().filter(person=request.user.person).exists():
        # the user is no Leader, so remove the leader only email targets
        form.remove_extra_targets()

    return render(request, 'main/email.html', {
        'title': 'Email Distributor',
        'form': form,
        'user': request.user
    })


@login_required
def telegram_groups(request):
    groups = TelegramGroup.objects.all()
    return render(request, 'main/telegram_groups.html', {
        'title': 'Telegram Groups',
        'groups': groups
    })


@login_required
def calendar(request):
    social_service_events = SocialService.objects.filter(show_as_event=True, time__gte=timezone.now())
    events = Event.objects.filter(time__gte=timezone.now())

    # Merges both querysets
    events = sorted(
        chain(events, social_service_events),
        key=lambda instance: instance.time)

    return render(request, 'main/calendar.html', {
        'title': 'Calendar',
        'events': events
    })


@login_required
def calendar_remind(request, is_social_service, event_id):
    try:
        if is_social_service:
            event = SocialService.objects.get(id=event_id)
        else:
            event = Event.objects.get(id=event_id)
        date_reminder = event.time - timezone.timedelta(days=1)

        mail.send(
            request.user.email,
            config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
            scheduled_time=date_reminder,
            priority='low',
            template='calendar_reminder_mail',
            context={'user': request.user,
                     'event': event},
        )
        messages.success(request, "You will receive a reminder email one day before the event starts.")
    except (Event.DoesNotExist, SocialService.DoesNotExist):
        messages.error(request, "An error occurred.")

    return redirect('calendar')


def no_permission(request):
    return render(request, 'main/no_permission.html', {
        'title': 'Ouch!',
    })
