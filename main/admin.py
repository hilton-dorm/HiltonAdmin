import cologne_phonetics
import nested_admin
from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from django.db.models import Q, Min, Exists, OuterRef
from django.http import HttpResponse
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html, mark_safe
from django.utils.translation import gettext_lazy as _

from association.models import Membership, SocialService, Registration, SocialServicePeriod
from networking.models import Mac
from workinggroups.models import GroupMembership
from .models import Person, Tenant, Room, Event, TelegramGroup


class TenantStatusListFilter(admin.SimpleListFilter):
    title = _('Tenant Status')

    parameter_name = 'move_in_status'

    def lookups(self, request, model_admin):
        return (
            ('0', _('moving in')),
            ('1', _('living')),
            ('2', _('moved out')),
        )

    def queryset(self, request, queryset):

        now = timezone.now()
        if self.value() == '0':
            people = Tenant.objects.filter(date_move_in__gte=now).values_list('person', flat=True)
            return queryset.filter(person__in=people)
        if self.value() == '1':
            people = Tenant.objects.filter(date_move_in__lte=now,
                                           date_move_out__gte=now).values_list('person', flat=True)
            return queryset.filter(person__in=people)
        if self.value() == '2':
            people = Tenant.objects.filter(date_move_out__lte=now).values_list('person', flat=True)
            return queryset.filter(person__in=people)


class TenantRoomInline(nested_admin.NestedTabularInline):
    model = Tenant
    can_delete = False
    fk_name = 'room'
    extra = 0
    ordering = ("date_move_in", "date_move_out")


@admin.register(Room)
class RoomAdmin(nested_admin.NestedModelAdmin):
    list_display = ('number', 'vlan_id', 'eth_port0', 'eth_port1', 'renovated', 'comment')
    list_editable = ['renovated']
    search_fields = ['number']
    inlines = [TenantRoomInline]
    list_per_page = 999

    def link_to_user(self, obj):
        link = reverse("admin:auth_user_change", args=[obj.tenant.person.user.id])
        return format_html('<b><a href="{}">{}</a></b>', link, obj.tenant.person.full_name())

    link_to_user.short_description = 'Person'


@admin.register(Event)
class EventAdmin(admin.ModelAdmin):
    list_display = ('name', 'type', 'description', 'time')
    readonly_fields = ('external_identifier',)
    search_fields = ['name']
    list_filter = ['type']
    save_as = True


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    search_fields = ['user__first_name', 'user__last_name']
    exclude = ['name_cologne_phonetics']

    def get_search_results(self, request, queryset, search_term):
        original, _ = super().get_search_results(request, queryset, search_term)
        queries = None
        for _, code in cologne_phonetics.encode(search_term, concat=True):
            query = Q(name_cologne_phonetics__contains=' ' + code + ' ')
            queries = query if queries is None else queries & query
        return original | self.model.objects.filter(queries), True


class TenantAdmin(nested_admin.NestedModelAdmin):
    list_select_related = (
        'person',
        'room',
    )
    autocomplete_fields = ('person', )

    def get_queryset(self, request):
        return super(TenantAdmin, self).get_queryset(request).prefetch_related('person__user')

    list_display = (
        'link_to_user', 'link_to_room', 'date_move_in', 'date_move_out', 'tenant_type', 'comment', 'tenant_status')
    list_filter = ['tenant_type']
    search_fields = ['person__user__first_name', 'person__user__last_name', 'room__number', 'date_move_in',
                     'date_move_out', 'tenant_type', 'comment']
    list_per_page = 999

    def link_to_user(self, obj):
        link = reverse("admin:auth_user_change", args=[obj.person.user.id])
        return format_html('<b><a href="{}">{}</a></b>', link, obj.person.full_name())

    link_to_user.short_description = 'Person'

    def link_to_room(self, obj):
        link = reverse("admin:main_room_change", args=[obj.room.id])
        return format_html('<b><a href="{}">{}</a></b>', link, obj.room)

    link_to_room.short_description = 'Room'


@admin.register(TelegramGroup)
class TelegramGroupAdmin(nested_admin.NestedModelAdmin):
    pass


class NestedRegistrationInline(nested_admin.NestedTabularInline):
    model = Registration
    can_delete = False
    fk_name = 'social_service_period'
    extra = 0
    readonly = True
    readonly_fields = ('social_service_own', 'verified', 'verified_at')
    fields = ('social_service_own', 'verified', 'verified_at')

    def social_service_own(self, instance):
        link = reverse("admin:association_socialservice_change", args=[instance.social_service.id])
        return format_html('Social Service: <b><a href="{}">{}</a></b>', link, instance.social_service)

    social_service_own.short_description = "Social Service"

    @staticmethod
    def has_add_permission(request, obj=None):
        return False


class SocialServicePeriodInline(nested_admin.NestedTabularInline):
    model = SocialServicePeriod
    can_delete = False
    fk_name = 'membership'
    extra = 0
    readonly = True
    readonly_fields = ('start_date', 'extra_days_everybody', 'days_ruhend', 'end_date', 'duration')
    inlines = [NestedRegistrationInline]

    def get_queryset(self, request):
        return super(SocialServicePeriodInline, self).get_queryset(request).prefetch_related(
            'membership__membershiptypeperiods_set')

    @staticmethod
    def has_add_permission(request, obj=None):
        return False

    def days_ruhend(self, instance):
        return instance.days_ruhend()

    days_ruhend.short_description = 'Days Ruhend'

    def end_date(self, instance):
        date = instance.estimated_end()
        if isinstance(date, str):
            return date
        return date.strftime("%d.%m.%Y")

    end_date.short_description = 'End Date'

    def duration(self, instance):
        if instance.end_date is None:
            return "Current Period"
        days = (instance.end_date - instance.start_date).days - instance.days_ruhend()
        return str(days) + " days"

    duration.short_description = 'Duration (ohne ruhend)'


class MembershipInline(nested_admin.NestedTabularInline):
    model = Membership
    can_delete = False
    fk_name = 'person'
    extra = 0
    inlines = [SocialServicePeriodInline]


class TenantPersonInline(nested_admin.NestedTabularInline):
    model = Tenant
    can_delete = False
    fk_name = 'person'
    extra = 0


class MacInline(nested_admin.NestedTabularInline):
    model = Mac
    can_delete = True
    fk_name = 'person'
    extra = 1
    readonly_fields = ('last_used',)


class GroupMembershipInline(nested_admin.NestedTabularInline):
    model = GroupMembership
    can_delete = False
    fk_name = 'person'
    extra = 0
    readonly_fields = ('working_group', )
    fields = ('working_group', 'date_join', 'date_leave', 'type', 'comment', 'mailing_list_enabled')


class RegistrationInline(nested_admin.NestedTabularInline):
    model = Registration
    can_delete = False
    fk_name = 'person'
    extra = 0
    readonly = True
    readonly_fields = ('social_service', 'verified', 'verified_at', 'social_service_period')

    def get_queryset(self, request):
        # the rest is already displayed at the NestedRegistrationInline
        return super(RegistrationInline, self).get_queryset(request).filter(social_service_period__isnull=True)


class PersonInline(nested_admin.NestedStackedInline):
    model = Person
    # User can have only one Person. just to avoid displaying "Persons" when we only have one person
    verbose_name_plural = 'Person'
    can_delete = False
    fk_name = 'user'
    readonly_fields = ('email_distributor', 'stw_id')
    exclude = ['name_cologne_phonetics', 'telegram_chat_id']
    inlines = [TenantPersonInline, GroupMembershipInline, MacInline, MembershipInline, RegistrationInline]


def create_list_of_email_addresses(modeladmin, request, queryset):
    email_addresses = []
    for user in queryset:
        email_addresses.append(user.email)
    return HttpResponse(",".join(email_addresses), content_type='text/plain')


create_list_of_email_addresses.short_description = "Download List of Email Addresses"


class UserRoomFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('floor')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'floor'

    def lookups(self, request, model_admin):
        return (
            ('0', _('base floor')),
            ('1', _('1. floor')),
            ('2', _('2. floor')),
            ('3', _('3. floor')),
            ('4', _('4. floor')),
            ('5', _('5. floor')),
            ('6', _('6. floor')),
            ('7', _('7. floor')),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value() in ["0", "1", "2", "3", "4", "5", "6", "7"]:
            rooms = Room.objects.filter(number__startswith=self.value())
            tenants = Tenant.objects.filter(room__in=rooms)
            people = Person.objects.filter(tenant__in=tenants)
            return queryset.filter(person__in=people)


class UserMembershipFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('membership')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'membership'

    def lookups(self, request, model_admin):
        return (
            ('0', _('is a member')),
            ('1', _('is not a member'))
        )

    def queryset(self, request, queryset):
        members = Membership.objects.filter(
            Q(date_leave__isnull=True) | Q(date_leave__lt=timezone.now()))
        if self.value() == "0":
            return queryset.filter(person__in=members.values_list('person', flat=True))

        if self.value() == "1":
            not_members = Person.objects.exclude(pk__in=members.values_list('person__id', flat=True))
            return queryset.filter(person__in=not_members)

        return queryset


class UserAttendedNewTenantbarFilter(SimpleListFilter):
    title = _('New Tenant Bar Attendance')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'newTenantBar'

    def lookups(self, request, model_admin):
        return (
            ('0', _('attended already')),
            ('1', _('not attended'))
        )

    def queryset(self, request, queryset):
        if self.value() == "0":
            return queryset.filter(person__registration__verified=True,
                                   person__registration__social_service__is_new_tenant_bar=True).distinct()

        if self.value() == "1":
            return queryset.filter(~(Q(person__registration__verified=True) & Q(
                person__registration__social_service__is_new_tenant_bar=True)))

        return queryset


class ExtendedUserAdmin(UserAdmin, nested_admin.NestedModelAdmin):
    list_select_related = (
        'person',
        'room',
    )

    def get_queryset(self, request):
        return super(ExtendedUserAdmin, self).get_queryset(request).select_related('person').annotate(
            date_move_in=Min('person__tenant__date_move_in'), attended_new_tenant_bar=Exists(Registration.objects.filter(person=OuterRef('person'), verified=True, social_service__is_new_tenant_bar=True))).prefetch_related(
            'person__user', 'person__tenant_set', 'person__tenant_set__room', 'person__membership_set')

    inlines = UserAdmin.inlines + (PersonInline, )
    list_display = (
        'first_name', 'last_name', 'link_to_email', 'room', 'nationality', 'gender', 'is_email_distributor',
        'has_attended_new_tenant_bar', 'has_membership', 'last_login', 'uses_telegram')

    list_filter = (
        'groups', UserRoomFilter,
        'person__email_distributor', 'is_staff', UserMembershipFilter, TenantStatusListFilter,
        UserAttendedNewTenantbarFilter)
    actions = [create_list_of_email_addresses]
    list_per_page = 999
    fieldsets_normal = (
        (None, {'fields': ('username',)}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'last_login', 'date_joined')}),
    )
    superuser_fieldsets = (
        (None, {'fields': ('username', 'password')}),
        (_('Personal info'), {'fields': ('first_name', 'last_name', 'email', 'last_login', 'date_joined')}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')}),
    )

    def get_search_results(self, request, queryset, search_term):
        original, use_distinct = super().get_search_results(request, queryset, search_term)
        queries = None
        for _, code in cologne_phonetics.encode(search_term, concat=True):
            if not code:
                continue
            query = Q(person__name_cologne_phonetics__contains=' ' + code + ' ')
            queries = query if queries is None else queries & query
        if queries is not None:
            return original | queryset.filter(queries), True
        return original, use_distinct

    def get_readonly_fields(self, request, obj=...):
        if request.user.is_superuser:
            return 'last_login', 'date_joined'
        return 'username', 'last_login', 'date_joined'

    # We are adding the person relationship so to avoid unnecessary queries to the database
    list_select_related = ['person']

    def link_to_email(self, instance):
        return format_html('<b><a href="mailto:{}">{}</a></b>', instance.email, instance.email)

    link_to_email.short_description = 'Email'

    def nationality(self, instance):
        return instance.person.nationality

    nationality.short_description = 'Nationality'

    def gender(self, instance):
        return instance.person.gender

    gender.short_description = 'Gender'

    def room(self, instance):
        rooms = []
        for room in instance.person.get_rooms():
            link = reverse("admin:main_room_change", args=[room.id])
            rooms.append(format_html('<b><a href="{}">{}</a></b>', link, room))
        return mark_safe(", ".join(rooms) + " since " + str(instance.date_move_in))

    def has_membership(self, instance):
        return instance.person.membership_set.exists()

    has_membership.short_description = 'Member'
    has_membership.boolean = True

    @admin.display(description="Telegram Bot", boolean=True, ordering="person__telegram_chat_id")
    def uses_telegram(self, instance):
        print(instance.person.has_telegram_chat())
        return instance.person.has_telegram_chat()

    def full_name(self, instance):
        return instance.person.full_name()

    def is_email_distributor(self, instance):
        return instance.person.email_distributor

    is_email_distributor.short_description = 'Email distributor'
    is_email_distributor.boolean = True

    def has_attended_new_tenant_bar(self, instance):
        if instance.attended_new_tenant_bar:
            return "Already attended"
        return "Missed " +\
            str(SocialService.new_tenant_bars_since(instance.date_move_in)) + " of 3 times" if instance.date_move_in else None

    has_attended_new_tenant_bar.short_description = 'New Tenant Bar'

    # We need to override the get_inline_instances method, so to display the inlines only in the edit form.
    # Otherwise we might get some problems because of how the Signals work. (The Signal is responsible for
    # creating the Profile instance)
    # Source: https://simpleisbetterthancomplex.com/tutorial/2016/11/23/how-to-add-user-profile-to-django-admin.html
    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(ExtendedUserAdmin, self).get_inline_instances(request, obj)

    # We want to prevent that users are able to change their own permissions especially if they are not allowed to do so
    def change_view(self, request, *args, **kwargs):
        # for superuser
        if request.user.is_superuser:
            try:
                self.fieldsets = self.superuser_fieldsets
                response = super(UserAdmin, self).change_view(request, *args, **kwargs)
            finally:
                # Reset fieldsets to its original value
                self.fieldsets = self.fieldsets_normal
            return response
        else:
            self.fieldsets = self.fieldsets_normal
            return super(UserAdmin, self).change_view(request, *args, **kwargs)


# We want to extend the UserAdmin page, hence we have to unregister the default one
admin.site.unregister(User)
admin.site.register(User, ExtendedUserAdmin)
admin.site.register(Tenant, TenantAdmin)
