from post_office import mail
from telegram import Update, ParseMode, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CommandHandler, CallbackContext, CallbackQueryHandler
from django_telegrambot.apps import DjangoTelegramBot
from main.models import Person
from workinggroups.models import WorkingGroup

import re
import html
import json
import logging
import traceback
import config

logger = logging.getLogger(__name__)


def send_not_logged_in_message(update: Update):
    update.message.reply_text("You are not logged in. "
                              "See [here](https://verwaltung.hilton.rwth-aachen.de/#telegram) how to log in.",
                              parse_mode=ParseMode.MARKDOWN)


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update: Update, context: CallbackContext) -> None:
    """Send a message when the command /start is issued."""
    if Person.get_for_chat_id(update.message.chat_id):
        help_command(update, context)
    else:
        update.message.reply_text(
            'Welcome to the Hilton Bot. Please login with `/login <your_pin>` from '
            '[here](https://verwaltung.hilton.rwth-aachen.de/#telegram). E.g. /login 12345678',
            parse_mode=ParseMode.MARKDOWN)


def help_command(update: Update, context: CallbackContext) -> None:
    if not Person.get_for_chat_id(update.message.chat_id):
        send_not_logged_in_message(update)
    update.message.reply_text('Type `/` to see available commands', parse_mode='Markdown')


def login(update: Update, context: CallbackContext) -> None:
    if Person.get_for_chat_id(update.effective_message.chat_id):
        update.effective_message.reply_text('You are already logged in.')
        return
    pin = update.effective_message.text[6:].strip(' ')
    if not re.match("^[0-9]+$", pin):
        if pin == "":
            update.effective_message.reply_text("You must provide a login pin. "
                                                "See [here](https://verwaltung.hilton.rwth-aachen.de/#telegram) "
                                                "how to log in. It must be of the format `/login your_pin`, "
                                                "eg `/login 234567234`",
                                                parse_mode=ParseMode.MARKDOWN)
            return
        update.effective_message.reply_text(f"The login pin must consists only of digits. Entered pin: '{pin}'."
                                            "See [here](https://verwaltung.hilton.rwth-aachen.de/#telegram) "
                                            "how to log in.",
                                            parse_mode=ParseMode.MARKDOWN)
        return
    person = Person.get_for_login_pin(int(pin))
    if not person:
        update.effective_message.reply_text("The login pin '" + pin + "' is not known")
    else:
        chat_id = update.effective_message.chat_id
        # acceptance of Datenschutzerklärung
        update.effective_message.reply_text(
            "In order to use the Fitnessroom you need to accept our Datenschutzerklärung!\n"
            + config.FITNESS_ROOM_TELEGRAM_DATENSCHUTZ_LINK,
            reply_markup=InlineKeyboardMarkup(
                [[InlineKeyboardButton(text="I accept!",
                                       callback_data=f"acceptDatenschutz,{pin}"),
                  InlineKeyboardButton(text="I don't accept!", callback_data="cancelDatenschutz")]]
            ))


def logout(update: Update, context: CallbackContext) -> None:
    person = Person.get_for_chat_id(update.message.chat_id)
    if not person:
        update.message.reply_text("You weren't logged in")
    else:
        person.logout_telegram()
        update.message.reply_text("Logout successful. A new login pin has been generated.")


# from https://github.com/python-telegram-bot/python-telegram-bot/blob/master/examples/errorhandlerbot.py
def error_handler(update: Update, context: CallbackContext) -> None:
    try:
        """Log the error and send a telegram message to notify the developer."""
        # Log the error before we do anything else, so we can see it even if something breaks.
        logger.error(msg="Exception while handling an update:", exc_info=context.error)
        # traceback.format_exception returns the usual python message about an exception, but as a
        # list of strings rather than a single string, so we have to join them together.
        tb_list = traceback.format_exception(None, context.error, context.error.__traceback__)
        tb_string = ''.join(tb_list)

        # Build the message with some markup and additional information about what happened.
        # You might need to add some logic to deal with messages longer than the 4096 character limit.
        messages = [(
            f'An exception was raised while handling an update\n'
            f'<pre>update = {html.escape(json.dumps(update.to_dict(), indent=2, ensure_ascii=False))}'
            '</pre>\n\n'
            f'<pre>context.chat_data = {html.escape(str(context.chat_data))}</pre>\n\n'
            f'<pre>context.user_data = {html.escape(str(context.user_data))}</pre>\n\n'),
            f'<pre>{html.escape(tb_string)}</pre>'
        ]
        if config.DEBUG:
            # send the error message to the developer
            context.bot.send_message(chat_id=update.effective_chat.id, text=messages[0], parse_mode=ParseMode.HTML)
            context.bot.send_message(chat_id=update.effective_chat.id, text=messages[1], parse_mode=ParseMode.HTML)
        else:
            # send and error message to the user and the stacktrace to the netzAG leader
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text="Internal error while processing your message")
            mail.send(
                config.EMAIL["EMAIL_ACCOUNT_ADMIN"],
                config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                html_message="\n\n".join(messages),
                subject="Error while handling telegram messages"
            )
    except Exception as e:
        print(e)


def accept_datenschutz(update: Update, _: CallbackContext) -> None:
    query = update.callback_query
    query.answer()

    query_list = query.data.split(",")
    pin = query_list[1]
    person = Person.get_for_login_pin(int(pin))
    if not person:
        query.edit_message_text("The login pin '" + pin + "' is unknown")
        return
    person.telegram_chat_id = update.effective_chat.id
    person.save()
    query.edit_message_text("Login successful. Welcome " + str(person))


def cancel_datenschutz(update: Update, _: CallbackContext) -> None:
    query = update.callback_query
    query.answer()
    query.edit_message_text("You were not logged in because you didn't accept our Datenschutzerklärung. "
                            "If you are ready to accept it, you can log in again.")


def main():
    dispatcher = DjangoTelegramBot.dispatcher

    # on different commands - answer in Telegram
    dispatcher.add_handler(CommandHandler("start", start))
    dispatcher.add_handler(CommandHandler("help", help_command))
    dispatcher.add_handler(CommandHandler("login", login))
    dispatcher.add_handler(CommandHandler("logout", logout))
    dispatcher.add_handler(CallbackQueryHandler(accept_datenschutz, pattern="^acceptDatenschutz"))
    dispatcher.add_handler(CallbackQueryHandler(cancel_datenschutz, pattern="^cancelDatenschutz"))

    # ...and the error handler
    dispatcher.add_error_handler(error_handler)
