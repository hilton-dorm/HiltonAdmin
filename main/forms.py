from django import forms
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from tinymce.widgets import TinyMCE

from main.models import Person, Tenant


class AuthenticationForm(forms.Form):
    username = forms.CharField(label="Username")
    password = forms.CharField(widget=forms.PasswordInput())


class UserForm(forms.ModelForm):
    class Meta:
        model = User
        fields = ('email',)


class NewUserForm(forms.ModelForm):

    # make every field required
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        for field in self.Meta.fields:
            self.fields[field].required = True

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class NewPersonForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ('nationality', 'birthday', 'stw_id', 'gender')


class NewTenantForm(forms.ModelForm):
    class Meta:
        model = Tenant
        fields = ('room', 'date_move_in', 'date_move_out', 'tenant_type', 'comment')


class PersonForm(forms.ModelForm):
    class Meta:
        model = Person
        labels = {"email_distributor": "Included in the email distributor",
                  "unfunded_account_warning_days_before": "Send uncovered account warnings the following days in advance:"}
        fields = ('nationality', 'gender', 'email_distributor', 'unfunded_account_warning_days_before')


EMAIL_TARGET = [
    ('tenants', 'Tenants'),
    ('floor_1', 'Room 1010-1102'),
    ('floor_2', 'Room 2010-2092'),
    ('floor_3', 'Room 3010-3092'),
    ('floor_4', 'Room 4010-4092'),
    ('floor_5', 'Room 5010-5091'),
    ('floor_6', 'Room 6010-6091'),
    ('floor_7', 'Room 7010-7092'),
    # Die Gruppennamen (matcht durch icontains) kommen von hier:
    # https://verwaltung.hilton.rwth-aachen.de/admin/workinggroups/workinggroup/
    ('Haussprecher', 'House Speaker'),
    ('Vorstand', 'Board Members (Vorstand)'),
    ('network', 'Networking Group'),
    ('symposion', 'Symposion'),
    ('fitness', 'Fitness Group'),
    ('Belegungs', 'Room Administration Group'),
    ('K14', 'Common Room Group (K14 AG)'),
    ('workshop', 'Workshop'),
    ('washing', 'Washing Machine Group'),
    ('Hausmeister', 'House Keeper'),
]

EXTRA_TARGETS = [
    ('moving_in_tenants', 'Tenants that are moving in in the next 3 Months'),
    ('members', 'All members of the Turmstraße 1 e.V.'),
    ('regular_members', 'All regular members of the Turmstraße 1 e.V.'),
]


class TargetSelectField(forms.MultipleChoiceField):
    def validate(self, value):
        if (not len(value)):
            raise ValidationError(self.error_messages["required"])
        return value


class EmailForm(forms.Form):
    required_msg = 'Diese Feld darf nicht leer sein.'
    leer_msg = 'Bitte wähle mindestens eine aus.'

    to_them = forms.MultipleChoiceField(
        #    required = True,
        label='',
        widget=forms.CheckboxSelectMultiple,
        choices=EMAIL_TARGET + EXTRA_TARGETS,
        error_messages={'isvalid_choices': 'choose at least 1'}
    )
    reply_to = forms.ChoiceField(label='Sender (reply to)', )
    subject = forms.CharField(max_length=80, error_messages={'required': required_msg})
    content = forms.CharField(widget=TinyMCE(), error_messages={'required': required_msg}, label='Message')
    attachement = forms.FileField(required=False)
    create_event = forms.BooleanField(label='Also create an event', required=False)
    event_name = forms.CharField(max_length=80, required=False)
    event_date = forms.DateTimeField(required=False)

    def remove_extra_targets(self):
        self.fields['to_them'] = forms.MultipleChoiceField(
            label='',
            widget=forms.CheckboxSelectMultiple,
            choices=EMAIL_TARGET,
            error_messages={'isvalid_choices': 'choose at least 1'}
        )
        del self.fields['create_event']
        del self.fields['event_name']
        del self.fields['event_date']

    def clean(self):
        cleaned_data = super(EmailForm, self).clean()
        activity = cleaned_data.get('create_event')
        if activity:
            if cleaned_data.get('event_name') is None:
                self.add_error('event_name', EmailForm.required_msg)
            if cleaned_data.get('event_date') is None:
                self.add_error('event_date', EmailForm.required_msg)
        return cleaned_data

    def is_valid(self):
        """Return True if the form has no errors, or False otherwise."""
        # return self.fields['to_them'].has_changed() and not self.error
        return self.is_bound and not self.errors


class PrintForm(forms.Form):
    pdf = forms.FileField(required=True, widget=forms.FileInput(attrs={'accept': 'application/pdf'}))
    pages = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'all'}), required=False)
    num_copies = forms.IntegerField(initial=1)
