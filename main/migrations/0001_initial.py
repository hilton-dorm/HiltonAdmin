# Generated by Django 2.1.5 on 2019-08-13 13:32

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django.utils.timezone
import django_countries.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Event',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(default='', max_length=255)),
                ('description', models.TextField(blank=True)),
                ('time', models.DateTimeField(default=django.utils.timezone.now)),
                ('external_identifier', models.CharField(blank=True, default='', max_length=50, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('birthday', models.DateField(default=django.utils.timezone.now)),
                ('stw_id', models.IntegerField(default=0)),
                ('tim_id', models.CharField(default='', max_length=8)),
                ('nationality', django_countries.fields.CountryField(blank=True, max_length=2, null=True)),
                ('gender', models.CharField(choices=[('Male', 'Male'), ('Female', 'Female'), ('Divers', 'Divers'), ('Unknown', 'Unknown')], default='Unknown', max_length=10)),
                ('cloud_quota', models.CharField(default='30 GB', max_length=10)),
                ('email_distributor', models.BooleanField(default=False)),
                ('has_subscribed_to_calendar', models.BooleanField(default=False)),
                ('has_attended_new_tenant_bar', models.BooleanField(default=False)),
                ('missed_new_tenant_bar_counter', models.IntegerField(default=0)),
                ('signed_usage_data_stw', models.BooleanField(default=False, help_text='This is mandatory for all active people who work with data about the tenants. Since we receive tenant data from the Studierendenwerk we have to accept their privacy policy.', verbose_name='Signed the privacy policy (STW)')),
                ('user', models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('user__last_name',),
            },
        ),
        migrations.CreateModel(
            name='Room',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('number', models.CharField(max_length=4)),
                ('eth_port0', models.SmallIntegerField()),
                ('eth_port1', models.SmallIntegerField()),
                ('comment', models.CharField(blank=True, max_length=255)),
            ],
            options={
                'ordering': ('number',),
            },
        ),
        migrations.CreateModel(
            name='Tenant',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_move_in', models.DateField()),
                ('date_move_out', models.DateField()),
                ('tenant_type', models.CharField(choices=[('0', 'Mieter'), ('1', 'International'), ('2', 'Untermieter')], default='0', max_length=10)),
                ('comment', models.CharField(blank=True, max_length=255)),
                ('person', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Person')),
                ('room', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='main.Room')),
            ],
            options={
                'ordering': ('room',),
            },
        ),
        migrations.AddField(
            model_name='room',
            name='tenants',
            field=models.ManyToManyField(through='main.Tenant', to='main.Person'),
        ),
    ]