# Generated by Django 2.2.5 on 2021-09-09 22:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0015_person_got_fitnessroom_introduction'),
    ]

    operations = [
        migrations.CreateModel(
            name='TelegramGroup',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=50)),
                ('text', models.TextField(max_length=255)),
                ('telegram_link', models.URLField()),
                ('telegram_button_text', models.CharField(default='Join Telegram Group', max_length=50)),
                ('discord_link', models.URLField(blank=True, null=True)),
                ('card_header_text', models.CharField(blank=True, max_length=50, null=True)),
            ],
        ),
    ]
