# Generated by Django 3.2.12 on 2023-10-23 12:49

from django.db import migrations


def detect_event_types(apps, schema_editor):
    # We can't import the Person model directly as it may be a newer
    # version than this migration expects. We use the historical version.
    Event = apps.get_model("main", "Event")
    for event in Event.objects.all():
        if "Senatssitzung" in event.name:
            event.type = 'S'
            event.save()
        elif "Mitgliedervollversammlung" in event.name or "Mitgliederversammlung" == event.name:
            event.type = 'M'
            event.save()

class Migration(migrations.Migration):

    dependencies = [
        ('main', '0022_event_type'),
    ]

    operations = [
        migrations.RunPython(detect_event_types),
    ]
