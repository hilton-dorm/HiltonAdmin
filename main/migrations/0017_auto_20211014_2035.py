# Generated by Django 2.2.5 on 2021-10-14 18:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0016_auto_20210910_0052'),
    ]

    operations = [
        migrations.AlterField(
            model_name='person',
            name='telegram_chat_id',
            field=models.BigIntegerField(blank=True, null=True, unique=True),
        ),
    ]
