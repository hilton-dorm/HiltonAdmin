#!/bin/sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
cd "$DIR"

if [ -x "$(command -v docker)" ]; then
    echo "docker installation satisfied"
else
    echo "Install docker please"
    exit 1
fi

if [ -x "$(command -v docker-compose)" ]; then
    echo "docker-compose installation satisfied"
else
    echo "Install docker-compose please"
    exit 1
fi


if [ -f "$DIR/db.sqlite3" ]; then
    echo "Found database file"
else
    echo "Did not found database file"
    exit 1
fi

if [ -f "$DIR/config.py" ]; then
    echo "Found config file"
else
    echo "Did not found config file"
    exit 1
fi

extra_args=""
if grep -qE "TELEGRAM_BOT_API_TOKEN *= *'[0-9]+:[0-9a-zA-Z_]+'" config.py
then
    echo "Found valid Telegram Bot Token. Also start Telegram Bot polling."
    extra_args="--profile telegram"
else
    echo "No valid Telegram Bot Token in config.py found. Don't start Telegram Bot polling."
fi

echo ""
echo "All requirements satisfied, building and deploying the project now..."
sleep 1

docker-compose $extra_args up --build
