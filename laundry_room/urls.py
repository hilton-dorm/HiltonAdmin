from django.urls import path, include
from rest_framework.routers import DefaultRouter

from . import views
from .views import BookingViewSet, DeviceProblemViewSet

router = DefaultRouter()
router.register(r'bookings', BookingViewSet, basename='booking')
router.register(r'device-problems', DeviceProblemViewSet, basename='device-problems')

urlpatterns = [
    path('timetable', views.timetable, name='timetable'),
    path('', include(router.urls)),
]
