from rest_framework import serializers, status
from rest_framework.exceptions import APIException

from .models import Booking, DeviceProblem


class ConflictError(APIException):
    status_code = 409
    default_detail = "This booking overlaps with an existing one."
    default_code = "booking_conflict"


class BookingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Booking
        fields = '__all__'
        read_only_fields = ['person']

    def validate(self, data):
        # Extract the necessary fields from the data
        device = data['device']
        start_time = data['start_time']
        end_time = data['end_time']

        # Check for overlapping bookings
        overlapping_bookings = Booking.objects.filter(
            device=device,
            start_time__lt=end_time,
            end_time__gt=start_time
        ).exclude(id=self.instance.id if self.instance else None)

        if overlapping_bookings.exists():
            raise ConflictError("This booking overlaps with an existing one.",
                                code=status.HTTP_409_CONFLICT)

        return data


class DeviceProblemSerializer(serializers.ModelSerializer):
    class Meta:
        model = DeviceProblem
        fields = '__all__'
        read_only_fields = ['reported_by']
