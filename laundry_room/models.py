from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.urls import reverse
from post_office import mail

import config
from config import BASE_URL
from main.models import Person
from workinggroups.models import WorkingGroup


# Create your models here.
class Device(models.Model):
    class Type(models.IntegerChoices):
        WASHING_MACHINE = 1, "Washing machine"
        DRYER = 2, "Dryer"

    class Status(models.IntegerChoices):
        WORKING = 1, "Working"
        BROKEN = 2, "Broken"
        DEGRADED = 3, "Degraded"

    number = models.IntegerField()
    type = models.IntegerField(choices=Type.choices)
    status = models.IntegerField(choices=Status.choices, default=Status.WORKING)

    def __str__(self):
        return f"{self.get_type_display()} {self.number}"


class DeviceProblem(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    reported_by = models.ForeignKey(Person, on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    message = models.CharField(max_length=1024)
    fixed = models.BooleanField(default=False)


class DeviceProblemLogMessage(models.Model):
    device_problem = models.ForeignKey(DeviceProblem, on_delete=models.CASCADE)
    message = models.CharField(max_length=256)
    created_at = models.DateTimeField(auto_now_add=True)


class Booking(models.Model):
    device = models.ForeignKey(Device, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    room = models.CharField(max_length=10)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()


@receiver(post_save, sender=DeviceProblem)
def create_registration_fee(sender, instance: DeviceProblem, created, **kwargs):
    if created:
        mail.send(
            WorkingGroup.objects.get(name__icontains='wasch').email,
            config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
            template='laundry_room_device_problem',
            context={
                'problem': instance,
                'url': f"{BASE_URL}{reverse('admin:laundry_room_deviceproblem_change', args=[instance.id])}",
                'device_url': f"{BASE_URL}{reverse('admin:laundry_room_device_change', args=[instance.device_id])}",
            }
        )
        DeviceProblemLogMessage.objects.create(device_problem=instance, message="Reported by Tenant")
    if instance.fixed:
        DeviceProblemLogMessage.objects.create(device_problem=instance, message="Problem was marked as fixed")
