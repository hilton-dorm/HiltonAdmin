from datetime import timedelta

from django.contrib.auth.decorators import login_required
from django.db.models import Q, OuterRef, Subquery
from django.shortcuts import render
from django.utils import timezone
from rest_framework import mixins, viewsets
from rest_framework.exceptions import PermissionDenied
from rest_framework.permissions import IsAuthenticated
from rest_framework.viewsets import ModelViewSet

from laundry_room.models import Device, Booking, DeviceProblem, DeviceProblemLogMessage
from laundry_room.serializers import BookingSerializer, DeviceProblemSerializer


def get_time_range():
    start_time = (timezone.now() - timedelta(hours=2)).replace(minute=0, second=0, microsecond=0)
    end_time = (timezone.now() + timedelta(days=2)).replace(hour=0, minute=0, second=0, microsecond=0)
    return start_time, end_time


def get_bookings(start_time, end_time):
    # Subquery to find the latest booking before now
    latest_booking_subquery = Booking.objects.filter(
        Q(device=OuterRef('device')) &  # must be the same device
        (Q(end_time__lte=timezone.now()) | Q(start_time__lte=start_time)) &
        Q(start_time__gte=timezone.now() - timedelta(hours=8))  # No very old bookings
    ).order_by('-end_time')

    queryset = Booking.objects.filter(
        Q(end_time=Subquery(latest_booking_subquery.values('end_time')[:1])) |  # the booking is the latest before now
        Q(end_time__gt=start_time, start_time__lte=end_time)  # in range
    )
    return queryset


@login_required
def timetable(request):
    start_time, end_time = get_time_range()
    devices = Device.objects.all().order_by('number')
    problems = DeviceProblem.objects.filter(
        Q(fixed=False) | (Q(fixed=True) & Q(created_at__gt=timezone.now() - timedelta(weeks=2))))
    context = {
        'title': 'Laundry Room Timetable',
        'deviceModels': devices,
        'devices': list(devices.values()),
        'start_time': start_time,
        'end_time': end_time,
        'bookings': list(get_bookings(start_time, end_time).values()),
        'room': request.user.person.current_room(),
        'compact': True,
        'problems': list(problems.values("id", "device_id", "device__number", "message", "fixed")),
        'problemLogs': list(DeviceProblemLogMessage.objects.filter(device_problem__in=problems).values())
    }
    return render(request, 'laundry_room/laundry_room_view.html', context)


class BookingViewSet(ModelViewSet):
    serializer_class = BookingSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        return get_bookings(*get_time_range())

    def perform_create(self, serializer):
        serializer.save(person=self.request.user.person)

    def perform_update(self, serializer):
        serializer.save(person=self.request.user.person)

    def perform_destroy(self, instance):
        if instance.person != self.request.user.person:
            raise PermissionDenied("You do not have permission to delete this booking.")
        instance.delete()


class DeviceProblemViewSet(mixins.CreateModelMixin, viewsets.GenericViewSet):
    queryset = DeviceProblem.objects.all()
    serializer_class = DeviceProblemSerializer
    permission_classes = [IsAuthenticated]

    def perform_create(self, serializer):
        serializer.save(reported_by=self.request.user.person)
