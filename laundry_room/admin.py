import nested_admin
from django.contrib import admin

from laundry_room.models import Device, DeviceProblem, Booking, DeviceProblemLogMessage


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    ordering = ["number"]
    list_display = ["__str__", "status"]


class DeviceProblemLogMessageAdmin(nested_admin.NestedTabularInline):
    model = DeviceProblemLogMessage
    extra = 1
    readonly_fields = ('created_at',)


@admin.register(DeviceProblem)
class DeviceProblemAdmin(admin.ModelAdmin):
    inlines = [DeviceProblemLogMessageAdmin]
    list_filter = ["fixed"]
    list_display = ["device", "message", "created_at", "fixed"]


@admin.register(Booking)
class BookingAdmin(admin.ModelAdmin):
    save_as = True
    list_display = ["device", "person", "start_time", "end_time"]
    pass
