FROM python:3.11.2
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
RUN apt-get update -y
RUN apt-get install -y texlive
RUN apt-get install -y texlive-latex-extra
RUN apt-get install -y texlive-lang-german
RUN apt-get install -y libsasl2-dev python3 libldap2-dev libssl-dev
RUN apt-get install -y cron
RUN apt-get install -y libcups2-dev # for pycups
ADD ./requirements.txt /code/
RUN pip install -r requirements.txt
ADD ./crontab.txt /code/
RUN /usr/bin/crontab crontab.txt
ENV PYTHONWARNINGS default
