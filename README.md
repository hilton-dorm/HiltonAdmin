# Hilton Administration (HiltonAdmin)

**Inhaltsverzeichnis**
- [Projektübersicht](#projektübersicht)
   - [Understanding the Code](#understanding-the-code)
   - [Concepts behind using multiple apps](#concepts-behind-using-multiple-apps)
   - [The framework Django](#the-framework-django)
   - [Finanzen](#finanzen)
   - [The user and person model](#the-user-and-person-model)
   - [Social Service System](#social-service-system)
   - [Management commands (and Cronjobs)](#management-commands-(and-cronjobs))
- [Projekt zum Entwickeln aufsetzen](#projekt-zum-entwickeln-aufsetzen)
   - [Voraussetzungen](#voraussetzungen)
   - [Preparation](#preparation)
   - [Installation](#installation)
   - [The Dockerized Way](#the-dockerized-way)
   - [The Manual Way(nicht zu empfehlen)](#the-manual-way)
   - [Server zum Entwickeln starten](#server-zum-entwickeln-starten)
   - [Change a Model](#change-a-model)
- [Contributing](#contributing)
- [Anonymisierte Datenbank erstellen](#anonymisierte-datenbank-erstellen)
- [Deployment (Production)](#deployment-(production))


Die Verwaltungswebseite für das Studierendenwohnheim Turmstraße 1 (intern Hilton genannt) und dem Verein Turmstraße 1 e.V. Die Dokumentation ist in deutsch gehalten.

Sie dient als zentraler Punkt für die Verwaltung allmöglicher Daten und ist unter https://verwaltung.hilton.rwth-aachen.de deployed.
Die Webseite ist zweigeteilt:
- Der öffentliche Bereich ist von allen Bewohnern zugänglich und bietet allmögliche Informationen zum Haus. Bewohner können darüberhinaus ihre Daten selber verwalten (Persönliche Daten ändern, Mac Adressen verwalten, etc.)
- Der Admin-Bereich ist nur von den aktiven Bewohnern des Hauses zugänglich wobei man als aktiv gilt sobald man einer Arbeitsgemeinschaft zugehörig ist. Der Adminbereich bietet dementsprechend die Möglichkeit, nicht nur seine eigenen Daten zu ändern, sondern auch die der anderen. Was genau von welcher aktiven Person gesehen und bearbeitet werden darf ist über ein Berechtigungssystem geregelt.  

# Projektübersicht 
## Understanding the Code
Die Seite ist in Python 3 implementiert. Genutzt wird das Django Framework und ein paar libraries (siehe requirements.txt).
Um sich im Code zurecht zu finden ist es sehr empfehlenswert, sich erst ins [Django Framework](https://www.djangoproject.com/) einzulesen.


## Concepts behind using multiple apps
Apps in Django werden als "Packages für Django" gesehen, sprich Packages/Libs die man potenziell auch in anderen Anwendungen verwenden kann.
In der Datei `settings.py` ist auch ersichtlich, dass einige Apps von anderen Entwicklern verwendet werden. 

Wichtig hierbei ist allerdings, dass die eigens für die Verwaltung angelegten Apps einen anderen Zweck dienen. Diese können nicht wiederverwendet werden und sind auch nicht voneinander Isoliert. Ganz im Gegenteil, denn sie referenzieren und nutzen sich Gegenseitig. 

Der Grund warum es mehrere apps statt eine app "hilton" gibt ist einfach. 
So können Models mit den ViewControllern und Templates logisch gruppiert und dementsprechend logisch strukturiert im Projekt verwaltet werden.
Zb. nicht alle Models müssen so in eine einzige `models.py` gequetscht werden bzw. durch eine neue Dateistruktur ausgelagert werden.

So kamen folgende Apps zustande:

- `main`: Hier liegen alle Models die für das Wohnheim grundlegend sind. Beispiel: `Person` welches Informationen über die Person speichert welche nicht im Model `User` gespeichert werden.
- `association`: Alles was vom Haus unabhängig zum Verein gehört. Das sind Finanzen, Mitgliedschaften, Soziale Dienste und co.
- `working_groups`: Hier werden Mitgliedschaften in AGs und Schlüsselträger verwaltet
- `networking`: Also Sessions der Nutzer, MAC Adressen und co.
- Siehe [hier](#datenbankschema)


## The framework Django
Django verfolgt den Model-View-Controller Ansatz. Sprich es gibt immer mindestens 3 Dateien:

- `models.py`: Hier werden Django Models definiert. Es ist empfehlenswert die Models ausführlich zu implementieren, sprich viel Logik hier zu beschreiben statt im Controller.  
- `view.py`: Man kann die `view.py` Datei als Controller sehen. Hier werden die Funktionen ausgeführt welche bei einem URL Aufruf getriggert werden soll. 
Meistens wird in diesen Funktionen nötige Objekte geladen, und diese mit dem View Template dem Renderer übergeben. Funktionen sollten hier so klein wie möglich gehalten werden.
- `template/<appname>/templates/<viewname>.html`: Dies ist die eigentlich View-Datei im HTML Format. Hier wird die Template Engine [Jinja2](https://palletsprojects.com/p/jinja/) verwendet.
- `admin.py`: Die Adminseiten werden alle automatisch generiert, lassen sich aber anpassen (z.B. kann man eigene Suchfilter erstellen). Dies passiert in der `admin.py` Datei. Eine gute Seite ist übrigens https://books.agiliq.com/projects/django-admin-cookbook/en/latest/index.html

### Ideas behind the Models
TODO

## Finanzen 
Wie die Finanzen verwaltet werden wird [hier](docs/finances.md) sehr ausführlich erklärt. 

## The user and person model
Das User Modell kommt aus einer schon vorhandenen App, beinhaltet aber nicht alle Informationen die wir über eine Person speichern wollen. Aus diesem Grund haben wir ein Person Model, das in einer 1 zu 1 Beziehung zu einem User Objekt steht in welchem wir weitere Attributen die wir benötigen abspeichern.

## Telegram Bot
Telegram Bot handlers kann man in einer `telegrambot_handlers.py` spezifieren (einfach aktuellen code anschauen, z.B. unten in [`fitness_room/telegrambot_handlers.py`](fitness_room/telegrambot_handlers.py)).
Im Hintergrund wird die [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot) lib verwendet. 
Um den Bot lokal zu testen den [@BotFather](https://t.me/botfather) anschreiben und einen Bot erstellen und in der [config.py](config.py) `TELEGRAM_BOT_API_TOKEN` entsprechend anpassen.  
Um den Bot dann zu starten `python manage.py botpolling --username=MainBot` ausführen.
Wenn man Docker verwendet `docker exec -it verwaltung.hilton.rwth-aachen.de python manage.py botpolling --username=MainBot`.

### Telegram Bot / Fitness Raum
Mithilfe des Telegram Bot wird aktuell die Tür des Fitnessraums gesteuert. Dafür werden MQTT Befehle gesendet und empfangen.
Die Einstellungen für den Server passieren in der [config.py](config.py), für die Topics in [`fitness_room/telegrambot_handlers.py`](fitness_room/telegrambot_handlers.py) nachschauen (ca. Zeile 60). 


## Social Service System
Das Social Service System wurde in [diesem Antrag](https://git.rwth-aachen.de/hilton-dorm/HiltonAdmin/uploads/d448dae0be188555cef180ae6c44da8f/Antrag_zur_Senatssitzung.pdf) (Issue #86) spezifiziert.
Jedes Vereinsmitglied muss alle sechs Monate einen sozialen Dienst machen. Ist es nicht mehr ordentlich, läuft die Zeit nicht weiter. Macht man einen zweiten sozialen Dienst in einer Periode startet eine neue Periode. Ist man in einer AG aktiv, wird der AG Sprecher 1 Monat vor Ablauf der Periode gefragt, ob die Person aktiv war. Leute werden 1,5 Monate vor Ende der Periode erinnert, dass sie einen sozialen Dienst machen müssen (wenn sie keinen gemacht haben). Leute, die wahrscheinlich in einer AG aktiv werden (AGs wo `Member are active by defaul` gesetzt ist) werden erst 15 Tage vorher gewarnt. Zusätzlich kann eine Periode für alle um x Tage verlängert werden, dies wird in dem Feld `extra_days_everbody` gespeichert.  
Hat eine Person im letzten Zeitraum keinen sozialen Dienst gemacht, kann die Person kein neues Internet mehr kaufen. Macht die Person dann einen sozialen Dienst, gilt dieser für den letzten Zeitraum und die Person kann wieder Internet kaufen.  
Will eine Person einen sozialen Dienst machen, registriert sie sich zu einem (Es wird ein `Registration` Objekt erstellt). Wird diese Registration verifiziert, gilt dies als gemachter sozialer Dienst und folgendes passiert:
- Das `verified_at` Datum wird aktualisiert
- Wenn die Person Vereinsmitglied ist: (Wenn Personen sich zu einer Neueinzieherbar, ein sozialer Dienst, registrieren, sind sie z.B. noch kein Vereinsmitglied)
    - Wenn in der letzten Periode kein sozialer Dienst:
        - Registrierung letzter Periode zuordnen
    - sonst:
        - Registrierung aktueller Periode zuordnen

#### Erstellung neuer Perioden 
Um neue Perioden zu erstellen läuft durch Cron immer um Mitternach der Command `create_new_social_service_periods`.  
Dieser prüft als erstes ob für ein Mitglied verifizierte Registrierungen existieren, die keiner Periode zugewiesen sind (z.B. eine teilgenmmene Neueinzieherbar). Diese werden dann der aktuellen Periode der Person zugeordnet.  
Weiter wird geprüft ob eine Person einen zweiten sozialen Dienst innerhalb einer Periode absolviert hat. Dann wird eine neue Periode erstellt und der neuste soziale Dienst dieser zugewiesen. Das Startdatum der Periode ist der verification Datum dieses sozialen Dienstes.  
Ist die Dauer der Periode ((aktuelles Datum - Startdatum der Periode) - Tage die die Person ruhend war) größer als 182 Tage (6 Monate) + `extra_days_everbody`, wird eine neue Periode angelegt.

#### AG Mitgliedschaft als sozialer Dienst
Ist man in einer AG, zählt dies möglicherweise als sozialer Dienst. Dies wird im `WorkingGroup` Objekt gespeichert: Gilt 1., 2. Sprecher sein als sozialer Dienst, können Mitglieder aktiv sein, sollen Mitglieder by default als aktiv gelten.  
Es wurde für jede AG ein sozialer Dienst _In einer AG aktiv sein (`AG Name`)_ erstellt. Ist ein Mitglied während seiner Periode mindestens 60 Tage in einer AG, so wird er für den entsprechenden sozialen Dienst registriert, wenn er ein ordentlichen Mitglied ist und seine aktuelle Periode in weniger als 30 Tagen endet (und wenn in den letzten 120 Tagen nicht schon eine Registrierung erstellt wurde, sodass nicht beim jedem durchlauf eine neue Registriung erstellt wird). Der AG Sprecher bekommt dann eine Email in welcher er entscheiden muss welche Person aktiv war.  
Der Command (`create_social_services_for_working_group_memberships`) läuft am 1. und 15. jeden Monats.

#### Erinnerung an soziale Dienste
Alle Mitglieder die einen sozialen Dienst machen müssen bekommen eine Erinnerung, wenn sie noch keinen sozialen Dienst in der aktullen Periode gemacht haben und diese in weniger als 60 Tagen endet. Mitglieder die in einer AG sind und wahrscheinlich aktiv sind bekommen die Mail erst wenn ihre Periode in weniger als 15 Tagen endet.  
Der Command (`send_social_service_reminder`) läuft alle 20 Tage.

### Neueinzieherbar
Neueinzieherbars sind soziale Dienste bei denen das `is_new_tenant_bar` flag gesetzt ist. Um zu bestimmen wie oft jemand die Neueinzieherbar verpasst hat, wird einfach gezählt wie viele soziale Dienste mit `is_new_tenant_bar` stattgefunden haben seitdem die Person das erste mal eingezogen ist.

## Management commands (and Cronjobs)
In den Ordnern `<appname>/managment/commands` liegen verschiedene Python Dateien, die jeweils einen Befehl implementieren. Diese Befehle werden oft von cronjobs regelmäßig aufgerufen. Z.b gibt es den Command `sync_gitlab_issues_with_social_service` in der app `association` der für Issues im GitLab soziale Dienste in der Verwaltung erstellt. Dieser Befehlt kann in der Kommandozeile über `python manage.py sync_gitlab_issues_with_social_service` ausgeführt werden, genauso wie alle anderen Befehle auch. 


# Projekt zum Entwickeln aufsetzen

### Voraussetzungen
Fürs Entwickeln in einem lokalen Deployment ist Docker sehr zu empfehlen. Neben Docker ist auch docker-compose vonnöten. Auf der Docker Webseite befinden sich Installationsanleitungen für jedes Betriebsystem. 
Cronjobs werden im Docker Container noch nicht ausgeführt.

Falls Docker nicht verwendet werden kann, muss folgendes beachtet werden:
- Derzeit wird nur Python >= 3.7 unterstützt

### Preparation

Eine SQLite Datenbank und die config.py Datei werden benötigt. Diese findet man [hier](https://cloud.hilton.rwth-aachen.de/apps/files/files/3288?dir=/Netzwerk-AG/git%20Projektdateien/Verwaltung).
Diese Dateien müssen im Projekt root Verzeichnis abgelegt werden.  
Der `game_rental` Order muss heruntergeladen und nach `uploads/` kopiert werden.

### Installation

1. Install [PyCharm **Professional**](https://www.jetbrains.com/pycharm/download/) (Nicht die Community Edition! Die kann keine Django Autovervollständigungen. Die Professional Edition [bekommt man als Student kostenlos](https://www.jetbrains.com/shop/eform/students))
2. Install git: git-scm.com
3. Install python 3
4. Wenn ssh-keys in GitLab noch nicht eingerichtet sind: 
   - Besuche git.rwth-aachen.de/profile/keys und füge SSH-Schlüssel hinzu. Auf der Seite ist eine Erklärung verlinkt.
5. Open a Terminal and clone the project: `git clone git@git.rwth-aachen.de:hilton-dorm/HiltonAdmin.git`
6. Open it in PyCharm
8. File->Settings->Project:HiltonAdmin->ProjectInterpreter: Add a new Interpreter with a new environment (inherit global site-packages). Don't choose the project directory or exclude it from git. We don't want your stuff there.
9. Install python packaging tools (you should be prompted somewhere)
10. Apply, wait and return
12. Install the packages that are listed in the requirements.txt

Weiter geht es mit [Server zum Entwickeln starten](#server-zum-entwickeln-starten)

### The Dockerized Way

1. `./deploy.sh`

Dies startet folgende Docker Container:
- verwaltung.hilton.rwth-aachen.de unter localhost:8000 
- sqlite-web unter localhost:8080 (Sqlite Browser)
- mailhog.hilton.rwth-aachen.de (Test Mailhooker)  unter localhost:8025
- ldap.hilton.rwth-aachen.de
- phpldapadmin unter localhost:8081

Diese Dienste sind in einem eigenen Netzwerk deployed und können sich gegenseitig unter deren Hostnames ansprechen. 
Genauere Details sind der `Dockerfile` und `docker-compose.yaml` zu entnehmen.



### The Manual Way

Ohne Docker wird mit den folgenden Schritten ausschließlich die Webseite deployed. Die anderen Dienste müssen bei Bedarf eigenständig installiert und eingerichtet werden.

<details>
<summary>Manuelle Anleitung</summary>
12. Install the packages that are listed in the requirements2.txt
15. Check if you are able to run it `python manage.py runserver`

16. Stop it and migrate the database: `python manage.py createsuperuser`, then `python manage.py migrate`

17. Create a nice superadmin. This account will be local and should still work if ldap fails. `python manage.py createsuperuser`

If you want to setup an email development server: [Installation](docs/email_development.md)

Falls etwas schief läuft können auch die Schritte der `Dockerfile` befolgt werden (Linux only). Bitte bedenken, dass wenn PDF Dokumente erzeugt werden sollen auch Latex installiert sein muss. Die Dockerfile gibt folgende Schritte vor:
```
apt-get update -y
apt-get install -y texlive
apt-get install -y texlive-latex-extra
apt-get install -y texlive-lang-german
apt-get install -y libsasl2-dev python-dev libldap2-dev libssl-dev
apt-get install -y cron
pip install -r requirements.txt
pip install -r requirements-2.txt
```

</details>

## Server zum Entwickeln starten
1. Docker starten wenn es noch nicht läuft  
2. Im PyCharm einen Terminal öffnen (wenn noch nicht zu sehen unter _View_ -> _Tool Windows_ -> _Terminal_)
3. Im Terminal `./deploy.sh` ausführen. Dies startet verschiedene Dienste und man kann unter localhost:8000 die Webseite öffnen 
4. Man kann sich nun mit dem Username/Passwort `admin`/`admin` anmelden

Möchte man _Commands_ ausführen öffnet man einen zweiten Termianal Tab und führt dort `docker exec verwaltung.hilton.rwth-aachen.de python manage.py <command_name>` aus.
Der Command-Name lautet so wie die Datei heißt in welchem der Befehl liegt.
Die Befehle liegen in den `xxx/managment/commands` Ordnern. 

Fordert ein Command die Interaktion des Nutzers, so muss man den Docker Befehl mit dem Parameter `-it` aufrufen, also z.B. `docker exec -it verwaltung.hilton.rwth-aachen.de python manage.py changepassword <username>`

### Change a Model
Hat man ein Modell geändert (in einer der `model.py` Dateien), so muss man den Befehl `docker exec verwaltung.hilton.rwth-aachen.de python manage.py makemigrations` in einem Terminal Tab ausführen.
Das `deploy.sh` Skript muss währenddessen noch laufen.


## Contributing

Kleinere Änderungen/Fixes werden vom Maintainer direkt auf den master branch committed. Alle anderen erstellen für ihren commits einen
neuen branch und erstellen dann ein Merge Request. Größere Features werden auf feature branches entwickelt.   
Der Masterbrach ist der live-Branch. Dieser wird in regelmäßigen Abständen von der Verwaltung gepullt, sodass das Echtsystem immer auf dem neusten Stand ist.

# Datenbankschema

![Datenbankschema](schema.svg)

#### Generate the image
1. `pip install django-extensions`
2. Add `django-extensions` to the list of `INSTALLED_APPS` in `hiltonadmin/settings.py`
3. Run `python manage.py graph_models -g --arrow-shape diamond -X 'LogEntry,ContentType,AbstractUser,PermissionsMixin,Ldap*,Model' $(find . -maxdepth 1 -type d -exec test -e '{}/models.py' \; -print | grep -v './finances' | sed 's|^\./||') auth --rankdir=RL > schema.dot`
4. Run `dot -Tsvg schema.dot > schema.svg && rm schema.dot` 
5. Commit the changed `schema.dot` file and discord the other changes (`git restore hiltonadmin/settings.py`).

## Anonymisierte Datenbank erstellen 
Zum Entwickeln ist es am einfachen, wenn man mit echtdaten arbeiteten kann. Da das aber nicht so ganz Datenschutzkonform ist hier eine kurze Anleitung um eine anonymisierte Datenbank zu erstellen. 

1. SSH auf den Datenbank/MariaDB Container und `mysqldump -p --skip-extended-insert verwaltung > verwaltung.sql` ausführen (Passwort für mariadb root user in passbolt). Siehe [hier](https://github.com/dumblob/mysql2sqlite#usage).
2. Die Datei auf den lokalen Computer kopieren (scp)
3. `./docs/mysql2sqlite verwaltung.sql | sqlite3 db.sqlite` ausführen (Die Datei `db.sqlite` darf nicht existieren)
4. Die Datenbank mit sqlite öffnen (`sqlite3 db.sqlite`)
5. Die folgenden Befehle ausführen: (Stand 10.2020)
```
update auth_user set first_name= "First name " || id;
update auth_user set last_name= "Last name " || id;
update auth_user set email= id || "@test.de";
update auth_user set username= "user" || id;
update auth_user set is_superuser = 1;
update auth_user set password= 'bcrypt_sha256$$2b$12$lzoP5W9yLXSKWFXMBQNKYeqaHyuHP6VJrNtLE0egFDq6gVDRwnzNe';
update auth_user set username= "admin" where id = 234;
update main_person set stw_id = -1;

update main_person set birthday  = date(strftime('%s', '1995-01-01 00:00:00') + abs(random() % (strftime('%s', '2010-01-31 23:59:59') -  strftime('%s', '2000-01-0100:00:00'))    ), 'unixepoch');
update main_person set gender = 'Male' where id % 2 = 0;
update main_person set gender = 'Female' where id % 2 = 1;
update main_person set gender = 'Divers' where id % 10 = 0;
update main_person set gender = 'Unknown' where id % 20 = 0;
delete from post_office_email;
update main_room set number = SUBSTR('0000' || number, -4, 4) where eth_port0 != -1;
delete from auditlog_logentry;
update networking_mac set device_name = "Device " || id;
update networking_mac set mac = "00-00-00-00-" || substr("0000" || id, -4, 2) || "-" || substr("00" || id, -2);
update networking_specialmac set mac = "AA-00-00-00-" || substr("0000" || id, -4, 2) || "-" || substr("00" || id, -2);
delete from django_admin_log;
delete from django_session;
delete from post_office_log;
delete from networking_radiusmacautherror;
```
6. Man kann sich nun mit dem Username/Passwort `admin`/`admin` anmelden



# Deployment (Production)

Um das Project im Production-Modus zu starten werden folgende Dinge geändert:
Statt einer Sqlite Datenbank wird eine MariaDB Datenbank genutzt. 

Änderungen in `config.py` wie folgt:
```
DEBUG = False
SECRET_KEY = '...'

LDAP = {
    'BASE_DN': 'dc=hilton,dc=rwth-aachen,dc=de',
    'NAME': 'ldap://192.168.0.53',
    'USER': 'cn=admin,dc=hilton,dc=rwth-aachen,dc=de',
    'PASSWORD': '...',

    'ORGANISATION_PEOPLE': 'ou=people',

    'GROUP_ADMINS': 'cn=admins,ou=groups',
    'GROUP_TENANTS': 'cn=tenants,ou=groups',
    'GROUP_STAFF': 'cn=staff,ou=groups',
    'GROUP_NETWORK': 'cn=network,ou=groups',
}

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'verwaltung',
        'USER': 'verwaltung',
        'PASSWORD': '...',
        'HOST': '192.168.0.52',
        'PORT': '',
        'OPTIONS': {
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'",
        },
    },
    'ldap': {
        'ENGINE': 'ldapdb.backends.ldap',
        'NAME': LDAP["NAME"],
        'USER': LDAP["USER"],
        'PASSWORD': LDAP["PASSWORD"],
     }
}

STW = {
    'key': '...'
}

ALLOWED_HOSTS = ['symposion.hilton.rwth-aachen.de', 'verwaltung.hilton.rwth-aachen.de', '192.168.0.59']

STATIC_ROOT = '/var/www/verwaltung/static'

EMAIL = {
    'EMAIL_HOST': '134.130.55.254',
    'EMAIL_PORT': 25,
    'EMAIL_HOST_USER': '',
    'EMAIL_HOST_PASSWORD': '',
    'EMAIL_USE_TLS': True,
    'EMAIL_ACCOUNT_NOREPLY': 'noreply@hilton.rwth-aachen.de',
    'EMAIL_ACCOUNT_ADMIN': 'admin@hilton.rwth-aachen.de',
    'EMAIL_ACCOUNT_NETWORK': 'network@hilton.rwth-aachen.de',
    'EMAIL_ACCOUNT_BA': 'ba@hilton.rwth-aachen.de',
    'EMAIL_ACCOUNT_HOUSE': 'haussprecher@hilton.rwth-aachen.de'
}

SYMPOSION_ORGA = {
    'username': 'VerwaltungsWebsite',
    'password': '...'
}

```

