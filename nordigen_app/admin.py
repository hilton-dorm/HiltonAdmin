from django.contrib import admin, messages
from django.contrib.admin import ModelAdmin, display
from django.core.management import call_command
from django.db.models import QuerySet
from django.utils.html import format_html

from common.util.start_new_thread import start_new_thread
from nordigen_app.models import NordigenCredential, NordigenRequest


def sync_now(self, request, queryset: QuerySet):
    @start_new_thread
    def async_call():
        call_command("import_known_transactions", credentials_ids=queryset.values_list("id", flat=True), fetch_now=True)

    async_call()
    messages.info(request, "Import started in background")


sync_now.short_description = "Import known transactions now"


@admin.register(NordigenCredential)
class CredentialsAdmin(ModelAdmin):
    list_display = ("account", "email", "status", "last_transaction_fetch", "action")
    fields = ("account", "email", "nordigen_institution_id")
    actions = [sync_now]

    @display()
    def status(self, obj: NordigenCredential):
        return obj.status_as_text()

    @display()
    def action(self, obj):
        return format_html('<a href="/nordigen/requisition-status/">Refresh here</a>')


@admin.register(NordigenRequest)
class RequestAdmin(ModelAdmin):
    class Meta:
        verbose_name = 'Nordigen Request'
        verbose_name_plural = 'Nordigen Requests'

    list_display = ("account", "timestamp", "request", "short_response", "success")
    search_fields = ("request", "response")
    list_filter = ("request", "error")

    @staticmethod
    def account(obj: NordigenRequest):
        return obj.credentials.account.title if obj.credentials else None

    @display(boolean=True, ordering="error")
    def success(self, obj: NordigenRequest):
        return not obj.error

    @display(ordering="response")
    def short_response(self, obj: NordigenRequest):
        if len(obj.response) > 75:
            return obj.response[:75] + "..."

        return obj.response

    @staticmethod
    def has_add_permission(request, obj=None):
        return False

    def has_change_permission(self, request, obj=None):
        return False

    def has_delete_permission(self, request, obj=None):
        return False
