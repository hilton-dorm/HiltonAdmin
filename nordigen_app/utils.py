def is_nordigen_external_identifier(identifier: str) -> bool:
    return identifier and len(identifier) == 32 and all(
        [c.isascii() and c.isalnum() for c in identifier])
