import datetime

from decimal import Decimal


class Transaction:

    def __init__(self, internal_id, debtor_name, remittance_information, amount, date, book_date,
                 currency):
        self.internal_id = internal_id
        self.debtor_name = debtor_name
        self.remittance_information = remittance_information
        self.amount = amount
        self.date = date
        self.book_date = book_date
        self.currency = currency

    @staticmethod
    def from_dict(d: dict):
        internal_id = d['internalTransactionId']
        amount = Decimal(d['transactionAmount']['amount'])
        date = datetime.date.fromisoformat(d['valueDate'])
        book_date = datetime.date.fromisoformat(d['bookingDate'])
        currency = d['transactionAmount']['currency']

        remittance_information = d.get('remittanceInformationUnstructured')
        if not remittance_information:
            remittance_information = d.get('remittanceInformationStructured')
        if not remittance_information:
            remittance_information = ' '.join(d.get('remittanceInformationStructuredArray', []))
        if not remittance_information:
            remittance_information = ''
            print("Remittance Information not provided.")

        debtor_name = ''
        try:
            debtor_name = d['debtorName']
        except KeyError:
            print("Debtor name not provided.")

        return Transaction(internal_id, debtor_name, remittance_information, amount,
                           date, book_date,
                           currency)

    def __str__(self):
        return f"Transaction(ID: {self.internal_id}, DebtorName: {self.debtor_name}, RemittanceInformation: {self.remittance_information}, Amount: {self.amount}, Currency: {self.currency}, ValueDate: {self.date})"

    def __repr__(self):
        return str(self)
