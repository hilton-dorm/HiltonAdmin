import datetime
from datetime import timedelta

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.core.management import call_command
from django.db.models import Q
from django.http import HttpResponseNotAllowed, HttpResponse, HttpResponseServerError, JsonResponse
from django.shortcuts import redirect
from django.template import loader
from django.utils import timezone

from association.models import Membership
from finances_v2.models import Accounts
from .models import NordigenCredential
from .api_requests import NordigenAPI


def nordigen_redirect_endpoint(request):
    if request.method != 'GET':
        return HttpResponseNotAllowed(['GET'])

    reference_id = request.GET.get('ref')

    def save_credentials() -> str | None:
        credentials = NordigenCredential.objects.filter(id=request.GET["id"]).first()
        if credentials is None:
            return 'No Nordigen credentials found'

        error = request.GET.get('error')
        if error is not None:
            return f"{error}: {request.GET.get('details')}"

        # Check if verification process IDs match
        if credentials.verification_reference == reference_id:
            # Fetch requisition to get accessible accounts
            client = NordigenAPI(credentials)
            requisition = client.fetch_requisition(credentials.requisition_id)

            if requisition is None:
                return 'Requisition fetching failed.'

            # Check if requisition state is linked
            if requisition['status'] != 'LN':
                return 'Status is not linked after user verification.'

            if requisition['accounts'] is None or len(requisition['accounts']) == 0:
                return 'No accounts linked to the requisition.'

            current_time = timezone.now()
            credentials.requisition_expiry = current_time + datetime.timedelta(days=89)

            # Save first account (we only have one at the moment)
            credentials.nordigen_account_id = requisition["accounts"][0]

            credentials.save()
        else:
            return "Reference id does not match last saved request."
    request.session['error'] = save_credentials()
    return redirect("/nordigen/requisition-status")


@login_required
def requisition_status(request):
    has_access = request.user.person.workinggroup_set.filter(Q(name__icontains="Vorstand") | Q(name__icontains="Kassenwarte")).exists()

    if not has_access:
        return HttpResponse(status=403, content="You are not in the Vorstand or Kassenwarte group")

    if request.method == 'GET':
        message = request.session.pop('error', None)
        if message:
            messages.error(request, message)
        template = loader.get_template('requisition_status.html')
        context = {
            'items': NordigenCredential.objects.all()
        }
        return HttpResponse(template.render(context, request))
    elif request.method == 'POST':
        api = NordigenAPI(NordigenCredential.objects.get(id=request.POST["id"]))
        link = api.create_requisition()

        if link is not None:
            return redirect(link)

        return HttpResponseServerError(content="Requisition creation failed")

    return HttpResponseNotAllowed(['GET', 'POST'])


@login_required
def transaction_fetch(request):
    if request.method == 'POST':
        try:
            last_update = NordigenCredential.objects.get(account=Accounts().NETWORK_BANK).last_transaction_fetch
        except NordigenCredential.DoesNotExist:
            last_update = datetime.datetime.fromtimestamp(0)
        if last_update > timezone.now() - timedelta(minutes=7):
            return HttpResponse(status=429, content=f"You have to wait until {last_update + timedelta(minutes=7)} to fetch transactions.")
        tx_before_count = request.user.person.transactionentry_set.all().count()
        call_command('fetch_nordigen_transactions')
        tx_after_count = request.user.person.transactionentry_set.all().count()
        m: Membership = Membership.get_current_membership_for(request.user.person)
        m.create_invoice_house_current_month()
        m.create_invoice_network()

        data = {"time": datetime.datetime.now().strftime("%d.%m.%Y - %H:%M"), "userChange": tx_before_count != tx_after_count}
        return JsonResponse(status=200, data=data)

    return HttpResponseNotAllowed(['POST'])
