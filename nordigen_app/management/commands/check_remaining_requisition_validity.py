from datetime import timedelta

from django.core.management import BaseCommand
from django.utils import timezone
from post_office import mail

import config
from nordigen_app.models import NordigenCredential


class Command(BaseCommand):
    help = 'Check remaining requisition validity'

    def handle(self, *args, **options):
        for credentials in NordigenCredential.objects.all():
            now = timezone.now()
            if now + timedelta(days=15) > credentials.requisition_expiry:
                delta_days = (credentials.requisition_expiry - now).days

                if delta_days > 7 and delta_days != 14:
                    return

                if delta_days > 0:
                    message = f"Die Requisition läuft in {delta_days} Tagen ab. Bitte unter {config.BASE_URL}/nordigen/requisition-status/ erneuern."
                else:
                    message = f"Die Requisition ist abgelaufen. Bitte unter {config.BASE_URL}/nordigen/requisition-status/ erneuern. Alternativ müssen die Transaktionen über die CSV-Schnittstelle hochgeladen werden."

                mail.send(
                    credentials.email,
                    config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                    subject='Problem mit der Nordigen Requisition',
                    message=message
                )
