from datetime import timedelta

from django.core.management.base import BaseCommand
from django.urls import reverse
from django.utils import timezone
from post_office import mail

import config
from config import BASE_URL
from finances_v2.models import TransactionEntry
from nordigen_app.NordigenTransaction import Transaction as NordTransaction
from nordigen_app.api_requests import NordigenAPI
from nordigen_app.models import NordigenCredential
from nordigen_app.utils import is_nordigen_external_identifier


def get_url(transaction_id):
    return BASE_URL + reverse("admin:finances_v2_transaction_change", args=[transaction_id])


class Command(BaseCommand):
    help = 'Validates External Nordigen Ids'

    def handle(self, *args, **options):
        for credentials in NordigenCredential.objects.all():
            booked = NordigenAPI(credentials).fetch_cached_transactions()
            if booked is None:
                continue
            transactions = [NordTransaction.from_dict(d) for d in booked if NordTransaction.from_dict(d) is not None]

            valid_external_identifiers = {}
            oldest_date = timezone.now().date()
            for t in transactions:
                valid_external_identifiers[t.internal_id] = t
                oldest_date = min(oldest_date, t.date, t.book_date)
            oldest_date += timedelta(days=1)  # Sometimes we don't get all transaction of the oldest date

            for trans in TransactionEntry.objects.filter(account=credentials.account, transaction__date__gte=oldest_date):
                if is_nordigen_external_identifier(trans.external_identifier):
                    if trans.external_identifier not in valid_external_identifiers:
                        mail.send(
                            credentials.email,
                            config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                            subject='External Id ungültig!',
                            html_message=f'Die Transaktion <a href="{get_url(trans.transaction_id)}">#{trans.id}</a> hat einen external identifier (<code>{trans.external_identifier}</code>) den es nicht mehr gibt!<br>'
                                         f'In der Vergangenheit hat die Nordigen API der selben Transaktion bei der Bank zu unterschiedlichen Zeiten verschiedene IDs gegeben, weswegen wir sie dann mehrfach importiert haben (für jede ID ein mal).<br>'
                                         f'Das heißt, dass die Transaktion wahrscheinlich doppelt importiert wurde und gelöscht werden muss. Bitte manuell prüfen ob die bei der Bank nur einmal und bei uns im System zwei mal vorkommt!<br>'
                                         f'Transaktionen an dem Tag findet man hier<a href="{BASE_URL}/admin/finances_v2/transactionentry/?account__id__exact={trans.account_id}&transaction__date__range__gte={trans.transaction.date}&transaction__date__range__lte={trans.transaction.date}"></a>',
                        )
                    else:
                        nordigen_amount = valid_external_identifiers[trans.external_identifier].amount
                        if trans.amount != nordigen_amount:
                            mail.send(
                                credentials.email,
                                config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                                subject='Betrag der Transaktion stimmt nicht',
                                message=f"Die Transaktion {get_url(trans.transaction_id)} mit dem external identifier {trans.external_identifier} hat laut Nordigen einen Betrag von {nordigen_amount}€, laut der Verwaltung aber einen von {trans.amount}. Bitte manuell prüfen! Die Transaktion muss eventuell gelöscht und neu angelegt werden. Dafür kann der automatische import genutzt werden.",
                            )
                        del valid_external_identifiers[trans.external_identifier]

                else:
                    mail.send(
                        credentials.email,
                        config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                        subject='Fehlende External Id',
                        message=f"Die Transaktion {get_url(trans.transaction_id)}, bzw der Eintrag mit dem Account {trans.account} hat keine external id, bzw keine validen external identifier von Nordigen",
                    )
