import time
from urllib.parse import urlencode

from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone

from post_office import mail
from requests import ReadTimeout

import config
import string
from config import BASE_URL
from finances.utils import match_tx_reference, match_tx_person
from finances_v2.models import Accounts, TransactionEntry, Transaction
from nordigen_app.NordigenTransaction import Transaction as NordTransaction
from nordigen_app.api_requests import NordigenAPI
from nordigen_app.models import NordigenCredential


class Command(BaseCommand):
    help = 'Fetch Nordigen transactions'

    @staticmethod
    def convert(nordigen_transaction: NordTransaction, email):
        tx_ref = nordigen_transaction.remittance_information
        tx_amount = nordigen_transaction.amount
        tx_person = nordigen_transaction.debtor_name
        tx_date = nordigen_transaction.date

        if TransactionEntry.objects.filter(external_identifier=nordigen_transaction.internal_id).exists():
            return

        user = match_tx_reference(tx_ref)
        comment = ""
        if not user:
            if tx_amount < 0:
                return  # Muss positiv sein
            if tx_amount % 6 != 0 and (tx_amount - 10) % 6 != 0 and float(tx_amount) % 6.5 != 0 and float(tx_amount - 10) % 6.5 != 0 and tx_amount % 10 != 0:
                return  # Muss ein Vielfaches von 6 oder von 10 (Anmeldegebühr) + Vielfaches von 6 sein (oder 6,5) oder "gerade"/vielfaches von 10 €
            if tx_amount > 90:
                return  # Bis jetzt hat noch nie jemand mehr als 90€ überwiesen
            user = match_tx_person(tx_person)
            if user:
                comment = f"Wrong reference code. Only use '{user.username}' as purpose of use."
        if user:
            base_query = Accounts().NETWORK_BANK.entries.filter(person=user.person,
                                                                transaction__date=tx_date,
                                                                amount=tx_amount,
                                                                external_identifier__isnull=True)
            count = base_query.count()
            if count == 0:
                tx = Transaction.create(person=user.person, date=tx_date, entries=[
                    TransactionEntry(amount=tx_amount, account=Accounts().NETWORK_FK, note=comment),
                    TransactionEntry(amount=tx_amount, account=Accounts().NETWORK_BANK, external_identifier=nordigen_transaction.internal_id)])
                if user.person.active_workinggroup_memberships().exists():
                    params = {
                        "date": tx_date,
                        "note": tx_ref,
                        "transactionentry_set-0=account": Accounts().NETWORK_BANK.id,
                        "transactionentry_set-0=amount": tx_amount,
                        "transactionentry_set-0=external_identifier": nordigen_transaction.internal_id,
                        "transactionentry_set-1=account": Accounts().NETWORK_EK.id,
                        "transactionentry_set-1=amount": tx_amount,
                    }
                    t = nordigen_transaction
                    if len(comment) > 0:
                        mail.send(email, config.EMAIL['EMAIL_ACCOUNT_NOREPLY'], subject="Transaktion verdächtig",
                                  html_message=f"""Hi, <br><br>
                                  die folgende Transaktion wurde automatisch {user.get_full_name()} als Guthaben gutgeschrieben. <strong>Falls dies ein Fehler war, muss diese Transaktion gelöscht und neu erstellt werden!</strong><br>
                                  - <code>{t.date} — {t.amount} {t.currency} {t.remittance_information} </code> from {string.capwords(t.debtor_name)}. <a style="color: red;" href="{BASE_URL}/admin/finances_v2/transaction/{tx.id}">Delete</a> <a href="{BASE_URL}/admin/finances_v2/transaction/add/?{urlencode(params, safe="=")}">Create New</a><br>
                                  War dies kein Fehler oder die Transaktion wurde neu erstellt, kann diese Mail gelöscht werden.<br>
                                  <br>
                                  Viele Grüße<br>
                                  Die Verwaltung""")

            elif count == 1:
                t = base_query.first()
                t.external_identifier = nordigen_transaction.internal_id
                t.save()
            elif count > 1:
                # Falls eine Person wirklich zweimal am Tag die gleiche Summer überwiesen hat, wird das manuell eingetragen
                transactions = base_query.exclude(transaction__note__icontains="Doppelte Buchung")

                t = transactions.first()
                t.external_identifier = nordigen_transaction.internal_id
                t.save()
        else:
            mail.send(
                email,
                config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                subject='Automatische Transaktionserstellung fehlgeschlagen. Manuelle Anlegung erforderlich',
                message=f"Bitte die Transaktion mit Verwendungszweck {tx_ref} manuell in der Verwaltung anlegen und dafür die externe ID {nordigen_transaction.internal_id} benutzen.\nEs handelt sich um die folgende Transaktion: {nordigen_transaction}.",
            )

    def handle(self, *args, **options):
        e = None
        for i in range(3):
            try:
                credentials = NordigenCredential.objects.get(account=Accounts().NETWORK_BANK)
                booked = NordigenAPI(credentials).fetch_booked_transactions()
                transactions = [NordTransaction.from_dict(d) for d in booked if NordTransaction.from_dict(d) is not None]

                for t in transactions:
                    self.convert(t, credentials.email)

                credentials.last_transaction_fetch = timezone.now()
                credentials.save()
                return
            except ReadTimeout as ex:
                e = ex
                time.sleep(i * 20)
                pass
        raise CommandError(f"Read timeout {e}. No data imported.")
