import json
import string
import urllib.parse
from datetime import timedelta

from django.core.management.base import BaseCommand
from django.db.models import Q
from django.utils import timezone
from post_office import mail

import config
from config import BASE_URL
from finances_v2.models import Accounts, TransactionEntry, Transaction, TransactionTypes, Account
from nordigen_app.NordigenTransaction import Transaction as NordTransaction
from nordigen_app.api_requests import NordigenAPI
from nordigen_app.models import NordigenCredential


class Command(BaseCommand):
    help = 'Imports known transactions via Nordigen'

    @staticmethod
    def transaction_exists(nordigen_transaction: NordTransaction, bank_account: Account):
        if TransactionEntry.objects.filter(external_identifier=nordigen_transaction.internal_id).exists():
            return True
        date_filter = Q()
        for offset in [-1, 0]:
            for date in [nordigen_transaction.book_date, nordigen_transaction.date]:
                date_filter |= Q(transaction__date=date + timedelta(days=offset))
        existing_transaction = bank_account.entries.filter(date_filter, amount=nordigen_transaction.amount, person__isnull=True).first()
        if existing_transaction:
            existing_transaction.external_identifier = nordigen_transaction.internal_id
            existing_transaction.save()
            return True
        return False

    @staticmethod
    def import_known_transaction(nordigen_transaction: NordTransaction, bank_account, ek_account):
        if nordigen_transaction.amount >= 0:
            return

        if not Command.transaction_exists(nordigen_transaction, bank_account):
            info = nordigen_transaction.remittance_information.lower()
            if ("abschluss per " in info and not "iban" in info) or ("abrechnung" in info and "siehe anlage" in info):
                return Transaction.create(entries=[TransactionEntry(amount=nordigen_transaction.amount, account=bank_account, external_identifier=nordigen_transaction.internal_id),
                                                   TransactionEntry(amount=nordigen_transaction.amount, account=ek_account, type=TransactionTypes().BANK_FEE)],
                                          date=nordigen_transaction.date,
                                          note=nordigen_transaction.remittance_information)

    def add_arguments(self, parser):
        parser.add_argument('-ids', '--credentials-ids', metavar="credentials_id", type=int, nargs="+", help='Only sync specific Bank Accounts.', )
        parser.add_argument('--fetch-now', action='store_true', help='Fetch nordigen data immediately without using old cached data.')

    def handle(self, *args, **options):
        credentials_list = NordigenCredential.objects.all()
        credentials_ids = options["credentials_ids"]
        if credentials_ids is not None:
            credentials_list = credentials_list.filter(id__in=credentials_ids)
        for credentials in credentials_list:
            api = NordigenAPI(credentials)
            booked = api.fetch_booked_transactions() if options['fetch_now'] else api.fetch_cached_transactions()
            transactions = [NordTransaction.from_dict(d) for d in booked]
            created_transactions = []
            create_transaction_links = []

            account_ek = Account.objects.get(working_group=credentials.account.working_group, type=Account.TYPE_EQUITY)

            for t in transactions:
                created = self.import_known_transaction(t, bank_account=credentials.account, ek_account=account_ek)
                if created:
                    created_transactions.append(
                        f'<li><a href="{config.BASE_URL}/admin/finances_v2/transaction/{created.id}"><code>{created.date}: {created.note}</code></a></li>')
                elif not self.transaction_exists(t, credentials.account):
                    params = {
                        "date": t.date,
                        "note": t.remittance_information,
                        "transactionentry_set-0=account": credentials.account.id,
                        "transactionentry_set-0=amount": t.amount,
                        "transactionentry_set-0=external_identifier": t.internal_id,
                        "transactionentry_set-1=account": account_ek.id,
                        "transactionentry_set-1=amount": t.amount,
                    }
                    create_transaction_links.append(
                        f'<li><code>{t.date}</code>: <code>{t.amount} {t.currency} {t.remittance_information} </code> from {string.capwords(t.debtor_name)}. <a href="{BASE_URL}/admin/finances_v2/transaction/add/?{urllib.parse.urlencode(params, safe="=")}">Create</a> External identifier: <code>{t.internal_id}</code> <a href="{BASE_URL}/admin/finances_v2/transactionentry/?q={t.amount}&account__id__exact={credentials.account.id}">Search existing</a></li>')

            text = ""
            if created_transactions:
                text += "Die folgen Transaktionen wurden in der Verwaltung automatisch angelegt:<br><ul>\n"
                text += "\n".join(created_transactions)
                text += "\n</ul><br><br>"
            if create_transaction_links:
                text += "Die folgen Transaktionen können angelegt werden indem auf <code>Create</code> geklickt wird. Existiert bereits eine Transaktion setze bei dieser den <code>external identifier</code>:<br>\n"
                text += "\n".join(create_transaction_links)
                text += "\n</ul><br>"

            if text:
                mail.send(
                    credentials.email,
                    config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                    subject='Automatische Transaktionserstellung',
                    html_message=text,
                )
            credentials.last_transaction_fetch = timezone.now()
            credentials.save()
