from django.db import models
from django.utils import timezone

from finances_v2.models import Account


class NordigenCredential(models.Model):
    # The represented account in the finances_v2 module
    account = models.OneToOneField(Account, on_delete=models.CASCADE)
    email = models.EmailField(
        help_text="To this email address notifications are send about the requisition status and when new transactions could be imported.")

    # API Tokens
    access_token = models.CharField(max_length=512, null=True)
    refresh_token = models.CharField(max_length=512, null=True)
    access_token_expiry = models.DateTimeField(null=True)
    refresh_token_expiry = models.DateTimeField(null=True)

    # Code for verifying requisition callback
    verification_reference = models.CharField(max_length=512, null=True)

    # Requisition data
    requisition_id = models.CharField(max_length=512, null=True)
    requisition_expiry = models.DateTimeField(null=True)

    # Bank account ID
    nordigen_account_id = models.CharField(max_length=512, null=True)
    nordigen_institution_id = models.CharField(
        max_length=256,
        help_text="The identifier of the linked bank. See here for values: https://docs.google.com/spreadsheets/d/1ogpzydzotOltbssrc3IQ8rhBLlIZbQgm5QCiiNJrkyA/")

    # Fetch metadata
    last_transaction_fetch = models.DateTimeField(null=True)

    def status_as_text(self):
        if self.requisition_expiry:
            remaining_days = (self.requisition_expiry - timezone.now()).days
            if remaining_days < 0:
                return "Requisition is no longer valid."
            else:
                return f"Requisition is still valid for {remaining_days} days."
        else:
            return "No requisition created yet."


class NordigenRequest(models.Model):
    class RequestType(models.TextChoices):
        FETCH_TRANSACTIONS = "Fetch Transactions", "Fetch Transactions"
        FETCH_REQUISITION = "Fetch Requisition", "Fetch Requisition"
        CREATE_REQUISITION = "Create Requisition", "Create Requisition"
        REFRESH_ACCESS_TOKEN = "Refresh Access Token", "Refresh Access Token"
        GENERATE_ACCESS_TOKEN = "Generate Access Token", "Generate Access Token"

    credentials = models.ForeignKey(NordigenCredential, null=True, on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now=True)
    request = models.TextField(choices=RequestType.choices)
    response = models.TextField()
    error = models.BooleanField(default=False)
