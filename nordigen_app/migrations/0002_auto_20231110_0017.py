# Generated by Django 3.2.12 on 2023-11-09 23:17

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('nordigen_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='nordigencredentials',
            name='access_token',
            field=models.CharField(max_length=512, null=True),
        ),
        migrations.AlterField(
            model_name='nordigencredentials',
            name='account_id',
            field=models.CharField(max_length=512, null=True),
        ),
        migrations.AlterField(
            model_name='nordigencredentials',
            name='refresh_token',
            field=models.CharField(max_length=512, null=True),
        ),
        migrations.AlterField(
            model_name='nordigencredentials',
            name='requisition_id',
            field=models.CharField(max_length=512, null=True),
        ),
        migrations.AlterField(
            model_name='nordigencredentials',
            name='verification_reference',
            field=models.CharField(max_length=512, null=True),
        ),
    ]
