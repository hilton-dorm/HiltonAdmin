from django.urls import path

from . import views

urlpatterns = [
    path("nordigen/", views.nordigen_redirect_endpoint, name='nordigen_redirect_endpoint'),
    path("requisition-status/", views.requisition_status, name='requisition_status'),
    path("fetch_transactions/", views.transaction_fetch, name='fetch_transactions')
]
