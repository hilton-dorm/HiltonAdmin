from django.apps import AppConfig


class NordigenAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'nordigen_app'
