import datetime
import json
import typing
import uuid
import dataclasses
from datetime import timedelta

import requests
from django.utils import timezone
from nordigen import NordigenClient
from requests.models import HTTPError
from telegram.passport import credentials

import config
from .models import NordigenCredential, NordigenRequest

Type = NordigenRequest.RequestType


class NordigenAPI:

    def __init__(self, credentials: NordigenCredential):
        self.client = NordigenClient(
            secret_key=config.NORDIGEN_USER_SECRET_KEY, secret_id=config.NORDIGEN_USER_SECRET_ID, timeout=20
        )
        self.credentials = credentials

    def generate_token(self):
        try:
            token_data = self.client.generate_token()
            self.log_request(Type.GENERATE_ACCESS_TOKEN, token_data)

            current_time = timezone.now()
            access_token_expiration_seconds = token_data['access_expires']
            refresh_token_expiration_seconds = token_data['refresh_expires']

            self.credentials.access_token = token_data['access']
            self.credentials.refresh_token = token_data['refresh']
            self.credentials.access_token_expiry = current_time + datetime.timedelta(
                seconds=access_token_expiration_seconds - 60)
            self.credentials.refresh_token_expiry = current_time + datetime.timedelta(
                seconds=refresh_token_expiration_seconds - 60)
            self.credentials.save()
        except HTTPError as e:
            self.log_error(Type.GENERATE_ACCESS_TOKEN, e.response)

    def refresh_access_token(self, refresh_token):
        try:
            token_data = self.client.exchange_token(refresh_token=refresh_token)
            self.log_request(Type.REFRESH_ACCESS_TOKEN, token_data)

            current_time = timezone.now()
            access_token_expiration_seconds = token_data['access_expires']
            access_token_expiry = current_time + datetime.timedelta(seconds=access_token_expiration_seconds - 60)

            self.credentials.access_token = token_data["access"]
            self.credentials.access_token_expiry = access_token_expiry

            self.credentials.save()
        except HTTPError as e:
            self.log_error(Type.REFRESH_ACCESS_TOKEN, e.response)

    def validate_token(self):
        current_time = timezone.now()
        # No access token available
        if not self.credentials.access_token_expiry:
            self.generate_token()
        # Access token invalidated
        elif self.credentials.access_token_expiry <= current_time:
            # Refresh token invalidated
            if self.credentials.refresh_token_expiry <= current_time:
                self.generate_token()
            else:
                self.refresh_access_token(self.credentials.refresh_token)

    def create_requisition(self):
        self.validate_token()
        self.client.token = self.credentials.access_token

        reference_id = uuid.uuid4().hex

        try:
            result = self.client.initialize_session(
                institution_id=self.credentials.nordigen_institution_id,
                redirect_uri=f"{config.BASE_URL}/nordigen/nordigen?id={self.credentials.id}", reference_id=reference_id,
                access_valid_for_days=90
            )
            self.log_request(Type.CREATE_REQUISITION, dataclasses.asdict(result))

            self.credentials.requisition_id = result.requisition_id
            self.credentials.verification_reference = reference_id
            self.credentials.requisition_expiry = None

            self.credentials.save()

            return result.link
        except HTTPError as e:
            self.log_error(Type.CREATE_REQUISITION, e.response)

    def fetch_requisition(self, requisition_id):
        self.validate_token()
        self.client.token = self.credentials.access_token

        try:
            result = self.client.requisition.get_requisition_by_id(requisition_id)
            self.log_request(Type.FETCH_REQUISITION, result)

            return result
        except HTTPError as e:
            self.log_error(Type.FETCH_REQUISITION, e.response)

    def fetch_booked_transactions(self):
        self.validate_token()
        self.client.token = self.credentials.access_token

        current_time = timezone.now()

        # Check if requisition is still valid
        if self.credentials.requisition_expiry and self.credentials.requisition_expiry > current_time:
            try:
                result = self.client.account_api(id=self.credentials.nordigen_account_id).get_transactions()
                self.log_request(Type.FETCH_TRANSACTIONS, result)

                return result["transactions"]["booked"]
            except HTTPError as e:
                self.log_error(Type.FETCH_TRANSACTIONS, e.response)
        else:
            print("Requisition already expired")
        return []

    def fetch_cached_transactions(self, max_age=timedelta(days=1)):
        try:
            last = NordigenRequest.objects.filter(timestamp__gt=timezone.now() - max_age, credentials=self.credentials,
                                                  error=False, request=NordigenRequest.RequestType.FETCH_TRANSACTIONS).latest('timestamp')
            result = json.loads(last.response)
            return result["transactions"]["booked"]
        except NordigenRequest.DoesNotExist:
            return self.fetch_booked_transactions()

    def log_request(self, name: NordigenRequest.RequestType, response: typing.Any):
        json_response = json.dumps(response)
        NordigenRequest(credentials=self.credentials, request=name, response=json_response).save()

    def log_error(self, name: NordigenRequest.RequestType, response: requests.Response):
        NordigenRequest(credentials=self.credentials, request=name, response=response.json(), error=True).save()
