from django.contrib import admin
from django.contrib.admin import DateFieldListFilter
import nested_admin
from fitness_room.models import FitnessRoomVisit, FitnessRoomDoorOpenEvent, FitnessRoomBlockList, \
    FitnessRoomDocumentation


class FitnessRoomDoorOpenEventAdmin(admin.ModelAdmin):
    list_filter = (
        ('opened_time', DateFieldListFilter),
    )


class FitnessRoomDocumentationInline(nested_admin.NestedStackedInline):
    model = FitnessRoomDocumentation
    can_delete = False
    fk_name = 'visit'
    readonly_fields = ('visit', 'upload_time', 'file')


class FitnessRoomVisitAdmin(nested_admin.NestedModelAdmin):
    autocomplete_fields = ('person',)

    def get_queryset(self, request):
        return super(FitnessRoomVisitAdmin, self).get_queryset(request).prefetch_related('person__user')

    search_fields = ['person__user__first_name', 'person__user__last_name']
    inlines = [FitnessRoomDocumentationInline]


class FitnessRoomBlockListAdmin(nested_admin.NestedModelAdmin):
    autocomplete_fields = ('person',)

    def get_queryset(self, request):
        return super(FitnessRoomBlockListAdmin, self).get_queryset(request).prefetch_related('person__user')

    search_fields = ['person__user__first_name', 'person__user__last_name']


admin.site.register(FitnessRoomDoorOpenEvent, FitnessRoomDoorOpenEventAdmin)
admin.site.register(FitnessRoomVisit, FitnessRoomVisitAdmin)
admin.site.register(FitnessRoomBlockList, FitnessRoomBlockListAdmin)
