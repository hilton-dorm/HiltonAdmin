import paho.mqtt.client as mqtt
from django.apps import AppConfig
from django.db.models import Q
from django.utils.timezone import now
from django_telegrambot.apps import DjangoTelegramBot
from telegram import InlineKeyboardMarkup, InlineKeyboardButton
from telegram.ext import CallbackContext
from datetime import timedelta
import sys
import config
import logging

from hiltonadmin import settings

logger = logging.getLogger(__name__)


class FitnessRoomConfig(AppConfig):
    name = 'fitness_room'
    client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
    mqtt_is_online = None
    _mqtt_is_offline_message_send = False
    mqtt_is_door_open = False
    mqtt_is_room_closed = True  # The room can be "closed" for situations like a pandemic
    ready_run = False

    def ready(self):
        if FitnessRoomConfig.ready_run:
            return
        FitnessRoomConfig.ready_run = True
        # Only run the mqtt client when using the commands runserver or botpolling
        #  otherwise we have a mqtt client for every cronjob and get the events multiple times
        # When no command is used (wsgi, is_manage_py is False), then we run the webserver with uses multiple processes.
        #  In this case we do not run the handlers (MQTT -> Telegram message), because we would do that in every worker
        #  process to that x messages would be send. Instead we require that the `run_mqtt_client` is run, so that we
        #  only have one process sending telegram messages as response to MQTT messages.
        args = [arg.casefold() for arg in sys.argv]
        is_manage_py = any(arg.endswith("manage.py") for arg in args)
        is_run_mqtt_client = ("run_mqtt_client" in args)
        is_polling = settings.DJANGO_TELEGRAMBOT.get('MODE', 'WEBHOOK') == 'POLLING'
        if is_manage_py:
            if is_polling and "botpolling" not in args:
                return
            elif not is_polling and "runserver" not in args and not is_run_mqtt_client:
                return

        if config.MQTT_USERNAME and not config.MQTT_PASSWORD:
            logger.error("No MQTT password given. Please change the config file.")
        client = FitnessRoomConfig.client
        client.username_pw_set(config.MQTT_USERNAME, config.MQTT_PASSWORD)
        try:
            client.connect(config.MQTT_SERVER, config.MQTT_SERVER_PORT, 60)
        except ConnectionRefusedError:
            logger.error("Failed to connect to MQTT Server: Connection Refused. You are probably not in the intranet.")

        # The callback for when the client receives a CONNACK response from the server.
        def on_connect(mqtt_client, userdata, flags, reason_code, properties):
            # Subscribing in on_connect() means that if we lose the connection and
            # reconnect then subscriptions will be renewed.
            mqtt_client.subscribe("fitness_room/online")
            mqtt_client.subscribe("fitness_room/door_open")
            mqtt_client.subscribe("fitness_room/room_closed")

        # we are in the ready method, so we can now import models
        from fitness_room.models import FitnessRoomDoorOpenEvent, FitnessRoomVisit
        from fitness_room.telegrambot_handlers import ask_if_room_is_ok, get_fitness_leader
        from workinggroups.models import KeyHolder

        # The callback for when a PUBLISH message is received from the server.
        def on_message(mqtt_client, userdata, msg):
            if msg.topic == "fitness_room/online":
                new_status = msg.payload == b'1'
                if new_status != FitnessRoomConfig.mqtt_is_online:
                    old_value = FitnessRoomConfig.mqtt_is_online
                    FitnessRoomConfig.mqtt_is_online = new_status
                    if is_manage_py and not old_value is None:
                        # only send the message if we are not a gunicorn worker process, for example by `python manage.py run_mqtt_client`
                        # don't send a message when the variable gets initialized
                        def send_message(context: CallbackContext | None = None):
                            if not FitnessRoomConfig.mqtt_is_online or FitnessRoomConfig._mqtt_is_offline_message_send:
                                leader = get_fitness_leader()
                                names = ", ".join(
                                    [str(visit.person) for visit in FitnessRoomVisit.objects.filter(end_time__isnull=True)])
                                names = names if names else "nobody"
                                state = "online" if new_status else "offline"
                                leader.person.send_telegram_or_mail(
                                    f"The esp in the fitness room to unlock the door is now {state} and "
                                    f"{names} is currently in the room.")
                                FitnessRoomConfig._mqtt_is_offline_message_send = True
                        if new_status:
                            send_message()
                            FitnessRoomConfig._mqtt_is_offline_message_send = False
                        else:
                            # wait 30 seconds until we send the offline message because often the esp is only offline for a few seconds
                            DjangoTelegramBot.dispatcher.job_queue.run_once(send_message, timedelta(minutes=2))
            elif msg.topic == "fitness_room/room_closed":
                FitnessRoomConfig.mqtt_is_room_closed = msg.payload == b'1'
            elif msg.topic == "fitness_room/door_open":
                was_open = FitnessRoomConfig.mqtt_is_door_open
                FitnessRoomConfig.mqtt_is_door_open = msg.payload == b'1'
                if not is_manage_py:
                    return  # don't create the objects each worker process, use `run_mqtt_client`
                if was_open and not FitnessRoomConfig.mqtt_is_door_open:
                    logger.warning("Door was closed")
                    # door was closed
                    latest = FitnessRoomDoorOpenEvent.get_latest_event()
                    if not latest:
                        return
                    if not latest.closed_time:
                        latest.closed_time = now()
                    else:
                        # no event was created when door was opened
                        latest = FitnessRoomDoorOpenEvent(opened_time=now(), closed_time=now())
                    latest.save()
                elif not was_open and FitnessRoomConfig.mqtt_is_door_open:
                    # door was opened
                    new = FitnessRoomDoorOpenEvent(opened_time=now())
                    new.save()
                    bot = DjangoTelegramBot.dispatcher.bot
                    for visit in FitnessRoomVisit.objects.filter(requested_open_door__gt=now() - timedelta(seconds=15),
                                                                 entering_door_open_event__isnull=True):
                        bot.send_message(chat_id=visit.person.telegram_chat_id,
                                         text="It seems that you have entered the room. "
                                              "If not, send /leave_fitness_room.")
                        ask_if_room_is_ok(visit.person)
                        visit.entering_door_open_event = new
                        visit.save()

                    for visit in FitnessRoomVisit.objects.filter(
                            Q(end_time__isnull=True) & ~Q(entering_door_open_event=new)):
                        visit.leaving_door_open_event = new
                        visit.save()
                        bot.send_message(chat_id=visit.person.telegram_chat_id,
                                         text="Someone opened the fitness room door.",
                                         reply_markup=InlineKeyboardMarkup(
                                             [[InlineKeyboardButton(text="I left the room", callback_data="left_room"),
                                               InlineKeyboardButton(text="I am still in the room",
                                                                    callback_data="other_opened_" + str(new.id))]]
                                         ))
                    # Jemand hat dem Raum manuell verlassen (/leave_fitness_room) und geht erst danach aus dem Raum
                    found_visit = False
                    for visit in FitnessRoomVisit.objects.filter(
                            Q(end_time__gt=now() - timedelta(minutes=2)) & Q(leaving_door_open_event__isnull=True)):
                        visit.leaving_door_open_event = new
                        visit.save()
                        found_visit = True

                    # Wenn niemand den Raum verlassen hat und keiner mehr im Raum ist:
                    if not found_visit and not FitnessRoomVisit.objects.filter(end_time__isnull=True).exists():
                        # someone opened the door but we don't know who, but the person must have a key
                        no_telegram = []
                        for holder in KeyHolder.get_active_key_holders().filter(room_key__name__icontains="K09"):
                            if holder.person.has_telegram_chat():
                                holder.person.send_telegram_or_mail("Someone opened the fitness room door. If you have "
                                                                    "opened the door, click on /entered_fitness_room")
                            else:
                                no_telegram.append(str(holder.person))
                        if no_telegram:
                            names = ", ".join(no_telegram)
                            get_fitness_leader().person.send_telegram_or_mail(
                                f"The persons {names} have not enabled the telegram bot are a key holder and someone "
                                f"opened the fitness room door. If in 5 minutes nobody said that he has entered the "
                                f"room, you get another message.")

                        def check_if_someone_reacted(context: CallbackContext) -> None:
                            if not FitnessRoomVisit.objects.filter(
                                    start_time__gte=now() - timedelta(minutes=5)).exists():
                                get_fitness_leader().person.send_telegram_or_mail(
                                    "Nobody said that he has entered the fitness room since 5 minutes.")

                        DjangoTelegramBot.dispatcher.job_queue.run_once(check_if_someone_reacted, timedelta(minutes=5))
                else:
                    logger.warning("The door sent the same state twice")

        client.on_connect = on_connect
        client.on_message = on_message
        if not is_run_mqtt_client:  # aka sonst läuft run_mqtt_client mit loop_forever() und beides gleichzeitig ist schlecht
            client.loop_start()
