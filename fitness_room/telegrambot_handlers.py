from telegram import Update, InlineKeyboardMarkup, InlineKeyboardButton, ParseMode
from telegram.ext import CommandHandler, CallbackContext, CallbackQueryHandler, MessageHandler, Filters
from django_telegrambot.apps import DjangoTelegramBot
from django.utils.timezone import now
from django.db.models import Q
from django.core.files.base import ContentFile

from main.models import Person
from fitness_room.apps import FitnessRoomConfig
from main.telegrambot_handlers import send_not_logged_in_message
from association.models import Membership
from workinggroups.models import WorkingGroup, KeyHolder
from fitness_room.models import FitnessRoomDoorOpenEvent, FitnessRoomVisit, FitnessRoomBlockList, \
    FitnessRoomDocumentation

from datetime import timedelta, datetime
import logging

logger = logging.getLogger(__name__)


def fitness_group():
    return WorkingGroup.objects.get(name__icontains="fitness")


def get_fitness_leader():
    return fitness_group().get_leader()


def remove_job_if_exists(name: str, context: CallbackContext) -> bool:
    """Remove job with given name. Returns whether job was removed."""
    current_jobs = context.job_queue.get_jobs_by_name(name)
    if not current_jobs:
        return False
    for job in current_jobs:
        job.schedule_removal()
    return True


def date(date: datetime) -> str:
    return date.astimezone().strftime('%Y-%m-%d %H:%M')


def time(date: datetime) -> str:
    return date.strftime("%H:%M:%S")


def get_fitness_leader_name() -> str:
    leader = get_fitness_leader()
    if not leader:
        leader = "the fitness working group leader"
    else:
        leader = leader.person.full_name()
    return leader


def personal_action(callback):
    def handle(update: Update, context: CallbackContext):
        person = Person.get_for_chat_id(update.effective_message.chat_id)
        if person:
            callback(person, update, context)
        else:
            send_not_logged_in_message(update)

    return handle


def personal_callback(callback):
    def handle(update: Update, context: CallbackContext):
        query = update.callback_query
        query.answer()
        person = Person.get_for_chat_id(update.effective_chat.id)
        if person:
            callback(person, query, update, context)
        else:
            send_not_logged_in_message(update)

    return handle


def open_door(person, update: Update, context: CallbackContext) -> None:
    chat_id = str(update.effective_message.chat_id)
    membership = Membership.get_current_membership_for(person)
    if not membership:
        update.effective_message.reply_text("You are not a member of the Turmstraße 1 e.V.")
        return

    errors = []

    if "normal" not in membership.type.name:
        errors.append("You are a {} member and not a 'normal' one".format(membership.type.name))

    if not membership.signed_usage_fitness:
        errors.append("You have not signed the fitness room usage terms")

    if not person.got_fitnessroom_introduction:
        errors.append("You must be instructed in the use of the room in order to use it. Please"
                      " contact a member of the Fitness AG to receive it!")

    if not membership.did_social_service_in_previous_social_service_period():
        errors.append("You have not done a social service in your previous social service period, you can "
                      "not use the fitness room")

    blocked = FitnessRoomBlockList.objects.filter(person=person).first()
    if blocked:
        errors.append(f"Your access to the fitness room has been blocked due to '{blocked.reason}'. "
                      f"If you think this is a mistake please contact {get_fitness_leader_name()}")

    if not FitnessRoomConfig.mqtt_is_online:
        errors.append("The door opener is currently offline. Can't open the door :( I will notify the "
                      "fitness room leader")

        leader = get_fitness_leader()
        leader.person.send_telegram_or_mail("Hallo {}, der ESP zum Öffnen der Tür "
                                            "scheint offline zu sein".format(leader.person.full_name()))

    if FitnessRoomConfig.mqtt_is_room_closed:
        if KeyHolder.get_active_key_holders().filter(person=person, room_key__name__icontains="K09").exists():
            update.effective_message.reply_text(
                "The room is closed at the moment, but you are a key holder, so we let you in.")
        else:
            errors.append("Sorry, the room is currently closed and can not be entered.")

    if errors:
        if len(errors) == 1:
            message = errors[0]
        else:
            message = "There are several problems:\n"
            for error in errors:
                message += "- " + error + "\n"
        update.effective_message.reply_text(message)
        return

    if FitnessRoomConfig.mqtt_is_door_open:
        update.effective_message.reply_text(
            "The door is already open!? If not, please write a message to " + get_fitness_leader_name())
        return

    def check_not_opened_callback(context: CallbackContext) -> None:
        visit = FitnessRoomVisit.objects.filter(person=person,
                                                requested_open_door__gt=now() - timedelta(seconds=20),
                                                entering_door_open_event__isnull=True).first()
        if visit:
            update.effective_message.reply_text(
                "It seems that you don't have entered the fitness room. If you have, send /entered_fitness_room")
            visit.delete()

    current_visit = FitnessRoomVisit.objects.filter(person=person, end_time=None).first()
    if current_visit:
        if not current_visit.entering_door_open_event and current_visit.requested_open_door and \
                current_visit.requested_open_door >= now() - timedelta(seconds=20):
            # the person has send the open_door command while the door is open and the person has not entered the room
            #  => delete the old visit
            current_visit.delete()
            remove_job_if_exists(chat_id, context)
        else:
            update.effective_message.reply_text(
                "According to our information, you are already/still in the room since {}. "
                "If not, send /leave_fitness_room".format(date(current_visit.start_time)))
            return

    visit = FitnessRoomVisit(person=person, requested_open_door=now(), start_time=now())
    visit.save()
    context.job_queue.run_once(check_not_opened_callback, 18, context=None, name=chat_id)
    FitnessRoomConfig.client.publish("fitness_room/open_door", "1")
    if current_visit:
        update.effective_message.reply_text(
            "The door is still open. If not, send a message to " + get_fitness_leader_name())
    else:
        update.effective_message.reply_text("The door should now be open")


def ask_if_room_is_ok(person):
    is_member = fitness_group().current_members().filter(person=person).exists()
    bot = DjangoTelegramBot.dispatcher.bot
    bot.send_message(person.telegram_chat_id,
                     "Confirm that the room is in an adequate state. If you confirm, you take responsibility "
                     "for the room. If not, the last user gets contacted due to the inadequate room.",
                     reply_markup=InlineKeyboardMarkup(
                         [[InlineKeyboardButton(text="Confirm adequate state", callback_data=f"room_ok:{person.id}"),
                           InlineKeyboardButton(text="Room looks not ok", callback_data=f"room_not_ok:{person.id}")]]
                     ))
    bot.send_message(person.telegram_chat_id,
                     "Please clean your used equipment with a disinfectant spray and open the windows when you leave!")
    if not is_member:
        job_id = f"{person.telegram_chat_id}_remind_send"

        def check_not_opened_callback(context: CallbackContext) -> None:
            visit = FitnessRoomVisit.objects.filter(person=person, end_time__isnull=True).first()
            if visit and not FitnessRoomDocumentation.objects.filter(visit=visit).exists():
                bot.send_message(person.telegram_chat_id,
                                 "Please provide a short video of the room, where every corner is shown.")
                count = context.job.context
                DjangoTelegramBot.dispatcher.job_queue.run_once(check_not_opened_callback, 60 * count,
                                                                context=count + 1, name=job_id)

        bot.send_message(person.telegram_chat_id,
                         "Please provide a short video (smaller than 20 MB) of the room, where every corner is shown "
                         "and we can see that the room is alright! Keep in mind, that by not doing so, your access will"
                         " be blocked next time!")
        DjangoTelegramBot.dispatcher.job_queue.run_once(check_not_opened_callback, 30, context=1, name=job_id)


def confirm_adequate_state(person, query, update: Update, _: CallbackContext) -> None:
    visit = FitnessRoomVisit.objects.filter(person=person, end_time__isnull=True).first()
    if visit is None:
        query.edit_message_text("You are currently not in the room anymore.")
        return
    visit.adequate_state = True
    visit.adequate_state_confirmation_time = now()
    visit.save()
    query.edit_message_text("You confirmed that the fitness room is in an adequate state.")


def room_is_not_ok(person, query, update: Update, _: CallbackContext) -> None:
    visit = FitnessRoomVisit.objects.filter(person=person, end_time__isnull=True).first()
    if visit is None:
        query.edit_message_text("You are currently not in the room anymore.")
        return
    visit.adequate_state = False
    visit.adequate_state_confirmation_time = now()
    visit.save()
    query.edit_message_text("You confirmed that the fitness room is not in an adequate state. Please take a video and "
                            "contact " + get_fitness_leader_name())
    leader = get_fitness_leader()
    if leader is None:
        print("Error: There is no fitness group leader")
    else:
        message = f"{person} complained that the fitness room is not in an adequate state just now.\n"
        last_visit = FitnessRoomVisit.objects.filter(end_time__isnull=False).order_by("-end_time").first()
        if last_visit:
            message += f'You find more information about the last visit here: {last_visit.get_admin_url()}'
        leader.person.send_telegram_or_mail(message, parse_mode=ParseMode.HTML)


def i_left_the_room(update: Update, _: CallbackContext) -> None:
    query = update.callback_query
    query.answer()

    query.edit_message_text(text="Did you really left the room?", reply_markup=InlineKeyboardMarkup(
        [[InlineKeyboardButton(text="Yes", callback_data="left_room_ack"),
          InlineKeyboardButton(text="Cancel", callback_data="fitness_cancel")]]
    ))


def i_left_the_room_ack(person, query, update: Update, _: CallbackContext) -> None:
    query.edit_message_text(text="You left the room")
    for visit in FitnessRoomVisit.objects.filter(person=person, end_time__isnull=True):
        visit.end_visit()


def other_opened(update: Update, _: CallbackContext) -> None:
    query = update.callback_query
    query.answer()
    query.edit_message_text(text="If someone else entered the room, the person must send /entered_fitness_room.")


def leave_fitness_room(person, update: Update, context: CallbackContext) -> None:
    current_visit = FitnessRoomVisit.objects.filter(person=person, end_time__isnull=True).first()
    if not current_visit:
        update.effective_message.reply_text("You are currently not in the room, you can not leave the room.")
        return
    current_visit.end_visit()
    update.effective_message.reply_text(
        "Ok, you visit from {} to {} is now completed".format(date(current_visit.start_time),
                                                              date(current_visit.end_time)))


def create_new_visit(person, entering_door_open_event=None):
    visit = FitnessRoomVisit(person=person, start_time=now(), entering_door_open_event=entering_door_open_event)
    visit.save()


def entered_fitness_room(person, update: Update, context: CallbackContext) -> None:
    current_visit = FitnessRoomVisit.objects.filter(person=person, end_time__isnull=True).first()
    if current_visit:
        update.effective_message.reply_text("You are currently in the room, you can not enter the room.")
        return
    event = FitnessRoomDoorOpenEvent.objects.filter(
        Q(opened_time__gt=now() - timedelta(minutes=5)) | Q(closed_time__gt=now() - timedelta(minutes=5))).last()
    if event:
        create_new_visit(person, entering_door_open_event=event)
        update.effective_message.reply_text("Your status is now: In the room.")
        ask_if_room_is_ok(person)

    else:
        update.effective_message.reply_text(
            "The door was not opened the last 5 minutes. Did you really entered the room?",
            reply_markup=InlineKeyboardMarkup(
                [[InlineKeyboardButton(text="I entered the room", callback_data="enter_room"),
                  InlineKeyboardButton(text="Cancel", callback_data="fitness_cancel")]]
            ))


def really_entered_room(person, query, update: Update, _: CallbackContext) -> None:
    create_new_visit(person)
    query.edit_message_text("Your state is now: In the fitness room.")
    ask_if_room_is_ok(person)


def cancel(person, query, update: Update, _: CallbackContext) -> None:
    message = "Action cancelled. Your state: "
    if FitnessRoomVisit.objects.filter(person=person, end_time__isnull=True).exists():
        message += "In the fitness room"
    else:
        message += "Not in the fitness room"
    query.edit_message_text(message)


def handle_documentation(person, update: Update, _: CallbackContext) -> None:
    visit = FitnessRoomVisit.objects.filter(
        Q(person=person) & (Q(end_time__isnull=True) | Q(end_time__gte=now() - timedelta(minutes=5)))).first()
    if visit:
        doc = FitnessRoomDocumentation(visit=visit, upload_time=update.effective_message.date)
        attachment = update.effective_message.effective_attachment
        if isinstance(attachment, list):
            attachment = attachment[-1]
        if attachment.file_size >= 20000000:  # the bot api only allows the download of files smaller than 20 MB
            update.effective_message.reply_text("Unfortnatly we can only accept files that are smaller than 20MB. "
                                                "Please split your video into pieces or reduce the quality.")
            return
        file_content = attachment.get_file().download_as_bytearray()
        file_name = attachment.file_name if hasattr(attachment, 'file_name') and attachment.file_name \
            else attachment.file_unique_id + "." + (
            attachment.mime_type.split("/")[-1] if hasattr(attachment, 'mime_type') else "jpg")
        doc.file.save(file_name, ContentFile(file_content))
        doc.save()
        update.effective_message.reply_text("Thanks! The file gets associated to your current visit.")


def room_status(update: Update, context: CallbackContext) -> None:
    if FitnessRoomConfig.mqtt_is_room_closed:
        update.effective_message.reply_text("The fitness room is currently closed.")
        return
    visits = FitnessRoomVisit.objects.filter(end_time__isnull=True)
    count = visits.count()
    if count == 0:
        update.effective_message.reply_text("The fitness room is currently free.")
    elif count == 1:
        timediff = now() - visits.first().start_time
        update.effective_message.reply_text(
            "There is currently one person in the fitness room since {} hours {} minutes".format(
                timediff.seconds // 3600, timediff.seconds // 60 % 60))
    else:
        times = str([now() - visit.start_time for visit in visits])[1:-1]
        update.effective_message.reply_text(
            f"There are currently {count} persons in the fitness room (since {times} hours)")


def my_room_status(person, update: Update, context: CallbackContext) -> None:
    visit = FitnessRoomVisit.objects.filter(person=person, end_time__isnull=True).first()
    if visit:
        door = f" ({time(visit.entering_door_open_event.opened_time)}🚪)" if visit.entering_door_open_event else ""
        update.effective_message.reply_text(
            f"You are currently in the fitness room since {date(visit.start_time)}{door}")
    else:
        update.effective_message.reply_text("You are currently not in the fitness room. "
                                            "See /my_fitness_room_history for your visits in the past.")


def my_room_history(person, update: Update, context: CallbackContext) -> None:
    visits = FitnessRoomVisit.objects.filter(person=person, end_time__isnull=False).order_by('-id')
    count = visits.count()
    if count == 0:
        update.effective_message.reply_text("You were never in the fitness room according to our information")
    else:
        info = ". Showing the last 10 events" if count > 10 else ""
        message = f"You were {count} time(s) in the fitness room in the past{info}:\n"
        for visit in visits[:10]:
            enter = f" (🚪{time(visit.entering_door_open_event.opened_time)})" if visit.entering_door_open_event else ""
            leave = f" (🚪{time(visit.leaving_door_open_event.opened_time)})" if visit.leaving_door_open_event else ""
            message += f"From {date(visit.start_time)}{enter} to {time(visit.end_time)}{leave}\n"

        update.effective_message.reply_text(message)


def fitness_room_leader_action(callback):
    def handle(person, update: Update, context: CallbackContext):
        if fitness_group().get_leaders().filter(person=person).exists():
            callback(update, context)
        else:
            update.effective_message.reply_text("This can only be done by the fitness group leader")

    return personal_action(handle)


def lock_fitness_room(update: Update, context: CallbackContext) -> None:
    FitnessRoomConfig.client.publish("fitness_room/room_closed", "1", retain=True)
    update.effective_message.reply_text("The fitness room is now closed, only person with keys can now enter the room.")


def unlock_fitness_room(update: Update, context: CallbackContext) -> None:
    FitnessRoomConfig.client.publish("fitness_room/room_closed", "0", retain=True)
    update.effective_message.reply_text("The fitness room is now open, all users can now enter the room.")


def send_fitness_message(update: Update, context: CallbackContext) -> None:
    # first remove command from message
    message = update.effective_message.text_markdown_v2_urled[len("/send\\_fitness\\_message "):]
    for person in Person.get_all_with_telegram().filter(got_fitnessroom_introduction=True):
        person.send_telegram_or_mail(message, parse_mode=ParseMode.MARKDOWN_V2)


def main():
    dispatcher = DjangoTelegramBot.dispatcher

    dispatcher.add_handler(CommandHandler("open_fitness_room", personal_action(open_door)))
    dispatcher.add_handler(CommandHandler("leave_fitness_room", personal_action(leave_fitness_room)))
    dispatcher.add_handler(CommandHandler("entered_fitness_room", personal_action(entered_fitness_room)))
    dispatcher.add_handler(CommandHandler("fitness_room_status", room_status))
    dispatcher.add_handler(CommandHandler("my_fitness_room_status", personal_action(my_room_status)))
    dispatcher.add_handler(CommandHandler("my_fitness_room_history", personal_action(my_room_history)))
    dispatcher.add_handler(CommandHandler("lock_fitness_room", fitness_room_leader_action(lock_fitness_room)))
    dispatcher.add_handler(CommandHandler("unlock_fitness_room", fitness_room_leader_action(unlock_fitness_room)))
    dispatcher.add_handler(CommandHandler("send_fitness_message", fitness_room_leader_action(send_fitness_message)))

    dispatcher.add_handler(MessageHandler(Filters.video | Filters.video_note | Filters.photo | Filters.voice |
                                          Filters.document.video | Filters.document.image,
                                          personal_action(handle_documentation)))

    dispatcher.add_handler(CallbackQueryHandler(i_left_the_room, pattern="^left_room$"))
    dispatcher.add_handler(CallbackQueryHandler(personal_callback(i_left_the_room_ack), pattern="^left_room_ack"))
    dispatcher.add_handler(CallbackQueryHandler(other_opened, pattern="^other_opened"))
    dispatcher.add_handler(CallbackQueryHandler(personal_callback(really_entered_room), pattern="^enter_room$"))
    dispatcher.add_handler(CallbackQueryHandler(personal_callback(cancel), pattern="^fitness_cancel"))
    dispatcher.add_handler(CallbackQueryHandler(personal_callback(confirm_adequate_state), pattern="^room_ok"))
    dispatcher.add_handler(CallbackQueryHandler(personal_callback(room_is_not_ok), pattern="^room_not_ok"))
