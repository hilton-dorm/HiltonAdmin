from datetime import timedelta

from django.db import models
from private_storage.fields import PrivateFileField
from django.utils.timezone import now
from django.urls import reverse
from django_telegrambot.apps import DjangoTelegramBot

from main.models import Person
from workinggroups.models import WorkingGroup
from config import BASE_URL


class FitnessRoomDoorOpenEvent(models.Model):
    opened_time = models.DateTimeField()
    closed_time = models.DateTimeField(null=True)

    def __str__(self):
        return "{} - {}".format(self.opened_time.astimezone().strftime("%Y-%m-%d %H:%M"),
                                self.closed_time.astimezone().strftime(
                                    "%Y-%m-%d %H:%M") if self.closed_time else "None")

    @staticmethod
    def get_latest_event():
        return FitnessRoomDoorOpenEvent.objects.latest('opened_time')


class FitnessRoomVisit(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)

    # the time when the person entered the room (entered the pin or door was opened from inside)
    start_time = models.DateTimeField()
    # when the person requested that the door should open
    requested_open_door = models.DateTimeField(null=True, blank=True)
    # when mqtt reported that the door was opened for entering (maybe from a person inside)
    entering_door_open_event = models.ForeignKey(FitnessRoomDoorOpenEvent, on_delete=models.SET_NULL, null=True,
                                                 related_name='start_visit', blank=True)

    adequate_state = models.BooleanField(null=True, blank=True)
    adequate_state_confirmation_time = models.DateTimeField(null=True, blank=True)

    keep_for_documentation_reasons = models.BooleanField(default=False,
                                                         help_text="We have to delete every data after a specified "
                                                                   "amount of tine. But sometimes we have to keep data "
                                                                   "(e.g. when a user destroyed the room), then check "
                                                                   "this field.")
    # when the user said that he left the room
    end_time = models.DateTimeField(null=True, blank=True)
    # when mqtt reported that the door was opened for leaving the room
    leaving_door_open_event = models.ForeignKey(FitnessRoomDoorOpenEvent, on_delete=models.SET_NULL, null=True,
                                                related_name='end_visit', blank=True)

    def __str__(self):
        def f(date):
            return date.astimezone().strftime("%Y-%m-%d %H:%M") if date else "None"

        def e(event):
            return event.opened_time.astimezone().strftime("%Y-%m-%d %H:%M") if event else "None"

        return "{}: {} ({}) - {} ({})".format(self.person, f(self.start_time), e(self.entering_door_open_event),
                                              f(self.end_time), e(self.leaving_door_open_event))

    def end_visit(self):
        self.end_time = now()
        self.save()
        if WorkingGroup.objects.get(name__icontains="fitness").current_members().filter(person=self.person).exists():
            return
        if self.end_time - self.start_time < timedelta(minutes=3):
            return  # no documentation needed for short visits
        # check is the documentation is missing and we have to block the person
        if not FitnessRoomDocumentation.objects.filter(visit=self).exists():
            bot = DjangoTelegramBot.dispatcher.bot
            bot.send_message(chat_id=self.person.telegram_chat_id,
                             text="You did not provide a short video of the room! You will be blocked.")
            blocked = FitnessRoomBlockList(person=self.person, reason="Did not provide a short video of the room")
            blocked.save()

    def get_admin_url(self):
        return BASE_URL + reverse("admin:%s_%s_change" % (self._meta.app_label, self._meta.model_name), args=(self.id,))


class FitnessRoomDocumentation(models.Model):
    visit = models.ForeignKey(FitnessRoomVisit, on_delete=models.CASCADE)
    upload_time = models.DateTimeField()
    file = PrivateFileField(upload_to='fitness_room/documentation')


class FitnessRoomBlockList(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    reason = models.CharField(max_length=255)

    def __str__(self):
        return f"{self.person} because of {self.reason}"
