from django.core.management.base import BaseCommand
from django.utils.timezone import now
from datetime import timedelta
from fitness_room.models import FitnessRoomVisit
from workinggroups.models import WorkingGroup


class Command(BaseCommand):
    help = 'Checks if a person is longer than 3 hours in the room and asks if the person is still in the room. ' \
           'Sends a message to the fitness room leader if the person is very long in the room. ' \
           'Should be executed every hour.'

    def handle(self, *args, **options):
        for visit in FitnessRoomVisit.objects.filter(end_time__isnull=True, start_time__lt=now() - timedelta(hours=3)):
            hours = (now() - visit.start_time).seconds // 3600
            if hours < 8 or hours % 8 == 0:
                visit.person.send_telegram_or_mail(f"You are now more than {str(hours)} hours in the fitness room. "
                                                   "Are you really still in the room? If not, send /leave_fitness_room")
                if hours >= 8:
                    WorkingGroup.objects.get(name__icontains="fitness").get_leader().person.send_telegram_or_mail(
                        f"{visit.person.full_name()} is now more than {str(hours)} hours in the fitness room."
                    )
