from django.core.management.base import BaseCommand
import config
import paho.mqtt.client as mqtt
import time


class Command(BaseCommand):
    help = 'Simulates a door open event'

    def handle(self, *args, **options):
        client = mqtt.Client(mqtt.CallbackAPIVersion.VERSION2)
        if config.MQTT_USERNAME and not config.MQTT_PASSWORD:
            print("No MQTT password given. Please change the config file.")
            return
        client.username_pw_set(config.MQTT_USERNAME, config.MQTT_PASSWORD)
        try:
            client.connect(config.MQTT_SERVER, config.MQTT_SERVER_PORT, 60)
        except ConnectionRefusedError:
            print("Failed to connect to MQTT Server: Connection Refused. You are probably not in the intranet.")
            return
        client.publish("fitness_room/door_open", "1", retain=True)
        print("Door opened\nWaiting 4 seconds before closing the door again...")
        time.sleep(4)
        client.publish("fitness_room/door_open", "0", retain=True)
        print("Door closed")
