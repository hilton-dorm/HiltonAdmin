from django.core.management.base import BaseCommand
from django.utils.timezone import now
from datetime import timedelta
from fitness_room.models import FitnessRoomVisit
from workinggroups.models import WorkingGroup
import config


class Command(BaseCommand):
    help = 'We have to delete old visits because of data privacy reasons.'

    def handle(self, *args, **options):
        FitnessRoomVisit.objects.filter(
            start_time__lt=now() - timedelta(days=config.FITNESS_ROOM_DELETE_DATA_AFTER_X_DAYS),
            keep_for_documentation_reasons=False).delete()
