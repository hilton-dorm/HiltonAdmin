from django.core.management.base import BaseCommand
from fitness_room.apps import FitnessRoomConfig
from hiltonadmin.settings import DJANGO_TELEGRAMBOT
import signal


class Command(BaseCommand):
    help = 'Runs the mqtt client handler when the verwaltung runs on the server.'

    def handle(self, *args, **options):
        is_polling = DJANGO_TELEGRAMBOT.get('MODE', 'WEBHOOK') == 'POLLING'
        if is_polling:
            print("ERROR: You only have to run this command when using WEBHOOKs for the telegram bot and use multiple "
                  "processes")
            return
        signal.signal(signal.SIGINT, FitnessRoomConfig.client.disconnect)
        FitnessRoomConfig.client.loop_forever()
