import datetime
from datetime import timedelta

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone
from freezegun import freeze_time
from moneyed import Money
from post_office.models import EmailTemplate

import config
import finances_v2.models
from association.models import Membership, MembershipType, SocialService, Registration, \
    MembershipTypePeriods, SocialServicePeriod, PlannedMembershipTypeChange
from finances_v2.models import TransactionEntry, Accounts, TransactionTypes, Transaction
from finances_v2.tests import FinancesV2TestCase
from main.models import Person, Tenant, Room
from workinggroups.models import WorkingGroup

DATE_MOVE_OUT = timezone.datetime(2023, 6, 30)


class MemberhipTestCase(TestCase):
    def setUp(self):
        FinancesV2TestCase.accountSetUp(self)
        user = User.objects.create(
            username="tester",
            first_name="test",
            last_name="tester",
            email="test@tester.de")
        self.person = Person.objects.create(user=user)
        self.room = Room.objects.create(number="2072", vlan_id=20, eth_port0=10, eth_port1=11)
        self.tenant = Tenant.objects.create(
            person=self.person, room=self.room, date_move_in=timezone.datetime(
                2020, 1, 1), date_move_out=DATE_MOVE_OUT)
        self.type_normal = MembershipType.objects.create(description="test", name="normal ordentlich", has_to_do_social_service=True)
        self.type_limited = MembershipType.objects.create(
            description="test",
            name="Ruhendes Mitglied (limited member)",
            has_to_pay_fees=False,
            has_to_do_social_service=False,
            internet_allowed=False)
        EmailTemplate.objects.create(name="new_tenant_bar_reminder")
        EmailTemplate.objects.create(name="unfunded_account_warning")
        EmailTemplate.objects.create(name="membership_type_changed_to_limited")

    def test_registration_fee(self):
        m = Membership.objects.create(person=self.person, type=self.type_normal)
        self.assertEqual(m.get_receivables_house(), Money(5, "EUR"))
        self.assertEqual(m.get_receivables_network(), Money(5, "EUR"))

    @freeze_time("2020-01-01", as_kwarg='frozen_time')
    def test_free_internet_new_member(self, frozen_time):
        m = Membership.objects.create(person=self.person, type=self.type_normal)
        self.assertTrue(m.has_network_access())
        frozen_time.move_to("2020-01-15")
        self.assertTrue(m.has_network_access())
        frozen_time.move_to("2020-01-16")
        self.assertFalse(m.has_network_access())

    def deposit_money(self, amount):
        finances_v2.models.Transaction.create(entries=[TransactionEntry(amount=amount, account=Accounts().NETWORK_FK, person=self.person),
                                                       TransactionEntry(amount=amount, account=Accounts().NETWORK_BANK, person=self.person)])

    @freeze_time("2020-01-10", as_kwarg='frozen_time')
    def test_partial_transfer(self, frozen_time):
        m = Membership.objects.create(person=self.person, type=self.type_normal)
        call_command("createinvoices")
        self.deposit_money(4)
        m._network_fk_to_house_receivables(1)
        self.assertEqual(m.get_receivables_house(), Money(6, "EUR"))  # 6 = 5 + 2 - 1
        self.assertEqual(m.get_receivables_network(), Money(5, "EUR"))
        self.assertEqual(m.get_guthaben(), Money(3, "EUR"))
        call_command('transfer_membership_transactions')
        self.assertEqual(m.get_receivables_house(), Money(6, "EUR"))
        self.assertEqual(m.get_receivables_network(), Money(5, "EUR"))
        self.assertEqual(m.get_guthaben(), Money(3, "EUR"))
        self.deposit_money(1)
        call_command('transfer_membership_transactions')
        self.assertEqual(m.get_guthaben(), Money(0, "EUR"))
        self.assertEqual(m.get_receivables_house(), Money(2, "EUR"))
        self.assertEqual(m.get_balance(), Money(-7, "EUR"))

    @freeze_time("2020-01-10", as_kwarg='frozen_time')
    def test_monthly_fees(self, frozen_time):
        m = Membership.objects.create(person=self.person, type=self.type_normal)
        call_command("createinvoices")
        self.assertEqual(m.get_receivables_house(), Money(7, "EUR"))
        self.assertEqual(m.get_receivables_network(), Money(5, "EUR"))
        self.deposit_money(9)
        self.assertEqual(m.get_balance(), Money(-3, "EUR"))
        call_command('transfer_membership_transactions')
        self.assertEqual(m.get_receivables_house(), Money(0, "EUR"))
        self.assertEqual(m.get_receivables_network(), Money(5, "EUR"))
        frozen_time.move_to("2020-01-25")
        self.assertFalse(m.has_network_access())
        self.deposit_money(5)
        call_command("createinvoices")
        self.assertEqual(m.get_receivables_house(), Money(0, "EUR"))
        self.assertEqual(m.get_receivables_network(), Money(0, "EUR"))
        self.assertEqual(m.get_balance(), Money(2, "EUR"))
        self.assertFalse(m.has_network_access())
        self.assertTrue(m.fulfill_additional_network_invoice_requirements())
        self.assertFalse(m.fulfill_basic_network_invoice_requirements(dont_check_balance=True))
        m.signed_usage_network = True
        m.save()
        self.assertTrue(m.fulfill_basic_network_invoice_requirements(dont_check_balance=True))
        m.create_invoice_network(pay_fee_if_not_funded=True)
        self.assertEqual(m.get_receivables_network(), Money(6, "EUR"))
        self.assertEqual(m.get_balance(), Money(-4, "EUR"))
        self.assertTrue(m.has_network_access())
        call_command('transfer_membership_transactions')
        self.assertEqual(m.get_receivables_network(), Money(6, "EUR"))
        frozen_time.move_to("2020-01-27")
        self.deposit_money(10)
        call_command('transfer_membership_transactions')
        self.assertEqual(m.get_receivables_network(), Money(0, "EUR"))
        self.assertEqual(m.get_balance(), Money(6, "EUR"))
        frozen_time.move_to("2020-02-01")
        call_command("createinvoices")
        self.assertEqual(m.get_balance(), Money(4, "EUR"))  # -2€ for house fee
        frozen_time.move_to("2020-02-25")  # network fee end
        self.assertFalse(m.has_network_access())
        call_command("createinvoices")
        self.assertTrue(m.has_network_access())
        self.assertEqual(m.get_balance(), Money(0, "EUR"))  # -4€ for network fee
        frozen_time.move_to("2020-02-27")
        m.type = self.type_limited
        m.save()
        frozen_time.move_to("2020-03-01")
        call_command("createinvoices")  # ruhend => no new fees
        self.assertEqual(m.get_balance(), Money(0, "EUR"))
        frozen_time.move_to("2020-03-25")  # network fee ends
        call_command("createinvoices")  # ruhend => no new fees
        self.assertEqual(m.get_balance(), Money(0, "EUR"))
        self.assertFalse(m.has_network_access())
        m.type = self.type_normal
        m.save()
        call_command("createinvoices")  # new house fee but no new network (not enough money)
        self.assertEqual(m.get_receivables_house(), Money(2, "EUR"))
        self.assertEqual(m.get_balance(), Money(-2, "EUR"))
        self.deposit_money(6)
        call_command("createinvoices")
        self.assertEqual(m.get_balance(), Money(0, "EUR"))
        self.assertTrue(m.has_network_access())

    @freeze_time("2020-01-10", as_kwarg='frozen_time')
    def test_get_list_of_unpaied_transaction_entries(self, frozen_time):
        m = Membership.objects.create(person=self.person, type=self.type_normal, signed_usage_network=True)
        call_command("createinvoices")
        entries, rest_amount, failed = m.get_list_of_unpaied_transaction_entries(Accounts().HOUSE_RECEIVABLES_TENANTS)
        self.assertFalse(failed)
        self.assertEqual(2, len(entries))
        self.deposit_money(5)
        self.assertEqual(m.get_receivables_house(), Money(7, "EUR"))
        call_command('transfer_membership_transactions')
        self.assertEqual(m.get_receivables_house(), Money(2, "EUR"))
        entries, rest_amount, failed = m.get_list_of_unpaied_transaction_entries(Accounts().HOUSE_RECEIVABLES_TENANTS)
        self.assertFalse(failed)
        self.assertEqual(1, len(entries))
        self.deposit_money(5)
        call_command('transfer_membership_transactions')
        self.assertEqual(m.get_receivables_house(), Money(0, "EUR"))
        entries, rest_amount, failed = m.get_list_of_unpaied_transaction_entries(Accounts().HOUSE_RECEIVABLES_TENANTS)
        self.assertFalse(failed)
        self.assertEqual(0, len(entries))
        self.deposit_money(1)
        finances_v2.models.Transaction.create(
            entries=[TransactionEntry(amount=-1, account=Accounts().NETWORK_FK, person=self.person),
                     TransactionEntry(amount=-1, account=Accounts().NETWORK_RECEIVABLES_TENANTS, person=self.person)])
        self.assertEqual(m.get_receivables_network(), Money(4, "EUR"))
        entries, rest_amount, failed = m.get_list_of_unpaied_transaction_entries(Accounts().NETWORK_RECEIVABLES_TENANTS)
        self.assertFalse(failed)
        self.assertEqual(1, len(entries))

    @freeze_time("2020-01-10", as_kwarg='frozen_time')
    def test_relief_registration_fee(self, frozen_time):
        m = Membership.objects.create(person=self.person, type=self.type_normal, signed_usage_network=True)
        call_command("createinvoices")
        m.date_leave = timezone.now()
        m.save()
        frozen_time.move_to(DATE_MOVE_OUT)
        frozen_time.tick(timedelta(days=1))
        m._create_network_receivable(2, TransactionTypes().NETWORK_FEE, "Block relief_receivables_for_resigned_members")
        self.assertEqual(m.get_receivables_network(), Money(7, "EUR"))
        call_command("relief_receivables_for_resigned_members")
        self.assertEqual(m.get_receivables_network(), Money(7, "EUR"))
        Transaction.objects.last().delete()  # 2€ Fee wieder löschen
        self.assertEqual(m.get_receivables_house(), Money(7, "EUR"))
        self.assertEqual(m.get_receivables_network(), Money(5, "EUR"))
        call_command("relief_receivables_for_resigned_members")
        self.assertEqual(m.get_receivables_house(), Money(0, "EUR"))
        self.assertEqual(m.get_receivables_network(), Money(0, "EUR"))

    @freeze_time("2020-01-10", as_kwarg='frozen_time')
    def test_relief_monthly_fees(self, frozen_time):
        m = Membership.objects.create(person=self.person, type=self.type_normal, signed_usage_network=True)
        self.deposit_money(16)
        call_command("createinvoices")
        self.assertEqual(m.get_receivables_house(), Money(0, "EUR"))
        self.assertEqual(m.get_receivables_network(), Money(0, "EUR"))
        frozen_time.move_to("2020-01-25")
        call_command("createinvoices")
        self.assertTrue(m.has_network_access())
        # forward three months
        for i in range(2, 5):
            frozen_time.move_to(f"2020-0{i}-01")
            call_command("createinvoices")
        self.assertEqual(m.get_receivables_house(), Money(6, "EUR"))
        self.assertEqual(m.get_receivables_network(), Money(0, "EUR"))
        m.date_leave = timezone.now()
        m.save()
        call_command("relief_receivables_for_resigned_members")
        self.assertEqual(m.get_receivables_house(), Money(6, "EUR"))  # noch nicht ausgezogen
        m.date_leave = None
        m.save()
        frozen_time.move_to(DATE_MOVE_OUT)
        frozen_time.tick(timedelta(days=1))
        call_command("relief_receivables_for_resigned_members")
        self.assertEqual(m.get_receivables_house(), Money(6, "EUR"))  # noch nicht ausgetreten
        m.date_leave = DATE_MOVE_OUT
        m.save()
        m._create_house_receivable(10, None, "Spiele kaputt gemacht")
        call_command("relief_receivables_for_resigned_members")
        self.assertEqual(m.get_receivables_house(), Money(16, "EUR"))  # nichts erlassen
        Transaction.objects.last().delete()  # Delete "Spiele kaputt gemacht"
        self.deposit_money(1)
        finances_v2.models.Transaction.create(  # Halben Betrag bezahlen
            entries=[TransactionEntry(amount=-1, account=Accounts().NETWORK_FK, person=self.person),
                     TransactionEntry(amount=-1, account=Accounts().HOUSE_RECEIVABLES_TENANTS, person=self.person)])
        call_command("relief_receivables_for_resigned_members")
        self.assertEqual(m.get_receivables_house(), Money(0, "EUR"))  # alles erlassen
        self.assertEqual(m.get_receivables_network(), Money(0, "EUR"))  # alles erlassen
        self.assertEqual(m.get_balance(), Money(0, "EUR"))  # alles erlassen

    @staticmethod
    def date(*args):
        return timezone.make_aware(timezone.datetime(*args, 20, 00))

    @freeze_time("2020-01-10", as_kwarg='frozen_time')
    def test_registration_fee_date(self, frozen_time):
        date_join = datetime.date(2020, 1, 20)
        Membership.objects.create(person=self.person, type=self.type_normal, date_join=date_join)
        self.assertEqual(Accounts().NETWORK_EK.entries.filter(person=self.person, type=TransactionTypes().REGISTRATION_FEE).last().transaction.date, date_join)
        self.assertEqual(Accounts().HOUSE_EK.entries.filter(person=self.person, type=TransactionTypes().REGISTRATION_FEE).last().transaction.date, date_join)

    @freeze_time("2020-01-01", as_kwarg='frozen_time')
    def test_new_tenant_bar(self, frozen_time):
        WorkingGroup.objects.update(should_attend_at_new_tenant_bar=False)  # to prevent emails
        m = Membership.objects.create(person=self.person, type=self.type_normal)
        self.assertEqual(m.missed_new_tenant_bars(), 0)
        SocialService.objects.create(name="Neueinzieherbar", is_new_tenant_bar=True, time=self.date(2019, 12, 30))
        self.assertEqual(m.missed_new_tenant_bars(), 0)
        SocialService.objects.create(name="Neueinzieherbar", is_new_tenant_bar=True, time=self.date(2020, 1, 3))
        self.assertEqual(m.missed_new_tenant_bars(), 0)
        frozen_time.move_to("2020-01-5")
        # Erst zwei Tage später wird der counter erhöht damit die Leute nicht das Internet verlieren, wenn sie da waren aber es erst später eingetragen wird
        self.assertEqual(m.missed_new_tenant_bars(), 0)
        frozen_time.move_to("2020-01-6")
        self.assertEqual(m.missed_new_tenant_bars(), 1)
        SocialService.objects.create(name="Neueinzieherbar", is_new_tenant_bar=True, time=self.date(2020, 2, 3))
        ss = SocialService.objects.create(name="Neueinzieherbar", is_new_tenant_bar=True, time=self.date(2020, 3, 3))
        frozen_time.move_to("2020-03-6")
        self.assertEqual(m.missed_new_tenant_bars(), 3)
        self.assertTrue(m.missed_new_tenant_bar_too_often())
        reg = Registration.objects.create(person=self.person, social_service=ss)
        self.assertTrue(m.missed_new_tenant_bar_too_often())
        reg.verified = True
        reg.save()
        self.assertTrue(m.person.attended_new_tenant_bar())

    @freeze_time("2020-01-01", as_kwarg='frozen_time')
    def test_social_service_periods_social_services(self, frozen_time):
        m = Membership.objects.create(person=self.person, type=self.type_normal)
        self.assertTrue(m.did_social_service_in_previous_social_service_period())
        self.assertFalse(m.did_social_service_in_current_social_service_period())
        frozen_time.tick(timedelta(days=config.SOCIAL_SERVICE_PERIOD_DURATION + 1))
        call_command("create_new_social_service_periods")
        self.assertFalse(m.did_social_service_in_previous_social_service_period())
        self.assertFalse(m.did_social_service_in_current_social_service_period())
        ss = SocialService.objects.create(name="Test", time=self.date(2020, 8, 3))
        reg = Registration.objects.create(person=self.person, social_service=ss)
        self.assertFalse(m.did_social_service_in_previous_social_service_period())
        self.assertFalse(m.did_social_service_in_current_social_service_period())
        reg.verified = True
        reg.save()
        self.assertTrue(m.did_social_service_in_previous_social_service_period())
        self.assertFalse(m.did_social_service_in_current_social_service_period())
        ss = SocialService.objects.create(name="Test", time=self.date(2020, 8, 4))
        reg = Registration.objects.create(person=self.person, social_service=ss, verified=True)
        self.assertTrue(m.did_social_service_in_previous_social_service_period())
        self.assertTrue(m.did_social_service_in_current_social_service_period())

    @freeze_time("2020-01-01", as_kwarg='frozen_time')
    def test_social_service_periods_non_normal(self, frozen_time):
        m = Membership.objects.create(person=self.person, type=self.type_normal)
        frozen_time.move_to("2020-02-01")
        m.type = self.type_limited
        m.save()
        frozen_time.move_to("2020-01-01")
        frozen_time.tick(timedelta(days=config.SOCIAL_SERVICE_PERIOD_DURATION))
        self.assertEqual(m.get_current_social_service_period().days_ruhend(), config.SOCIAL_SERVICE_PERIOD_DURATION - 31)
        call_command("create_new_social_service_periods")
        self.assertTrue(m.did_social_service_in_previous_social_service_period())  # since no new period created since we are not a normal member
        m.type = self.type_normal
        m.save()
        call_command("create_new_social_service_periods")
        self.assertEqual(m.get_current_social_service_period().days_ruhend(), config.SOCIAL_SERVICE_PERIOD_DURATION - 31)
        self.assertTrue(m.did_social_service_in_previous_social_service_period())
        # we were 31 days a normal member in january, so (period - 31)  days after limited ends a new period should start:
        frozen_time.tick(timedelta(days=config.SOCIAL_SERVICE_PERIOD_DURATION - 31))
        self.assertEqual(m.get_current_social_service_period().duration(), config.SOCIAL_SERVICE_PERIOD_DURATION)
        call_command("create_new_social_service_periods")
        frozen_time.tick(timedelta(days=4))
        self.assertFalse(m.did_social_service_in_previous_social_service_period())  # new period created
        self.assertEqual(m.get_previous_social_service_period().duration(), config.SOCIAL_SERVICE_PERIOD_DURATION)

    def test_periods_manager(self):
        m1 = Membership.objects.create(person=self.person, type=self.type_normal)
        m2 = Membership.objects.create(person=self.person, type=self.type_normal)
        for Model in (MembershipTypePeriods, SocialServicePeriod):
            Model.plain_objects.all().delete()  # clear automatically created entries
            extra_args = {'type': self.type_normal} if Model is MembershipTypePeriods else {}
            Model.objects.create(membership=m1, start_date=timezone.datetime(2020, 1, 1), **extra_args)
            Model.objects.create(membership=m2, start_date=timezone.datetime(2020, 5, 1), **extra_args)
            self.assertEqual(Model.objects.filter(membership=m1).first().end_date, None)
            Model.objects.create(membership=m1, start_date=timezone.datetime(2020, 1, 5), **extra_args)
            self.assertEqual(Model.objects.filter(membership=m1).first().end_date, datetime.date(2020, 1, 5))
            # end dates must respect membership id:
            entries = list(Model.objects.all().order_by('id'))
            self.assertEqual(entries[0].start_date, datetime.date(2020, 1, 1))
            self.assertEqual(entries[0].end_date, datetime.date(2020, 1, 5))
            self.assertEqual(entries[1].start_date, datetime.date(2020, 5, 1))
            self.assertEqual(entries[1].end_date, None)
            self.assertEqual(entries[2].start_date, datetime.date(2020, 1, 5))
            self.assertEqual(entries[2].end_date, None)
            # end dates must also work when filter the queryset:
            self.assertEqual(Model.objects.filter(membership=m1, start_date__lt=datetime.date(2020, 1, 2)).get().end_date, datetime.date(2020, 1, 5))

    @freeze_time("2020-01-01", as_kwarg='frozen_time')
    def test_planned_membership_type_changes(self, frozen_time):
        with self.assertRaises(ValidationError):
            PlannedMembershipTypeChange.objects.create(person=self.person, planned_membership_type=self.type_limited,
                                                       start=self.date(2020, 2, 1), last_day=self.date(2020, 1, 1))

        PlannedMembershipTypeChange.objects.create(person=self.person, planned_membership_type=self.type_limited,
                                                   start=self.date(2019, 2, 1), last_day=self.date(2020, 2, 15))
        call_command("process_planned_membership_type_changes")
        self.assertFalse(PlannedMembershipTypeChange.objects.exists())  # gelöscht weil keine Membership da

        PlannedMembershipTypeChange.objects.create(person=self.person, planned_membership_type=self.type_limited,
                                                   start=self.date(2020, 2, 1), last_day=self.date(2020, 2, 15))
        m = Membership.objects.create(person=self.person, type=self.type_normal)
        frozen_time.move_to("2020-01-31")
        call_command("process_planned_membership_type_changes")  # nichts passiert
        m.refresh_from_db()
        self.assertEqual(m.type, self.type_normal)

        frozen_time.move_to("2020-02-01")
        call_command("process_planned_membership_type_changes")  # start change
        m.refresh_from_db()
        self.assertEqual(m.type, self.type_limited)
        self.assertEqual(PlannedMembershipTypeChange.objects.first().original_membership_type, self.type_normal)

        frozen_time.move_to("2020-02-15")
        call_command("process_planned_membership_type_changes")  # nichts passiert
        self.assertTrue(PlannedMembershipTypeChange.objects.exists())
        m.refresh_from_db()
        self.assertEqual(m.type, self.type_limited)

        frozen_time.move_to("2020-02-16")
        call_command("process_planned_membership_type_changes")  # end gets processed
        self.assertFalse(PlannedMembershipTypeChange.objects.exists())
        m.refresh_from_db()
        self.assertEqual(m.type, self.type_limited)

        PlannedMembershipTypeChange.objects.create(person=self.person, planned_membership_type=self.type_normal,
                                                   start=self.date(2020, 2, 16))
        call_command("process_planned_membership_type_changes")  # start gets processed
        self.assertFalse(PlannedMembershipTypeChange.objects.exists())  # gets deleted because no end
        m.refresh_from_db()
        self.assertEqual(m.type, self.type_normal)
