from decimal import Decimal

from dateutil.relativedelta import relativedelta, WE
import nested_admin
from django import forms
from django.contrib import admin, messages
from django.contrib.admin import SimpleListFilter
from django.contrib.auth.models import User
from django.db.models import Q, Sum, F
from django.http import HttpResponse
from django.urls import reverse
from django.utils import timezone
from django.utils.encoding import force_str
from django.utils.formats import localize
from django.utils.html import format_html, mark_safe
from django.utils.translation import gettext_lazy as _
from datetime import timedelta, date
import cologne_phonetics
import re

from finances_v2.models import Accounts
from .models import MembershipType, Membership, SocialService, Registration, MembershipTypePeriods, \
    PlannedMembershipTypeChange
from main.models import Tenant


class RegistrationAdminForm(forms.ModelForm):
    user = forms.ModelChoiceField(queryset=User.objects.order_by('last_name'))


class MembershipInline(nested_admin.NestedTabularInline):
    model = Membership
    can_delete = False
    fk_name = 'type'
    extra = 0


class BalanceFundedFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('balance fund')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'funded'

    def lookups(self, request, model_admin):
        return (
            ('0', _('funded (>= 0€)')),
            ('1', _('unfunded (< 0€)'))
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value() == "0":
            return queryset.filter(balance__gte=0)
        if self.value() == "1":
            return queryset.filter(balance__lt=0)

        return queryset


class CanceledMembershipFilter(SimpleListFilter):
    title = _('Membership status')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'left'

    def lookups(self, request, model_admin):
        return (
            ('ruhend', _('12 Months ruhend')),
            ('active', _('Not left yet')),
            ('left', _('Already left'))
        )

    def queryset(self, request, queryset):
        if self.value() == "left":
            return queryset.filter(date_leave__lte=timezone.now())
        if self.value() == "active":
            return queryset.filter(Q(date_leave__isnull=True) | Q(date_leave__gt=timezone.now()))
        if self.value() == "ruhend":
            last_year = timezone.now() - timedelta(days=365)
            return queryset.filter(Q(type__name__icontains="ruhend") & Q(date_membership_type_since__lt=last_year)
                                   & (Q(date_leave__isnull=True) | Q(date_leave__gt=timezone.now())))
        return queryset


class SocialServiceFilter(SimpleListFilter):
    title = _('social service')
    parameter_name = 'social_service'

    def lookups(self, request, model_admin):
        return (
            ('0', _('no social service (current period)')),
            ('1', _('no social service (previous period)'))
        )

    def queryset(self, request, queryset):
        if self.value() == "0":
            memberships = Membership.objects.all()
            memberships = list(
                filter(
                    lambda x: not x.did_social_service_in_current_social_service_period(),
                    memberships))
            memberships = [x.id for x in memberships]
            return queryset.filter(pk__in=memberships)
        if self.value() == "1":
            memberships = Membership.objects.all()
            memberships = list(
                filter(
                    lambda x: not x.did_social_service_in_previous_social_service_period(),
                    memberships))
            memberships = [x.id for x in memberships]
            return queryset.filter(pk__in=memberships)

        return queryset


class DebtFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('debts')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'debts'

    def lookups(self, request, model_admin):
        return (
            ('house', _('has house debts')),
            ('network', _('has network debts'))
        )

    def queryset(self, request, queryset):
        if self.value() == "house":
            return queryset.filter(house_debt__gt=0)
        if self.value() == "network":
            return queryset.filter(network_debt__gt=0)

        return queryset


class MovedOutFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('Living Status')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'livingStatus'

    def lookups(self, request, model_admin):
        return (
            ('moved_out', _('moved out')),
            ('living', _('is living here / moves in'))
        )

    def queryset(self, request, queryset):
        if self.value() == "moved_out":
            return queryset.filter(~Q(person__tenant__in=Tenant.current_tenants()))
        if self.value() == "living":
            return queryset.filter(person__tenant__date_move_out__gte=date.today())
        return queryset


def create_list_of_email_addresses(modeladmin, request, queryset):
    email_addresses = []
    for membership in queryset:
        email_addresses.append(membership.person.user.email)
    return HttpResponse(",".join(email_addresses), content_type='text/plain')


create_list_of_email_addresses.short_description = "Download List of Email Addresses"


def forderungen_erlassen(modeladmin, request, queryset):
    for membership in queryset:
        membership.relief_receivables()


forderungen_erlassen.short_description = "Alle Forderungen gegenüber der Person erlassen"


def create_network_invoice_with_fee(modeladmin, request, queryset):
    for membership in queryset:
        membership.create_invoice_network(pay_fee_if_not_funded=True)


create_network_invoice_with_fee.short_description = "Create network invoice with 2€ fee for manual transfer receipt check"


class MembershipPeriodsInline(nested_admin.NestedTabularInline):
    model = MembershipTypePeriods
    can_delete = False
    extra = 0
    readonly_fields = ('start_date', 'end_date', 'type',)

    def end_date(self, obj):
        return localize(obj.end_date)

    def has_add_permission(self, request, obj):
        return False

    # Remove object name, see https://stackoverflow.com/a/43349858/10162645
    class Media:
        css = {
            'all': ('css/admin_remove_object_text.css',)
        }


@admin.register(Membership)
class MembershipAdmin(admin.ModelAdmin):
    def get_queryset(self, request):
        return super(MembershipAdmin, self).get_queryset(request).select_related('person', 'type').prefetch_related(
            'person__user').annotate(
            network_debt=Sum("person__transactionentry__amount",
                             filter=Q(person__transactionentry__account=Accounts().NETWORK_RECEIVABLES_TENANTS),
                             default=0),
            house_debt=Sum("person__transactionentry__amount",
                           filter=Q(person__transactionentry__account=Accounts().HOUSE_RECEIVABLES_TENANTS),
                           default=0),
            network_outside=Sum("person__transactionentry__amount",
                                filter=Q(person__transactionentry__account=Accounts().NETWORK_FK),
                                default=0),
            balance=F('network_outside') - F('house_debt') - F('network_debt')
        )

    list_display = (
        'member_id',
        'link_to_user',
        'room',
        'link_to_type',
        'date_leave',
        'balance',
        'house_debt',
        'network_debt',
        'attended_new_tenant_bar',
        'social_service_current_period',
        'social_service_previous_period',
        'social_service_period',
        'has_network_access',
        'network_access_notes')
    list_editable = []
    actions = [create_list_of_email_addresses, forderungen_erlassen, create_network_invoice_with_fee]
    list_filter = [CanceledMembershipFilter, BalanceFundedFilter, DebtFilter, SocialServiceFilter, MovedOutFilter,
                   'type',
                   'skip_internet_fee',
                   'primary_internet_access']
    readonly_fields = ('member_id', 'negative_balance_month_counter', 'date_membership_type_since')
    inlines = [MembershipPeriodsInline]
    search_fields = ['person__user__first_name', 'person__user__last_name', '=id']
    autocomplete_fields = ('person', )
    list_per_page = 10

    def get_search_results(self, request, filtered_queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, filtered_queryset, search_term)
        queries = None
        for _, code in cologne_phonetics.encode(search_term, concat=True):
            if not code:
                continue
            query = Q(person__name_cologne_phonetics__contains=' ' + code + ' ')
            queries = query if queries is None else queries & query
        if queries is not None:
            return queryset | filtered_queryset.filter(queries), True
        return queryset, use_distinct

    def link_to_user(self, obj):
        link = reverse("admin:auth_user_change", args=[obj.person.user.id])
        return format_html('<b><a href="{}">{}</a></b>', link, obj.person.full_name())

    link_to_user.short_description = 'Person'

    def link_to_type(self, obj):
        link = reverse("admin:association_membershiptype_change", args=[obj.type.id])
        return format_html('<b><a href="{}">{}</a></b>', link, obj.type)

    link_to_type.short_description = 'Type'

    def room(self, obj):
        tenant = obj.person.get_latest_tenant()
        link = reverse("admin:main_room_change", args=[tenant.room_id])
        text = format_html('<b><a href="{}">{}</a></b>', link, tenant.room.number)
        if tenant.date_move_out < date.today():
            text += mark_safe("<br>moved out")
        return text

    def social_service_current_period(self, obj):
        return obj.did_social_service_in_current_social_service_period()

    social_service_current_period.boolean = True

    def social_service_previous_period(self, obj):
        return obj.did_social_service_in_previous_social_service_period()

    social_service_previous_period.boolean = True

    def social_service_period(self, obj):
        current = obj.get_current_social_service_period()
        return str(current.start_date) + " - " + str(current.estimated_end())

    social_service_period.short_description = 'Social Service Period'

    def member_id(self, obj):
        return str(obj.id).rjust(5, '0')

    member_id.short_description = 'Membership ID'

    def balance(self, obj):
        return f"€{obj.balance:.2f}"

    balance.admin_order_field = "balance"

    def house_debt(self, obj):
        return f"€{obj.house_debt:.2f}"

    house_debt.admin_order_field = "house_debt"

    def network_debt(self, obj):
        return f"€{obj.network_debt:.2f}"

    network_debt.admin_order_field = "network_debt"

    def attended_new_tenant_bar(self, obj):
        return obj.person.attended_new_tenant_bar()

    attended_new_tenant_bar.boolean = True

    def has_network_access(self, obj):
        return obj.has_network_access()

    has_network_access.boolean = True
    has_network_access.short_description = 'Network Access'

    def network_access_notes(self, obj):
        return obj.get_network_access_notes()

    network_access_notes.short_description = 'Notes'


@admin.register(PlannedMembershipTypeChange)
class PlannedMembershipTypeChangeAdmin(admin.ModelAdmin):
    list_display = ('person', 'planned_membership_type', 'start', 'last_day')
    readonly_fields = ('original_membership_type',)
    autocomplete_fields = ('person',)


@admin.register(MembershipType)
class MembershipTypeAdmin(nested_admin.NestedModelAdmin):
    list_display = ('name', 'description', 'membership_fee', 'membership_fee_network', 'internet_allowed')
    readonly_fields = ('name',)


class RegistrationInline(nested_admin.NestedTabularInline):
    model = Registration
    can_delete = False
    fk_name = 'social_service'
    extra = 0
    readonly_fields = ('verified_at', 'social_service_period')
    autocomplete_fields = ('person', )


@admin.register(SocialService)
class SocialServiceAdmin(nested_admin.NestedModelAdmin):
    list_display = ('name', 'description', 'time', 'people_limit')
    inlines = [RegistrationInline]
    search_fields = ['name', 'description']
    readonly_fields = ('external_identifier',)
    save_as = True

    def get_changeform_initial_data(self, request):
        if request.GET.get('new_tenant_bar', None) is not None:
            try:
                ss = SocialService.objects.filter(is_new_tenant_bar=True).latest('time')
                time = ss.time + relativedelta(months=1, day=1, weekday=WE(1))
                name = re.sub("[0-9]{1,2}\\.[0-9]{1,2}\\.[0-9]{2,4}", time.strftime("%d.%m.%Y"), ss.name)
                messages.add_message(request, messages.WARNING, "Don't forget to set the Zoom/Participate link!")
                messages.add_message(request, messages.INFO, "If you click save an email is send to all tenants who "
                                                             "have not yet participated in a new tenant bar")
                return {
                    'responsible_working_group': ss.responsible_working_group_id,
                    'people_limit': ss.people_limit,
                    'is_new_tenant_bar': True,
                    'description': ss.description,
                    'name': name,
                    'time': time,
                    'link_to_participate': "Don't forget the link" if ss.link_to_participate is not None else None
                }
            except SocialService.DoesNotExist:
                pass
        return super().get_changeform_initial_data(request)


class SocialServiceInline(nested_admin.NestedTabularInline):
    model = SocialService
    can_delete = False
    extra = 0


@admin.register(Registration)
class RegistrationAdmin(admin.ModelAdmin):
    search_fields = ['person__user__first_name',
                     'person__user__last_name',
                     'social_service__name']
    readonly_fields = ('verified_at', 'social_service_period')
    list_display = ('person', 'social_service', 'verified')
    list_filter = ('verified', )
    autocomplete_fields = ('person', )
