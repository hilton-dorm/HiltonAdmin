from django.core.management.base import BaseCommand
from post_office import mail

import config
from association.models import Membership
from workinggroups.models import WorkingGroup


class Command(BaseCommand):
    help = 'Sends a reminder if the number of members exceeds a certain threshold'

    def handle(self, *args, **options):
        num_memberships = Membership.current_memberships().count()
        if num_memberships >= config.REGISTERED_MEMBERS:
            mail.send(
                WorkingGroup.objects.get(name__icontains='Vorstand').email,
                config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                template='member_number_reminder',
                context={
                    'num_members': num_memberships,
                    'threshold': config.REGISTERED_MEMBERS,
                    'threshold_last_update': config.REGISTERED_MEMBERS_LAST_UPDATE
                }
            )
