from django.core.management.base import BaseCommand
from post_office import mail
from django.db.models import Q
from datetime import date, timedelta

import config
from association.models import Membership, Registration
from workinggroups.models import GroupMembership


class Command(BaseCommand):
    help = 'Reminder for social services. Send to members with less than 62 days left.'

    def handle(self, *args, **options):
        # die person muss mindestens 60 tage in der AG sein
        base_filter = Q(date_join__lte=date.today() - timedelta(days=60)) & (Q(date_leave__isnull=True) | Q(
            date_leave__gt=date.today() - timedelta(days=config.SOCIAL_SERVICE_PERIOD_DURATION + 60)))
        active_filter = (Q(working_group__first_leader_is_social_service=True) & Q(type=GroupMembership.TYPE_FIRST_LEADER)) \
            | (Q(working_group__second_leader_is_social_service=True) & Q(type=GroupMembership.TYPE_SECOND_LEADER)) \
            | (Q(working_group__members_are_active_by_default=True) & Q(type=GroupMembership.TYPE_MEMBER))

        memberships = Membership.current_memberships()
        for membership in memberships:
            if membership.type.has_to_do_social_service \
                    and not membership.did_social_service_in_current_social_service_period():
                ss_period = membership.get_current_social_service_period()
                remaining_days = config.SOCIAL_SERVICE_PERIOD_DURATION - ss_period.duration() + ss_period.extra_days_everybody
                if remaining_days < 62:
                    # if the member gets a social service from a working group the person dont need a reminder, but when
                    # only 14 days are left, a reminder is better because the person maybe dont get a social service
                    # from a working group
                    if remaining_days >= 14 and GroupMembership.objects.filter(
                            base_filter & Q(person=membership.person) & active_filter).exists():
                        # if there is a registration that is not verified and has a verified_at date the registration
                        # is from a 'in einer ag aktiv sein' social service, but the person was set to verified=False
                        # explicitly. So the person needs a reminder if there is such a registration
                        if not Registration.objects.filter(
                                person=membership.person, verified=False, verified_at__gte=date.today() - timedelta(days=30)).exists():
                            continue
                    mail.send(
                        membership.person.user.email,
                        config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                        template='social_service_reminder',
                        context={
                            'first_name': membership.person.user.first_name,
                            'remaining_days': remaining_days,
                            'ss_period': ss_period
                        },
                        priority='medium'
                    )
