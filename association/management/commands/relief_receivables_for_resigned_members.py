from typing import List

from django.core.management.base import BaseCommand
from django.utils.timezone import now

from association.models import Membership
from finances_v2.models import Accounts, TransactionTypes, Transaction, Account, TransactionEntry


def is_house_fee(t: Transaction) -> bool:
    for e in t.transactionentry_set.all():
        if e.account == Accounts().HOUSE_EK and e.type == TransactionTypes().HOUSE_FEE:
            return True
    return False


def is_registration_fee(t: Transaction, ek_account: Account) -> bool:
    for e in t.transactionentry_set.all():
        if e.account == ek_account and e.type == TransactionTypes().REGISTRATION_FEE:
            return True
    return False


def only_max_3_house_fees_and_one_registration_fee(transaction_entries: List[TransactionEntry]):
    if len(transaction_entries) > 4:
        return False
    house_fees = 0
    registration_fees = 0
    for entry in transaction_entries:
        if is_house_fee(entry.transaction):
            house_fees += 1
        elif is_registration_fee(entry.transaction, Accounts().HOUSE_EK):
            registration_fees += 1
        else:
            return False
    return house_fees <= 3 and registration_fees <= 1


class Command(BaseCommand):
    help = 'Iterates over each resigned member who only has less than 3 open house fee debts and cancel this debts.'

    def handle(self, *args, **options):
        members = Membership.objects.filter(date_leave__lt=now())
        for member in members:
            if member.person.get_latest_tenant().date_move_out > now().date():
                continue
            house = member.get_receivables_house().amount
            network = member.get_receivables_network().amount
            if network >= 0 and house > 0:
                transactions, rest_amount, failed = member.get_list_of_unpaied_transaction_entries(Accounts().HOUSE_RECEIVABLES_TENANTS)
                if not failed and only_max_3_house_fees_and_one_registration_fee(transactions):
                    if network == 0:  # Keine NetzAG Schulden
                        member.relief_receivables()
                    else:
                        network_transaction_entries, rest_amount, failed = member.get_list_of_unpaied_transaction_entries(
                            Accounts().NETWORK_RECEIVABLES_TENANTS)
                        if not failed:
                            if len(network_transaction_entries) == 1 and is_registration_fee(network_transaction_entries[0].transaction, Accounts().NETWORK_EK):
                                # Einzige NetzAG Transaction ist Registration Fee
                                member.relief_receivables()
