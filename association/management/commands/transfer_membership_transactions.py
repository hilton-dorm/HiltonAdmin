from datetime import datetime

from django.core.management.base import BaseCommand
from django.db import transaction

from association.models import Membership
from finances_v2.models import Accounts


class Command(BaseCommand):
    help = 'This script handles new transactions. It transfers transactions from and to subaccounts'

    def handle(self, *args, **options):
        # Iterate over all memberships and check if transactions in receivables
        # accounts can be liquidated
        for membership in Membership.objects.all():
            # Note that user transactions always belong to the network fk
            outsite_capital_value = Accounts().NETWORK_FK.get_balance(person=membership.person)

            for account in (Accounts().HOUSE_RECEIVABLES_TENANTS, Accounts().NETWORK_RECEIVABLES_TENANTS):
                receivables = account.get_balance(person=membership.person)

                # These balances should not be negative
                if outsite_capital_value < 0:
                    # Kann passieren wenn wir von Nordigen doppelt importierte Transaktionen löschen
                    continue
                assert receivables >= 0, "Negativ for Membership of " + str(membership.person)

                if outsite_capital_value >= receivables:
                    transferable_value = receivables
                else:
                    open_transactions, rest_amount, matching_failed = membership.get_list_of_unpaied_transaction_entries(account)
                    if matching_failed:
                        transferable_value = outsite_capital_value
                    else:
                        transferable_value = 0
                        for tx in open_transactions:
                            if tx.amount - rest_amount + transferable_value <= outsite_capital_value:
                                transferable_value += tx.amount - rest_amount
                                rest_amount = 0
                            else:
                                break

                # Reduce transferable value from receivable and outside capital and increase liquidity
                if transferable_value > 0:
                    if account == Accounts().NETWORK_RECEIVABLES_TENANTS:
                        membership._network_fk_to_receivables(transferable_value)
                    else:
                        membership._network_fk_to_house_receivables(transferable_value)
                        if membership.get_balance().amount >= 0:  # see Issue #93
                            membership.negative_balance_month_counter = 0
                            membership.save()
                    outsite_capital_value -= transferable_value

                # Keine NetzAG Rechnungen bezahlen, wenn es noch offenen Haus Rechnungen gibt
                if receivables > transferable_value:
                    break
