from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.utils import timezone
from post_office import mail

import config
from association.models import Membership, MembershipType
from main.models import Person, Tenant


class Command(BaseCommand):
    help = 'Iterates over each moved out tenant and decides whether the membership type has to be changed to "förderned"'

    def handle(self, *args, **options):
        supportive_membership_type = MembershipType.objects.get(name="Fördermitglied (sponsor)")
        ordinary_membership_type = MembershipType.objects.get(name="Ordentliches Mitglied (normal member)")

        tenants = Tenant.objects.filter(date_move_out__isnull=False)
        changed_memberships = []
        for tenant in tenants:
            # There might be more than one tenant. get the current one
            tenant = tenant.get_latest_tenant()
            if tenant.date_move_out <= timezone.localdate():
                try:
                    # There might be more than one membership
                    memberships = Membership.objects.filter(person=tenant.person)
                    for membership in memberships:
                        if membership.is_active() and membership.type == ordinary_membership_type:
                            membership.type = supportive_membership_type
                            membership.save()
                            changed_memberships.append({'person': membership.person.full_name()})

                except Membership.DoesNotExist:
                    pass

        if len(changed_memberships) > 0:
            mail.send(
                config.EMAIL["EMAIL_ACCOUNT_HOUSE"],
                config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                template='membership_changes_on_move_out',
                context={'changed_memberships': changed_memberships},
                priority='medium'
            )
