from django.core.management.base import BaseCommand
from datetime import date
from post_office import mail

from association.models import Membership
import config


class Command(BaseCommand):
    help = 'Sends a reminder email every 30 days to association members that moved out (the command should be executed every day)'

    def handle(self, *args, **options):
        for m in Membership.current_memberships():
            move_out_date = m.person.tenant_set.order_by('date_move_out').last().date_move_out
            if move_out_date is None:
                continue
            if move_out_date >= date.today():
                continue
            if "ehren" in m.type.name.lower():
                continue
            day_diff = (date.today() - move_out_date).days
            # send email every 30 days
            if day_diff % 30 == 1:
                mail.send(
                    m.person.user.email,
                    config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                    template='leave_association_reminder',
                    context={'name': m.person.user.get_full_name()},
                    priority='medium'
                )
