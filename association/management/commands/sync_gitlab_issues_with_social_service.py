from django.core.management.base import BaseCommand, CommandError
from django.utils import timezone
from gitlab import GitlabGetError

import config
from association.models import SocialService
from workinggroups.models import WorkingGroup

import gitlab
import gitlab.const


class Command(BaseCommand):
    help = 'Synchronises gitlab issues with social services'

    def handle(self, *args, **options):
        # docks: https://python-gitlab.readthedocs.io/en/stable/

        if len(config.GITLAB['access_token']) == 0:
            raise CommandError("Gitlab access token is missing!")

        responsible_working_group = WorkingGroup.objects.filter(group_django__name="network")
        if responsible_working_group.exists():
            responsible_working_group = responsible_working_group.first()
        else:
            responsible_working_group = None

        # private token or personal token authentication
        gl = gitlab.Gitlab('https://git.rwth-aachen.de/', private_token=config.GITLAB['access_token'])

        try:
            gl.auth()
        except GitlabGetError as e:
            if 500 <= e.response_code < 600:
                return  # ignore some internal server error
            raise e

        # create and update social services from issues
        issues = gl.issues.list(labels=[config.GITLAB['social_service_label']],
                                per_page=100,
                                scope='all')
        issues += gl.issues.list(labels=[config.GITLAB['count_as_active_label']],
                                 per_page=100,
                                 scope='all')

        active_issues = []  # to detect issues where the label was removed

        for issue in issues:
            project = gl.projects.get(issue.project_id, lazy=True)
            # check if we are a member of the project
            if len(project.members_all.list(user_ids=[gl.user.id])) == 0:
                # if we are not a member of the project => ignore issue
                continue
            ss, created = SocialService.objects.get_or_create(external_identifier="gitlabId_" + str(issue.id))
            active_issues.append(ss.id)
            if created:
                ss.time = None
            if issue.state == 'opened':  # when somebody reopens the issue
                ss.time = None
            if not created and ss.time and ss.time < timezone.now():
                # we do not change old social services
                continue
            if issue.state == 'closed':
                ss.time = timezone.now()
                editable_issue = project.issues.get(issue.iid, lazy=True)
                editable_issue.notes.create({'body': "Do not forget to mark the social service as verified \
                 [here](https://verwaltung.hilton.rwth-aachen.de/admin/association/socialservice/" + str(ss.id) + ")"})
                for reg in ss.registrations.all():
                    reg.save()

            ss.name = issue.title
            ss.description = issue.description
            if hasattr(issue, 'weight') and issue.weight:
                ss.people_limit = issue.weight
                print(ss.people_limit, issue.weight)
            else:
                ss.people_limit = 3
            ss.counts_as_being_active = True if config.GITLAB['count_as_active_label'] in issue.labels else False
            ss.responsible_working_group = responsible_working_group
            ss.save()
            print("Create or Updated", issue.title.encode('ascii', 'ignore').decode('ascii'))

            if not created:
                for reg in ss.registrations.all():
                    users = gl.users.list(search=reg.user.get_full_name())
                    if len(users) != 1:
                        users = gl.users.list(search=reg.user.email)
                    if len(users) == 1:
                        user = users[0]
                        if user.name.lower() != reg.user.get_full_name().lower():
                            continue
                        project = gl.projects.get(issue.project_id, lazy=True)
                        editable_issue = project.issues.get(issue.iid)

                        if user not in project.members_all.list(all=True):
                            member = project.members.create({'user_id': user.id, 'access_level':
                                                             gitlab.const.REPORTER_ACCESS})

                        assigned = list(u['id'] for u in editable_issue.assignees)
                        if user.id not in assigned:
                            assigned.append(user.id)
                            editable_issue.assignee_ids = assigned
                            editable_issue.save()
                    else:
                        try:
                            project.invitations.create({
                                "email": reg.user.email,
                                "access_level": gitlab.const.AccessLevel.REPORTER
                            })
                        except gitlab.GitlabInvitationError as e:
                            if not "Access level should be greater" in str(e.error_message):
                                raise e

        # alle restlichen offenen social services für die es kein gitlab issue mit label mehr gibt schließen
        for ss in responsible_working_group.socialservice_set.filter(
                external_identifier__startswith="gitlabId_", time__isnull=True).exclude(id__in=active_issues):
            ss.time = timezone.now()
            ss.save()
