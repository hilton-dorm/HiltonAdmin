from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils.timezone import now
from post_office import mail

import config
from association.models import Membership, MembershipTypePeriods, MembershipType
from association.views import membership
from workinggroups.models import WorkingGroup


class Command(BaseCommand):
    help = 'Reports changes of the membership type of members'

    def add_arguments(self, parser):
        parser.add_argument('-d', '--days', help='How many days in the past should be considered', type=int,
                            default=92)

    def handle(self, *args, **options):
        days = options['days']
        members = MembershipTypePeriods.objects.filter(
            start_date__gt=now() - timedelta(days=days)).order_by('-start_date')
        members_by_type = {}
        for type in MembershipType.objects.all():
            m = members.filter(type=type)
            if m.exists():
                members_by_type[type.name] = m

        mail.send(
            WorkingGroup.objects.get(name__icontains='Vorstand').email,
            config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
            template='membership_type_changes',
            context={
                'all_members': members,
                'members_by_type': members_by_type,
                'days': days
            }
        )
