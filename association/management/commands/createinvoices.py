from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db import transaction

from association.models import Membership


class Command(BaseCommand):
    help = 'Create invoices for all members'

    @transaction.atomic
    def handle(self, *args, **options):
        memberships = Membership.objects.all()
        for membership in memberships:
            membership.create_invoice_house_current_month()
            membership.create_invoice_network()

        # Rebalance accounts
        call_command('transfer_membership_transactions')
