from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils.timezone import now
from post_office import mail

import config
from association.models import Membership


class Command(BaseCommand):
    help = 'Iterates over each active member, who was mahned more then 14 days ago and sends a reminder to the Vorstand'

    def handle(self, *args, **options):
        members = Membership.current_memberships().filter(date_last_mahnung_send__lt=now() - timedelta(days=14))

        if len(members) > 0:
            members_without_debts = []
            members_with_debts = []
            for member in members:
                if member.get_balance().amount >= 0:
                    members_without_debts.append(member)
                    member.date_last_mahnung_send = None
                    member.save()
                else:
                    members_with_debts.append(member)

            mail.send(
                config.EMAIL["EMAIL_ACCOUNT_VORSTAND"],
                config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                template='reminder_throw_out_members',
                context={'members_with_debts': members_with_debts,
                         'members_without_debts': members_without_debts,
                         'base_url': config.BASE_URL},
                priority='medium'
            )
