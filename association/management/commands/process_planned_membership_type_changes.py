from django.core.management import call_command
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone

from association.models import Membership, PlannedMembershipTypeChange


class Command(BaseCommand):
    help = 'Processes planned membership type changes'

    @transaction.atomic
    def handle(self, *args, **options):
        for change in PlannedMembershipTypeChange.objects.filter(last_day__lt=timezone.now().today()):
            membership = Membership.get_current_membership_for(change.person)
            if membership:
                membership.type = change.original_membership_type
                membership.save()
            change.delete()

        for change in PlannedMembershipTypeChange.objects.filter(start__lte=timezone.now().date()):
            membership = Membership.get_current_membership_for(change.person)
            if membership:
                change.original_membership_type = membership.type
                change.save()

                membership.type = change.planned_membership_type
                membership.save()

                if change.last_day is None:
                    change.delete()
            else:
                change.delete()
