from django.core.management.base import BaseCommand
from django.db.models import Q
from post_office import mail
from datetime import date, timedelta, timezone

import config
from association.models import Membership, SocialService, Registration
from workinggroups.models import WorkingGroup, GroupMembership


class Command(BaseCommand):
    help = 'Checks if a leader/member of a working group can be active and creates a social service registration'

    def handle(self, *args, **options):

        # die person muss mindestens 60 tage in der AG sein
        base_filter = Q(date_join__lte=date.today() - timedelta(days=60)) & (Q(date_leave__isnull=True) | Q(
            date_leave__gt=date.today() - timedelta(days=config.SOCIAL_SERVICE_PERIOD_DURATION + 60)))
        current_filter = Q(date_join__lte=date.today()) & (Q(date_leave__isnull=True) | Q(date_leave__gt=date.today()))

        def shouldCreateRegistration(membership: 'Membership', ss: 'SocialService'):
            if not membership.type.has_to_do_social_service:
                return False
            ss_period = membership.get_current_social_service_period()
            # create registration only 30 days before the end of a period
            if config.SOCIAL_SERVICE_PERIOD_DURATION - ss_period.duration() + ss_period.extra_days_everybody > 30:
                return False
            # Don't create a new registration if there is already one for the current period
            if ss.registration_set.filter(person=membership.person, social_service_period=ss_period).exists():
                return False
            # Do not create a second registration every x days: If a registration is created but not verified, the
            # social_service_period is still null, so the previous if is False.
            # It can be that the last verification was done on the last day of the previous period, but we want to
            # create a new registration 30 days before the current period ends => `period_duration - 30`
            oldest_verification_date = date.today() - timedelta(days=config.SOCIAL_SERVICE_PERIOD_DURATION - 30)
            if ss.registration_set.filter(person=membership.person, verified_at__gt=oldest_verification_date).exists():
                return False
            return True

        for working_group in WorkingGroup.objects.all():
            social_service, _ = SocialService.objects.get_or_create(name=f"In einer AG aktiv sein ({working_group.name})")
            types = []
            if working_group.first_leader_is_social_service:
                types.append(GroupMembership.TYPE_FIRST_LEADER)
            if working_group.second_leader_is_social_service:
                types.append(GroupMembership.TYPE_SECOND_LEADER)

            for leader in working_group.groupmembership_set.filter(base_filter & Q(type__in=types)):
                membership = Membership.get_current_membership_for(leader.person)
                if membership is None:
                    if leader.date_leave is None or leader.date_leave > date.today():
                        print(leader.person, " is a leader of the ", working_group.name, " but is not active")
                    continue
                if shouldCreateRegistration(membership, social_service):
                    Registration.objects.create(person=leader.person, social_service=social_service, verified=True)

            if working_group.members_can_be_active:
                new_registrations = []
                for member in working_group.groupmembership_set.filter(
                        base_filter & Q(type=GroupMembership.TYPE_MEMBER)):
                    membership = Membership.get_current_membership_for(member.person)
                    if membership is None:
                        continue
                    if shouldCreateRegistration(membership, social_service):
                        registration = Registration.objects.create(person=member.person, social_service=social_service,
                                                                   verified=working_group.members_are_active_by_default,
                                                                   verified_at=date.today() + timedelta(
                                                                       days=4))
                        new_registrations.append(registration)
                        # only for the email template:
                        registration.social_service_period = membership.get_current_social_service_period()

                if len(new_registrations) > 0:
                    leader = working_group.groupmembership_set.filter(
                        current_filter & Q(type=GroupMembership.TYPE_FIRST_LEADER)).first()
                    if leader is None:
                        leader = working_group.groupmembership_set.filter(
                            current_filter & Q(type=GroupMembership.TYPE_SECOND_LEADER)).first()
                    if leader is not None:
                        mail.send(
                            leader.person.user.email,
                            config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                            template='working_group_social_service',
                            context={'registrations': new_registrations,
                                     'working_group': working_group},
                            priority='medium'
                        )
                    else:
                        print("The working group ", working_group.name, " has no first and second leader!")
