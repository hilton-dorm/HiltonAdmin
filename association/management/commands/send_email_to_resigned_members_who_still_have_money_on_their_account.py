from django.core.management.base import BaseCommand
from datetime import date

from django.utils import timezone
from post_office import mail

from association.models import Membership
import config


class Command(BaseCommand):
    help = 'Sends an email to all resigned members who still have money on they account'

    def handle(self, *args, **options):
        for member in Membership.objects.filter(date_leave__lt=timezone.now()):
            amount = member.get_balance().amount
            if amount > 0:
                mail.send(
                    member.person.user.email,
                    config.EMAIL["EMAIL_ACCOUNT_VORSTAND"],
                    template='still_have_balance_reminder',
                    context={
                        'name': member.person.user.get_full_name(),
                        'amount': amount
                    },
                    priority='medium'
                )
