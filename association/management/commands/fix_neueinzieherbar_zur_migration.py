from django.core.management.base import BaseCommand
from datetime import timedelta

import config
from association.models import Membership, SocialService


class Command(BaseCommand):
    help = 'Der wurde bei manchen Personen zur aktuellen Periode zugewiesen sodass diese keinen sozialen Dienst machen mussten. Das soll so nicht sein. Die Zuweisung wieder löschen und richtige Periode zuweisen.'

    def handle(self, *args, **options):
        ss = SocialService.objects.get(name="Neueinzieherbar zur Migration")
        # config.SOCIAL_SERVICE_PERIOD_DURATION + period.extra_days_everybody
        for reg in ss.registration_set.filter(social_service_period__isnull=False):
            membership = Membership.get_current_membership_for(reg.person)
            if not membership:
                continue
            real_period = membership.socialserviceperiod_set.filter(
                start_date__lt=reg.verified_at,
                start_date__gt=reg.verified_at -
                timedelta(
                    days=config.SOCIAL_SERVICE_PERIOD_DURATION)).first()
            if real_period is None:
                # Wurde richtig erstellt, die leute sind erst letztens eingezogen
                continue
            if reg.social_service_period == real_period:
                continue
            if reg.social_service_period.registration_set.count() > 1:
                reg.social_service_period = real_period
                reg.save()
                continue
            next_period = reg.social_service_period.get_next_period()
            to_short = ((next_period.start_date - reg.social_service_period.start_date).days -
                        reg.social_service_period.extra_days_everybody - config.SOCIAL_SERVICE_PERIOD_DURATION) < 0
            if not to_short:
                reg.social_service_period = real_period
                reg.save()
                continue
            if next_period.get_next_period() is not None:
                print("Skip", reg)
                continue

            # adjust start date of next period
            next_period.start_date = reg.social_service_period.start_date + \
                timedelta(days=reg.social_service_period.extra_days_everybody +
                          config.SOCIAL_SERVICE_PERIOD_DURATION + 1)
            next_period.save()
            # move one ss of next period to current one
            ss_to_move = next_period.registration_set.filter(verified=True).first()
            if ss_to_move:
                ss_to_move.social_service_period = reg.social_service_period
                ss_to_move.save()
            # move migration ss to real_period
            reg.social_service_period = real_period
            reg.save()
