from django.core.management.base import BaseCommand

from association.models import Membership


class Command(BaseCommand):
    help = 'Shows current balances of all members'

    def handle(self, *args, **options):
        memberships = Membership.objects.all()

        for membership in memberships:
            balance = membership.get_balance()
            if float(balance) >= 0:
                self.stdout.write(self.style.SUCCESS('%s: %s' % (membership.person, balance)))
            else:
                self.stdout.write(self.style.ERROR('%s: %s' % (membership.person, balance)))
