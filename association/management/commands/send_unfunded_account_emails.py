from django.core.management.base import BaseCommand
from django.utils import timezone
from post_office import mail
from django.db.models import Q

import config
from association.models import Membership


class Command(BaseCommand):
    help = 'Create invoices for all members'

    def handle(self, *args, **options):
        today = timezone.now().today()
        if today.month == 12:
            first = timezone.datetime(today.year + 1, 1, 1)
        else:
            first = timezone.datetime(today.year, today.month + 1, 1)
        days_left = (first - today).days
        # sollen aktuell mitglied sein + zahlen müssen + email bekommen wollen
        memberships = Membership.current_memberships().filter(
            type__has_to_pay_fees=True, person__unfunded_account_warning_days_before__isnull=False)
        for membership in memberships:
            days_warning = membership.person.unfunded_account_warning_days_before
            # skip if wrong day before
            if days_left != days_warning and (membership.skip_internet_fee or days_warning != membership.amount_days_left_of_network_invoice()):
                continue
            invoice_amount = 0
            if days_left <= days_warning:
                invoice_amount = membership.type.membership_fee
            if membership.amount_days_left_of_network_invoice() <= days_warning and not membership.skip_internet_fee:
                invoice_amount = invoice_amount + membership.type.membership_fee_network
            invoices = "House invoice: " + str(membership.type.membership_fee) + "€ in " + str(days_left) + " days\n"
            if not membership.skip_internet_fee:
                invoices = invoices + "Network invoice: " + str(membership.type.membership_fee_network) + "€ in " + str(
                    membership.amount_days_left_of_network_invoice()) + " days\n"
            if membership.get_balance().amount.real < invoice_amount:
                mail.send(
                    membership.person.user.email,
                    config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                    template='unfunded_account_reminder',
                    context={'first_name': membership.person.user.first_name,
                             'balance': membership.get_balance(),
                             'invoice_amount': invoice_amount,
                             'invoices': invoices,
                             'days_before': days_warning},
                    priority='medium'
                )
