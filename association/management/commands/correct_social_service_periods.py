from django.core.management.base import BaseCommand

import config
from association.models import Membership, MembershipType, SocialServicePeriod


class Command(BaseCommand):
    help = 'There were periods that starts at the same date, maybe a bug. This script deletes the duplicated periods'

    def handle(self, *args, **options):
        for membership in Membership.current_memberships().prefetch_related('socialserviceperiod_set'):
            last = None
            for period in membership.socialserviceperiod_set.all():
                if last is not None:
                    if last.start_date == period.start_date:
                        # decide which one we want to delete
                        previous = last.get_previous_period()
                        if previous is None:
                            for r in last.registration_set:
                                r.social_service_period = period
                                r.save()
                            last.delete()
                        else:
                            last_bad = last.registration_set.filter(verified=True, verified_at__isnull=True).exists()
                            current_bad = period.registration_set.filter(
                                verified=True, verified_at__isnull=True).exists()
                            if last_bad or not current_bad:
                                for r in last.registration_set.all():
                                    r.social_service_period = previous
                                    r.save()
                                last.delete()
                            else:
                                for r in period.registration_set.all():
                                    r.social_service_period = previous
                                    r.save()
                                period.delete()

                last = period
