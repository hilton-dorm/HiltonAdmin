from datetime import timedelta

from dateutil.utils import today
from django.core.management.base import BaseCommand
from django.utils.timezone import now
from post_office import mail

import config
from association.models import Membership


class Command(BaseCommand):
    help = 'Iterates over each active member, who is at least 12 months ruhend, has debts and have not got a Mahnung ' \
           'and sends a Mahnung to this member.'

    def handle(self, *args, **options):
        members = Membership.current_memberships().filter(type__name__icontains="limited",
                                                          date_membership_type_since__lt=now() - timedelta(days=365),
                                                          date_last_mahnung_send__isnull=True)

        for member in members:
            balance = member.get_balance()
            if balance.amount < 0:
                mail.send(
                    member.person.user.email,
                    config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                    template='mahnung_12_months_ruhend_with_debts',
                    context={'name': member.person.full_name(),
                             'debts': -balance,
                             'in_14_days': (today() + timedelta(days=14)).date()},
                    priority='medium'
                )
                member.date_last_mahnung_send = now().date()
                member.save()
