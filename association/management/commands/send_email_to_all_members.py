from django.core.management.base import BaseCommand
from post_office import mail

import config
from association.models import Membership


class Command(BaseCommand):
    help = 'Reminder for social services. It does not get triggered in a cron job and has to be executed manually.'

    def handle(self, *args, **options):
        memberships = Membership.objects.all()
        for membership in memberships:
            if membership.is_active():
                mail.send(
                    membership.person.user.email,
                    config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                    template='membership_email',
                    context={'first_name': membership.person.user.first_name},
                    priority='medium'
                )
