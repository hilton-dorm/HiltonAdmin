from django.core.management.base import BaseCommand
from datetime import timedelta, date

import config
from association.models import Membership, MembershipType, SocialServicePeriod


class Command(BaseCommand):
    help = 'Creates new SocialServicePeriods for active members when the duration is over or when a second social service was done'

    def handle(self, *args, **options):
        normal_member = MembershipType.objects.get(name__icontains="ordentlich")

        for membership in Membership.current_memberships().prefetch_related('socialserviceperiod_set').filter(type=normal_member):
            period = membership.get_current_social_service_period()

            # maybe this is a new member and there are Registrations without a period
            for registration in membership.person.registration_set.filter(verified=True,
                                                                          social_service_period__isnull=True,
                                                                          verified_at__lte=date.today()):
                registration.social_service_period = period
                registration.save()

            # if someone do a second social service in the period, we can create a new period so that the member
            # benefits from the second social service, otherwise a second social service within a period is useless
            made_social_services = period.social_services_done()
            if made_social_services.count() > 1:
                latest = made_social_services.latest('verified_at')
                if latest.verified_at != period.start_date:  # do not create a period that starts at the same day
                    new_period = SocialServicePeriod.objects.create(
                        membership=membership, start_date=latest.verified_at)
                    latest.social_service_period = new_period
                    latest.save()
                    continue

            # if the period is longer than 6 Month create a new period
            if period.duration() >= config.SOCIAL_SERVICE_PERIOD_DURATION + period.extra_days_everybody:
                SocialServicePeriod.objects.create(membership=membership, start_date=period.start_date + timedelta(
                    days=config.SOCIAL_SERVICE_PERIOD_DURATION + period.extra_days_everybody + period.days_ruhend()))
                continue
