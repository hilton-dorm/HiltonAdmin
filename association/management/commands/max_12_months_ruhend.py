from datetime import timedelta

from dateutil.utils import today
from django.core.management.base import BaseCommand
from django.utils.timezone import now
from post_office import mail

import config
from association.models import Membership, MembershipType


class Command(BaseCommand):
    help = 'Iterates over each active member, who is at least 12 months ruhend and have a nagative balance counter ' \
           'smaller than tree and makes this members normal again (and sends a mail)'

    def handle(self, *args, **options):
        members = Membership.current_memberships().filter(type__name__icontains="limited",
                                                          date_membership_type_since__lt=now() - timedelta(days=365),
                                                          negative_balance_month_counter__lt=3)
        normal_type = MembershipType.objects.filter(name__icontains="normal").first()
        sponsor_type = MembershipType.objects.filter(name__icontains="sponsor").first()
        for member in members:
            member.type = normal_type if member.person.is_living_here() else sponsor_type
            member.save()
            mail.send(
                member.person.user.email,
                config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                template='max_12_months_ruhend',
                context={'name': member.person.full_name()},
                priority='medium'
            )
