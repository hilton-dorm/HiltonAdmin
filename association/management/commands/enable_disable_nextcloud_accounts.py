from django.core.management.base import BaseCommand
from django.contrib.auth.models import User
from nextcloud import NextCloud
from django.utils import timezone
from django.db.models import Q

import config
from association.models import Membership
from ldap_sync.models import LdapPerson


class Command(BaseCommand):
    help = 'Checks if a user should have access to the cloud and en-/disables the account'

    def add_arguments(self, parser):
        parser.add_argument('-u', '--username', type=str, help='Only sync a specific person', )

    def handle(self, *args, **options):
        username = options['username']

        nxc = NextCloud(endpoint=config.NEXTCLOUD["URL"],
                        user=config.NEXTCLOUD["USERNAME"],
                        password=config.NEXTCLOUD["PASSWORD"],
                        json_output=True)

        if username:
            users = User.objects.filter(username=username)
        else:
            users = User.objects.all()

        for user in users:
            # Searches for the corresponding ldap entry
            try:
                ldap_person = LdapPerson.objects.get(username=user.username, last_name=user.last_name)
            except LdapPerson.DoesNotExist:
                continue
            # Search for a currently valid membership that is not ruhend
            membership = Membership.objects.filter(Q(person=user.person),
                                                   Q(date_join__lte=timezone.now()),
                                                   Q(date_leave__gte=timezone.now()) | Q(date_leave__isnull=True),
                                                   ~Q(type__name__icontains="ruhend")
                                                   ).first()
            if membership is None:
                nxc.disable_user(ldap_person.uuid)
            elif membership.agreed_cloud_storage_terms:
                nxc.enable_user(ldap_person.uuid)
            else:
                nxc.disable_user(ldap_person.uuid)
