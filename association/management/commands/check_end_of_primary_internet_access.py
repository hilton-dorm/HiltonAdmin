from django.core.management.base import BaseCommand
from django.utils import timezone

from association.models import Membership


class Command(BaseCommand):
    help = 'Checks the primary internet access end date and removed the primary internet access if the date is in the past'

    def handle(self, *args, **options):
        memberships = Membership.objects.filter(primary_internet_access_end__lt=timezone.now())
        for membership in memberships:
            membership.primary_internet_access_end = None
            membership.primary_internet_access = False
            membership.save()
