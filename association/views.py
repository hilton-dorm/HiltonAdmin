import datetime
import logging
import math
from datetime import timedelta

from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.core.exceptions import ObjectDoesNotExist
from django.core.management import call_command
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.template import loader
from django.urls import reverse_lazy
from django.utils import timezone
from post_office import mail
from nextcloud import NextCloud

from finances_v2.models import Accounts, BankAccountCSV
from ldap_sync.models import LdapPerson
from django.db.models import Q, Model, F

import config
from association.forms import SkipInternetFeeForm, RegisterForm
from association.models import Membership, SocialService, Registration, is_member_association, MembershipType
from main.models import Tenant
from nordigen_app.models import NordigenCredential

logger = logging.getLogger(__name__)


def next_invoice(date):
    next_invoice_date = date.replace(day=1) + timedelta(days=32)
    return next_invoice_date.replace(day=1)


@login_required
def register(request):
    if Membership.get_current_membership_for(request.user.person):
        return redirect("membership")
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            # Updates the user
            request.user.person.email_distributor = form.cleaned_data['accept_add_email_distributor']
            request.user.person.birthday = form.cleaned_data['birthday_field']
            request.user.person.save()

            # creates membership
            membership = Membership()
            membership.person = request.user.person
            membership.type = MembershipType.objects.filter(name__icontains="normal").first()
            membership.date_join = datetime.date.today()
            membership.date_leave = None
            membership.signed_usage_workshop = form.cleaned_data['accept_terms_workshop']
            membership.signed_usage_fitness = form.cleaned_data['accept_terms_fitness_group']
            membership.signed_usage_network = form.cleaned_data['accept_terms_networking_group']
            membership.signed_usage_symposion = form.cleaned_data['accept_terms_bar']
            membership.save()
            return redirect('membership')
    # Needed so that form errors are shown to the user
    else:
        form = RegisterForm(initial={
            'birthday_field': '',
            'accept_add_email_distributor': "True",
            'accept_terms_networking_group': None,
            'accept_terms_fitness_group': None,
            'accept_terms_workshop': None,
            'accept_terms_bar': None,
        })

    try:
        user_tenant = request.user.person.get_latest_tenant()
    except ObjectDoesNotExist:
        return redirect('profile')

    date_move_in = user_tenant.date_move_in
    user_tenant_type = Tenant.TENANT_TYPES_EN[int(user_tenant.tenant_type)][1]

    template = loader.get_template('association/register.html')
    context = {
        'title': 'Register',
        'form': form,
        'userdata': {
            'Gender': request.user.person.gender,
            'Name': request.user.person.full_name(),
            'E-Mail': request.user.email,
            'Nationality': request.user.person.nationality,
            'Move in Date': date_move_in.strftime("%d.%m.%Y"),
            'Tenant Type': user_tenant_type,
        },
        'membership_type': MembershipType.objects.get(name__icontains="normal"),
        'can_register': request.user.person.get_current_tenant() is not None
    }

    return HttpResponse(template.render(context, request))


@login_required
@user_passes_test(is_member_association, login_url=reverse_lazy('no_permission'))
def finances(request):
    try:
        membership = Membership.objects.filter(person=request.user.person).order_by('-date_join').first()
    except Membership.DoesNotExist:
        membership = None

    try:
        last_update = NordigenCredential.objects.get(account=Accounts().NETWORK_BANK).last_transaction_fetch
    except NordigenCredential.DoesNotExist:
        last_update = datetime.datetime.fromtimestamp(0)

    monthly_fee = 0
    if membership.type.has_to_pay_fees:
        monthly_fee = monthly_fee + membership.type.membership_fee
        if not membership.skip_internet_fee:
            monthly_fee = monthly_fee + membership.type.membership_fee_network

    normal_membership = MembershipType.objects.get(name__icontains="normal")
    template = loader.get_template('association/payments.html')
    accounts = (Accounts().NETWORK_FK, Accounts().NETWORK_EK, Accounts().HOUSE_EK)
    context = {
        'title': 'Payments & Invoices',
        'user': request.user,
        'transactions': request.user.person.transactionentry_set.filter(account__in=accounts, amount__gt=0).order_by(
            "-transaction__date").select_related("account", "transaction"),
        'network_fk_name': Accounts().NETWORK_FK.title,
        'balance': None if not membership else membership.get_balance(),
        'monthly_fee': monthly_fee,
        'normal_monthly_fee': normal_membership.membership_fee + normal_membership.membership_fee_network,
        'last_update': last_update,
        'next_update_in': int(
            math.ceil((last_update - (timezone.now() - datetime.timedelta(minutes=7))).total_seconds() / 60)),
        'is_skipping_network': None if not membership else membership.skip_internet_fee,
        'next_invoice': next_invoice(timezone.localdate()),
        'membership': membership
    }

    return HttpResponse(template.render(context, request))


@login_required
@user_passes_test(is_member_association, login_url=reverse_lazy('no_permission'))
def cloud(request):
    try:
        membership = Membership.objects.filter(person=request.user.person).order_by('-date_join').first()
    except Membership.DoesNotExist:
        membership = None

    nxc = NextCloud(endpoint=config.NEXTCLOUD["URL"],
                    user=config.NEXTCLOUD["USERNAME"],
                    password=config.NEXTCLOUD["PASSWORD"],
                    json_output=True)

    uuid = LdapPerson.objects.get(username=request.user.username).uuid
    nxc_user = nxc.get_user(uuid)
    template = loader.get_template('association/cloud.html')
    context = {
        'title': 'Cloud',
        'user': request.user,
        'nxc_user': nxc_user.data,
        'membership': membership
    }

    return HttpResponse(template.render(context, request))


@login_required
@user_passes_test(is_member_association, login_url=reverse_lazy('no_permission'))
def agree_terms_cloud(request):
    try:
        membership = Membership.objects.filter(person=request.user.person).order_by('-date_join').first()
        membership.agreed_cloud_storage_terms = True
        membership.save()

        call_command('sync_attributes', username=membership.person.user.username)

        messages.success(request, 'Please login again to activate your cloud account.')
        return redirect('logout')
    except Membership.DoesNotExist:
        messages.error(request, 'Something went wrong. :( Please contact the networking group.')
        return redirect('cloud')


@login_required
def social(request):
    social_services_one_time = SocialService.objects.filter(Q(time__gt=timezone.now()) | Q(time__isnull=True),
                                                            counts_as_being_active=False).order_by(
        F('time').asc(nulls_last=True)).select_related("responsible_working_group").prefetch_related("registrations")
    social_services_working_group = SocialService.objects.filter(Q(time__gt=timezone.now()) | Q(time__isnull=True),
                                                                 counts_as_being_active=True).order_by(
        'time').select_related("responsible_working_group").prefetch_related("registrations")
    user_services = SocialService.objects.filter(registration__person=request.user.person)
    user_registrations = Registration.objects.filter(person=request.user.person)

    template = loader.get_template('association/social.html')
    context = {
        'social_services_one_time': social_services_one_time,
        'social_services_working_group': social_services_working_group,
        'user_services': user_services,
        'user_registrations': user_registrations,
        'title': 'Social Service',
        'user': request.user
    }

    return HttpResponse(template.render(context, request))


@login_required
def register_social_service(request, social_id):
    social_service = SocialService.objects.get(id=social_id)

    if social_service.registrations.all().count() >= social_service.people_limit:
        messages.info(request, 'Limit exceeded!')
        return redirect('social')

    if request.user.person in social_service.registrations.all():
        messages.info(request, 'You are already registered!')
        return redirect('social')

    if social_service.time and timezone.now() > social_service.time:
        messages.info(request, 'This event is in the past.')
        return redirect('social')

    registration = Registration.objects.create(person=request.user.person, social_service_id=social_id, verified=False)
    registration.save()
    return redirect('social')


@login_required
def unregister_social_service(request, social_id):
    registration = Registration.objects.filter(person=request.user.person, social_service_id=social_id)
    registration.delete()

    return redirect('social')


@login_required
def membership(request):
    if not is_member_association(request.user):
        return redirect('register')
    try:
        membership = Membership.get_current_membership_for(request.user.person)
        form_internet_fee = SkipInternetFeeForm(instance=membership)
    except Membership.DoesNotExist:
        return redirect('register')

    if 'network_10_euro' in request.POST:
        membership.create_invoice_network_pay_for_no_additional_requirements()
    elif 'network_InternetAccessFine' in request.POST:
        if membership.create_invoice_block_internet_access_fine():
            messages.success(request, 'Your fine is paid.')
        else:
            messages.error(request, 'Your account is not sufficiently funded.')
    elif 'accept_terms_networking' in request.POST:
        membership.signed_usage_network = True
        membership.save()
        messages.success(request, 'Changes saved. You signed the networking group usage terms.')
    elif 'accept_terms_fitness' in request.POST:
        membership.signed_usage_fitness = True
        membership.save()
        messages.success(request, 'Changes saved. You signed the fitness group usage terms.')
    elif 'accept_terms_workshop' in request.POST:
        membership.signed_usage_workshop = True
        membership.save()
        messages.success(request, 'Changes saved. You signed the workshop group usage terms.')
    elif 'accept_terms_bar' in request.POST:
        membership.signed_usage_symposion = True
        membership.save()
        messages.success(request, 'Changes saved. You signed the bar usage terms.')
    elif 'normal_membership' in request.POST:
        if "limited" in membership.type.name.lower() and membership.get_balance().amount >= 0:
            membership.type = MembershipType.objects.filter(name__icontains="normal").first()
            membership.save()
            messages.success(request, 'Changed membership type to normal.')
        else:
            messages.error(request,
                           'You are not a limited member or your balance is negative. Membership type was not changed')
    elif request.method == 'POST':
        old_skip = membership.skip_internet_fee
        form_internet_fee = SkipInternetFeeForm(request.POST, instance=membership)
        form_internet_fee.save()
        if membership.skip_internet_fee != old_skip:
            messages.success(request, f'Changes saved. (You {"started" if membership.skip_internet_fee else "stopped"} '
                                      'skipping internet fee.)')
            # Create a new invoice if skip is removed
            if not membership.skip_internet_fee:
                membership.create_invoice_network()
        else:
            messages.info(request, 'You did not change anything.')

    if request.method == 'POST':
        return HttpResponseRedirect(reverse_lazy('membership'))

    balance = membership.get_balance()

    template = loader.get_template('association/membership.html')
    context = {
        'title': 'Membership',
        'is_member': membership and membership.is_active(),
        'ss_period': membership.get_current_social_service_period(),
        'completed_social_service_current_semester': membership.did_social_service_in_current_social_service_period(),
        'membership': membership,
        'next_invoice': next_invoice(timezone.localdate()),
        'balance': balance,
        'network_fee': membership.type.membership_fee_network,
        'network_fee_total': membership.type.membership_fee_network + 10,
        'account_funded': balance.amount >= membership.type.membership_fee_network,
        'missed_new_tenant_bars': membership.missed_new_tenant_bars(),
        'membership_changes': request.user.person.plannedmembershiptypechange_set.all(),
        'block_internet_access': membership.is_banned_from_network(),
        'block_internet_access_end': membership.block_internet_access_end,
        'block_internet_access_fine': membership.block_internet_access_fine,
        'block_internet_access_reason': membership.block_internet_access_reason,
        'form': form_internet_fee,
    }

    return HttpResponse(template.render(context, request))


@login_required
def leave_association_confirmation(request):
    if request.method != "POST":
        return redirect("membership")

    success = True
    try:
        membership = Membership.objects.filter(person=request.user.person).order_by('-date_join').first()
        if not membership:
            return redirect("membership")
        membership.date_leave = timezone.now()
        membership.save()

        mail.send(
            config.EMAIL['EMAIL_ACCOUNT_HOUSE'],
            config.EMAIL['EMAIL_ACCOUNT_NETWORK'],
            subject='Kündigung ' + str(membership.person),
            message=f'Hi there! {membership.person} mit ID {membership.get_membership_id()} hat gekündigt. '
                    'Ensprechend bitte die Anmeldung markieren/umsortieren.')

        mail.send(
            request.user.email,
            config.EMAIL['EMAIL_ACCOUNT_NETWORK'],
            template='leave_association_confirmation',
            context={'user': request.user},
        )
    except Membership.DoesNotExist:
        success = False
        messages.error(request,
                       'We encountered an error. Please contact network@hilton.rwth-aachen.de!')

    messages.success(request,
                     'You are not a member anymore. If this was an accident please contact ba@hilton.rwth-aachen.de!')

    template = loader.get_template('association/leave_association_confirmation.html')
    context = {
        'title': 'Membership Termination',
        'success': success
    }

    return HttpResponse(template.render(context, request))
