from django.urls import path

from association import views

urlpatterns = [
    path('finances', views.finances, name='finances'),
    path('membership/', views.membership, name='membership'),
    path('register/', views.register, name='register'),
    path('social/', views.social, name='social'),
    path('cloud/', views.cloud, name='cloud'),
    path('agree_terms_cloud/', views.agree_terms_cloud, name='agree_terms_cloud'),
    path('social/register/<int:social_id>', views.register_social_service, name='register_social_service'),
    path('social/unregister/<int:social_id>', views.unregister_social_service, name='unregister_social_service'),
    path('membership/leave_association_confirmation', views.leave_association_confirmation,
         name='leave_association_confirmation')
]
