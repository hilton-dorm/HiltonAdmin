# Generated by Django 2.2.5 on 2020-11-09 17:15

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('association', '0019_auto_20201105_2232'),
    ]

    operations = [
        migrations.AddField(
            model_name='socialservice',
            name='is_new_tenant_bar',
            field=models.BooleanField(default=False, help_text='Is used to determine if a members has attended a new tenant bar.'),
        ),
        migrations.AddField(
            model_name='socialservice',
            name='link_to_participate',
            field=models.URLField(blank=True, default=None, help_text='E.g. a Zoom or Discord link. Or the link to an public gitlab issue.', max_length=256, null=True),
        ),
    ]
