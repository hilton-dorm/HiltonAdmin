# Generated by Django 2.2.5 on 2019-10-04 12:46

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('association', '0004_membership_negative_balance_month_counter'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='socialservice',
            options={'ordering': ['-date']},
        ),
        migrations.RenameField(
            model_name='socialservice',
            old_name='time',
            new_name='date',
        ),
    ]
