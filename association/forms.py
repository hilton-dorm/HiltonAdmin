from bootstrap_datepicker_plus.widgets import DatePickerInput
from django import forms
from datetime import date
from django.core.exceptions import ValidationError

from association.models import Membership, Person, Tenant


def calculate_age(born):
    today = date.today()
    return today.year - born.year - ((today.month, today.day) < (born.month, born.day))


def AgeValidator(value):
    if calculate_age(value) < 16:
        raise ValidationError("You should be at least 16 years old.")


class SkipInternetFeeForm(forms.ModelForm):
    class Meta:
        model = Membership
        fields = ('skip_internet_fee',)


class RegisterForm(forms.Form):
    YesNoChoice = [
        ('True', "Debug: accept"),
        ('False', "Debug: decline"),
    ]

    birthday_field = forms.DateField(label="Birthday", help_text="We need your date of birth. Won't be shared publicly.",
                                     initial="", required=True, validators=[AgeValidator])

    # accept_X are shown with red and green button
    accept_add_email_distributor = forms.ChoiceField(choices=YesNoChoice, widget=forms.RadioSelect, required=True,
                                                     initial=1,
                                                     label="Include E-Mail in distributor",
                                                     help_text="Accept if you want to receive E-Mails from our dorm email distributor, which is accessible by all tenants.<br>Your E-Mail address will not be visible to others.")

    accept_terms_networking_group = forms.ChoiceField(choices=YesNoChoice, widget=forms.RadioSelect, required=True,
                                                      label="Networking Group", help_text="I hereby confirm a responsible use of all services of the Netzwerk-AG.<br>In particular, I acknowledge the network regulations with my signature.")

    accept_terms_fitness_group = forms.ChoiceField(choices=YesNoChoice, widget=forms.RadioSelect, required=True,
                                                   label="Fitness Group", help_text="I hereby confirm that I will only use the fitness room once instructed on a new tenant bar.<br>I am aware that the use of the room is at my own risk.<br>With my signature, I especially accept the AG regulations of the Fitness-AG.")

    accept_terms_workshop = forms.ChoiceField(choices=YesNoChoice, widget=forms.RadioSelect, required=True,
                                              label="Workshop", help_text="I accept the terms and conditions of the Workshop.<br>I acknowledge that the usage of the Workshop is only allowed after an introduction by a member and happens at my own risk.<br>I also acknowledge that willfully or neglectfully destroyed tools must be replaced at my own cost.")

    accept_terms_bar = forms.ChoiceField(choices=YesNoChoice, widget=forms.RadioSelect, required=True,
                                         label="Bar / Symposion / Pub Group", help_text="I accept the terms and conditions of the Bar. I acknowledge that the usage of the Bar outside of planed events has to be requested and happens at my own risk.")

    def is_valid(self):
        return self.is_bound and not self.errors
