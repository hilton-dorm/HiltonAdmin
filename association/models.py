import calendar
import logging
from datetime import date, timedelta, datetime
from decimal import Decimal
from typing import List

from auditlog.registry import auditlog
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import Q, Window, F, CASCADE, RESTRICT, QuerySet
from django.db.models.functions import Lead
from django.db.models.signals import post_save, pre_delete, post_init
from django.dispatch import receiver
from django.urls import reverse
from django.utils import timezone
from django_cte import With, CTEManager
from djmoney.money import Money
from nextcloud import NextCloud
from post_office import mail

import config
import finances_v2.models
from common.util.membership_validator import ensure_one_membership_at_any_time
from finances.models import Account, Transaction, SubAccount
from finances_v2.models import TransactionEntry, Accounts, TransactionTypes, Transaction as NewTransaction
from ldap_sync.models import LdapPerson
from main.models import Person, Tenant
from workinggroups.models import WorkingGroup, KeyHolder


class MembershipType(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    membership_fee = models.DecimalField(max_digits=10, decimal_places=2, blank=False, null=False, default=2)
    membership_fee_network = models.DecimalField(max_digits=10, decimal_places=2, blank=False, null=False, default=4)
    internet_allowed = models.BooleanField(blank=False, null=False, default=True)
    has_to_pay_fees = models.BooleanField(blank=False, null=False, default=True)
    has_to_do_social_service = models.BooleanField(blank=False, null=False, default=True)
    memberships = models.ManyToManyField(
        Person,
        through='Membership',
    )

    def __str__(self):
        return self.name


class Membership(models.Model):
    def validate_date_leave(date):
        # is not used anymore, but needed to run migrations :(
        if date > timezone.localdate():
            raise ValidationError(
                "Members have to cancel their contract by their own. Thus, this date cannot be in the future.")

    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    type = models.ForeignKey(MembershipType, on_delete=models.RESTRICT)
    date_join = models.DateField(default=date.today)
    date_leave = models.DateField(blank=True, null=True)
    date_membership_type_since = models.DateField(blank=True, null=True)
    date_last_mahnung_send = models.DateField(blank=True, null=True,
                                              help_text="Clear this field if after the last Mahnung the person was not thrown out.")
    skip_internet_fee = models.BooleanField(blank=False, null=False, default=False)
    negative_balance_month_counter = models.IntegerField(blank=False, null=False, default=0,
                                                         help_text="Ruhendes Mitglied if not payed for three month. "
                                                                   "This counter is adjusted each month and used to determine the current state.")
    primary_internet_access = models.BooleanField(blank=False, null=False, default=False)
    primary_internet_access_note = models.CharField(max_length=255, blank=True)
    primary_internet_access_end = models.DateField("Last Day with primary internet access", blank=True, null=True)
    block_internet_access = models.BooleanField(blank=False, null=False, default=False)
    block_internet_access_reason = models.CharField(max_length=255, blank=True)
    block_internet_access_fine = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=False, default=0,
                                                     help_text="The fine that has to be paid to get internet access "
                                                               "back. 0€ means no fine.")
    block_internet_access_end = models.DateField("Last Day with blocked internet access", blank=True, null=True,
                                                 help_text="Leave empty if the block is permanent or "
                                                           "ends with paying the fine.")
    signed_usage_workshop = models.BooleanField(blank=False, null=False, default=False)
    signed_usage_fitness = models.BooleanField(blank=False, null=False, default=False)
    signed_usage_network = models.BooleanField(blank=False, null=False, default=False)
    signed_usage_symposion = models.BooleanField(blank=False, null=False, default=False)
    agreed_cloud_storage_terms = models.BooleanField(blank=False, null=False, default=False)
    agreed_k14ag_terms = models.BooleanField(blank=False, null=False, default=False)

    # from https://stackoverflow.com/questions/31858418/django-update-date-automatically-after-a-value-change
    def __init__(self, *args, **kwargs):
        super(Membership, self).__init__(*args, **kwargs)
        # original werte speichern um beim Speichern festzustellen ob sich was geändert hat
        self._type_id = self.type_id
        self._agreed_cloud_storage_terms = self.agreed_cloud_storage_terms

    def save(self, *args, **kwargs):
        if self.type_id != self._type_id:
            self.date_membership_type_since = timezone.now()
            self.date_last_mahnung_send = None  # Clear the 'last Mahnung send field' when the Membership types changes
            if "update_fields" in kwargs:
                kwargs["update_fields"] = {"date_membership_type_since", "date_last_mahnung_send"}.union(kwargs["update_fields"])
        if self.agreed_cloud_storage_terms != self._agreed_cloud_storage_terms:
            try:
                ldap_person = LdapPerson.objects.get(username=self.person.user.username,
                                                     last_name=self.person.user.last_name)
                nxc = NextCloud(endpoint=config.NEXTCLOUD["URL"],
                                user=config.NEXTCLOUD["USERNAME"],
                                password=config.NEXTCLOUD["PASSWORD"],
                                json_output=True)
                if self.agreed_cloud_storage_terms:
                    nxc.enable_user(ldap_person.uuid)
                else:
                    nxc.disable_user(ldap_person.uuid)
            except LdapPerson.DoesNotExist:
                pass

        super(Membership, self).save(*args, **kwargs)
        if self.type_id != self._type_id and self.type.has_to_pay_fees:
            # For example the member was not a normal member before but now is one
            # Create transactions so that the member directly gets internet if all requirements are fulfilled
            self.create_invoice_house_current_month()
            self.create_invoice_network()

    def __str__(self):
        return self.person.full_name()

    def clean(self):
        ensure_one_membership_at_any_time(self, Membership.objects)
        if self.id is None and "limited" in self.type.name.lower():
            raise ValidationError(
                "You can not create a new membership that is limited. You can only change an active membership to a limited one. To make a membership limited in the future use 'Planned Membership Type Changes'")

    def get_membership_id(self):
        # We append zeros without any deeper meaning in it
        return str(self.id).rjust(5, '0')

    def get_receivables_house_v1(self):
        account_house = Account.objects.select_related('subaccount_receivables').get(title="House")
        return account_house.subaccount_receivables.get_balance(self.person)

    def get_receivables_house(self):
        return Money(Accounts().HOUSE_RECEIVABLES_TENANTS.get_balance(person=self.person), "EUR")

    def get_receivables_network_v1(self):
        account_house = Account.objects.select_related('subaccount_receivables').get(title="Network")
        return account_house.subaccount_receivables.get_balance(self.person)

    def get_receivables_network(self):
        return Money(Accounts().NETWORK_RECEIVABLES_TENANTS.get_balance(person=self.person), "EUR")

    def get_balance_v1(self):
        account_network = Account.objects.select_related('subaccount_outsite_capital', 'subaccount_receivables').get(
            title="Network")
        account_house = Account.objects.select_related('subaccount_receivables').get(title="House")
        return account_network.subaccount_outsite_capital.get_balance(self.person) \
            - account_network.subaccount_receivables.get_balance(self.person) \
            - account_house.subaccount_receivables.get_balance(self.person)

    def get_guthaben(self):
        return Money(Accounts().NETWORK_FK.get_balance(person=self.person), "EUR")

    def get_balance(self):
        return self.get_guthaben() - self.get_receivables_network() - self.get_receivables_house()

    def get_transactions_v1(self, include_payments=True, include_invoices=True,
                            date_month=None, date_year=None, only_network=False):
        account_network = Account.objects.select_related('subaccount_receivables', 'subaccount_outsite_capital') \
            .get(title="Network")

        filter_subaccounts = Q()
        if include_invoices:
            filter_subaccounts = filter_subaccounts | Q(subaccount=account_network.subaccount_receivables)
            if not only_network:
                account_house = Account.objects.select_related('subaccount_receivables').get(title="House")
                filter_subaccounts |= Q(subaccount=account_house.subaccount_receivables)

        if include_payments:
            filter_subaccounts = filter_subaccounts | Q(subaccount=account_network.subaccount_outsite_capital)

        transactions = self.person.transaction_set.order_by('-date').filter(filter_subaccounts, amount__gte=0)
        transactions = transactions.filter(date__month=date_month) if date_month else transactions
        transactions = transactions.filter(date__year=date_year) if date_year else transactions
        return transactions

    def get_payments_v1(self):
        return self.get_transactions_v1(include_payments=True, include_invoices=False)

    def get_invoices_v1(self, date_month=None, date_year=None):
        return self.get_transactions_v1(include_payments=False, include_invoices=True, date_year=date_year,
                                        date_month=date_month)

    def get_latest_network_invoice_v1(self):
        return self.get_transactions_v1(include_payments=False, include_invoices=True, only_network=True).filter(
            note="#network").order_by("-date").first()

    def get_latest_network_invoice(self) -> TransactionEntry:
        return Accounts().NETWORK_EK.entries.filter(person=self.person, type=TransactionTypes().NETWORK_FEE).order_by("-transaction__date").first()

    def amount_days_left_of_network_invoice(self):
        # The network invoice grants network permissions for the amount of days of the month the invoice is generated
        invoice_latest = self.get_latest_network_invoice()

        if not invoice_latest:
            return 0

        invoice_valid_until = self.network_invoice_valid_until(invoice_latest)
        # Check whether the invoice is still valid
        if timezone.localdate() < invoice_latest.transaction.date or timezone.localdate() > invoice_valid_until:
            return 0
        return (invoice_valid_until - timezone.localdate()).days + 1

    def network_invoice_valid_until(self, invoice=None):
        if not invoice:
            invoice = self.get_latest_network_invoice()

        # If there is no invoice generated yet
        if not invoice:
            return None

        # Thus, first calculate the amount of days in the corresponding month
        amount_days_in_month = calendar.monthrange(invoice.transaction.date.year, invoice.transaction.date.month)[1]
        return invoice.transaction.date + timedelta(days=amount_days_in_month - 1)

    def is_already_invoice_network_running(self):
        return self.amount_days_left_of_network_invoice() > 0

    def is_already_invoice_house_current_month_v1(self):
        for invoice in self.get_invoices_v1(date_month=timezone.now().month, date_year=timezone.now().year):
            if "#house" in invoice.note:
                return True
        return False

    def is_already_invoice_house_current_month(self):
        return Accounts().HOUSE_EK.entries.filter(person=self.person, type=TransactionTypes().HOUSE_FEE,
                                                  transaction__date__month=timezone.now().month, transaction__date__year=timezone.now().year).exists()

    @staticmethod
    def get_current_membership_for(person) -> 'Membership' or None:
        return Membership.current_memberships().filter(person=person).first()

    @staticmethod
    def current_memberships(time_point=None):
        if time_point is None:
            time_point = timezone.now()  # don't use timezone.now() as default argument. It is executed only the first time
        return Membership.objects.filter(Q(date_join__lte=time_point) &
                                         (Q(date_leave__isnull=True) | Q(date_leave__gt=time_point)))

    @staticmethod
    def create_transaction_v1(subaccount, amount, note, membership):
        transaction = Transaction.objects.create(subaccount=subaccount,
                                                 person=membership.person,
                                                 amount=amount,
                                                 note=note,
                                                 include_in_turnover=True)
        transaction.save()

    def _network_fk_to_receivables(self, amount):
        NewTransaction.create(person=self.person, entries=[TransactionEntry(amount=-amount, account=Accounts().NETWORK_FK),
                                                           TransactionEntry(amount=-amount, account=Accounts().NETWORK_RECEIVABLES_TENANTS)])

    def _network_fk_to_house_receivables(self, amount):
        NewTransaction.create(person=self.person, entries=[TransactionEntry(amount=-amount, account=Accounts().NETWORK_FK),
                                                           TransactionEntry(amount=-amount, account=Accounts().HOUSE_RECEIVABLES_TENANTS)])

    @transaction.atomic
    def transfer_money_v1(self, account_network, account_house, target_account, amount):
        """
        Transfers money from the outsite capital account to the house or network account and reduces the receivables
        account
        :param account_network: The network account object
        :param account_house: The house account object
        :param target_account: The network or house account object
        :param amount: The amount to transfer, must be > 0
        """
        assert amount > 0, f"The amount must be > 0 but was {amount}"
        assert target_account == account_house or target_account == account_network
        Transaction.objects.create(person=self.person,
                                   amount=-amount,
                                   subaccount=target_account.subaccount_receivables,
                                   include_in_turnover=False)
        Transaction.objects.create(person=self.person,
                                   amount=-amount,
                                   subaccount=account_network.subaccount_outsite_capital,
                                   include_in_turnover=False)
        # Transactions are not transferred to the liquidity account of the house but
        # to the special reserves of the networking group
        target_subaccount = account_network.subaccount_reserves_special if target_account == account_house else account_network.subaccount_liquidity
        Transaction.objects.create(person=self.person,
                                   amount=amount,
                                   subaccount=target_subaccount,
                                   include_in_turnover=False)

    @transaction.atomic
    def relief_receivables(self):
        available = Accounts().NETWORK_FK.get_balance(person=self.person)
        network_debts = self.get_receivables_network().amount
        house_debts = self.get_receivables_house().amount
        if available > 0:
            each = available / 2 if house_debts > 0 and network_debts > 0 else available
            if house_debts > 0:
                assert each < house_debts
                self._network_fk_to_house_receivables(each)
                house_debts -= each
            if network_debts > 0:
                assert each < network_debts
                self._network_fk_to_receivables(each)
                network_debts -= each
        # Now remove the remaining receivables
        if house_debts > 0:
            NewTransaction.create(person=self.person, note="Erlassen von Forderungen",
                                  entries=[TransactionEntry(amount=-house_debts, account=Accounts().HOUSE_EK, type=TransactionTypes().WAIVED_RECEIVABLES),
                                           TransactionEntry(amount=-house_debts, account=Accounts().HOUSE_RECEIVABLES_TENANTS)])
        if network_debts > 0:
            NewTransaction.create(person=self.person, note="Erlassen von Forderungen",
                                  entries=[TransactionEntry(amount=-network_debts, account=Accounts().NETWORK_EK, type=TransactionTypes().WAIVED_RECEIVABLES),
                                           TransactionEntry(amount=-network_debts, account=Accounts().NETWORK_RECEIVABLES_TENANTS)])

    @transaction.atomic
    def create_invoice_registration_fee_v1(self):
        account_network = Account.objects.get(title="Network")
        account_house = Account.objects.get(title="House")
        if self.type.has_to_pay_fees:
            self.create_transaction_v1(account_house.subaccount_receivables, Money(5, 'EUR'),
                                       '#house registration fee', self)
            self.create_transaction_v1(account_network.subaccount_receivables, Money(5, 'EUR'),
                                       '#network registration fee', self)

    def _create_network_receivable(self, amount, tx_type, note, tx_date=None):
        NewTransaction.create(person=self.person, note=note, date=tx_date, entries=[TransactionEntry(account=Accounts().NETWORK_EK, amount=amount, type=tx_type),
                                                                                    TransactionEntry(account=Accounts().NETWORK_RECEIVABLES_TENANTS, amount=amount)])

    def _create_house_receivable(self, amount, tx_type, note, tx_date=None):
        NewTransaction.create(person=self.person, note=note, date=tx_date, entries=[TransactionEntry(account=Accounts().HOUSE_EK, amount=amount, type=tx_type),
                                                                                    TransactionEntry(account=Accounts().HOUSE_RECEIVABLES_TENANTS, amount=amount)])

    @transaction.atomic()
    def create_invoice_registration_fee(self):
        if self.type.has_to_pay_fees:
            self._create_house_receivable(5, TransactionTypes().REGISTRATION_FEE, "#house registration fee", self.date_join)
            self._create_network_receivable(5, TransactionTypes().REGISTRATION_FEE, "#network registration fee", self.date_join)

    @transaction.atomic
    def create_invoice_network(self, pay_fee_if_not_funded=False):
        if not self.is_already_invoice_network_running() \
                and self.fulfill_basic_network_invoice_requirements(dont_check_balance=pay_fee_if_not_funded) \
                and self.fulfill_additional_network_invoice_requirements():
            self._create_network_receivable(self.type.membership_fee_network, TransactionTypes().NETWORK_FEE, "#network")
            if pay_fee_if_not_funded:
                self._create_network_receivable(2, TransactionTypes().MANUAL_TRANSFER_RECEIPT_CHECK_FEE,
                                                "Fee for manual checking transfer receipt")

    @transaction.atomic
    def create_invoice_network_pay_for_no_additional_requirements(self):
        if not self.is_already_invoice_network_running() \
                and self.fulfill_basic_network_invoice_requirements():
            self._create_network_receivable(10, TransactionTypes().NETWORK_REQUIREMENTS_NOT_FULFILLED, "#network requirements not fulfilled")
            self._create_network_receivable(self.type.membership_fee_network, TransactionTypes().NETWORK_FEE, "#network")

    @transaction.atomic
    def create_invoice_block_internet_access_fine(self, dont_check_balance=False) -> bool:
        # only return False if the user has not enough money to pay the fine
        if self.block_internet_access and self.block_internet_access_fine > 0:
            if self.get_balance().amount < self.block_internet_access_fine and not dont_check_balance:
                return False
            self._create_network_receivable(amount=self.block_internet_access_fine,
                                            tx_type=TransactionTypes().BLOCK_INTERNET_ACCESS_FINE,
                                            note="#network internet access fine")
            self.block_internet_access_fine = 0
            if self.block_internet_access_end is None:  # unblock if no end date is set
                self.block_internet_access = False
            self.save()
        return True

    @transaction.atomic
    def create_invoice_house_current_month(self):
        if not self.is_already_invoice_house_current_month() \
                and self.is_active() \
                and self.type.has_to_pay_fees:
            self._create_house_receivable(self.type.membership_fee, TransactionTypes().HOUSE_FEE, "#house")
            # Adjust "negative balance counter": if over three -> ruhendes mitglied
            self.update_negative_balance_month_counter()

    def get_list_of_unpaied_transaction_entries(self, account: finances_v2.models.Account) -> (List[TransactionEntry], Decimal, bool):
        """
        Returns a list of transactions where no corresponding negative transaction was found (unpaid transactions)
        :param subaccount: The subaccount to check, normally subaccount_receivables
        :return: The list of unpaid transactions and a bool indicating if the matching failed (in case of failure, all
                 transactions are returned) and the amount that is left over when transactions can only be half matched
        """
        all_transaction_entries = account.entries.filter(person=self.person).order_by("transaction__date", "transaction__id")
        transaction_queue = []
        amount = 0
        # Transaction list should be something like: +2, +2, +5, -9, +2, -2, +4, -4, ...
        for transaction in all_transaction_entries:
            if transaction.amount > 0:  # the house/network group requested money
                transaction_queue.append(transaction)
            elif transaction.amount < 0:  # the person paid something
                amount += -transaction.amount
                while amount > 0 and len(transaction_queue) > 0:  # while we can match money to transactions
                    if amount >= transaction_queue[0].amount:
                        amount -= transaction_queue[0].amount
                        transaction_queue.pop(0)
                    else:
                        # failed to match transactions: amount > 0 but amount < transaction_queue[0].amount
                        break
        if amount > 0 and len(transaction_queue) == 0:  # error failed to match, money left over but no transactions
            return all_transaction_entries, None, True
        if len(transaction_queue) > 0:
            assert transaction_queue[0].amount > amount, "Should have been matched"
        return transaction_queue, amount, False

    def is_active(self):
        return self.date_join <= timezone.localdate() and (self.date_leave is None or self.date_leave >= timezone.localdate())

    def has_joined_recently(self):
        return self.date_join >= timezone.localdate() - timezone.timedelta(days=14)

    def missed_new_tenant_bars(self):
        return SocialService.new_tenant_bars_since(self.person.get_earliest_move_in_date())

    def missed_new_tenant_bar_too_often(self):
        return self.missed_new_tenant_bars() >= 3

    def get_current_social_service_period(self) -> 'SocialServicePeriod':
        return self.socialserviceperiod_set.last()

    def did_social_service_in_current_social_service_period(self):
        return self.get_current_social_service_period().has_done_social_service()

    def get_previous_social_service_period(self) -> 'SocialServicePeriod':
        return self.get_current_social_service_period().get_previous_period()

    def did_social_service_in_previous_social_service_period(self):
        period = self.get_previous_social_service_period()
        return True if period is None else period.has_done_social_service()

    def get_latest_verified_social_service(self):
        registration = Registration.objects.filter(person=self.person, verified=True).order_by('verified_at').last()
        return None if registration is None else registration.social_service

    def get_network_access_notes(self):
        if not self.is_active():
            return "left association"

        notes = ""
        if self.is_banned_from_network():
            notes += "banned from network ("
            if self.block_internet_access_end is not None:
                notes += "until " + str(self.block_internet_access_end) + ", "
            if self.block_internet_access_fine > 0:
                notes += "fine: " + str(self.block_internet_access_fine) + "€, "
            notes += "reason: " + self.block_internet_access_reason + "), "
        if self.primary_internet_access:
            notes += "network access manually activated, "
        if self.has_joined_recently():
            notes += "joined recently, "
        if not self.type.internet_allowed:
            notes += "type does not allow network access, "
        if self.skip_internet_fee:
            notes += "skips network access, "
        if self.is_already_invoice_network_running():
            notes += "Network payment valid for the next  " + \
                str(self.amount_days_left_of_network_invoice()) + " days, "
        if not self.is_already_invoice_network_running():
            notes += "No network invoice running"
        if not self.person.attended_new_tenant_bar() and self.missed_new_tenant_bar_too_often():
            notes += "missed the new tenant bar, "
        if not self.signed_usage_fitness:
            notes += "did not sign fitness room usage, "
        if not self.signed_usage_network:
            notes += "did not sign network usage, "
        if not self.signed_usage_workshop:
            notes += "did not sign workshop usage, "
        if not self.signed_usage_symposion:
            notes += "did not sign symposion usage, "
        previous_period = self.get_previous_social_service_period()
        if previous_period is not None and not previous_period.has_done_social_service():
            notes += "did no social service in the previous period, "

        # Removes last ","
        if notes.endswith(", "):
            notes = notes[:-2]
        return notes

    def has_network_access(self):

        if self.is_banned_from_network():
            return False

        # The first 14 days everybody gets internet access
        if self.primary_internet_access or self.has_joined_recently():
            return True

        if self.is_active() and self.signed_usage_network and \
                self.is_already_invoice_network_running():
            return True

        return False

    def is_banned_from_network(self):
        if self.block_internet_access:
            # block_internet_access_end is reached
            if self.block_internet_access_end is not None and self.block_internet_access_end < timezone.now().date():
                if self.block_internet_access_fine > 0:
                    # only delete time requirement, fine has still to be paid
                    self.block_internet_access_end = None  # housekeeping
                    self.save()
                    return True
                else:
                    # block time is over and fine is paid or there is no fine -> unblock
                    self.block_internet_access_end = None  # housekeeping
                    self.block_internet_access = False
                    self.save()
                    return False
            return True
        return False

    def fulfill_basic_network_invoice_requirements(self, dont_check_balance=False):
        return (dont_check_balance or self.get_balance().amount >= self.type.membership_fee_network) and \
            self.signed_usage_network and \
            self.type.internet_allowed and \
            not self.skip_internet_fee and \
            self.is_active() and \
            not self.is_banned_from_network()

    def fulfill_additional_network_invoice_requirements(self):
        return (self.person.attended_new_tenant_bar() or not self.missed_new_tenant_bar_too_often()) and \
            self.did_social_service_in_previous_social_service_period()

    def update_negative_balance_month_counter(self):
        limited_membership_type = MembershipType.objects.get(name="Ruhendes Mitglied (limited member)")

        # ignore newly moved in tenants or already moved out ones
        if self.has_joined_recently() or not self.is_active():
            return

        if self.get_balance().amount.real < 0:
            self.negative_balance_month_counter = self.negative_balance_month_counter + 1

        else:
            self.negative_balance_month_counter = 0

        if self.negative_balance_month_counter == 2:
            mail.send(
                self.person.user.email,
                config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                template='unfunded_account_warning',
                context={'first_name': self.person.user.first_name},
                priority='medium'
            )

        if self.negative_balance_month_counter >= 3 and self.type is not limited_membership_type:
            self.type = limited_membership_type
            mail.send(
                self.person.user.email,
                config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                template='membership_type_changed_to_limited',
                context={'first_name': self.person.user.first_name},
                priority='medium'
            )
        self.save()


class PlannedMembershipTypeChange(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    planned_membership_type = models.ForeignKey(MembershipType, on_delete=models.RESTRICT, related_name='+')
    original_membership_type = models.ForeignKey(MembershipType, on_delete=models.RESTRICT, related_name='+', blank=True, null=True)
    start = models.DateField(help_text="On which date the membership should be changed")
    last_day = models.DateField(blank=True, null=True)

    def __str__(self):
        return f"{self.person} - {self.planned_membership_type} from {self.start} to {self.last_day}"

    def save(self, **kwargs):
        if self.last_day and self.last_day < self.start:
            raise ValidationError("Last day must be greater than start date")
        super().save(**kwargs)


class PeriodsManager(models.Manager):
    def get_queryset(self):
        # get the end date of the period, oldest period first, newest last
        cte = With(
            QuerySet(self.model)
            .annotate(
                end_date=Window(
                    partition_by=[F('membership_id')],
                    expression=Lead('start_date'),
                    order_by=F('start_date').asc()
                )
            ),
            base_manager=self.model.plain_objects
        )
        return cte.queryset().with_cte(cte).order_by("start_date")


class MembershipTypePeriods(models.Model):
    objects = PeriodsManager()
    plain_objects = CTEManager()
    start_date = models.DateField(default=date.today)
    type = models.ForeignKey(MembershipType, on_delete=RESTRICT)
    membership = models.ForeignKey(Membership, on_delete=CASCADE)

    @staticmethod
    @receiver(post_init, sender=Membership)
    def membership_initialized(sender, instance, **kwargs):
        instance._period_type_id = instance.type_id if hasattr(
            instance, 'type_id') else None  # save original value to detect change

    @staticmethod
    @receiver(post_save, sender=Membership)
    def membership_saved(sender, instance, created, **kwargs):
        if created or hasattr(instance, 'type_id') and instance.type_id != instance._period_type_id:
            MembershipTypePeriods.objects.create(type_id=instance.type_id, membership=instance)
            instance._period_type_id = instance.type_id


class SocialServicePeriod(models.Model):
    objects = PeriodsManager()
    plain_objects = CTEManager()
    start_date = models.DateField(default=date.today)
    membership = models.ForeignKey(Membership, on_delete=CASCADE)
    extra_days_everybody = models.IntegerField(blank=False, null=False, default=0)

    def clean(self):
        # the implementation assumes the following
        if self.start_date > date.today():
            raise ValidationError('The SocialServicePeriod can not start in the future')

    def __str__(self):
        if hasattr(self, "end_date"):
            end_date = self.end_date
        else:
            next_period = self.get_next_period()
            end_date = "is not yet clear (current period)" if next_period is None else next_period.start_date
        return f"period {self.id} from {self.start_date} to {end_date}"

    @staticmethod
    @receiver(post_save, sender=Membership)
    def on_new_membership(sender, instance, created, **kwargs):
        if created:
            SocialServicePeriod.objects.create(membership=instance, start_date=instance.date_join)

    def get_next_period(self):
        return SocialServicePeriod.objects.filter(membership=self.membership, start_date__gt=self.start_date).first()

    def get_previous_period(self):
        previous_period = SocialServicePeriod.objects.filter(membership=self.membership,
                                                             start_date__lt=self.start_date).last()
        if previous_period is not None:
            previous_period.end_date = self.start_date
        return previous_period

    def days_ruhend(self):
        # maybe a period starts or ends while the member is ruhend (or not normal), so also get the edge periods
        periods = MembershipTypePeriods.objects.filter(membership=self.membership,
                                                       start_date__lt=timezone.now() if self.end_date is None else self.end_date, type__has_to_do_social_service=False)
        periods = list(periods)
        # do 'end_date__gt=self.start_date' which is not possible because end_date is computed by a window function
        if len(periods) == 0:
            return 0
        last = periods[-1]
        if last.end_date is None:
            last.end_date = date.today()  # maybe the last period is the current one, then the end date is missing
        periods = [period for period in periods if period.end_date > self.start_date]
        days = 0
        end_date = date.today() if self.end_date is None else self.end_date  # the current period has no end_date
        for period in periods:
            days += (min(period.end_date, end_date) - max(period.start_date, self.start_date)).days
        return days

    def duration(self):
        end_date = date.today() if self.end_date is None else self.end_date
        return (end_date - self.start_date).days - self.days_ruhend()

    def social_services_done(self):
        return Registration.objects.filter(
            Q(person=self.membership.person) & Q(verified=True) & Q(social_service_period=self) & (
                Q(verified_at__isnull=True) | Q(verified_at__lte=date.today())))

    def has_done_social_service(self):
        return self.social_services_done().exists()

    def estimated_end(self):
        if self.end_date is not None:
            return self.end_date
        if not self.membership.type.has_to_do_social_service:
            return str(config.SOCIAL_SERVICE_PERIOD_DURATION - self.duration() +
                       self.extra_days_everybody) + " days after the membership is normal again"
        return self.start_date + timedelta(days=config.SOCIAL_SERVICE_PERIOD_DURATION +
                                           self.extra_days_everybody + self.days_ruhend())


def today():
    return timezone.now().strftime('%m%d%y')


class SocialService(models.Model):
    name = models.CharField(max_length=255, default='')
    description = models.TextField(blank=True)
    registrations = models.ManyToManyField(
        Person,
        through='Registration',
    )
    time = models.DateTimeField(default=None, null=True, blank=True,
                                help_text="This can be set in the far future if it is not time critical")
    responsible_working_group = models.ForeignKey(WorkingGroup, on_delete=models.SET_NULL,
                                                  blank=True,
                                                  null=True,
                                                  help_text="The responsible working group will receive an email whenever a new registration is done."
                                                  )

    people_limit = models.IntegerField(default=124,
                                       help_text="Some tasks do not require a limit, set it to 124 then.")

    counts_as_being_active = models.BooleanField(default=False,
                                                 help_text="Some services require more work or belong to the tasks of a AG. "
                                                           "If set TRUE, the person accepts to become a member of the corresponding group.")

    show_as_event = models.BooleanField(default=False,
                                        help_text="Parties for example should be included in the calendar as well")

    external_identifier = models.CharField(max_length=50, default='', blank=True, null=True,
                                           help_text="This field is for internal use only and can be helpful for automated tasks.")

    is_new_tenant_bar = models.BooleanField(default=False,
                                            help_text="Is used to determine if a members has attended a new tenant bar.")

    link_to_participate = models.URLField(max_length=256, default=None, blank=True, null=True,
                                          help_text="E.g. a Zoom or Discord link. Or the link to an public gitlab issue.")

    def __str__(self):
        return self.name

    def get_admin_url(self):
        return reverse('admin:%s_%s_change' % (self._meta.app_label, self._meta.model_name),
                       args=[self.id])

    @staticmethod
    def new_tenant_bars_since(first_date):
        # -2 days so that the BA has time to verify the registrations
        return SocialService.objects.filter(is_new_tenant_bar=True,
                                            time__gte=timezone.make_aware(
                                                datetime.combine(first_date, datetime.min.time())),
                                            time__lt=timezone.now() - timedelta(days=2)).count()

    class Meta:
        ordering = ['-time']

    def send_new_tenant_bar_reminder_mails(self):
        mails = []
        for tenant in Tenant.objects.filter(date_move_in__lte=self.time, date_move_out__gt=self.time).select_related(
                'person'):
            if tenant.person.attended_new_tenant_bar():
                continue
            mails = mails + [{'recipients': tenant.person.user.email,
                              'sender': config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                              'template': 'new_tenant_bar_reminder',
                              'context': {'first_name': tenant.person.user.first_name,
                                          'missed_new_tenant_bar_counter': SocialService.new_tenant_bars_since(
                                              tenant.person.get_earliest_move_in_date()),
                                          'link': self.link_to_participate,
                                          'time': self.time,
                                          'id': self.id},
                              'headers': {'Reply-to': config.EMAIL['EMAIL_ACCOUNT_BA']},
                              'priority': 'medium'}]
        for group in WorkingGroup.objects.filter(should_attend_at_new_tenant_bar=True):
            leader = group.get_leader()
            if leader is None:
                logging.getLogger(__name__).warning("Working group " + group.name + " has no leader")
                continue
            mails = mails + [{'recipients': leader.person.user.email,
                              'sender': config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                              'template': 'new_tenant_bar_reminder_for_working_group_leader',
                              'context': {'first_name': leader.person.user.first_name,
                                          'group_name': group.name,
                                          'link': self.link_to_participate,
                                          'time': self.time,
                                          'id': self.id},
                              'headers': {'Reply-to': config.EMAIL['EMAIL_ACCOUNT_BA']},
                              'priority': 'medium'}]
        mail.send_many(mails)


@receiver(post_save, sender=SocialService)
def new_social_service(sender, instance, created, **kwargs):
    if created and instance.is_new_tenant_bar:
        instance.send_new_tenant_bar_reminder_mails()


class Registration(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE, blank=False)
    social_service = models.ForeignKey(SocialService, on_delete=models.RESTRICT)
    verified = models.BooleanField(default=False)
    verified_at = models.DateField(null=True, blank=True)
    social_service_period = models.ForeignKey(SocialServicePeriod, blank=True, on_delete=models.SET_NULL, null=True,
                                              help_text="The social serice period is important for internal decisions if e.g. the service is done by a person in a specific period."
                                              )

    # from https://stackoverflow.com/questions/31858418/django-update-date-automatically-after-a-value-change
    def __init__(self, *args, **kwargs):
        super(Registration, self).__init__(*args, **kwargs)
        self._verified = self.verified

    def save(self, *args, **kwargs):
        if (self.verified_at is None or self._verified is False) and self.verified:
            self.verified_at = timezone.now()
            if "update_fields" in kwargs:
                kwargs["update_fields"] = {"verified_at"}.union(kwargs["update_fields"])
            if self.social_service_period is None:
                membership = Membership.get_current_membership_for(self.person)
                if membership is not None:  # maybe the user is not a member
                    current_period = Membership.get_current_membership_for(
                        self.person).get_current_social_service_period()
                    previous_period = current_period.get_previous_period()
                    if previous_period is not None and not previous_period.has_done_social_service():
                        self.social_service_period = previous_period
                    else:
                        self.social_service_period = current_period
        super(Registration, self).save(*args, **kwargs)

    def __str__(self):
        return self.person.user.get_full_name()

    def send_mail_new_registration(self):
        mail.send(
            self.social_service.responsible_working_group.email,
            config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
            headers={'Reply-to': self.person.user.email},
            template='new_social_service_registration',
            context={'social_service': self.social_service,
                     'social_service_url': self.social_service.get_admin_url(),
                     'person': self.person},
            priority='medium'
        )

    def send_mail_deregister(self):
        mail.send(
            self.social_service.responsible_working_group.email,
            config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
            headers={'Reply-to': self.person.user.email},
            template='new_social_service_deregistration',
            context={'social_service': self.social_service,
                     'social_service_url': self.social_service.get_admin_url(),
                     'person': self.person},
            priority='medium'
        )


def is_member_association(user):
    return Membership.current_memberships().filter(person=user.person).exists()


@receiver(post_save, sender=Membership)
def create_registration_fee(sender, instance, created, **kwargs):
    if created:
        instance.create_invoice_registration_fee()


@receiver(post_save, sender=Registration)
def send_new_social_service_registration_mail(sender, instance, created, **kwargs):
    if created and instance.social_service.responsible_working_group:
        instance.send_mail_new_registration()


@receiver(post_save, sender=Registration)
def create_invoice(sender, instance: Registration, created, **kwargs):
    if instance.verified:
        membership = Membership.get_current_membership_for(instance.person)
        if membership:
            membership.create_invoice_network()


@receiver(pre_delete, sender=Registration)
def send_new_social_service_deregistration_mail(sender, instance, **kwargs):
    if instance.social_service.responsible_working_group:
        instance.send_mail_deregister()


auditlog.register(MembershipType)
auditlog.register(Membership)
auditlog.register(PlannedMembershipTypeChange)
auditlog.register(SocialService)
auditlog.register(Registration)
