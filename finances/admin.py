import datetime
import io
import re

from django.contrib import admin
from django.contrib.auth.models import User
from django.core.exceptions import MultipleObjectsReturned
from django.core.management import call_command
from django.db.models import Q
from django.http import HttpResponseRedirect, HttpResponse
from django.template.response import TemplateResponse
from django.urls import reverse, re_path
from django.utils import timezone
from django.utils.html import format_html
from django.db import connection
from moneyed import Money
from post_office import mail
import zipfile

from rangefilter.filters import NumericRangeFilterBuilder
from totalsum.admin import TotalsumAdmin

import config
from common.util.start_new_thread import start_new_thread
from finances.forms import InternalTransferForm
from finances.models import Document
from .models import Transaction, Account, BankAccountCSV, SubAccount
from .utils import match_tx_reference, match_tx_person


@start_new_thread
def rebalance_accounts(modeladmin, request, queryset):
    call_command('transfer_membership_transactions')
    # see https://stackoverflow.com/a/28913218/10162645
    connection.close()


@start_new_thread
def process_csv_file(modeladmin, request, queryset):
    subaccount_network = Account.objects.get(title='Network').subaccount_outsite_capital
    for csv_file in queryset:
        csv_file.file.open(mode="rb")
        content = csv_file.file.read().decode("UTF-8")
        content = content.split("\n")
        content = content[1:-1]

        for line in content:
            csv_parts = line.split(";")
            tx_date = csv_parts[5]
            tx_date = datetime.datetime.strptime(tx_date, "%d.%m.%Y").date()

            tx_person = csv_parts[6].strip()
            tx_ref = csv_parts[10].strip()
            tx_amount = float(csv_parts[11].replace(",", "."))

            if tx_date < timezone.localdate() - timezone.timedelta(days=180):
                continue

            user = match_tx_reference(tx_ref)
            if not user:
                if tx_amount < 0 or int(tx_amount) != tx_amount:
                    continue  # Muss positiv und ganzzahlig sein
                if tx_amount % 6 != 0 and (tx_amount - 10) % 6 != 0:
                    continue  # Muss ein Vielfaches von 6 oder von 10 (Anmeldegebühr) + Vielfaches von 6 sein
                user = match_tx_person(tx_person)
            if user:
                try:
                    Transaction.objects.get_or_create(person=user.person,
                                                      subaccount=subaccount_network,
                                                      date=tx_date,
                                                      amount=Money(tx_amount, 'EUR'),
                                                      include_in_turnover=False)
                except MultipleObjectsReturned:
                    # Falls eine Person wirklich zweimal am Tag die gleiche Summer überwiesen hat, wird das manuell eingetragen
                    transactions = Transaction.objects.filter(person=user.person,
                                                              subaccount=subaccount_network,
                                                              date=tx_date,
                                                              amount=Money(tx_amount, 'EUR'),
                                                              include_in_turnover=False).exclude(note__icontains="Doppelte Buchung")
                    if transactions.count() > 1:
                        mail.send(
                            'netzag-kasse@hilton.rwth-aachen.de',
                            config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                            subject=f'Überweisungsfehler - Doppelte Buchungen - {user.person} am {tx_date} über {tx_amount}€',
                            message=f"Die doppelten Buchungen bitte mit 'Doppelte Buchung' in der Notiz markieren. Buchungen: {', '.join(map(str,transactions.values_list('id', flat=True)))}",
                        )
            else:
                mail.send(
                    'netzag-kasse@hilton.rwth-aachen.de',
                    config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                    subject='Überweisungsfehler - falscher username - ' + tx_ref,
                    message=line,
                )

        csv_file.file.close()
        csv_file.last_update = timezone.now()
        csv_file.save()

        # Rebalance accounts
        call_command('createinvoices')

        # see https://stackoverflow.com/a/28913218/10162645
        connection.close()


process_csv_file.short_description = "Process csv file"


def download_pdfs(self, request, queryset):
    buffer = io.BytesIO()
    zip_file = zipfile.ZipFile(buffer, 'w')
    for index, item in enumerate(queryset):
        if not item.file:
            continue
        item.file.open(mode='br')
        file_content = item.file.read()
        item.file.close()
        zip_file.writestr(item.file.name, file_content)
    zip_file.close()

    response = HttpResponse(buffer.getvalue())
    response['Content-Type'] = 'application/x-zip-compressed'
    response['Content-Disposition'] = 'attachment; filename=documents.zip'
    return response


download_pdfs.short_description = "Download PDFs"


@admin.register(BankAccountCSV)
class BankAccountCSVAdmin(admin.ModelAdmin):
    actions = [process_csv_file]


@admin.register(Transaction)
class TransactionAdmin(TotalsumAdmin):
    search_fields = ['note', 'amount', 'person__user__first_name', 'person__user__last_name', 'subaccount__title']
    list_display = ('id', 'person', 'date', 'amount', 'subaccount', 'note')
    date_hierarchy = 'date'
    list_filter = ('subaccount', 'include_in_turnover', ('amount', NumericRangeFilterBuilder(title="By amount")))
    autocomplete_fields = ('person',)
    totalsum_list = ('amount',)

    def get_queryset(self, request):
        return super(TransactionAdmin, self).get_queryset(request).select_related('person', 'subaccount').prefetch_related(
            'person__user')


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        'title', 'balance', 'liquidity', 'liquidity_cash', 'receivables', 'outsite_capital', 'reserves',
        'reserves_special', 'turnover_value', 'account_actions')
    actions = [rebalance_accounts]

    def balance(self, obj):
        return obj.get_balance()

    def liquidity(self, obj):
        return obj.get_liquidity()

    def liquidity_cash(self, obj):
        return obj.get_liquidity_cash()

    def outsite_capital(self, obj):
        return obj.get_outsite_capital()

    def reserves(self, obj):
        return obj.get_reserves()

    def reserves_special(self, obj):
        return obj.get_reserves_special()

    def receivables(self, obj):
        return obj.get_receivables()

    def turnover_value(self, obj):
        return obj.get_turnover_value()

    turnover_value.short_description = 'Turnover Value (current year)'

    def get_urls(self):
        urls = super().get_urls()
        custom_urls = [
            re_path(
                r'^(?P<account_id>.+)/internal_transfer/$',
                self.admin_site.admin_view(self.process_internal_transfer),
                name='account-internal-transfer',
            )
        ]
        return custom_urls + urls

    def account_actions(self, obj):
        return format_html(
            '<a class="button" href="{}">Internal Transfer</a>',
            reverse('admin:account-internal-transfer', args=[obj.pk])
        )

    account_actions.short_description = 'Account Actions'
    account_actions.allow_tags = True

    def process_internal_transfer(self, request, account_id, *args, **kwargs):
        return self.process_action(
            request=request,
            account_id=account_id,
            action_form=InternalTransferForm,
            action_title='Internal Transfer',
        )

    def process_action(
            self,
            request,
            account_id,
            action_form,
            action_title
    ):
        account = self.get_object(request, account_id)
        if request.method != 'POST':
            form = action_form()
        else:
            form = action_form(request.POST)
        if form.is_valid():
            form.save(account, request.user)
            self.message_user(request, 'Success')
            url = reverse(
                'admin:finances_account_changelist',
                current_app=self.admin_site.name,
            )
            return HttpResponseRedirect(url)

        context = self.admin_site.each_context(request)
        context['opts'] = self.model._meta
        context['form'] = form
        context['account'] = account
        context['title'] = action_title
        return TemplateResponse(
            request,
            'admin/account/account_action.html',
            context,
        )


@admin.register(SubAccount)
class SubAccountAdmin(admin.ModelAdmin):
    list_display = ('title', 'balance')

    def balance(self, obj):
        return obj.get_balance()


@admin.register(Document)
class DocumentAdmin(admin.ModelAdmin):
    list_display = ('title', 'date_from', 'date_until', 'created', 'download')
    readonly_fields = ('file', 'created',)
    list_filter = ('document_type', )
    date_hierarchy = 'date_from'
    actions = [download_pdfs]

    def title(self, obj):
        return str(obj)

    def download(self, obj):
        if obj.file:
            return format_html('<b><a href="{}" target="_blank">Download</a></b>', obj.file.url)
        return format_html('<b>currently processing...</b>')
