import re
from typing import Optional

from django.contrib.auth.models import User
from django.db.models import Q


def match_tx_reference(tx_ref: str) -> Optional[User]:
    match = re.match(r"^[a-z-]{3,}[1-9]?", tx_ref)
    user = User.objects.filter(username=match.group(0)).first() if match else None
    if not user:
        match = re.match(r"^[A-Z-]{3,}[1-9]?", tx_ref)
        user = User.objects.filter(username=match.group(0).lower()).first() if match else None
    if not user:
        match = re.search(r"(referenz|eref)[:\s]+([a-z-]{3,}[1-9]?)", tx_ref, re.IGNORECASE)
        user = User.objects.filter(username=match.group(2).lower()).first() if match else None
    if not user:
        match = re.search(r"[:\s]([a-z-]{3,}[1-9]?)$", tx_ref, re.IGNORECASE)
        user = User.objects.filter(username=match.group(1).lower()).first() if match else None
    return user


def match_tx_person(tx_person: str) -> Optional[User]:
    users = User.objects
    for name in tx_person.lower().split():
        # See https://stackoverflow.com/questions/3825676/postgresql-regex-word-boundariespy
        regex_name = f"\\y{name}\\y"  # only match full words, but does not work with sqlite
        # Manche Banken ersetzen Sonderzeichen. Wir speichern aber tendenziell mit Sonderzeichen
        regex_name = regex_name.replace("ae", "(ä|ae)")
        regex_name = regex_name.replace("ue", "(ü|ue)")
        regex_name = regex_name.replace("oe", "(ö|oe)")
        regex_name = regex_name.replace("ss", "(ß|ss)")
        users = users.filter(Q(first_name__iregex=regex_name) | Q(last_name__iregex=regex_name))
    return users.first() if users.count() == 1 else None
