from django.core.management.base import BaseCommand
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY
from django.utils import timezone
from datetime import date

from finances.models import Document


class Command(BaseCommand):
    help = 'Creates a document about network and house fees for every month since 01.09.2018 until last month'

    def handle(self, *args, **options):
        for start_date in rrule(MONTHLY, dtstart=date(2018, 9, 1), until=date.today() - relativedelta(months=1)):
            Document.objects.get_or_create(date_from=start_date, date_until=start_date,
                                           document_type=Document.NETWORK_FEE)
            Document.objects.get_or_create(date_from=start_date, date_until=start_date,
                                           document_type=Document.HOUSE_FEE)
            Document.objects.get_or_create(date_from=start_date, date_until=start_date,
                                           document_type=Document.NETWORK_OUTSIDE_CAPITAL_INFLOW)
            Document.objects.get_or_create(date_from=start_date, date_until=start_date,
                                           document_type=Document.NETWORK_OUTSIDE_CAPITAL_OUTFLOW)
