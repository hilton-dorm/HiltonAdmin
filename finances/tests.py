from django.contrib.auth.models import User
from django.core.management import call_command
from django.test import TestCase
from moneyed import Money

from association.models import Membership, MembershipType, Account, Transaction
from finances.models import SubAccount
from finances_v2.tests import FinancesV2TestCase
from main.models import Person
from workinggroups.models import WorkingGroup


class FinancialTestCase(TestCase):

    def accountSetUp(self):
        network_group = WorkingGroup.objects.create(name="Network", email="network@test.de", description="test")
        network_liquidity = SubAccount.objects.create(title="Network Liquidity")
        network_reserves = SubAccount.objects.create(title="Network Reserves")
        network_outside_capital = SubAccount.objects.create(title="Network Outside Capital")
        network_receivables = SubAccount.objects.create(title="Network Receivables")
        network_reserves_special = SubAccount.objects.create(title="Network Reserves Special")
        self.network_account = Account.objects.create(
            title="Network",
            working_group=network_group,
            subaccount_liquidity=network_liquidity,
            subaccount_outsite_capital=network_outside_capital,
            subaccount_receivables=network_receivables,
            subaccount_reserves=network_reserves,
            subaccount_reserves_special=network_reserves_special)

        house_group = WorkingGroup.objects.create(name="House", email="house@test.de", description="test")
        house_liquidity = SubAccount.objects.create(title="House Liquidity")
        house_reserves = SubAccount.objects.create(title="House Reserves")
        house_outside_capital = SubAccount.objects.create(title="House Outside Capital")
        house_receivables = SubAccount.objects.create(title="House Receivables")
        house_reserves_special = SubAccount.objects.create(title="House Reserves Special")
        self.house_account = Account.objects.create(
            title="House",
            working_group=house_group,
            subaccount_liquidity=house_liquidity,
            subaccount_outsite_capital=house_outside_capital,
            subaccount_receivables=house_receivables,
            subaccount_reserves=house_reserves,
            subaccount_reserves_special=house_reserves_special)

    def setUp(self):
        self.accountSetUp()
        FinancesV2TestCase.accountSetUp(self)

        user = User.objects.create(
            username="tester",
            first_name="test",
            last_name="tester",
            email="test@tester.de")
        person = Person.objects.create(user=user)
        membership_type = MembershipType.objects.create(description="test", name="normal", membership_fee=2,
                                                        internet_allowed=True)
        self.membership = Membership.objects.create(person=person, type=membership_type)

        # Delete all side effect transaction (e.g. registration fees)
        Transaction.objects.all().delete()

    def test_user_balance_increase_after_transaction(self):
        current_balance = self.membership.get_balance_v1()
        Transaction.objects.create(
            subaccount=self.network_account.subaccount_outsite_capital,
            person=self.membership.person,
            amount=Money(1, "EUR"))
        self.assertEqual(self.membership.get_balance_v1(), current_balance + Money(1, "EUR"))

        current_balance = current_balance + Money(1, "EUR")
        Transaction.objects.create(
            subaccount=self.network_account.subaccount_outsite_capital,
            person=self.membership.person,
            amount=Money(0.5, "EUR"))
        self.assertEqual(self.membership.get_balance_v1(), current_balance + Money(0.5, "EUR"))

    def test_user_balance_reduced_after_invoice(self):
        self.assertEqual(self.membership.get_balance_v1(), Money(0, "EUR"))

        # Invoice for House
        Transaction.objects.create(
            subaccount=self.house_account.subaccount_receivables,
            person=self.membership.person,
            amount=Money(2, "EUR"))
        self.assertEqual(self.membership.get_balance_v1(), Money(-2, "EUR"))

        # Invoice for Network
        Transaction.objects.create(
            subaccount=self.network_account.subaccount_receivables,
            person=self.membership.person,
            amount=Money(4, "EUR"))
        self.assertEqual(self.membership.get_balance_v1(), Money(-6, "EUR"))

    def test_user_can_clear_off_invoices(self):
        tx_invoice1 = Transaction.objects.create(
            subaccount=self.house_account.subaccount_receivables,
            person=self.membership.person,
            amount=Money(50, "EUR"))
        tx_invoice2 = Transaction.objects.create(
            subaccount=self.network_account.subaccount_receivables,
            person=self.membership.person,
            amount=Money(50, "EUR"))
        tx_payment1 = Transaction.objects.create(
            subaccount=self.network_account.subaccount_outsite_capital,
            person=self.membership.person,
            amount=Money(50, "EUR"))
        tx_payment2 = Transaction.objects.create(
            subaccount=self.network_account.subaccount_outsite_capital,
            person=self.membership.person,
            amount=Money(50, "EUR"))

        self.assertEqual(self.membership.get_balance_v1(), Money(0, "EUR"))
        self.assertEqual(self.membership.get_invoices_v1().count(), 2)
        self.assertEqual(self.membership.get_payments_v1().count(), 2)
        self.assertEqual(self.membership.get_invoices_v1().first(), tx_invoice1)

        # Rebalance accounts internally
        call_command('transfer_membership_transactions')

        self.assertEqual(self.membership.get_balance_v1(), Money(0, "EUR"))
        self.assertEqual(self.membership.get_invoices_v1().count(), 2)
        self.assertEqual(self.membership.get_payments_v1().count(), 2)

        # self.assertEqual(self.network_account.get_reserves_special(), Money(50, "EUR"))
        # self.assertEqual(self.network_account.get_liquidity(), Money(50, "EUR"))
