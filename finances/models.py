from calendar import monthrange
from datetime import timedelta
from functools import reduce

from auditlog.registry import auditlog
from dateutil.relativedelta import relativedelta
from dateutil.rrule import rrule, MONTHLY
from django.contrib.contenttypes.models import ContentType
from django.core.files.base import ContentFile
from django.db import models, transaction
from django.db.models import DateField, Count, Sum
from django.db.models import DateTimeField, Q
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.utils import timezone
from django_tex.core import compile_template_to_pdf
from djmoney.models.fields import MoneyField
from djmoney.money import Money
from private_storage.fields import PrivateFileField

from main.models import Person
from workinggroups.models import WorkingGroup, KeyHolder


class SubAccount(models.Model):
    title = models.CharField(max_length=255, blank=False, null=False)

    def __str__(self):
        return self.title

    def get_balance(self, person=None, until_date=None):
        transactions = self.transaction_set.filter(date__lte=until_date) if until_date else self.transaction_set
        if person is not None:
            transactions = transactions.filter(person=person)
        transactions = transactions.aggregate(balance=Sum('amount'))
        if transactions['balance'] is None:
            return Money(0, 'EUR')
        return Money(transactions['balance'], 'EUR')

    class Meta:
        ordering = ['title']


class Account(models.Model):
    title = models.CharField(max_length=255, blank=False, null=False)
    description = models.TextField(blank=True)
    working_group = models.ForeignKey(WorkingGroup, on_delete=models.CASCADE)
    subaccount_liquidity = \
        models.ForeignKey(SubAccount,
                          on_delete=models.SET_NULL,
                          blank=False,
                          null=True,
                          related_name='subaccount_liquidity',
                          help_text="Liquidity represents the financial assets that is owned by the account holder. "
                                    "This money can be spend.")

    subaccount_liquidity_cash = \
        models.ForeignKey(SubAccount,
                          on_delete=models.SET_NULL,
                          blank=False,
                          null=True,
                          related_name='subaccount_liquidity_cash',
                          help_text="Liquidity Cash represents the financial assets that is owned by the account holder in cash. "
                                    "This money can be spend.")

    subaccount_outsite_capital = \
        models.ForeignKey(SubAccount,
                          on_delete=models.SET_NULL,
                          blank=False,
                          null=True,
                          related_name='subaccount_outsite_capital',
                          help_text="Outside capital represents the financial assets that is NOT owned "
                                    "by the account holder. This money cannot be spend. "
                                    "Most of the assets are leftover transactions of membership transactions. "
                                    "This money can be spend.")

    subaccount_reserves = \
        models.ForeignKey(SubAccount,
                          on_delete=models.SET_NULL,
                          blank=False,
                          null=True,
                          related_name='subaccount_reserves',
                          help_text="Reserves represent the financial assets that is are owned "
                                    "by the account holder but is reserved for known future transactions. "
                                    "These typically include insurance invoices or reserved assets by working groups.")

    subaccount_reserves_special = \
        models.ForeignKey(SubAccount,
                          on_delete=models.SET_NULL,
                          blank=False,
                          null=True,
                          related_name='subaccount_reserves_special',
                          help_text="Special reserves represent the financial assets that is are owned "
                                    "by the account holder but is reserved for known future transactions. "
                                    "The networking group uses this subaccount to hold paid house fees that need to be transferred internally.")

    subaccount_receivables = \
        models.ForeignKey(SubAccount,
                          on_delete=models.SET_NULL,
                          blank=False,
                          null=True,
                          related_name='subaccount_receivables',
                          help_text="Receivables represent the financial assets that are not owned yet physically but "
                                    "requested by e.g. unpaid membership fee invoices."
                                    "This money cannot be spend.")

    def __str__(self):
        return self.title

    def get_liquidity(self, until_date=None):
        return self.subaccount_liquidity.get_balance(until_date=until_date)

    def get_liquidity_cash(self, until_date=None):
        return self.subaccount_liquidity_cash.get_balance(until_date=until_date)

    def get_outsite_capital(self, until_date=None):
        return self.subaccount_outsite_capital.get_balance(until_date=until_date)

    def get_reserves(self, until_date=None):
        return self.subaccount_reserves.get_balance(until_date=until_date)

    def get_reserves_special(self, until_date=None):
        return self.subaccount_reserves_special.get_balance(until_date=until_date)

    def get_receivables(self, until_date=None):
        return self.subaccount_receivables.get_balance(until_date=until_date)

    def get_balance(self, until_date=None):
        return self.get_liquidity(until_date=until_date) + \
            self.get_liquidity_cash(until_date=until_date) + \
            self.get_outsite_capital(until_date=until_date) + \
            self.get_reserves_special(until_date=until_date) + \
            self.get_reserves(until_date=until_date)

    def get_turnover_value(self):
        filter_subaccounts = Q(subaccount=self.subaccount_reserves) | \
            Q(subaccount=self.subaccount_reserves_special) | \
            Q(subaccount=self.subaccount_liquidity) | \
            Q(subaccount=self.subaccount_outsite_capital) | \
            Q(subaccount=self.subaccount_receivables) | \
            Q(subaccount=self.subaccount_liquidity_cash)

        transactions = Transaction.objects.filter(filter_subaccounts, date__year=timezone.now().year,
                                                  include_in_turnover=True)
        transactions = [Money(0, 'EUR')] + [abs(transaction.amount) for transaction in list(transactions)]
        return reduce(lambda x, y: x + y, transactions)

    @staticmethod
    @transaction.atomic
    def internal_transfer(subaccount_from=None, subaccount_to=None, amount=Money(0, 'EUR'), date=timezone.now, note=''):
        if not subaccount_from or not subaccount_to:
            return

        tx_from = Transaction.objects.create(subaccount=subaccount_from,
                                             amount=-amount,
                                             date=date,
                                             note=note,
                                             include_in_turnover=False)
        tx_to = Transaction.objects.create(subaccount=subaccount_to,
                                           amount=amount,
                                           date=date,
                                           note="Internal transfer from tx#" + str(tx_from.id) + ". Note:" + note,
                                           include_in_turnover=False)

        tx_from.note = "Internal transfer to tx#" + str(tx_to.id) + ". Note:" + note
        tx_from.save()


class BankAccountCSV(models.Model):
    file = models.FileField(upload_to='account/csv')
    last_update = DateTimeField(default=timezone.now)

    def __str__(self):
        return 'CSV transaction file'

    class Meta:
        verbose_name_plural = "bank account csv files"


class Transaction(models.Model):
    person = models.ForeignKey(Person, on_delete=models.SET_NULL, null=True, blank=True)
    subaccount = models.ForeignKey(SubAccount, on_delete=models.PROTECT, null=False, blank=False)
    date = models.DateField(blank=False, default=timezone.now)  # Nimmt das Datum der aktuellen Zeitzone
    amount = MoneyField(max_digits=10, decimal_places=2, default_currency='EUR')
    note = models.CharField(max_length=255, default="", blank=True, null=False)
    receipt_1 = PrivateFileField(upload_to='transactions/receipts', null=True, blank=True, default=None)
    receipt_2 = PrivateFileField(upload_to='transactions/receipts', null=True, blank=True, default=None)
    receipt_3 = PrivateFileField(upload_to='transactions/receipts', null=True, blank=True, default=None)
    receipt_4 = PrivateFileField(upload_to='transactions/receipts', null=True, blank=True, default=None)
    receipt_5 = PrivateFileField(upload_to='transactions/receipts', null=True, blank=True, default=None)

    include_in_turnover = models.BooleanField(default=False,
                                              help_text='Should the transaction be included in the turnover value (in german: "Umsatz"?')

    def __str__(self):
        return str(self.id)

    @staticmethod
    def next_invoice(date):
        next_invoice = date.replace(day=1) + timedelta(days=32)
        return next_invoice.replace(day=1)


class Document(models.Model):
    file = PrivateFileField(upload_to='finances/documents')
    created = DateTimeField(default=timezone.now, blank=True)
    date_from = DateField()
    date_until = DateField()

    TRANSACTIONS = "TX"
    NETWORK_FEE = "NETWORK_FEE"
    HOUSE_FEE = "HOUSE_FEE"
    NETWORK_OUTSIDE_CAPITAL_INFLOW = "NETWORK_OUTSIDE_CAPITAL_INFLOW"
    NETWORK_OUTSIDE_CAPITAL_OUTFLOW = "NETWORK_OUTSIDE_CAPITAL_OUTFLOW"
    CASH_FLOW_SUMMARY = "CFS"
    OPEN_RECEIVABLES_LEAVED = "OPEN_RECEIVABLES_LEAVED"
    DOCUMENT_TYPE_CHOICES = (
        (TRANSACTIONS, 'Transactions Overview'),
        (CASH_FLOW_SUMMARY, 'Cash Flow Summary'),
        (NETWORK_FEE, 'Fees at the network liquidity account'),
        (HOUSE_FEE, 'Fees at the network reserves special account'),
        (NETWORK_OUTSIDE_CAPITAL_INFLOW, 'Inflow of the network outside capital account'),
        (NETWORK_OUTSIDE_CAPITAL_OUTFLOW, 'Outflow of the network outside capital account'),
        (OPEN_RECEIVABLES_LEAVED, 'Open receivables against resigned members'),
    )
    document_type = models.CharField(
        max_length=64,
        choices=DOCUMENT_TYPE_CHOICES,
        default=TRANSACTIONS,
    )

    def __str__(self):
        return str(self.document_type) + '_' + \
            self.date_from.strftime('%Y_%m_%d') + '-' + \
            self.date_until.strftime('%Y_%m_%d') + '_(' + str(self.id) + ')'

    def process(self):
        if self.document_type == self.TRANSACTIONS:
            self.process_tx()
        elif self.document_type == self.NETWORK_FEE or self.document_type == self.HOUSE_FEE or \
                self.document_type == self.NETWORK_OUTSIDE_CAPITAL_OUTFLOW or \
                self.document_type == self.NETWORK_OUTSIDE_CAPITAL_INFLOW:
            self.process_fee()
        elif self.document_type == self.OPEN_RECEIVABLES_LEAVED:
            self.process_open_receivables()
        elif self.document_type == self.CASH_FLOW_SUMMARY:
            self.process_cash_flow_summary()
        pass

    # if you have to debug a latex file:
    #     temp = render_to_string('your_template_name.tex', context)
    #     f = open("test.tex", "w")
    #     f.write(temp)
    #     f.close()

    def process_tx(self):
        template_name = 'transactions.tex'

        account_house = Account.objects.get(title='House')
        account_network = Account.objects.get(title='Network')

        # Retrives all transactions and filters them accordingly
        txs = Transaction.objects.filter(date__gte=self.date_from, date__lte=self.date_until)
        txs_house = txs.filter(person__isnull=False, subaccount=account_house.subaccount_liquidity) \
            .values('person', 'amount') \
            .annotate(txcount=Count('person'))
        txs_network = txs.filter(person__isnull=False, subaccount=account_network.subaccount_liquidity) \
            .values('person', 'amount') \
            .annotate(txcount=Count('person'))
        # get wanted columns
        txs_network = txs_network.values_list("person__user__first_name", "person__user__last_name", "amount",
                                              "txcount")
        txs_house = txs_house.values_list("person__user__first_name", "person__user__last_name", "amount", "txcount")

        # Converts list into csv data
        data_csv_house = list(map(lambda x: x[0] + ' ' + x[1] + ',' + str(x[2]) + ',' + str(x[3]), txs_house))
        data_csv_house = '\n'.join(data_csv_house)

        data_csv_network = list(map(lambda x: x[0] + ' ' + x[1] + ',' + str(x[2]) + ',' + str(x[3]), txs_network))
        data_csv_network = '\n'.join(data_csv_network)

        context = {'date_created': self.created,
                   'date_from': self.date_from,
                   'date_until': self.date_until,
                   'data_csv_house': data_csv_house,
                   'data_csv_network': data_csv_network
                   }

        PDF = compile_template_to_pdf(template_name, context)
        self.file.save(str(self) + ".pdf", ContentFile(PDF))

    def process_fee(self):
        self.date_from = self.date_from.replace(day=1)
        self.date_until = self.date_until.replace(day=monthrange(self.date_until.year, self.date_until.month)[1])
        self.save()

        account = Account.objects.get(title='Network')
        if self.document_type == self.NETWORK_FEE:
            subaccount = account.subaccount_liquidity
            account_name = "Network Liquidity"
            fees_type = "Network Fees"
        elif self.document_type == self.HOUSE_FEE:
            subaccount = account.subaccount_reserves_special
            account_name = "Network Reserve Special"
            fees_type = "House Fees"
        else:
            subaccount = account.subaccount_outsite_capital
            account_name = "Network Outside Capital"
            fees_type = "Inflow" if self.document_type == self.NETWORK_OUTSIDE_CAPITAL_INFLOW else "Outflow"

        months = []
        for start_date in rrule(MONTHLY, dtstart=self.date_from, until=self.date_until):
            end_date = start_date + relativedelta(months=1)
            start_date = timezone.make_aware(start_date)
            end_date = timezone.make_aware(end_date)

            query_filter = Q(date__gte=start_date, date__lt=end_date, person__isnull=False, subaccount=subaccount)
            if self.document_type == self.NETWORK_OUTSIDE_CAPITAL_INFLOW:
                query_filter &= Q(amount__gt=0)
            elif self.document_type == self.NETWORK_OUTSIDE_CAPITAL_OUTFLOW:
                query_filter &= Q(amount__lt=0)
            txs = Transaction.objects.select_related('person').select_related('person__user').filter(query_filter)
            network_sum = txs.aggregate(Sum('amount'))['amount__sum']
            months.append({
                'start_date': start_date,
                'end_date': end_date,
                'txs': txs,
                'network_sum': 0 if network_sum is None else network_sum,
            })

        context = {
            'date_created': self.created,
            'date_from': self.date_from,
            'date_until': self.date_until,
            'months': months,
            'account_name': account_name,
            'fees_type': fees_type,
        }

        PDF = compile_template_to_pdf('monthly_fees.tex', context)
        self.file.save(str(self) + ".pdf", ContentFile(PDF))
        pass

    def process_open_receivables(self):
        network_subaccount_receivables = Account.objects.get(title='Network').subaccount_receivables
        network_subaccount_outside_capital = Account.objects.get(title='Network').subaccount_outsite_capital
        house_subaccount_receivables = Account.objects.get(title='House').subaccount_receivables
        members = []
        zero = Money(0, 'EUR')
        from association.models import Membership  # prevent circular dependencies
        for member in Membership.objects.filter(date_leave__lt=timezone.now()):
            if member.get_balance() >= zero:
                continue

            if member.person.membership_set.filter(Q(date_leave__gte=timezone.now()) | Q(date_leave__isnull=True)).exists():
                continue

            network_transactions, network_failed = member.get_list_of_unpaied_transaction_entries(
                network_subaccount_receivables)
            network_sum = sum(t.amount for t in network_transactions)
            house_transactions, house_failed = member.get_list_of_unpaied_transaction_entries(house_subaccount_receivables)
            house_sum = sum(t.amount for t in house_transactions)

            members.append({
                'name': member.person.full_name(),
                'date_leave': member.date_leave,
                'balance': network_subaccount_outside_capital.get_balance(person=member.person).amount,
                'network_transactions': network_transactions,
                'network_matching_failed': network_failed,
                'network_sum': 0 if network_sum == 0 else network_sum.amount,
                'house_transactions': house_transactions,
                'house_matching_failed': house_failed,
                'house_sum': 0 if house_sum == 0 else house_sum.amount,
            })

        context = {
            'date_created': self.created,
            'members': members,
        }
        pdf = compile_template_to_pdf('open_receivables.tex', context)
        self.file.save(str(self) + ".pdf", ContentFile(pdf))

    def process_cash_flow_summary(self):
        template_name = 'cash_flow_summary.tex'

        txs = Transaction.objects.filter(date__gte=self.date_from, date__lte=self.date_until)
        txs = txs.values_list('date', 'person__user__first_name', 'person__user__last_name', 'subaccount', 'amount',
                              'note')

        # number,date,revenue,expenses,notes
        data_csv = list(
            map(lambda x: '0,' + str(x[0]).replace(',', '') + ',' + str(x[4]).replace(',', '') + ',' + str(
                x[4]).replace(',', '') + ',' + "testnote", txs))
        data_csv = '\n'.join(data_csv)
        print(data_csv)
        context = {
            'date_created': self.created,
            'date_from': self.date_from,
            'date_until': self.date_until,
            'data_csv': data_csv
        }

        PDF = compile_template_to_pdf(template_name, context)
        self.file.save(str(self) + ".pdf", ContentFile(PDF))


@receiver(post_save, sender=Document)
def process_document(sender, instance, created, **kwargs):
    if created:
        instance.process()


auditlog.register(Document)
auditlog.register(BankAccountCSV)
auditlog.register(Account)
auditlog.register(Transaction)
