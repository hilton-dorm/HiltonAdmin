from django import forms
from django.utils import timezone
from djmoney.forms import MoneyField

from finances.models import SubAccount, Account


class InternalTransferForm(forms.Form):
    amount = MoneyField(min_value=0, max_digits=10, decimal_places=2, default_currency='EUR',
                        help_text='How much to transfer?')

    subaccount_from = forms.ModelChoiceField(queryset=SubAccount.objects.all(), required=True)
    subaccount_to = forms.ModelChoiceField(queryset=SubAccount.objects.all(), required=True)
    date = forms.DateField(required=True)

    note = forms.CharField(
        required=False,
        widget=forms.TextInput,
    )

    field_order = (
        'amount',
        'subaccount_from',
        'subaccount_to',
        'note',
    )

    def save(self, account, user):
        return Account.internal_transfer(
            amount=self.cleaned_data['amount'],
            subaccount_from=self.cleaned_data['subaccount_from'],
            subaccount_to=self.cleaned_data['subaccount_to'],
            date=self.cleaned_data['date'],
            note=self.cleaned_data['note']
        )
