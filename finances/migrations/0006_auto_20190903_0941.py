# Generated by Django 2.1.5 on 2019-09-03 07:41

import datetime
from django.db import migrations, models
from datetime import timezone
import private_storage.fields
import private_storage.storage.files


class Migration(migrations.Migration):

    dependencies = [
        ('finances', '0005_auto_20190903_0931'),
    ]

    operations = [
        migrations.AlterField(
            model_name='document',
            name='date_from',
            field=models.DateField(default=datetime.datetime(2019, 8, 3, 7, 41, 45, 541124, tzinfo=timezone.utc)),
        ),
        migrations.AlterField(
            model_name='document',
            name='file',
            field=private_storage.fields.PrivateFileField(storage=private_storage.storage.files.PrivateFileSystemStorage(), upload_to='finances/documents'),
        ),
    ]
