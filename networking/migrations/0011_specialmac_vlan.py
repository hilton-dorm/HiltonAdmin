# Generated by Django 2.2.5 on 2021-12-16 15:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('networking', '0010_auto_20211015_0031'),
    ]

    operations = [
        migrations.AddField(
            model_name='specialmac',
            name='vlan',
            field=models.IntegerField(blank=True, null=True),
        ),
    ]
