from django.urls import path

from networking import views

urlpatterns = [
    path('status/', views.status, name='status'),
    path('macs/', views.macs, name='macs'),
    path('services/', views.services, name='services'),
    path('no_internet/', views.no_internet, name='no_internet'),
    path('forwarding/<int:forwarding_id>/delete/', views.delete_forwarding, name='delete'),
    path('macs/<int:mac_id>/delete/', views.delete_mac, name='delete'),
    path('macs/<int:mac_id>/change_device_name/', views.change_device_name, name='change_device_name'),
    path('dns_adblocker_users', views.dns_adblocker_users, name='dns_adblocker_users'),
    path('write_all_kea_changes', views.write_all_kea_changes, name='write_all_kea_changes'),
    path('port_forwardings', views.port_forwardings, name='port_forwardings'),
    path('radius/user/<mac_address>/', views.radius, name='radius'),
    path('rz/mails.php', views.mails, name='mails'),
    path('noc/aps', views.noc_ap_status, name='noc_ap_status'),
    path('zabbix/ap-offline', views.send_ap_offline_email, name='send_ap_offline_email'),
]
