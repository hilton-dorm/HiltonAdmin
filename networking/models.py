from auditlog.registry import auditlog
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import UniqueConstraint
from macaddress import mac_linux
from macaddress.fields import MACAddressField
from netaddr import EUI, mac_unix_expanded

import config
from main.models import Person
import json
from bisect import bisect_right


class MacVendorDatabase(object):
    _instance = None

    def __new__(cls):
        # singleton pattern from https://python-patterns.guide/gang-of-four/singleton/
        if cls._instance is None:
            cls._instance = super(MacVendorDatabase, cls).__new__(cls)
            # file from https://macaddress.io/database-download
            macs = {}
            with open('macaddress.io-db.json', 'r', encoding="utf8") as myfile:
                for line in myfile.readlines():
                    entry = json.loads(line)
                    macs[entry['oui']] = entry['companyName']
            keys = sorted(macs.keys())
            cls._instance.macs = macs
            cls._instance.keys = keys
        return cls._instance

    @staticmethod
    def get_vendor(mac_address: str):
        assert mac_address[2] == ':', "mac address must be seperated by :"
        index = bisect_right(MacVendorDatabase().keys, mac_address)
        if index == 0:  # Smaller then the smallest known MAC
            return None
        mac_prefix = MacVendorDatabase().keys[index - 1]
        if not mac_address.startswith(mac_prefix):
            return None
        return MacVendorDatabase().macs[mac_prefix]

    @staticmethod
    def is_vendor_disallowed(vendor: str) -> bool:
        if vendor is None:
            return False
        for v in config.MAC_DISALLOWED_VENDORS:
            if vendor.startswith(v):
                return True
        return False

    @staticmethod
    def is_mac_disallowed(mac_address: EUI) -> bool:
        return MacVendorDatabase.is_vendor_disallowed(
            MacVendorDatabase.get_vendor(mac_address.format(dialect=mac_linux)))


class Mac(models.Model):
    """
    Mac Model
    """
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    mac = MACAddressField(max_length=18, integer=False, unique=True, null=True)
    device_name = models.CharField(max_length=120, blank=True, null=True)
    blocked = models.BooleanField(default=False, blank=False)
    deleted = models.BooleanField(default=False, blank=False,
                                  help_text="We mark addresses as deleted instead of deleting them so that you can not "
                                            "'move' mac addresses from one user to another.")
    last_used = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return "%s" % self.mac

    def vendor(self):
        return MacVendorDatabase.get_vendor(str(self.mac))

    class Meta:
        verbose_name = 'MAC Adress'
        verbose_name_plural = 'MAC Adresses'


class RadiusMacAuthError(models.Model):
    created = models.DateTimeField()
    mac = MACAddressField(max_length=18, integer=False)
    url = models.CharField(max_length=1024)
    reason = models.CharField(max_length=1024)

    def vendor(self):
        return MacVendorDatabase.get_vendor(str(self.mac))


class SpecialMac(models.Model):
    """
    Mac Model for special addresses, this can be e.g. MAC addesses of access points.
    These MACs have always internet access and do not belong to a person
    """
    mac = MACAddressField(max_length=18, integer=False, unique=True, null=True)
    notes = models.CharField(max_length=120, blank=True, null=True)
    vlan = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return "%s" % self.mac

    class Meta:
        verbose_name = 'Special MAC Adress'
        verbose_name_plural = 'Special MAC Adresses'


class PortForwarding(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    source_port = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(65536)])
    destination_port = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(65536)])
    destination_ip = models.PositiveSmallIntegerField(validators=[MinValueValidator(2), MaxValueValidator(254)])

    def __str__(self):
        return f"134.130.55.x:{self.source_port} -> 10.72.x.{self.destination_ip}:{self.destination_port}"

    class Meta:
        constraints = [UniqueConstraint(fields=['person', 'source_port'], name='Only can map a source port once')]
