import nested_admin
from django.contrib import admin
from django.contrib.admin import SimpleListFilter
from django.db.models import Count
from django.urls import reverse
from django.utils.html import format_html

from .models import Mac, SpecialMac, RadiusMacAuthError, PortForwarding, MacVendorDatabase


class IllegalVendorFilter(SimpleListFilter):
    title = 'mac vendor'
    parameter_name = 'vendor'

    def lookups(self, request, model_admin):
        return (
            ('0', 'disallowed vendors'),
        )

    def queryset(self, request, queryset):
        if self.value() == "0":
            disallowed = [mac.id for mac in queryset.all() if MacVendorDatabase.is_mac_disallowed(mac.mac)]
            return queryset.filter(pk__in=disallowed)
        return queryset


@admin.register(Mac)
class MacAdmin(nested_admin.NestedModelAdmin):
    """
    Mac Admin
    """
    list_display = ['person', 'mac', 'vendor', 'last_used', 'device_name', 'blocked', 'deleted']
    list_display_links = ['mac']
    list_filter = ['blocked', 'deleted', IllegalVendorFilter]
    search_fields = ['person__user__first_name', 'person__user__last_name', 'mac', 'device_name']

    def get_search_results(self, request, queryset, search_term):
        search_term = search_term.replace(':', '-')  # In der Datenbank sind die Mac Adressen im format AA-BB-CC-... gespeichert
        return super(MacAdmin, self).get_search_results(request, queryset, search_term)


class NewMacAuthErrorsFilter(SimpleListFilter):
    title = '"new" entries'
    parameter_name = 'new'

    def lookups(self, request, model_admin):
        return (
            ('0', 'New entries'),
        )

    def queryset(self, request, queryset):
        if self.value() == "0":
            new = RadiusMacAuthError.objects.all().values('mac').annotate(total=Count('mac')).filter(total__lt=100).values('mac')
            return queryset.filter(mac__in=new)
        return queryset


@admin.register(RadiusMacAuthError)
class RadiusMacAuthErrorAdmin(admin.ModelAdmin):
    list_display = ['created', 'mac', 'vendor', 'url', 'reason']
    search_fields = ['url']
    date_hierarchy = 'created'
    list_filter = ['created', NewMacAuthErrorsFilter]
    list_max_show_all = 10000
    pass


@admin.register(SpecialMac)
class SpecialMacAdmin(admin.ModelAdmin):
    list_display = ['mac', 'notes', 'vlan']
    list_display_links = ['mac']
    search_fields = ['mac', 'notes']

    def get_search_results(self, request, queryset, search_term):
        search_term = search_term.replace(':', '-')  # In der Datenbank sind die Mac Adressen im format AA-BB-CC-... gespeichert
        return super(SpecialMacAdmin, self).get_search_results(request, queryset, search_term)


@admin.register(PortForwarding)
class PortForwardingAdmin(admin.ModelAdmin):
    list_filter = ('person',)
