from django import forms
from django.forms import CharField
from macaddress.formfields import MACAddressField

from main.models import Person
from networking.models import PortForwarding


class MacForm(forms.Form):
    mac = MACAddressField(label="Add a new MAC address")
    device_name = CharField(max_length=120)


class WantDnsBlockerForm(forms.ModelForm):
    class Meta:
        model = Person
        fields = ('want_dns_adblocker',)
        labels = {
            "want_dns_adblocker": "Enable Pi-hole"
        }


class PortForwardingForm(forms.ModelForm):
    class Meta:
        model = PortForwarding
        exclude = ['person']
        help_texts = {
            'destination_ip': 'Only the last block. For 10.72.10.52 it would be 52.',
        }
