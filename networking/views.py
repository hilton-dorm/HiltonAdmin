import json
import urllib
from urllib.parse import quote
from xml.dom import minidom

from django.contrib import messages
from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib.auth.models import AnonymousUser
from django.db.models import Q
from django.http import HttpResponse, HttpResponseRedirect, JsonResponse
from django.template import loader
from django.urls import reverse_lazy
from django.utils import timezone
from django.utils.timezone import now
from django.views.decorators.csrf import csrf_exempt
from netaddr import EUI, AddrFormatError
from post_office import mail
from proxy.views import proxy_view
import base64
import requests
from requests.auth import HTTPBasicAuth

import config
from association.models import is_member_association, Membership
from hilton_mail.models import MailForwarding
from main.models import Person, Room, Tenant
from networking.forms import MacForm, WantDnsBlockerForm, PortForwardingForm
from networking.models import Mac, RadiusMacAuthError, MacVendorDatabase, SpecialMac


def make_kea_request(command, arguments=None) -> dict | None:
    res = requests.post("http://10.72.4.64:8000/", json={"service": ["dhcp4"], "command": command, "arguments": arguments},
                        auth=HTTPBasicAuth("admin", "1234"))
    body = None
    if res.status_code == 200:
        body = res.json()
    if res.status_code != 200 or body is None or len(body) != 1 or body[0]["result"] != 0:
        mail.send(
            config.EMAIL["EMAIL_ACCOUNT_ADMIN"],
            config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
            subject="Failed to update port ad blocker",
            message=f"The server returned http status {res.status_code} and the following error msg: {json.dumps(body, indent=4)}"
        )
        return None
    return body


def change_subnet(subnet, want_dns_adblocker):
    # first remove all dns entries
    subnet["option-data"] = list(filter(lambda entry: entry["name"] != "domain-name-servers", subnet["option-data"]))
    # add dns entry if requested
    if want_dns_adblocker:
        subnet["option-data"].append({
            "name": "domain-name-servers",
            "data": "10.72.4.21"
        })


def make_kea_change(vlan_id_to_want_dns_adblocker: dict[int, bool]):
    body = make_kea_request("config-get")
    if not body:
        return
    subnets = body[0]["arguments"]["Dhcp4"]["subnet4"]
    for subnet in subnets:
        want_dns_adblocker = vlan_id_to_want_dns_adblocker.pop(subnet["id"], None)
        if want_dns_adblocker is not None:
            change_subnet(subnet, want_dns_adblocker)
            if len(vlan_id_to_want_dns_adblocker) == 0:
                break
    make_kea_request("config-set", body[0]["arguments"])


def write_all_kea_changes(request):
    vlan_id_to_want_dns_adblocker = {}
    for t in Tenant.current_tenants().select_related("room", "person"):
        vlan_id_to_want_dns_adblocker[t.room.vlan_id] = t.person.want_dns_adblocker
    make_kea_change(vlan_id_to_want_dns_adblocker)
    return HttpResponse()


@login_required
@user_passes_test(is_member_association, login_url=reverse_lazy('no_permission'))
def macs(request):
    macs = Mac.objects.filter(person=request.user.person, deleted=False)

    pi_hole_form = WantDnsBlockerForm(instance=request.user.person)
    port_forwarding_form = PortForwardingForm()
    mac_form = MacForm()
    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        if 'adblock' in request.POST:
            pi_hole_form = WantDnsBlockerForm(request.POST, instance=request.user.person)
            if pi_hole_form.is_valid():
                pi_hole_form.save()
                want_dns_adblocker = pi_hole_form.cleaned_data["want_dns_adblocker"]
                tenant = request.user.person.get_current_tenant()
                if tenant:
                    id = tenant.room.vlan_id
                    if id and not config.DEBUG:
                        make_kea_change({id: want_dns_adblocker})

                messages.success(
                    request,
                    f'Pi-hole is now {"enabled" if request.user.person.want_dns_adblocker else "disabled"}.')
                return HttpResponseRedirect(reverse_lazy('macs'))
        elif 'port_forwarding' in request.POST:
            port_forwarding_form = PortForwardingForm(request.POST)
            if port_forwarding_form.is_valid():
                port_forwarding_form.instance.person = request.user.person
                # update existing entry instead of creating a new one if one exists
                existing_entry = request.user.person.portforwarding_set.filter(
                    source_port=port_forwarding_form.instance.source_port).first()
                if existing_entry:
                    port_forwarding_form.instance.id = existing_entry.id
                entry = port_forwarding_form.save()
                if not config.DEBUG:
                    res = requests.get("http://updater.router.hilton.rwth-aachen.de/update.php?type=port_forwardings")
                    if res.status_code != 204:
                        mail.send(
                            config.EMAIL["EMAIL_ACCOUNT_ADMIN"],
                            config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                            subject="Failed to update port forwardings",
                            message=f"The server ({res.url}) returned http status {res.status_code} and the following error msg: {res.text}"
                        )
                        messages.error(
                            request,
                            f"Due to an internal error, port forwarding {entry} could not be added.")
                        return HttpResponseRedirect(reverse_lazy('macs'))
                messages.success(
                    request,
                    f'Port-Forwarding {entry} added.')
                return HttpResponseRedirect(reverse_lazy('macs'))
            else:
                messages.error(request, 'Please correct the errors in the form below.')
        elif 'mac_form' in request.POST:
            mac_form = MacForm(request.POST)
            if mac_form.is_valid():
                existing_mac = Mac.objects.filter(mac=mac_form.cleaned_data['mac']).first()
                if existing_mac:
                    if existing_mac.person_id == request.user.person.id:
                        if existing_mac.deleted:
                            existing_mac.deleted = False
                            existing_mac.save()
                            messages.success(request, 'MAC address added.')
                        else:
                            messages.error(request, 'MAC address already added.')
                    else:
                        messages.error(request,
                                       'MAC address already used by a different person. If you think this is an'
                                       f' error, contact {config.EMAIL["EMAIL_ACCOUNT_NETWORK"]}.')
                else:
                    current_tenant = request.user.person.get_current_tenant()
                    if current_tenant is None or current_tenant != current_tenant.room.current_tenant():
                        messages.error(request, "You are currently not living here and therefore can not add mac addresses")
                    else:
                        mac = Mac(person=request.user.person,
                                  mac=mac_form.cleaned_data['mac'],
                                  device_name=mac_form.cleaned_data['device_name'],
                                  blocked=MacVendorDatabase.is_mac_disallowed(mac_form.cleaned_data['mac']))
                        mac.save()
                        if mac.blocked:
                            message = f"mailto:{config.EMAIL['EMAIL_ACCOUNT_NETWORK']}?subject=My mac address was blocked.&" \
                                      f"body=Hi,%0Amy mac address {mac.mac} was blocked. Here you can see that it is not an " \
                                      f"router: %0A%0AINSERT IMAGE HERE%0A%0ABest Regards,%0A{request.user.person}"
                            messages.error(request,
                                           'MAC address was added and blocked. We suspect that the mac belongs to a router '
                                           'which are prohibited. If the mac does not belong to a router, please take a '
                                           f'picture of the device and send it to '
                                           f'<a href="{message}">{config.EMAIL["EMAIL_ACCOUNT_NETWORK"]}</a>.')
                        else:
                            messages.success(request,
                                             'MAC address added. '
                                             'You may want to reconnect your Ethernet cable in order to reauthenticate.')
                    return HttpResponseRedirect(reverse_lazy('macs'))

    template = loader.get_template('networking/mac_addresses.html')
    current_tenant = request.user.person.get_current_tenant()
    context = {
        'title': 'Internet Access',
        'macs': macs,
        'mac_form': mac_form,
        'pi_hole_form': pi_hole_form,
        'port_forwarding_form': port_forwarding_form,
        'vlan_id': current_tenant.room.vlan_id if current_tenant else "x",
        'port_forwardings': request.user.person.portforwarding_set.all()
    }

    return HttpResponse(template.render(context, request))


def no_internet(request):
    mac = request.GET.get("mac", "")
    name = request.GET.get("name", "Added automatically")
    mac_blocked = False
    link = None
    user = None
    membership_over = None
    try:
        valid_mac = EUI(mac)
    except AddrFormatError:
        valid_mac = None
        messages.error(request, "Error while loading current Mac address ")
    existing_mac = Mac.objects.filter(mac=valid_mac).first()
    if not isinstance(request.user, AnonymousUser):
        user = request.user
        membership_over = Membership.get_current_membership_for(request.user.person)
        has_internet = membership_over.has_network_access() if membership_over else False
        addition = " Please reconnect to get internet access. If you use an own switch, unplug the cable from the wall." if has_internet else ""
        if existing_mac:
            if existing_mac.blocked:
                mac_blocked = True
                messages.error(request, "Your mac address is blocked because it is probably a router")
            elif existing_mac.deleted:
                existing_mac.deleted = False
                existing_mac.save()
                messages.success(request, "Your mac address was added." + addition)
            else:
                messages.success(request, "Your mac address is registered." + addition)
        else:
            if valid_mac is not None:
                current_tenant = user.person.get_current_tenant()
                if current_tenant is None or current_tenant != current_tenant.room.current_tenant():
                    messages.error(request, "You are currently not living here and therefore can not add mac addresses")
                else:
                    mac = Mac(person=request.user.person, mac=valid_mac, device_name=name)
                    if MacVendorDatabase.is_mac_disallowed(valid_mac):
                        mac.blocked = True
                        mac.save()
                        mac_blocked = True
                        messages.error(request, "Your mac address was added but blocked because it is probably a router")
                    else:
                        mac.save()
                        messages.success(request, "Your mac address was added." + addition)
    elif existing_mac:
        if existing_mac.blocked:
            mac_blocked = True
            messages.error(request, "Your mac address is blocked because it is probably a router")
        elif existing_mac.deleted:
            existing_mac.deleted = False
            existing_mac.save()
            messages.success(request, "Your mac address was added. Please reconnect to get internet access.")
        else:
            messages.success(request, "Your mac address is registered.")
            messages.warning(request, "Please login to get further information.")
    template = loader.get_template('networking/no_internet.html')
    context = {
        "user": user,
        "mac": mac,
        "login_url": f"/user/login/?next={quote(request.get_full_path())}",
        "mac_blocked": mac_blocked,
        "network_mail": config.EMAIL["EMAIL_ACCOUNT_NETWORK"],
        "membership": membership_over
    }
    return HttpResponse(template.render(context, request))


@login_required
@user_passes_test(is_member_association, login_url=reverse_lazy('no_permission'))
def delete_forwarding(request, forwarding_id):
    forwarding = request.user.person.portforwarding_set.filter(id=forwarding_id).first()
    if forwarding:
        messages.success(request, f'Forwarding {forwarding} deleted.')
        forwarding.delete()
    return HttpResponseRedirect(reverse_lazy('macs'))


@login_required
@user_passes_test(is_member_association, login_url=reverse_lazy('no_permission'))
def delete_mac(request, mac_id):
    mac = Mac.objects.filter(person=request.user.person, id=mac_id).first()
    if mac:
        mac.deleted = True
        mac.save()
        messages.success(request, 'MAC address deleted.')
    return HttpResponseRedirect(reverse_lazy('macs'))


@login_required
@user_passes_test(is_member_association, login_url=reverse_lazy('no_permission'))
def change_device_name(request, mac_id):
    if request.method != 'POST':
        return HttpResponseRedirect(reverse_lazy('macs'))
    mac = Mac.objects.filter(person=request.user.person, id=mac_id, blocked=False).first()
    device_name = request.POST['device_name']
    if mac:
        mac.device_name = device_name
        mac.save()
        messages.success(request, f"Device name changed to {device_name} successfully")
    else:
        messages.error(request, "Error while saving new device name")
    return HttpResponseRedirect(reverse_lazy('macs'))


@login_required
@user_passes_test(is_member_association, login_url=reverse_lazy('no_permission'))
def status(request):
    template = loader.get_template('networking/status.html')
    context = {
        'title': 'Network Status',
    }
    return HttpResponse(template.render(context, request))


@login_required
@user_passes_test(is_member_association, login_url=reverse_lazy('no_permission'))
def services(request):
    headers = {
        'PRIVATE-TOKEN': config.GITLAB["access_token"]
    }
    response = requests.get("https://git.rwth-aachen.de/api/v4/projects/32309/repository/files/Verwaltung.drawio.svg?ref=master", headers=headers)
    base64_content = response.json()["content"]

    svg_doc = minidom.parseString(base64.b64decode(base64_content).decode('utf-8'))

    # get the first element
    first_element = svg_doc.getElementsByTagName("svg")[0]

    encoded_content = urllib.parse.quote(first_element.getAttribute("content"))

    template = loader.get_template('networking/services.html')
    context = {
        'title': 'Our Services for You',
        'encoded_content': encoded_content,
    }
    return HttpResponse(template.render(context, request))


def dns_adblocker_users(request):
    rooms = []
    for room in Room.objects.all():
        tenant = room.current_tenant()
        if tenant and tenant.person.want_dns_adblocker:
            rooms.append(room.number)
    return JsonResponse(rooms, safe=False)


def port_forwardings(request):
    users = {}
    for room in Room.objects.all():
        tenant = room.current_tenant()
        if tenant and tenant.person.portforwarding_set.exists():
            forwardings = []
            for f in tenant.person.portforwarding_set.all():
                forwardings.append({
                    'source_port': f.source_port,
                    'destination_ip': f.destination_ip,
                    'destination_port': f.destination_port,
                })
            users[room.vlan_id] = forwardings
    return JsonResponse(users, safe=False)


def radius(request, mac_address):
    # see https://git.rwth-aachen.de/hilton-dorm/freeradius-config/-/blob/master/3.0/mods-available/rest for response json
    # see https://wiki.mikrotik.com/wiki/Manual:RADIUS_Client#Access-Accept for attributes
    port_type = request.GET.get('port_type', '')
    wireless = "Wireless" in port_type
    mac = SpecialMac.objects.filter(mac=EUI(mac_address)).first()
    if mac and mac.vlan:
        return JsonResponse({'Mikrotik-Wireless-VLANID': str(mac.vlan),
                             'Mikrotik-Wireless-Comment': f"Special Mac (vlan {mac.vlan})",
                             'Session-Timeout': str(3600 * 24)})
    mac = Mac.objects.filter(mac=EUI(mac_address)).first()
    if mac:
        mac.last_used = timezone.now()
        mac.save()
    if mac and not mac.deleted and not mac.blocked:
        membership = Membership.get_current_membership_for(mac.person)
        if membership and membership.has_network_access():
            if not wireless:
                return JsonResponse({}, status=204)  # accept no content
            tenant = mac.person.get_current_tenant()
            if tenant and tenant.room.vlan_id:
                return JsonResponse({'Mikrotik-Wireless-VLANID': str(tenant.room.vlan_id),
                                     'Mikrotik-Wireless-Comment': f"{tenant.person} {tenant.room.number} (vlan {tenant.room.vlan_id})",
                                     'Session-Timeout': str(3600 * 24)})
            else:
                reason = f"{mac.person}: No vlan id for room" if tenant else f"{mac.person}: Not a tenent"
        else:
            reason = f"{mac.person}: No network access" if membership else f"{mac.person} is not a member"
    else:
        reason = f"blocked: {mac.blocked}, deleted: {mac.deleted}" if mac else "mac unknown"
    RadiusMacAuthError(created=now(), mac=mac_address, url=request.get_full_path(), reason=reason).save()
    # Only keep 100 newest entries per mac
    for e in RadiusMacAuthError.objects.filter(mac=mac_address).order_by("-created")[100:]:
        e.delete()
    if wireless:
        who = "unbekannt"
        if mac:
            tenant = mac.person.get_current_tenant()
            who = f"{mac.person} {tenant.room.number if tenant else 'extern'}"
        return JsonResponse(
            {'Mikrotik-Wireless-VLANID': '2',
             'Mikrotik-Wireless-Comment': f"{who} Abgelehnt: {reason} (vlan 2)",
             'Session-Timeout': '30'})
    return JsonResponse({}, status=401)  # reject


def mails(request):
    # see https://help.itc.rwth-aachen.de/en/service/uvjom6mlb9yw/article/0e93c414608f4837b877a9781acf94b5/
    text = ''
    for address in MailForwarding.objects.using('iredmail').values_list('address', flat=True).distinct():
        text += address[:address.find('@')].lower() + "\n"

    return HttpResponse(text.encode('utf-8'))


@csrf_exempt
def noc_ap_status(request):
    extra_requests_args = {}
    remoteurl = 'https://noc-portal.rz.rwth-aachen.de/mops-admin/coverage/organizations/RM-ORG-c23189f36a39731da44afaee4ce5b199cd419497/organization_in_buildings/106'
    return proxy_view(request, remoteurl, extra_requests_args)


def send_ap_offline_email(request):
    ap_name: str = request.GET.get('ap', None)
    if ap_name:
        main_email = config.EMAIL['EMAIL_ACCOUNT_ADMIN']
        bcc_email = None
        name = "Admin"

        try:
            room_number = int(ap_name)  # will trow ValueError if ap number is not a number (e.g. workshop)
            room: Room = Room.objects.filter(number=room_number).first()
            person: Person = room.current_tenant().person  # will trow AttributeError if room == None (not found)
            main_email = person.user.email
            # bcc_email = config.EMAIL['EMAIL_ACCOUNT_ADMIN']
            name = person.full_name()
        except Exception:
            pass

        mail.send(
            recipients=main_email,
            bcc=bcc_email,
            sender=config.EMAIL['EMAIL_ACCOUNT_NETWORK'],
            template='ap_offline_email',
            context={
                'name': name,  # full name
                'ap_name': ap_name,
            }
        )
        return HttpResponse(f"Mail for AP-{ap_name} has been send to user '{name}'".encode('utf-8'))
    else:
        return HttpResponse("Request needs 'ap' and 'last_seen' parameter.".encode('utf-8'), status=400)
