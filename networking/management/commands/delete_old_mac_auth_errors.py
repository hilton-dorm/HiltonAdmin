from datetime import timedelta

from django.core.management.base import BaseCommand
from django.utils import timezone

from networking.models import RadiusMacAuthError


class Command(BaseCommand):
    help = 'Delete mac auth errors older than 14 days'

    def handle(self, *args, **options):
        RadiusMacAuthError.objects.filter(created__lt=timezone.now() - timedelta(days=14)).delete()
