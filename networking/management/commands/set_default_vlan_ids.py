from django.core.management.base import BaseCommand
from main.models import Room


class Command(BaseCommand):
    """
    Sets the vlan ids for rooms according to https://git.rwth-aachen.de/hilton-dorm/network-documentation/-/blob/62fd6b0c0153d9a059e1b88c08a4c7b2290624ad/infrastructure/VLANs.md
    """
    help = 'Sets the vlan ids for rooms'

    def handle(self, *args, **options):
        rooms = [1010, 1020, 1030, 1040, 1050, 1061, 1062, 1071, 1072, 1081, 1082, 1083, 1090, 1101, 1102, 2010, 2020,
                 2031, 2032, 2041, 2042, 2051, 2052, 2061, 2062, 2071, 2072, 2073, 2080, 2091, 2092, 3010, 3020, 3031,
                 3032, 3041, 3042, 3051, 3052, 3061, 3062, 3071, 3072, 3073, 3080, 3091, 3092, 4010, 4020, 4031, 4032,
                 4041, 4042, 4051, 4052, 4061, 4062, 4071, 4072, 4073, 4080, 4091, 4092, 5010, 5020, 5031, 5032, 5041,
                 5042, 5051, 5052, 5061, 5062, 5071, 5072, 5073, 5080, 5091, 5092, 6010, 6020, 6031, 6032, 6041, 6042,
                 6051, 6052, 6061, 6062, 6071, 6072, 6073, 6080, 6091, 6092, 7010, 7020, 7031, 7032, 7041, 7042, 7051,
                 7052, 7061, 7062, 7071, 7072, 7073, 7080, 7091, 7092]
        for i, room in enumerate(rooms, start=11):
            r = Room.objects.get(number=str(room))
            r.vlan_id = i
            r.save()
