# Finances

Die Finanzlogik ist in der Django app "finances" realisiert.
Die Hauptidee ist, dass jeder Bewohner ein Guthaben-Konto führt welches für die Netzwerkrechnungen, Gebühren, Mahnungen, und Mitgliedsbeiträgen genutzt wird.

Bei der Relisierung waren uns folgendes Eigenschaften wichtig und wurden wie folgt realisiert:

## Totale Transparenz
Das heißt, es bleiben immer alle Informationen über Transaktionen und Rechnungen erhalten.
Das Guthaben ist also kein Wert in unserer Datenbank sondern wird über die Inputs und Outputs on-the-fly über die History aller Transaktionen des jeweiligen Bewohners berechnet.
Wie Bitcoin lol.

Zahlt ein Bewohner ein, so wird ein positiver Betrag verbucht.
Zahlt er damit eine Rechnung, wird diese dem Betrag gegengerechnet.
Die Differenz ist die Höhe des Guthabens.

Das Model der Transaktionen enthält also folgende Infos:

- person: Die Person die überwiesen hat. Dieses Feld ist optional, damit Vereinsrechnungen verbucht werden können
- subaccount: Siehe "Virtuelle Unterkonten"
- date
- amount: Kann auch ein negativer Wert sein (Ist bei Rechnungen der Fall)
- note
- receipt_1 - receipt_5
- include_in_turnover: Boolean ob die Transaktion im Umsatz des Vereines verrechnet werden soll oder nicht

Im folgendem Bild ist die Verwaltungswebseite zu sehen. Auf diese haben alle Bewohner zugriff.

![/association/finances](img/client_overview.png)

Jeden Monat werden Rechnungen abgebucht. Rutscht das Mitglied ins Minus, so wird er vom Internet gesperrt und so weiter. Das Erstellen der Rechnungen wollen wir evtl. noch ändern, aber das ändert nichts an der generellen Implementation.


## Virtuelle Unterkonten
Zuerst ein kleiner Exkurs wie unser Verein Mitgliederzahlungen entgegennimmt:
Der Verein ist in 3 Teile aufgeteilt, bei der jeder Teil ein eigenes Bankkonto besitzt. Dazu gehören die Netz-AG, die Bar-AG, und "das Haus" (für Werkstatt, Fitnessraum, Sommer/Winterfeste/...).
Die Gebührenordnung sieht wie folgt aus:

* Jeden Monat: 2€ Haus + 4€ Netzwerk (optional)

Um Gebühren zu sparen und es für die Mitglieder so einfach wie möglich zu gestalten wird ausschließlich auf das Netzwerkkonnto überwiesen.
Der Bankauszug wird dann eingelesen und die Transaktion wird erstellt (mit entsprechender Person, mapped über den Verwendungszweck). Ein Großteil der Mitglieder richtet einfach Daueraufträge dafür ein oder überweist im Vorraus um Transaktionsgebühren zu sparen (was bei internationalen Studenten der Fall sein kann).

Die Logik, also was mit dem Geld passiert, wird aber auf einem anderen Layer entschieden.
Dazu wurden Unterkonten eingeführt. Diese werden bei der Transaktion einfach referenziert.
Die Transaktion wird also auf dem Netzwerkkonto in der Verwaltung verbucht, allerdings auf einem von mehreren Unterkonten.

Diese Unterkonten erlauben besseres Planen und eine bessere Übersicht:

- (Balance: Das ist der berechnete Wert der auf dem Bankkonto liegen sollte. Ist nur zum schnellem Gegencheck gedacht)
- Liquidity: Die Summe die der Inhaber des Kontos (Netz, Bar, oder Haus), frei zur Verfügung hat um dieses zb. zu investieren.
- Liquidity Cash: Gleiches Prinzip, aber hier ist nur aufsummiert wie viel Vermögen der Inhaber in bar hat. Das ist nur bei der Bar relevant.
- Receivables: Die Summe die potentiell noch offen steht. Forderungen also. Diese sollte man aber nicht als gegeben einplanen sondern stellt nur eine obere Grenze dar. Meistens sind hier unbezahlte Rechnungen einbegriffen. Diese werden oft nicht mehr bezahlt wenn der Bewohner ausgezogen ist.
- Outsite Capital: Das Fremdkapital was uns nicht gehört. Ergo: Summe der Guthaben der Mitglieder. Diese Summe ist daher auch nicht in Liquidity berücksichtigt, wohl aber in "Balance".
- Reserves: Hier können Rücklagen für Investitionen oder bevorstehende Rechnugen (zb. Notar) gebildet werden. Diese Summe ist auch nicht in Liquidity berüchsichtigt damit sie nicht für anderes ausgegeben wird.
- Reserves special: Ähnlich wie reserves. Dieses Unterkonto wurde für einen besseren Überblick hinzugefügt. Es wird nur von der NetzAg genutzt um zu vermerken wie viel dem Haus noch überwiesen werden muss, schließlich bezahlen die Mitglieder nur auf das Bankkonto der Netz-AG ein.


Im folgendem Screenshot ist der Admin Bereich der Konten zu sehen. Hier haben nur Vereinsverantwortliche drauf Zugriff. Hier zu sehen ist zb., dass auf dem "OUTSITE CAPITAL" Unterkonto vom Netzwerkkonto recht viel Geld lagert. Das ist darauf zurückzuführen dass dies die Summe aller Mitgliederguthaben ist. Ergo: Uns gehört dieses Geld (noch) nicht. Es liegt allerdings schon auf unserem Konto. Ausgegeben werden darf es nicht, deshalb wird es auch nicht in der Spalte der "Liquidity" Unterkonten verrechnet.

![/admin/finances/account/](img/account_overview.png)

### Cashflows
#### Einzahlung des Bewohners
Transaktion wird aufs OUTSITE CAPITAL konto der Netz-Ag verbucht.

#### Rechungserstellung für Netzwerk
Transaktion mit Rechnungshöhe und betroffener Person wird aufs RECEIVEABLES der Netz-AG verbucht.

Danach: -> Matching


#### Rechungserstellung des Mitgliederbeitrages
Transaktion mit Rechnungshöhe und betroffener Person wird aufs RECEIVEABLES des Hauses verbucht.

Danach: -> Matching

#### Matching
Es existieren nun offene Forderungen die evtl. beglichten werden können.
Der Mitgliedsbeitrag wird zuerst beglichten, danach erst der Netzbeitrag.
Dadurch wird sicher gestellt dass der Mitgliedsbeitrag Bedingung fürs Netzwerk ist.
Es wird also über jedes Mitglied iteriert:
amount(HOUSE/RECEIVABLES) - amount(NETWORK/OUTSITE_CAPITAL) >= 0?

- negierte Transaktion auf OUTSITE_CAPITAL von der NetzAG
- negierte Transaktion auf RECEIVABLES vom Haus
- positive Transaktion auf RECEIVABLES_SPECIAL von NetzAG (Von dort aggregiert über alle Transaktionen manuelle Überweiung an LIQUIDITY vom Haus).

Also 3 Transaktionen. Bedenke, dass die Netz-AG nun eine Verbindlichkeit gegenüber des Hauses offen hat. Daher wird es zurückgelegt und regelmäßig aggregiert aufs Hauskonto überwiesen.

Reicht das Guthaben des Mitgliedes dagegen nicht aus, so landet er im Minus und muss erst mal neu überweisen bis sein Internet wieder freigeschaltet wird.
Es wird auch ein Counter erhöht, welcher mitzählt wie lange die Person schon im Rückstand ist. Nach mehreren Monaten wird über seine Mitgliedschaft entschieden.

Angenommen, die Hausrechnung wurde gemachted. Das bedeutet:

- Guthaben ist verringert
- Es wurden 3 neue Transaktionen erstellt.

Das gleiche Spiel wird jetzt auch mit der Netzrechnung durchgegangen.
Hier wird allerdings folgendes auf den Unterkonten der Netz-AG verbucht:

- negiert auf RECEIVEABLES von NetzAG
- negiert auf OUTSITE_CAPITAL von NetzAG
- positiv auf LIQUIDITY von NetzAG


Diese Methode ermöglicht auf eine einfache Implementierung von Übersichtsseiten.
Im folgendem zb. eine Übersicht auf die AG-Mitglieder Zugriff haben. Die Netz AG sieht hier zb. die Gründe warum Personen keinen Netzwerkzugriff haben, wie viel Schulden sie haben, und vieles mehr.

 ![/admin/association/membership/](img/membership_overview.png)
