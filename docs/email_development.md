Installation
============

#### Development Server

> Since we use celery workers for background tasks please follow the instructions [here](celery_setup.md) first

First head over and open the 'config.py' file.
Change the mail settings as following.

<pre>
EMAIL = {
    'EMAIL_HOST': '0.0.0.0,
    'EMAIL_PORT': 1025,
    'EMAIL_HOST_USER': '',
    'EMAIL_HOST_PASSWORD': '',
    'EMAIL_USE_TLS': not DEBUG
}
</pre>

Then, `cd docs` into the directory containing the 'docker-compose.yml' file.
Make sure you have docker installed.

Start the MailHog Server: `docker-compose up -d mailhog`

You will find the webgui here: [http://0.0.0.0:8025/](http://0.0.0.0:8025/)

You can test it by starting a Django Shell:

* `python manage.py shell`
* `from django.core.mail import send_mail​`
* `send_mail('Subject here', 'Here is the message.', 'messanger@localhost.com',['any@email.com'], fail_silently=False)`

It should appear in MailHog locally.