### Migrate old database
This script was used to migrate data from the old website to the new one. However, this is probably not needed anymore and can be ignored.

#### Old MySQL -> Sqlite

Copy the mysql2sqlite script (you can find it [here](https://github.com/dumblob/mysql2sqlite) or [locally](mysql2sqlite.sh)).

* `chmod +x mysql2sqlite.sh`
* `mysqldump -u root -p --skip-extended-insert --compact wohnheim > dump.sql`
* `./mysql2sqlite.sh dump.sql | sqlite3 db_old.sqlite3`

Download the db_old.sqlite3 database and place it next to the db.sqlite3 file.
DO NOT COMMIT THIS. It should be ignored by .gitignore but double check it!

Remove all data (including the superuser).

Then `manage.py migrate` and `python3 docs/migrate.py` from the django shell.

