Installation
============

#### Requirements
Install a `rabbitmq-server` locally and start it (enable it as a daemon in production).

#### Start celery workers

For development purposes: `celery -A hiltonadmin worker -l info`

For a production environment follow instructions on [daemonization](http://docs.celeryproject.org/en/latest/userguide/daemonizing.html#daemonizing).