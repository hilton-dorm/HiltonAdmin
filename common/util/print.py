import os.path

import cups
import tempfile
from django.core.files.uploadedfile import TemporaryUploadedFile, UploadedFile

import config
from post_office import mail

job_cache = {}


def print_file(uploaded_file: UploadedFile, page_range: None | str, num_copies: int):
    conn = cups.Connection()
    printers = conn.getPrinters()
    if not config.CUPS_PRINTER_NAME in printers:
        mail.send(
            config.EMAIL["EMAIL_ACCOUNT_ADMIN"],
            config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
            subject=f"Verwaltung: Printer {config.CUPS_PRINTER_NAME} not found",
            message=f"The printer {config.CUPS_PRINTER_NAME} configured in config.py in the Verwaltungs container was not found."
                    f"Only the following printers are available: {', '.join(printers.keys())}"
        )
        raise Exception(f"Printer not found. Networking group is informed.")
    options = {
        'copies': str(num_copies)
    }
    if page_range:
        options['page-ranges'] = page_range
    temp_file = tempfile.NamedTemporaryFile(delete=False)
    for chunk in uploaded_file.chunks():
        temp_file.write(chunk)
    temp_file.close()
    job_id = conn.printFile(config.CUPS_PRINTER_NAME, temp_file.name, f"{uploaded_file.name}", options)
    job_cache[job_id] = options | {'name': uploaded_file.name}
    return job_id


# from https://www.pwg.org/ipp/ippguide.html#job-status-attributes
JOB_STATES = {
    cups.IPP_JOB_ABORTED: "Aborted",
    cups.IPP_JOB_CANCELED: "Canceled",
    cups.IPP_JOB_COMPLETED: "Completed",
    cups.IPP_JOB_HELD: "Held",
    cups.IPP_JOB_PENDING: "Pending",
    cups.IPP_JOB_PROCESSING: "Printing",
    cups.IPP_JOB_STOPPED: "Stopped",
}


def printer_jobs() -> list[dict[str, str]]:
    conn = cups.Connection()
    jobs_dict = conn.getJobs(requested_attributes=["job-name", "job-state"])
    jobs = []
    for job_id in jobs_dict:
        job: dict = jobs_dict[job_id]
        job['id'] = job_id
        if job_id in job_cache:
            job.update(job_cache[job_id])
            text = f"{job['name']}"
            if 'page-ranges' in job:
                text += f", Page {job['page-ranges']}"
            if int(job['copies']) > 1:
                text += f", {job['copies']} copies"
        elif "job-name" in job:
            text = job["job-name"]
        else:
            text = "Job " + job_id
        if "job-state" in job:
            job["state"] = JOB_STATES[job["job-state"]]
            text += f", State {job['state']}"
        job["text"] = text
    return jobs


def cancel_job(job_id):
    conn = cups.Connection()
    conn.cancelJob(job_id)
