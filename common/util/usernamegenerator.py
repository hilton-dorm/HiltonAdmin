from django.contrib.auth.models import User

# non-ascii characters and their ascii equivalents
USERNAME_CARACTER_REPLACEMENTS = (
    ("ä", "ae"),
    ("ö", "oe"),
    ("ü", "ue"),
    ("ß", "ss")
)


class UsernameGenerator:
    @staticmethod
    def generate(first_name, last_name):
        """
        Generates a unique username given an user by taking the first letter
        of the first name concatinated with the last name.
        If this username is not unique the generator will add a number at the end until it is unique.

           Args:
               first_name, last_name

           Returns:
               A string containing the unique username
        """

        username = f"{first_name[0]}{last_name}".lower().replace(" ", "")

        # Replace specified non-ascii characters with their ascii equivalents
        for replacement in USERNAME_CARACTER_REPLACEMENTS:
            username = username.replace(replacement[0], replacement[1])

        # Remove non-english characters
        username = username.encode("ascii", errors="ignore").decode()
        x = 1
        while True:
            if x == 1 and User.objects.filter(username=username).count() == 0:
                return username
            else:
                new_username = f"{username}{x}"
                if User.objects.filter(username=new_username).count() == 0:
                    return new_username
            x += 1
            if x > 1000000:
                raise Exception("Name is super popular!")
