import json
import datetime


class DateEncoder(json.JSONEncoder):
    def default(self, z):
        if isinstance(z, datetime.date):
            return str(z)
        else:
            return super().default(z)


def dump_json(obj, *args, **kwargs):
    return json.dumps(obj, *args, cls=DateEncoder, **kwargs)
