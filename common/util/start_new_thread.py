from threading import Thread

from django.core.mail import mail_admins


def start_new_thread(function):
    # from https://stackoverflow.com/questions/43187991/how-to-catch-exceptions-that-happened-in-a-python-threading-thread-execution
    def catch_exceptions(*args, **kwargs):
        try:
            function(*args, **kwargs)
        except BaseException:  # Any not handled exception will send an email with the traceback
            import traceback
            details = traceback.format_exc()
            mail_admins('Background exception happened', details)
    # beim Benutzen auf https://stackoverflow.com/a/28913218/10162645 achten

    def decorator(*args, **kwargs):
        t = Thread(target=catch_exceptions, args=args, kwargs=kwargs)
        t.daemon = True
        t.start()
    return decorator
