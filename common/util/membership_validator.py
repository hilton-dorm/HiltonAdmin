from django.core.exceptions import ValidationError
from django.db.models import Q


# Don't allow new active memberships if there is an active (date_leave == None => active membership)
def ensure_one_membership_at_any_time(instance, objects, additional_query=Q()):
    active_membership = objects.filter(additional_query, ~Q(id=instance.id), person=instance.person,
                                       date_leave__isnull=True).first()
    if active_membership is not None and instance.date_leave is None:
        raise ValidationError(
            f'Can not create a new membership if there is an active existing one with id {active_membership.id}')
    # check if membership boundaries overlap
    existing_memberships = objects.filter(~Q(id=instance.id), additional_query, person=instance.person)
    for m in existing_memberships:
        if m.date_leave is None:  # m is the active membership
            if instance.date_leave >= m.date_join:
                raise ValidationError(
                    f'The membership overlaps with the active membership {m.id} (start: {m.date_join}, no end)')
        else:
            if m.date_join <= instance.date_join <= m.date_leave:
                raise ValidationError(
                    f'The join date ({instance.date_join}) overlaps with the membership {m.id} (start: {m.date_join}, end: {m.date_leave})')
            if instance.date_leave is not None:
                if m.date_join <= instance.date_leave <= m.date_leave:
                    raise ValidationError(
                        f'The leave date ({instance.date_leave}) overlaps with the membership {m.id} (start: {m.date_join}, end: {m.date_leave})')
                if instance.date_join <= m.date_join and m.date_leave <= instance.date_leave:
                    raise ValidationError(
                        f'The membership overlaps with the membership {m.id} (start: {m.date_join}, end: {m.date_leave})')
