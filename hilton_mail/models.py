from django.db import models


class MailForwarding(models.Model):
    """
    This mirrors the forwarding table of the vmail database used by iredmail
    To use this model add ".using('iredmail')" to the query. For Example MailForwarding.objects.using('iredmail').all()
    """
    address = models.CharField(max_length=255, null=False)
    forwarding = models.CharField(max_length=255, null=False)
    domain = models.CharField(max_length=255, null=False, default=False)
    dest_domain = models.CharField(max_length=255, null=False, default=False)
    is_list = models.BooleanField(default=True)
    active = models.BooleanField(default=True)

    class Meta:
        managed = False
        db_table = 'forwardings'


class GroupMailAlias(models.Model):
    """
    This mirrors the alias table of the vmail database used by iredmail
    To use this model add ".using('iredmail')" to the query. For Example GroupMailAlias.objects.using('iredmail').all()
    """
    address = models.CharField(max_length=255, null=False, primary_key=True)
    domain = models.CharField(max_length=255, null=False, default=False)
    active = models.BooleanField(default=True)

    class Meta:
        managed = False
        db_table = 'alias'
