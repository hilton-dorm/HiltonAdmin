from django.apps import AppConfig


class HiltonMailConfig(AppConfig):
    name = 'hilton_mail'
