from auditlog.registry import auditlog
from django.contrib.auth.models import Group
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Q
from django.db.models.signals import post_save, pre_delete
from django.dispatch import receiver
from django.utils import timezone

from main.models import Person
from common.util.membership_validator import ensure_one_membership_at_any_time


class WorkingGroup(models.Model):
    group_django = models.ForeignKey(Group, on_delete=models.CASCADE, null=True)
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    email = models.CharField(max_length=250)
    should_attend_at_new_tenant_bar = models.BooleanField(default=False)
    first_leader_is_social_service = models.BooleanField(default=True)
    second_leader_is_social_service = models.BooleanField(default=False)
    members_can_be_active = models.BooleanField(default=False)
    members_are_active_by_default = models.BooleanField(default=False)
    is_mail_forwarding_sync_enabled = models.BooleanField(default=False, help_text="If enabled mails are forwarded to the working group leaders")

    group_membership = models.ManyToManyField(
        Person,
        through='GroupMembership',
    )

    def __str__(self):
        return self.name

    def is_member(self, person):
        return self.current_members().filter(person=person).exists()

    def current_members(self):
        return self.groupmembership_set.filter(
            Q(date_join__lte=timezone.now()) & (Q(date_leave__isnull=True) | Q(date_leave__gt=timezone.now())))

    def get_leaders(self):
        return self.current_members().filter(
            Q(type=GroupMembership.TYPE_FIRST_LEADER) | Q(type=GroupMembership.TYPE_SECOND_LEADER)).order_by(
            'type')

    def get_leader(self):
        return self.get_leaders().first()


class RoomKey(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField(blank=True)
    amount = models.SmallIntegerField()
    key_holder = models.ManyToManyField(
        Person,
        through='KeyHolder',
    )

    def __str__(self):
        return self.name


class GroupMembership(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    working_group = models.ForeignKey(WorkingGroup, on_delete=models.CASCADE)
    date_join = models.DateField(default=timezone.now)
    date_leave = models.DateField(blank=True, null=True)
    TYPE_FIRST_LEADER = '0'
    TYPE_SECOND_LEADER = '1'
    TYPE_MEMBER = '2'
    TYPES = (
        (TYPE_FIRST_LEADER, '1. Leader'),
        (TYPE_SECOND_LEADER, '2. Leader'),
        (TYPE_MEMBER, 'Member'),
    )
    type = models.CharField(max_length=2, choices=TYPES, default=0)
    comment = models.CharField(max_length=200, blank=True)
    mailing_list_enabled = models.BooleanField(default=True)

    def __str__(self):
        return self.person.full_name() + "_" + self.working_group.name

    @staticmethod
    def current_members():
        return GroupMembership.objects.filter(
            Q(date_join__lte=timezone.now()) & (Q(date_leave__isnull=True) | Q(date_leave__gte=timezone.now())))

    def clean(self):
        ensure_one_membership_at_any_time(self, GroupMembership.objects, Q(working_group=self.working_group))

    def is_active(self):
        """
        check if membership is running
        :return: true for active memberships
        """
        if self.date_leave is None:
            if self.date_join <= timezone.localdate():
                return True
            else:
                return False
        else:
            if self.date_leave > timezone.localdate():
                return True
            return False


class KeyHolder(models.Model):
    room_key = models.ForeignKey(RoomKey, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    amount_responsible_for = models.SmallIntegerField(default=1, blank=False, null=False)
    date_received = models.DateField()
    date_until = models.DateField(blank=True, null=True)
    internal = models.BooleanField(default=False, help_text="Wenn ein Schlüssel intern ausgegeben wurde und der Hausmeister dies nicht wissen soll.")

    def __str__(self):
        return self.person.full_name()

    @staticmethod
    def get_active_key_holders(when: timezone.datetime = timezone.now()):
        return KeyHolder.objects.filter(
            Q(date_received__lte=when) & (Q(date_until__isnull=True) | Q(date_until__gte=when)))

    def is_active(self) -> bool:
        """
        check if membership is running
        :return: true for active memberships
        """
        if self.date_until is None:
            if self.date_received <= timezone.localdate():
                return True
            else:
                return False
        else:
            if self.date_until > timezone.localdate():
                return True
            return False


@receiver(post_save, sender=GroupMembership, dispatch_uid="add_group_django")
def add_group_django(sender, instance, **kwargs):
    instance.person.user.groups.add(instance.working_group.group_django)
    instance.person.user.is_staff = True
    instance.person.user.save()
    # if he left the group he should not be a member anymore...
    if instance.date_leave and instance.date_leave <= timezone.localdate():
        instance.working_group.group_django.user_set.remove(instance.person.user)


@receiver(pre_delete, sender=GroupMembership, dispatch_uid='delete_group_django')
def delete_group_django(sender, instance, **kwargs):
    instance.working_group.group_django.user_set.remove(instance.person.user)
    print(instance.person.user.groups.all())
    if len(instance.person.user.groups.all()) == 0:
        instance.person.user.is_staff = False
        instance.person.user.save()


auditlog.register(WorkingGroup)
auditlog.register(RoomKey)
auditlog.register(GroupMembership)
auditlog.register(KeyHolder)
