from django.contrib.auth.decorators import login_required
from django.db.models import Q, Value, Max
from django.db.models.functions import Coalesce
from django.http import HttpResponse
from django.template import loader
from django.utils import timezone

from workinggroups.models import WorkingGroup


@login_required
def contacts(request):
    """
    Shows the contact people in an overview
    :param request:
    :return:
    """
    working_groups = WorkingGroup.objects.all()
    today = timezone.localdate()
    for working_group in working_groups:
        working_group.memberships = working_group.current_members().order_by('type') \
            .select_related("person").prefetch_related("person__user").annotate(
            room=Coalesce(Max("person__tenant__room__number", filter=Q(person__tenant__date_move_in__lte=today, person__tenant__date_move_out__gte=today)),
                          Value("extern")))
    template = loader.get_template('workinggroups/contact_people.html')
    context = {
        'title': 'Contacts',
        'workinggroups': working_groups,
    }

    return HttpResponse(template.render(context, request))
