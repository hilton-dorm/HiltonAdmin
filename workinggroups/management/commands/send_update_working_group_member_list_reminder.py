from django.core.management.base import BaseCommand
from django.db.models import Q
from post_office import mail
from datetime import date

import config
from workinggroups.models import WorkingGroup, GroupMembership


class Command(BaseCommand):
    help = 'Sends a email to working group leaders with the current working group member list'

    def handle(self, *args, **options):
        type_map = {
            GroupMembership.TYPE_FIRST_LEADER: '1. Leader',
            GroupMembership.TYPE_SECOND_LEADER: '2. Leader',
            GroupMembership.TYPE_MEMBER: 'Member',
        }

        def get_current(members, type):
            for member in members.filter(type=type):
                if member.person.get_latest_tenant().date_move_out > date.today():
                    return member.person
            return None

        def get_responsible_person(members):
            leader = get_current(members, GroupMembership.TYPE_FIRST_LEADER)
            if leader is None:
                leader = get_current(members, GroupMembership.TYPE_SECOND_LEADER)
            if leader is None:
                # we choose the oldest member
                for member in members.order_by('date_join'):
                    if member.person.get_latest_tenant().date_move_out > date.today():
                        return member.person
            return leader

        for working_group in WorkingGroup.objects.filter(~Q(name__icontains="Hausmeister")):
            members = working_group.groupmembership_set.filter(Q(date_join__lt=date.today()) & (
                Q(date_leave__isnull=True) | Q(date_leave__gt=date.today())))
            person = get_responsible_person(members)
            if person is not None:
                for member in members:
                    member.type = type_map[member.type]
                mail.send(
                    person.user.email,
                    config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                    template='working_group_member_list',
                    context={'working_group': working_group.name, 'members': members},
                    priority='medium'
                )
            else:
                print("The working group ", working_group.name, " has no active member!")
