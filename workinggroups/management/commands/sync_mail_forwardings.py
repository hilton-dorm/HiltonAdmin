from django.core.management.base import BaseCommand
from django.db.models import Q
from datetime import date

from workinggroups.models import WorkingGroup
from hilton_mail.models import MailForwarding, GroupMailAlias


# Implemented based on https://docs.iredmail.org/sql.create.mail.alias.html

class Command(BaseCommand):
    help = 'Syncs the group memberships with the forwarding table of iredmail'

    def handle(self, *args, **options):
        working_groups = WorkingGroup.objects.filter(is_mail_forwarding_sync_enabled=True)

        for group in working_groups:
            # Create alias if not existing yet
            # Extract the domains which is necessary for the iredmail entry
            domain = group.email.split("@")[-1]
            group_alias, _ = GroupMailAlias.objects.using('iredmail') \
                .get_or_create(address=group.email, domain=domain)
            group_alias.save(using='iredmail')

            # Retrieve list of forwarding email addresses of the group
            current_forwards = MailForwarding.objects.using('iredmail')\
                .filter(address=group.email) \
                .values_list('forwarding', flat=True)

            group_members = group.get_leaders()
            # Add missing entries
            for member in group_members:
                if member.person.user.email not in current_forwards:

                    # Extract the domains which is necessary for the iredmail entry
                    dest_domain = member.person.user.email.split("@")[-1]

                    new_entry = MailForwarding.objects.using('iredmail')\
                        .create(address=group.email,
                                forwarding=member.person.user.email,
                                domain=domain,
                                dest_domain=dest_domain,
                                is_list=True,
                                active=True)
                    new_entry.save(using='iredmail')

            # Delete email addresses which do not belong to any group member
            for forward_entry in current_forwards:
                if not group_members.filter(person__user__email__iexact=forward_entry).exists():
                    MailForwarding.objects.using('iredmail')\
                        .filter(address=group.email, forwarding=forward_entry)\
                        .delete()
