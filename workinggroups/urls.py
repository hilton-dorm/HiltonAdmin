from django.urls import path

from workinggroups import views

urlpatterns = [
    path('contacts/', views.contacts, name='contacts'),
]
