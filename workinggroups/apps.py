from django.apps import AppConfig


class WorkinggroupsConfig(AppConfig):
    name = 'workinggroups'
