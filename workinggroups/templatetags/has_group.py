from django import template
from django.db.models import Q

register = template.Library()


@register.filter(name='should_see_finances_dashboard')
def should_see_finances_dashboard(user):
    return user.groups.filter(Q(name="senat") | Q(name="kassenwarte")).exists()
