# Generated by Django 2.2.5 on 2021-01-14 22:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('workinggroups', '0004_auto_20210114_2321'),
    ]

    operations = [
        migrations.CreateModel(
            name='GroupMailAlias',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('address', models.CharField(max_length=255)),
                ('domain', models.CharField(default=False, max_length=255)),
                ('active', models.BooleanField(default=True)),
            ],
            options={
                'db_table': 'forwardings',
                'managed': False,
            },
        ),
    ]
