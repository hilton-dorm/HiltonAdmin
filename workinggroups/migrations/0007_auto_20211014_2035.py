# Generated by Django 2.2.5 on 2021-10-14 18:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('workinggroups', '0006_auto_20210114_2346'),
    ]

    operations = [
        migrations.DeleteModel(
            name='GroupMailAlias',
        ),
        migrations.DeleteModel(
            name='MailForwarding',
        ),
    ]
