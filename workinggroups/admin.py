import nested_admin
from django.contrib import admin
from django.db.models import Q
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html
from django.utils.translation import gettext_lazy as _

from .models import RoomKey, GroupMembership, WorkingGroup, KeyHolder


class GroupMembershipActiveFilter(admin.SimpleListFilter):
    """
    Filter for active and non active Memberships in
    GroupMembership view
    """
    title = _('Aktive Memberships')
    parameter_name = 'move_in_status'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('0', _('inactive')),
            ('1', _('active')),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        now = timezone.localdate()
        if self.value() == '0':
            return queryset.filter(Q(date_leave__lte=now) |
                                   Q(date_join__gte=now))
        if self.value() == '1':
            return queryset.filter(Q(date_leave__gt=now) | Q(date_leave=None),
                                   date_join__lt=now)


@admin.register(GroupMembership)
class GroupMembershipAdmin(admin.ModelAdmin):
    """
    Define Group stuff
    """
    list_display = ('link_to_user',
                    'link_to_working_group',
                    'type', 'is_active',
                    'date_join',
                    'date_leave',
                    'mailing_list_enabled')
    list_editable = ['mailing_list_enabled']
    list_filter = ['working_group', GroupMembershipActiveFilter, 'type']
    autocomplete_fields = ('person', )
    list_select_related = ("person__user", "working_group")

    def link_to_user(self, obj):
        link = reverse("admin:auth_user_change", args=[obj.person.user.id])
        return format_html('<b><a href="{}">{}</a></b>', link, obj.person.full_name())

    link_to_user.short_description = 'Person'

    def link_to_working_group(self, obj):
        link = reverse("admin:workinggroups_workinggroup_change", args=[obj.working_group.id])
        return format_html('<b><a href="{}">{}</a></b>', link, obj.working_group)

    link_to_working_group.short_description = 'Working Group'


class GroupMembershipInline(nested_admin.NestedTabularInline):
    """
    Import GroupMembership in WorkingGroup
    """
    model = WorkingGroup.group_membership.through
    can_delete = False
    extra = 0
    ordering = ['date_join']
    autocomplete_fields = ('person',)

    def get_queryset(self, request):
        return super().get_queryset(request).filter(date_leave__isnull=True)


@admin.register(WorkingGroup)
class WorkingGroupAdmin(nested_admin.NestedModelAdmin):
    """
    WorkingGroup Admin
    """
    list_display = ('name', 'description', 'email')
    inlines = [GroupMembershipInline]
    exclude = ('group_membership',)


def can_access_internal_keyholders(request):
    return request.user.groups.filter(name='senat').exists() or request.user.is_superuser


class KeyHolderInline(nested_admin.NestedTabularInline):
    """
    Import KeyHolder in RoomKey
    """
    model = RoomKey.key_holder.through
    autocomplete_fields = ('person', )
    can_delete = False
    extra = 0

    def get_queryset(self, request):
        queryset = super().get_queryset(request)
        if not can_access_internal_keyholders(request):
            queryset = queryset.filter(internal=False)
        return queryset

    def get_exclude(self, request, obj=None):
        exclude = super().get_exclude(request, obj) or []
        if not can_access_internal_keyholders(request):
            exclude.append('internal')
        return exclude


@admin.register(RoomKey)
class RoomKeyAdmin(nested_admin.NestedModelAdmin):
    """
    RoomKey Admin
    """
    view_on_site = False
    list_display = ('name', 'amount')
    inlines = [KeyHolderInline]
    exclude = ('key_holder',)

    def get_model_perms(self, request):
        """
        Return empty perms dict thus hiding the model from admin index.
        RoomKey overview is not necessary because RoomKey is inline in
        User and KeyHolder. Filter for KeyHolder is available.
        """
        return {}


class KeyHolderActiveFilter(admin.SimpleListFilter):
    """
    Filter for active and non active Key Holders in
    Key Holder view
    """
    title = _('Active Key Holders')
    parameter_name = 'active'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return (
            ('0', _('Inactive')),
            ('1', _('Active')),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        now = timezone.localdate()
        if self.value() == '0':
            return queryset.filter(Q(date_until__lte=now) |
                                   Q(date_received__gte=now))
        if self.value() == '1':
            return queryset.filter(Q(date_until__gt=now) | Q(date_until=None),
                                   date_received__lte=now)


def end_key_holder(modeladmin, request, queryset):
    queryset.filter(date_until=None).update(date_until=timezone.localdate())


end_key_holder.short_description = "Person hat den Schlüssel nicht mehr"


@admin.register(KeyHolder)
class KeyHolderAdmin(admin.ModelAdmin):
    """
    KeyHolder Admin
    """
    list_display = ('link_to_room_key', 'link_to_user', 'is_living_here', 'is_active', 'date_received', 'date_until')
    list_filter = ['room_key', KeyHolderActiveFilter]
    search_fields = ['person__user__first_name', 'person__user__last_name']
    autocomplete_fields = ('person', )
    actions = [end_key_holder]

    def get_queryset(self, request):
        queryset = super(KeyHolderAdmin, self).get_queryset(request).select_related('person', 'room_key').prefetch_related('person__user')
        if not can_access_internal_keyholders(request):
            queryset = queryset.filter(internal=False)
        return queryset

    def link_to_user(self, obj):
        link = reverse("admin:auth_user_change", args=[obj.person.user.id])
        return format_html('<b><a href="{}">{}</a></b>', link, obj.person.full_name())

    link_to_user.short_description = 'Person'

    def link_to_room_key(self, obj):
        link = reverse("admin:workinggroups_roomkey_change", args=[obj.room_key.id])
        return format_html('<b><a href="{}">{}</a></b>', link, obj.room_key)

    link_to_room_key.short_description = 'Room Key'

    def is_living_here(self, obj):
        return obj.person.is_living_here()

    is_living_here.short_description = 'Is living here'
    is_living_here.boolean = True

    @admin.display(boolean=True, description="Has Key")
    def is_active(self, obj):
        return obj.is_active()

    def get_list_display(self, request):
        list_display = super().get_list_display(request)
        # Check if the user is in a specific group
        if can_access_internal_keyholders(request):
            list_display = list_display + ('internal', )
        return list_display

    def get_fields(self, request, obj=None):
        fields = super().get_fields(request, obj)
        # Check if the user is in a specific group
        if not can_access_internal_keyholders(request):
            # Remove the field for the group
            fields.remove('internal')
        return fields
