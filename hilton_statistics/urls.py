from django.urls import path

from hilton_statistics import views

urlpatterns = [
    path('statistics', views.statistics, name='statistics'),
]
