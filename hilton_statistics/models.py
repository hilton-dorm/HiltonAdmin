from django.db import models
from djmoney.models.fields import MoneyField


class TimeStatistics(models.Model):
    day = models.DateField(primary_key=True)

    female_tenants = models.IntegerField(null=True)
    male_tenants = models.IntegerField(null=True)
    divers_tenants = models.IntegerField(null=True)
    sex_unknown_tenants = models.IntegerField(null=True)

    normal_tenants = models.IntegerField(null=True)
    subtenants = models.IntegerField(null=True)
    international_tenants = models.IntegerField(null=True)

    german_tenants = models.IntegerField(null=True)
    non_german_tenants = models.IntegerField(null=True)

    active_tenants = models.IntegerField(null=True)

    normal_members = models.IntegerField(null=True)
    limited_members = models.IntegerField(null=True)
    honorary_members = models.IntegerField(null=True)
    sponsoring_members = models.IntegerField(null=True)
    unknown_members = models.IntegerField(null=True)

    tenant_members = models.IntegerField(null=True)

    network_balance = MoneyField(max_digits=10, decimal_places=2, default_currency='EUR', null=True)
    house_balance = MoneyField(max_digits=10, decimal_places=2, default_currency='EUR', null=True)
    network_with_receivables = MoneyField(max_digits=10, decimal_places=2, default_currency='EUR', null=True)
    house_with_receivables = MoneyField(max_digits=10, decimal_places=2, default_currency='EUR', null=True)
