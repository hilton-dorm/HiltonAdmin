from django.apps import AppConfig


class HiltonStatisticsConfig(AppConfig):
    name = 'hilton_statistics'
