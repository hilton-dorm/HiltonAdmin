from django.contrib.auth.decorators import login_required
from django.db.models import Count
from django.db.models.functions import ExtractMonth
from django.shortcuts import render
from django.utils.html import mark_safe

from hilton_statistics.models import TimeStatistics
from main.models import Tenant

MONTHS = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December',
]


@login_required
def statistics(request):
    values = TimeStatistics.objects.all().order_by("day")
    dates = "','".join([str(value.day) for value in values])
    female_count = ",".join([str(value.female_tenants) for value in values])
    male_count = ",".join([str(value.male_tenants) for value in values])
    divers_count = ",".join([str(value.divers_tenants) for value in values])
    sex_unknown_count = ",".join([str(value.sex_unknown_tenants) for value in values])
    tenant_count = ",".join([str(value.german_tenants + value.non_german_tenants) for value in values])

    normal_tenants = ",".join([str(value.normal_tenants) for value in values])
    subnormal_tenants = ",".join([str(value.subtenants) for value in values])
    international_tenants = ",".join([str(value.international_tenants) for value in values])

    active_tenants = ",".join([str(value.active_tenants) for value in values])
    tenant_members = ",".join([str(value.tenant_members) for value in values])
    german_tenants = ",".join([str(value.german_tenants) for value in values])
    non_german_tenants = ",".join([str(value.non_german_tenants) for value in values])

    normal_members = ",".join([str(value.normal_members) for value in values])
    limited_members = ",".join([str(value.limited_members) for value in values])
    honorary_members = ",".join([str(value.honorary_members) for value in values])
    sponsoring_members = ",".join([str(value.sponsoring_members) for value in values])
    unknown_members = ",".join([str(value.unknown_members) for value in values])
    total_members = ",".join(
        [str(value.normal_members + value.limited_members + value.honorary_members + value.sponsoring_members +
             value.unknown_members) for value in values])

    move_in_month = list(Tenant.objects.annotate(month=ExtractMonth('date_move_in')).values('month')
                         .annotate(count=Count('id')).order_by('month').values_list('count', flat=True))

    context = {
        'title': 'Hilton Statistics',
        'dates': mark_safe(dates),
        'female_count': mark_safe(female_count),
        'male_count': mark_safe(male_count),
        'divers_count': mark_safe(divers_count),
        'sex_unknown_count': mark_safe(sex_unknown_count),
        'tenant_count': mark_safe(tenant_count),

        'normal_tenants': mark_safe(normal_tenants),
        'subnormal_tenants': mark_safe(subnormal_tenants),
        'international_tenants': mark_safe(international_tenants),

        'active_tenants': mark_safe(active_tenants),
        'tenant_members': mark_safe(tenant_members),

        'german_tenants': mark_safe(german_tenants),
        'non_german_tenants': mark_safe(non_german_tenants),

        'normal_members': mark_safe(normal_members),
        'limited_members': mark_safe(limited_members),
        'honorary_members': mark_safe(honorary_members),
        'sponsoring_members': mark_safe(sponsoring_members),
        'unknown_members': mark_safe(unknown_members),
        'total_members': mark_safe(total_members),

        'move_in_month': mark_safe(",".join([str(n) for n in move_in_month])),
        'months': mark_safe("','".join(MONTHS))
    }

    """ Old finance statistics
    if request.user.is_superuser or request.user.groups.filter(name='board_members').exists():
        context["network_balance"] = mark_safe(",".join([str(value.network_balance.amount) for value in values]))
        context["house_balance"] = mark_safe(",".join([str(value.house_balance.amount) for value in values]))
        context["network_with_receivables"] = mark_safe(
            ",".join([str(value.network_with_receivables.amount) for value in values]))
        context["house_with_receivables"] = mark_safe(
            ",".join([str(value.house_with_receivables.amount) for value in values]))
    """

    return render(request, 'hilton_statistics/statistics.html', context)
