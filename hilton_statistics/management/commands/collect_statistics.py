from datetime import datetime, date as Date

from django.db.models import Count, Q
from django.core.management.base import BaseCommand
from django.utils.timezone import make_aware

from main.models import Person, Tenant
from association.models import Membership, MembershipType
from hilton_statistics.models import TimeStatistics
from finances.models import Account


class Command(BaseCommand):
    help = 'Computes statistics for this month if not already done'

    def add_arguments(self, parser):
        parser.add_argument('-a', '--all', help='Create statistics for every month since 01.01.2019',
                            action='store_true')
        parser.add_argument('-t', '--today', help='Create statistics for today',
                            action='store_true')
        parser.add_argument('-r', '--recompute', help='Recomputes the values if the entry already exists',
                            action='store_true')
        parser.add_argument('-c', '--clear', help='Removes all entries',
                            action='store_true')
        parser.add_argument('-e', '--entries', help='How many entries should be created per month', type=int,
                            default=1)

    def handle(self, *args, **options):
        today = Date.today()
        per_month = options['entries']
        recompute = options['recompute']
        if per_month == 1:
            days = [1]
        else:
            days = []
            for i in range(0, per_month):
                days.append(1 + int(28 / per_month * i))
        if options['clear']:
            TimeStatistics.objects.all().delete()
        if options['all']:
            for year in range(2019, today.year):
                for month in range(1, 13):
                    for day in days:
                        create_statistics(Date(year, month, day), recompute)
            for month in range(1, today.month):
                for day in days:
                    create_statistics(Date(today.year, month, day), recompute)
            for day in days:
                if day > today.day:
                    break
                create_statistics(Date(today.year, today.month, day), recompute)
        elif options['today']:
            create_statistics(today, recompute)
        else:
            days.reverse()
            for i in days:
                if today.day >= i:
                    create_statistics(Date(today.year, today.month, i), recompute)
                    return


def create_statistics(date, recompute):
    obj, created = TimeStatistics.objects.get_or_create(day=date)
    if created or recompute:
        daytime = make_aware(datetime.combine(date, datetime.min.time()))
        # set default value to 0
        obj.female_tenants = 0
        obj.male_tenants = 0
        obj.divers_tenants = 0
        obj.sex_unknown_tenants = 0
        for i in Tenant.current_tenants(daytime).values("person__gender").annotate(Count("id")).order_by(
                "person__gender"):
            if i['person__gender'] == Person.FEMALE:
                obj.female_tenants = i['id__count']
            elif i['person__gender'] == Person.MALE:
                obj.male_tenants = i['id__count']
            elif i['person__gender'] == Person.DIVERS:
                obj.divers_tenants = i['id__count']
            elif i['person__gender'] == Person.UNKNOWN:
                obj.sex_unknown_tenants = i['id__count']

        obj.normal_tenants = 0
        obj.subtenants = 0
        obj.international_tenants = 0
        for i in Tenant.current_tenants(daytime).values("tenant_type").annotate(Count("id")).order_by(
                "tenant_type"):
            if i['tenant_type'] == Tenant.TYPE_TENANT:
                obj.normal_tenants = i['id__count']
            elif i['tenant_type'] == Tenant.TYPE_SUBTENANT:
                obj.subtenants = i['id__count']
            elif i['tenant_type'] == Tenant.TYPE_INTERNATIONAL:
                obj.international_tenants = i['id__count']

        obj.german_tenants = Tenant.current_tenants(daytime).filter(person__nationality='DE').count()
        obj.non_german_tenants = Tenant.current_tenants(daytime).count() - obj.german_tenants

        obj.active_tenants = Tenant.current_tenants(daytime).filter(
            person__groupmembership__isnull=False).distinct().count()

        type_unknown = -1
        counter = {type_unknown: 0}
        for type in MembershipType.objects.all():
            counter[type.id] = 0

        normal_type_id = MembershipType.objects.filter(name__icontains="normal").first().id

        for member in Membership.current_memberships(daytime).select_related("type"):
            period = member.membershiptypeperiods_set.filter(start_date__lte=daytime).order_by("-start_date").first()
            if period:
                counter[period.type_id] += 1
            elif member.date_membership_type_since and member.date_membership_type_since <= daytime.date():
                # the periods were implemented after the date_membership_type_since field
                counter[member.type_id] += 1
            elif daytime >= make_aware(datetime(2020, 6, 5)) and not member.date_membership_type_since:
                # if the check for a date after the 5.6.2020, were the tracking was implemented, the type must be the
                # same as now, otherwiese there would be a date_membership_type_since entry.
                counter[member.type_id] += 1
            elif member.date_membership_type_since:
                # has a date_membership_type_since field, so was properly a normal member before
                counter[normal_type_id] += 1
            elif not member.date_leave:
                # the member has properly the same type as now
                counter[member.type_id] += 1
            else:
                # we can only guess
                counter[type_unknown] += 1

        obj.unknown_members = counter[type_unknown]
        for type in MembershipType.objects.all():
            if "normal" in type.name:
                obj.normal_members = counter[type.id]
            elif "limited" in type.name:
                obj.limited_members = counter[type.id]
            elif "sponsor" in type.name:
                obj.sponsoring_members = counter[type.id]
            elif "honorary" in type.name:
                obj.honorary_members = counter[type.id]

        obj.tenant_members = Tenant.current_tenants(daytime).filter(Q(person__membership__date_join__lt=daytime) & (
            Q(person__membership__date_leave__gt=daytime) | Q(person__membership__date_leave__isnull=True))
        ).count()

        network_account = Account.objects.get(title="Network")
        house_account = Account.objects.get(title="House")
        network_to_house = network_account.get_reserves_special(date)
        # do not subtract network_to_house from network_account.get_liquidity because it is not contained there
        obj.network_balance = network_account.get_liquidity(date)
        obj.house_balance = house_account.get_liquidity(date) + network_to_house
        obj.network_with_receivables = network_account.get_receivables(date) + obj.network_balance
        obj.house_with_receivables = house_account.get_receivables(date) + obj.house_balance

        obj.save()
