from django.core.exceptions import ValidationError
from django.db import models
from mptt.models import MPTTModel, TreeForeignKey


class Position(MPTTModel):
    name = models.CharField(max_length=100)
    image = models.ImageField(upload_to='inventory/position/images/')
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    def __str__(self):
        return self.parent.name + " " + self.name if self.parent else self.name

    def clean(self):
        end = self.parent
        while end is not None:
            if end == self:
                raise ValidationError("A position cannot be its own parent.")
            end = end.parent

    def save(self, *args, **kwargs):
        self.full_clean()
        super().save(*args, **kwargs)

    class Meta:
        ordering = ['name']

    class MPTTMeta:
        order_insertion_by = ['name']


class ArticleGroup(MPTTModel):
    name = models.CharField(max_length=100)
    parent = TreeForeignKey('self', on_delete=models.CASCADE, null=True, blank=True, related_name='children')

    def clean(self):
        end = self.parent
        while end is not None:
            if end == self:
                raise ValidationError("A position cannot be its own parent.")
            end = end.parent

    def __str__(self):
        return self.name

    class Meta:
        ordering = ['name']

    class MPTTMeta:
        order_insertion_by = ['name']


class Article(models.Model):
    name = models.CharField(max_length=200)
    image = models.ImageField(upload_to='inventory/article/images/')
    description = models.TextField(blank=True)
    quantity = models.PositiveIntegerField(default=1)
    position = models.ForeignKey(Position, on_delete=models.SET_NULL, null=True, blank=True)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    type = models.ForeignKey(ArticleGroup, on_delete=models.SET_NULL, null=True, blank=True)

    def __str__(self):
        return self.name
