from django.contrib import admin
from imagekit import ImageSpec
from imagekit.admin import AdminThumbnail
from imagekit.cachefiles import ImageCacheFile
from mptt.admin import MPTTModelAdmin, TreeRelatedFieldListFilter
from pilkit.processors import ResizeToFill

from inventory.models import Position, Article, ArticleGroup


class AdminThumbnailSpec(ImageSpec):
    processors = [ResizeToFill(150, 150)]
    format = 'WEBP'


def cached_admin_thumb(instance):
    # `image` is the name of the image field on the model
    cached = ImageCacheFile(AdminThumbnailSpec(instance.image))
    # only generates the first time, subsequent calls use cache
    cached.generate()
    setattr(cached, 'source', instance.image)
    return cached


@admin.register(Position)
class PositionAdmin(MPTTModelAdmin):
    search_fields = ('name',)
    list_display = ('name', 'admin_thumbnail')
    admin_thumbnail = AdminThumbnail(image_field=cached_admin_thumb, template='admin/inventory/list_thumbnail.html')


@admin.register(ArticleGroup)
class ArticleGroupAdmin(MPTTModelAdmin):
    search_fields = ('name',)
    pass


@admin.register(Article)
class ArticleAdmin(admin.ModelAdmin):
    list_select_related = ('position',)
    list_display = ('name', 'article_thumbnail', 'quantity', 'position', 'position_thumbnail')
    article_thumbnail = AdminThumbnail(image_field=cached_admin_thumb, template='admin/inventory/list_thumbnail.html')
    article_thumbnail.short_description = "Image"
    position_thumbnail = AdminThumbnail(
        image_field=lambda instance: cached_admin_thumb(instance.position) if instance.position else None,
        template='admin/inventory/list_thumbnail.html')
    position_thumbnail.short_description = "Position"
    search_fields = ('name', 'description', 'type__name')
    list_filter = (('type', TreeRelatedFieldListFilter), ('position', TreeRelatedFieldListFilter))
    autocomplete_fields = ('type', 'position',)

    def get_changeform_initial_data(self, request):
        initial = super().get_changeform_initial_data(request)
        if 'type' in request.GET:
            initial['type'] = request.GET.get('type')
        if 'position' in request.GET:
            initial['position'] = request.GET.get('position')
        return initial

    def response_add(self, request, obj, post_url_continue=None):
        response = super().response_add(request, obj, post_url_continue)
        if '_addanother' in request.POST:
            preserved_filters = self.get_preserved_filters(request)
            response['Location'] += f'?{preserved_filters}&type={obj.type_id}&position={obj.position_id}'
        return response
