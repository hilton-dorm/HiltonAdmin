from django.forms import BaseInlineFormSet, ModelForm
from django import forms

from finances_v2.models import validate_TransactionEntrySet, Account, Transaction


# from https://stackoverflow.com/questions/13526792/validation-of-dependant-inlines-in-django-admin
class TransactionEntryFormSet(BaseInlineFormSet):
    def clean(self):
        super().clean()
        if any(form.cleaned_data.get('DELETE') for form in self.forms):
            raise ValueError("It is forbidden to delete transaction entries")
        if all(form.is_valid() for form in self.forms):
            # for form in self.forms:
            # print(form.save(commit=False))
            #  print("data:",  form.cleaned_data)
            validate_TransactionEntrySet([form.save(commit=False) for form in self.forms if form.cleaned_data])


class SimplyTransactionHeaderForm(ModelForm):

    type = forms.ChoiceField(choices=[('+', 'Einnahme'), ('-', 'Ausgabe')])
    group_account = forms.ModelChoiceField(queryset=Account.objects.filter(type=Account.TYPE_EQUITY).all(), required=True)
    transaction_account = forms.ModelChoiceField(
        queryset=Account.objects.filter(type__in=[Account.TYPE_BANK, Account.TYPE_CASH]).all(), required=True)

    def clean(self):
        cleaned_data = super().clean()
        group_account = cleaned_data.get('group_account')
        transaction_account = cleaned_data.get('transaction_account')
        if group_account and transaction_account and group_account.working_group != transaction_account.working_group:
            self.add_error("transaction_account", "Must be owned by the working group of the field above")

    class Meta:
        model = Transaction
        fields = ["date", "note"]
