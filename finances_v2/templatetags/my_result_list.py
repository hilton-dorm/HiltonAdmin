from collections import defaultdict

from django import template
from django.contrib.admin.templatetags.admin_list import result_hidden_fields, result_headers, results
from django.db.models import QuerySet, ExpressionWrapper, Q, BooleanField
from django.utils.safestring import mark_safe
from django.utils import formats

from finances_v2.models import Account

register = template.Library()


def my_result_list(cl):
    """
    Displays the headers and data list together
    Code from https://github.com/django/django/blob/dfc77637ea5c1aa81caa72b1cf900e6931d61b54/django/contrib/admin/templatetags/admin_list.py#L331
    """
    headers = list(result_headers(cl))
    num_sorted_fields = 0
    for h in headers:
        if h["sortable"] and h["sorted"]:
            num_sorted_fields += 1

    # We want to add a column for every used Account in the result list
    q: QuerySet = cl.result_list
    accounts = Account.objects.filter(entries__transaction__in=q).distinct().annotate(active=ExpressionWrapper(Q(type__in=Account.ACTIVE_ACCOUNTS),
                                                                                                               output_field=BooleanField())).order_by("-active", "working_group", "type")

    for a in accounts:
        headers.insert(-1, {
            'text': str(a),
            'class_attrib': 'class=rotated-text',
            'sortable': False
        })
    res = list(results(cl))
    for (html, item) in zip(res, cl.result_list.prefetch_related("transactionentry_set__person", "transactionentry_set__person__user")):
        entries = defaultdict(list)
        for e in item.transactionentry_set.all():
            entries[e.account_id].append(f'<span title="{e.note} {e.person}">{formats.number_format(e.amount)}</span>')
        for a in accounts:
            if a.id in entries:
                if not a.active:
                    entries[a.id].insert(0, "")
                html.insert(-1, mark_safe(f'<td class="right-text">{"<br>".join(entries[a.id])}</td>'))
            else:
                html.insert(-1, mark_safe('<td></td>'))

    return {
        "cl": cl,
        "result_hidden_fields": list(result_hidden_fields(cl)),
        "result_headers": headers,
        "num_sorted_fields": num_sorted_fields,
        "results": res,
    }


register.inclusion_tag("admin/change_list_results.html")(my_result_list)
