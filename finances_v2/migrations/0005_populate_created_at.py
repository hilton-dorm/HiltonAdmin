# Generated by Django 4.2.10 on 2024-07-18 21:01
from datetime import datetime

from django.db import migrations
from django.utils import timezone


def populate_created_at(apps, schema_editor):
    Transaction = apps.get_model('finances_v2', 'Transaction')
    for transaction in Transaction.objects.all():
        transaction.created_at = timezone.make_aware(datetime.combine(transaction.date, datetime.min.time()))
        transaction.save()


class Migration(migrations.Migration):
    dependencies = [
        ('finances_v2', '0004_transaction_created_at'),
    ]

    operations = [
        migrations.RunPython(populate_created_at),
    ]
