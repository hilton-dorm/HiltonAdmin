# Generated by Django 4.2.10 on 2025-01-30 15:39

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('finances_v2', '0006_deletedtransaction'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='budget',
            options={'ordering': ('-start__time',)},
        ),
    ]
