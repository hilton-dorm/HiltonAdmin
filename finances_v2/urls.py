from django.urls import path

from . import views

urlpatterns = [
    path('dashboard', views.dashboard, name='dashboard'),
    path('pie_chart', views.pie_chart, name='pie_chart'),
    path('transactions', views.transactions, name='transactions'),
]
