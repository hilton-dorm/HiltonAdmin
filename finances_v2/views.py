import datetime
import json
from decimal import Decimal

from django.contrib.auth.decorators import login_required, user_passes_test
from django.db.models import Sum, Q
from django.http import JsonResponse, HttpResponse
from django.shortcuts import render
from django.urls import reverse_lazy, reverse
from django.utils.safestring import mark_safe

from finances_v2.models import Accounts, Account, TransactionEntry, Transaction, TransactionType
from main.models import Event


def get_entry_type_balances_per_year(account: Account, chart_name):
    data = account.entries.values("transaction__date__year", "type__name").order_by("transaction__date__year",
                                                                                    "type").annotate(
        balance_pos=Sum("amount", filter=Q(amount__gt=0)),
        balance_neg=Sum("amount", filter=Q(amount__lt=0)))
    result = {}
    names = {}
    types = set()
    for entry in data:
        year = result.get(entry['transaction__date__year'], {})
        name = entry['type__name'] or "Sonstiges"
        for postfix in ("pos", "neg"):
            if entry['balance_' + postfix] is not None:
                year[name + postfix] = float(entry['balance_' + postfix])
                types.add(name + postfix)
                names[name + postfix] = name
        result[entry['transaction__date__year']] = year

    as_list = []
    sums = {}
    sums.values()
    for key in result:
        sums[key] = sum(result[key].values())
        result[key]["year"] = key
        as_list.append(result[key])
    return [chart_name, mark_safe(json.dumps(as_list)), mark_safe(json.dumps(list(types))), mark_safe(json.dumps(sums)), mark_safe(json.dumps(names))]


def pie_chart_data(filters):
    return TransactionEntry.objects.filter(filters).values("type__name").order_by("type").annotate(
        balance=Sum("amount"))


def transaction_data(filters, sort):
    return TransactionEntry.objects.filter(filters).order_by(sort).values("transaction__id",
                                                                          "transaction__date",
                                                                          "transaction__note",
                                                                          "note",
                                                                          "amount")[:6]


def should_see_dashboard(user):
    return user.groups.filter(Q(name='senat') | Q(name='kassenwarte')).exists()


@login_required
@user_passes_test(should_see_dashboard, login_url=reverse_lazy('no_permission'))
def dashboard(request):
    data = []
    ek_net_sum = Decimal(0)
    ek_house_sum = Decimal(0)
    ek_symposion_sum = Decimal(0)
    receiv_net_sum = Decimal(0)
    receiv_house_sum = Decimal(0)
    last_week = None

    for o in TransactionEntry.objects.values("transaction__date__week", "transaction__date__year").order_by(
        "transaction__date__year", "transaction__date__week").annotate(
            ek_net=Sum('amount', filter=Q(account=Accounts().NETWORK_EK)),
            ek_house=Sum('amount', filter=Q(account=Accounts().HOUSE_EK)),
            ek_symposion=Sum('amount', filter=Q(account=Accounts().SYMPOSION_EK)),
            receiv_network=Sum('amount', filter=Q(account__working_group_id=Accounts().NETWORK_EK.working_group_id, account__type=Account.TYPE_RECEIVABLES)),
            receiv_house=Sum('amount', filter=Q(account__working_group_id=Accounts().HOUSE_EK.working_group_id, account__type=Account.TYPE_RECEIVABLES))):
        week = o["transaction__date__week"]
        year = o["transaction__date__year"]

        ek_net_sum += o["ek_net"] or 0
        ek_house_sum += o["ek_house"] or 0
        ek_symposion_sum += o["ek_symposion"] or 0
        receiv_net_sum += o["receiv_network"] or 0
        receiv_house_sum += o["receiv_house"] or 0
        end_of_week = datetime.datetime.strptime(f"{year} {week - 1} 6", "%Y %W %w")
        # when the year changes there are two half weeks that should be merged
        if last_week is not None and last_week == end_of_week:
            last_data = data[-1]
            last_data["ek_net"] = float(ek_net_sum)
            last_data["ek_house"] = float(ek_house_sum)
            last_data["ek_symposion"] = float(ek_symposion_sum)
            last_data["net_without_receiv"] = float(ek_net_sum - receiv_net_sum)
            last_data["house_without_receiv"] = float(ek_house_sum - receiv_house_sum)
        else:
            data += [{"date": end_of_week.isoformat(),
                      "ek_net": float(ek_net_sum),
                      "ek_house": float(ek_house_sum),
                      "ek_symposion": float(ek_symposion_sum),
                      "net_without_receiv": float(ek_net_sum - receiv_net_sum),
                      "house_without_receiv": float(ek_house_sum - receiv_house_sum),
                      }]
        last_week = end_of_week

    context = {
        'title': 'Finance Dashboard',
        'data': [
            get_entry_type_balances_per_year(Accounts().NETWORK_EK, "network"),
            get_entry_type_balances_per_year(Accounts().HOUSE_EK, "house"),
            get_entry_type_balances_per_year(Accounts().SYMPOSION_EK, "symposion"),
        ],
        'events': Event.objects.filter(type__isnull=False).order_by("-time")[:10],
        'sums': mark_safe(json.dumps(data)),
    }

    return render(request, 'finances_v2/dashboard.html', context)


def build_query(request):
    account = request.GET.get("account")
    if account == "network":
        query = Q(account=Accounts().NETWORK_EK)
    elif account == "house":
        query = Q(account=Accounts().HOUSE_EK)
    elif account == "symposion":
        query = Q(account=Accounts().SYMPOSION_EK)
    elif account is not None and account != "all":
        raise ValueError(f"{account} must be 'network', 'house' or 'symposion'")
    else:
        query = Q(account__in=(Accounts().NETWORK_EK, Accounts().HOUSE_EK, Accounts().SYMPOSION_EK))
    if "to" in request.GET:
        query &= Q(transaction__date__lte=request.GET.get("to"))
    if "from" in request.GET:
        query &= Q(transaction__date__gte=request.GET.get("from"))
    return query


def get_more_link(request, direction: str):
    link = reverse("admin:finances_v2_transactionentry_changelist")
    link += "?account__id__exact="
    account = request.GET.get("account")
    if account == "network":
        link += str(Accounts().NETWORK_EK.id)
    elif account == "house":
        link += str(Accounts().HOUSE_EK.id)
    elif account == "symposion":
        link += str(Accounts().SYMPOSION_EK.id)
    else:
        return None
    if "from" in request.GET:
        link += "&transaction__date__range__gte=" + request.GET.get("from")
    if "to" in request.GET:
        link += "&transaction__date__range__lte=" + request.GET.get("to")
    tx_type = request.GET.get("type")
    if tx_type == "Sonstiges":
        link += "&type__isnull=True"
    elif not tx_type is None:
        link += "&type__id__exact=" + str(TransactionType.objects.get(name=tx_type).id)
    if direction == "in":
        link += "&amount__range__gte=0&o=-4"
    else:
        link += "&amount__range__lte=0&o=4"
    return link


@login_required
@user_passes_test(should_see_dashboard, login_url=reverse_lazy('no_permission'))
def pie_chart(request):
    try:
        query = build_query(request)
    except ValueError as e:
        return HttpResponse(e.message, status=400)
    time_query = Q()
    if "to" in request.GET:
        time_query &= Q(date__lte=request.GET.get("to"))
    if "from" in request.GET:
        time_query &= Q(date__gte=request.GET.get("from"))
    house_fees_income = Transaction.objects.filter(time_query &  # transactions that reduce the tenant funds account and the receivables we get from them
                                                   Q(transactionentry__transaction__transactionentry__account=Accounts().HOUSE_RECEIVABLES_TENANTS) &
                                                   Q(transactionentry__account=Accounts().NETWORK_FK)) \
        .aggregate(balance=Sum("transactionentry__amount", default=0, filter=Q(transactionentry__amount__lte=0)))['balance']
    network_fees_income = Transaction.objects.filter(time_query &  # transactions that reduce the tenant funds account and the receivables we get from them
                                                     Q(transactionentry__transaction__transactionentry__account=Accounts().NETWORK_FK) & Q(
                                                         transactionentry__account=Accounts().NETWORK_RECEIVABLES_TENANTS)).aggregate(
        balance=Sum("transactionentry__amount", default=0, filter=Q(transactionentry__amount__lte=0)))['balance']
    data = {
        'in': {
            'values': list(pie_chart_data(query & Q(amount__gt=0))),
            'tx': list(transaction_data(query & Q(amount__gt=0), "-amount")),
            'more': get_more_link(request, "in"),
        },
        'out': {
            'values': list(pie_chart_data(query & Q(amount__lt=0))),
            'tx': list(transaction_data(query & Q(amount__lt=0), "amount")),
            'more': get_more_link(request, "out"),
        },
        'house_fees_income': -float(house_fees_income),
        'network_fees_income': -float(network_fees_income),
    }
    return JsonResponse(data, safe=False)


@login_required
@user_passes_test(should_see_dashboard, login_url=reverse_lazy('no_permission'))
def transactions(request):
    try:
        query = build_query(request)
    except ValueError as e:
        return HttpResponse(e.message, status=400)
    tx_type = request.GET.get("type")
    if tx_type == "Sonstiges":
        query &= Q(type__isnull=True)
    else:
        query &= Q(type__name=tx_type)
    dir = request.GET.get("direction")
    if dir == "in":
        query &= Q(amount__gt=0)
    else:
        query &= Q(amount__lt=0)
    data = {
        'tx': list(transaction_data(query, "-amount" if dir == "in" else "amount")),
        'more': get_more_link(request, dir),
    }
    return JsonResponse(data, safe=False)
