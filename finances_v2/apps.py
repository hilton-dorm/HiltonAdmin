from django.apps import AppConfig


class FinancesV2Config(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'finances_v2'
