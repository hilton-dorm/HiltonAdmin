from datetime import timedelta

from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone
from post_office import mail

import config
from finances_v2.models import Accounts
from main.models import Event
from nordigen_app.models import NordigenCredential


class Command(BaseCommand):
    help = 'Notifies the owner of the Networking Account if there are no semester start events in the next 6 months'

    @transaction.atomic()
    def handle(self, *args, **options):
        credentials = NordigenCredential.objects.filter(account=Accounts().NETWORK_BANK).first()
        if credentials:
            email = credentials.email
        else:
            email = config.EMAIL["EMAIL_ACCOUNT_VORSTAND"]
        in_180_days = timezone.now() + timedelta(days=180)
        if not Event.objects.filter(time__gt=in_180_days, type=Event.TYPE_SEMESTERSTART).exists():
            mail.send(
                email,
                config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                template='reminder_create_semester_start_events',
            )
