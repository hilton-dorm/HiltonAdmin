import csv
import os
from decimal import Decimal
from datetime import datetime

from django.core.exceptions import ValidationError
from django.core.management.base import BaseCommand
from django.db import transaction

from finances_v2.models import Transaction as NewTransaction, TransactionEntry, Account as NewAccount, TransactionType, \
    Accounts, TransactionTypes, Account, Receipt
from workinggroups.models import WorkingGroup


def get_single_value(value: str):
    if value == "":
        return 0
    return Decimal(value[:-2].replace(".", "").replace(",", "."))


def get_value(line, number):
    return get_single_value(line[number]) + get_single_value(line[number + 1]) if not number is None else 0


class Command(BaseCommand):
    help = 'Parses the exported Symposion Excel Files and imports the data'

    def add_arguments(self, parser):
        parser.add_argument('path_to_csv', type=str)
        parser.add_argument('path_to_receipts', type=str)
        # parser.add_argument('-c', '--clear', help='Removes all existing entries',
        #                    action='store_true')

    @transaction.atomic()
    def handle(self, *args, **options):
        print(options["path_to_csv"])
        receipts = os.listdir(options['path_to_receipts'])

        type_net_fee = TransactionTypes().NETWORK_FEE
        type_registration = TransactionTypes().REGISTRATION_FEE
        type_net_requirements = TransactionTypes().NETWORK_REQUIREMENTS_NOT_FULFILLED
        type_net_manual_check = TransactionTypes().MANUAL_TRANSFER_RECEIPT_CHECK_FEE
        type_waived_receivables = TransactionTypes().WAIVED_RECEIVABLES
        type_house_fee = TransactionTypes().HOUSE_FEE
        type_vereionskosten = TransactionType.objects.get_or_create(name="Vereinskosten")[0]
        type_bank = TransactionType.objects.get_or_create(name="Bankgebühren")[0]
        type_werkstatt = TransactionType.objects.get_or_create(name="Werkstatt")[0]
        type_hausfest = TransactionType.objects.get_or_create(name="Hausfeste")[0]
        type_aktivengrillen = TransactionType.objects.get_or_create(name="Aktivengrillen")[0]
        type_fitness = TransactionType.objects.get_or_create(name="Fitness AG")[0]
        type_festplatten = TransactionType.objects.get_or_create(name="Festplatten")[0]
        type_server = TransactionType.objects.get_or_create(name="Server")[0]
        type_netzwerktechnik = TransactionType.objects.get_or_create(name="Netzwerktechnik")[0]
        type_gema = TransactionType.objects.get_or_create(name="Gema")[0]
        type_domain = TransactionType.objects.get_or_create(name="Domains")[0]
        type_no_umsatz = TransactionType.objects.get_or_create(name="Nicht Umsatzrelevant")[0]
        type_bar = TransactionType.objects.get_or_create(name="Bar")[0]
        type_vermietung = TransactionType.objects.get_or_create(name="Vermietung")[0]
        type_getränkeverkauf = TransactionType.objects.get_or_create(name="Getränkeverkauf")[0]
        type_strafen = TransactionType.objects.get_or_create(name="Strafen")[0]
        type_renovierungen = TransactionType.objects.get_or_create(name="Renovierungen")[0]
        type_metro = TransactionType.objects.get_or_create(name="Metro")[0]
        type_kachouri = TransactionType.objects.get_or_create(name="Kachouri")[0]
        net_ek = Accounts().NETWORK_EK
        net_fk = Accounts().NETWORK_FK
        net_bank = Accounts().NETWORK_BANK
        net_ford = Accounts().NETWORK_RECEIVABLES_TENANTS
        net_group = WorkingGroup.objects.filter(name__icontains="network").first()
        net_ford_haus = NewAccount.objects.get_or_create(title="NetzAG Forderungen gegenüber Haus", type=Account.TYPE_RECEIVABLES, working_group=net_group)[0]
        net_ford_bar = NewAccount.objects.get_or_create(
            title="NetzAG Forderungen gegenüber Symposion",
            type=Account.TYPE_RECEIVABLES,
            working_group=net_group)[0]
        symp_bank = NewAccount.objects.get_or_create(
            title="Symposion Bank",
            type=Account.TYPE_BANK,
            working_group_id=Accounts().SYMPOSION_EK.working_group_id)[0]
        symp_haupt = NewAccount.objects.get_or_create(
            title="Symposion Hauptkasse",
            type=Account.TYPE_CASH,
            working_group_id=Accounts().SYMPOSION_EK.working_group_id)[0]
        symp_gelb = NewAccount.objects.get_or_create(
            title="Symposion Gelbe Kasse",
            type=Account.TYPE_CASH,
            working_group_id=Accounts().SYMPOSION_EK.working_group_id)[0]

        # NewTransaction.objects.filter(transactionentry__account__in=(Accounts().SYMPOSION_EK, symp_haupt, symp_gelb, symp_bank)).delete()
        last_transaction = TransactionEntry.objects.all().last()

        def get_tx_type(gegen, was):
            if "gema" in gegen:
                return type_gema
            elif gegen == "bank":
                return type_bank
            elif "united domains" in gegen:
                return type_domain
            elif "bar" == gegen or "einnahmen aus der bar" == gegen:
                return type_bar
            elif "obi" == gegen or "bauhaus" == gegen:
                return type_renovierungen
            elif "metro" == gegen:
                return type_metro
            elif "kachouri" == gegen:
                return type_kachouri
            elif "vermietung" == gegen or "stornierung" == gegen:
                return type_vermietung
            elif "versicherungen" in was:
                return type_vereionskosten
            elif "getränkeverkauf" == gegen or "getränke verkauf" in was:
                return type_getränkeverkauf
            elif "rücknahme" == gegen:
                return type_getränkeverkauf if "getränke" in was or "Bier" in was else type_strafen
            return None

        with open(options["path_to_csv"], newline='') as csvfile:
            spamreader = csv.reader(csvfile, delimiter=';', quotechar='"')
            # STATE_SEARCH_DATE = 0
            STATE_SEARCH_START = 1
            STATE_GEP_KASSENSTAND = 2
            STATE_PARSE = 3
            state = STATE_SEARCH_START
            COL_NR = 1
            COL_GEGEN = 2
            COL_WAS = 3
            COL_TYPE = 4
            COL_BANK_NEW = None
            einzelrechnungen = []
            for line in spamreader:
                if state == STATE_SEARCH_START:
                    if line[0] == "Datum" and line[2] == "Gegenüber" or line[2] == "Zweck" and line[4] == "Umsatzart":
                        assert line[1] == "Nr"
                        assert line[2] == "Gegenüber" or line[2] == "Zweck"
                        assert line[3] == "Zweck/Grund" or line[3] == "Kommentar"
                        assert line[4] == "Umsatzart"
                        assert line[5] == "Bar Kassen"
                        COL_HAUPTKASSE = 5
                        COL_GELBEKASSE = 8
                        COL_BANK = 11
                        state = STATE_GEP_KASSENSTAND
                    elif line[0] == "Datum" and line[2] == "Gegenüber" or line[2] == "Zweck" and (line[4] == "Bar Kassen" or line[4] == "Bargeld-Kassen"):
                        assert line[1] == "Nr"
                        assert line[2] == "Zweck"
                        assert line[3] == "Kommentar"
                        assert line[4] == "Bar Kassen" or line[4] == "Bargeld-Kassen"
                        COL_HAUPTKASSE = 4
                        COL_GELBEKASSE = 7
                        COL_BANK = 10
                        if line[13] == "Neues Konto":
                            COL_BANK_NEW = 13
                        state = STATE_GEP_KASSENSTAND
                elif state == STATE_GEP_KASSENSTAND:
                    if line[COL_WAS] == "Geprüfter Kontostand":
                        START_HAUPT = get_single_value(line[COL_HAUPTKASSE + 2])
                        START_GELB = get_single_value(line[COL_GELBEKASSE + 2])
                        START_KONTO = get_single_value(line[COL_BANK + 2]) + (0 if COL_BANK_NEW is None else get_single_value(line[COL_BANK_NEW + 2]))
                        state = STATE_PARSE
                elif state == STATE_PARSE:
                    gegen = line[COL_GEGEN].lower().strip()
                    if gegen == "" or gegen == "rücklage":
                        continue
                    if line[COL_HAUPTKASSE + 2] == "":
                        einzelrechnungen.append(line)
                        continue
                    last_line = line
                    was = line[COL_WAS].lower()
                    tx_date = datetime.strptime(line[0], "%d.%m.%Y").date()
                    tx_note = f"{line[COL_GEGEN]}: {line[COL_WAS]}"
                    if line[COL_NR]:
                        tx_note += f" ({line[COL_NR]})"
                    tx_type = get_tx_type(gegen, was)
                    if tx_type is None and "umbuchung" == gegen and not "10 € wechselgeld von marcel" == was:
                        value = get_value(line, COL_HAUPTKASSE)
                        account = symp_haupt

                        value2 = get_value(line, COL_GELBEKASSE)
                        account2 = symp_gelb
                        if not value2:
                            value2 = get_value(line, COL_BANK) + get_value(line, COL_BANK_NEW)
                            account2 = symp_bank
                            if get_value(line, COL_BANK) == -get_value(line, COL_BANK_NEW):
                                print("Skip transaction between new and old konto")
                                continue
                        if value != -value2:
                            print("ERROR on:", line)
                            continue
                        assert value == -value2
                        NewTransaction.objects.filter(note=tx_note, date=tx_date, transactionentry__amount=value).delete()
                        NewTransaction.create(date=tx_date,
                                              note=tx_note,
                                              entries=[TransactionEntry(amount=value, account=account, external_identifier=f"Alte Barkasse {line[COL_NR]}"),
                                                       TransactionEntry(amount=value2, account=account2, external_identifier=f"Alte Barkasse {line[COL_NR]}")])
                        continue

                    entries = []
                    value = get_value(line, COL_HAUPTKASSE)
                    if value:
                        entries.append(TransactionEntry(amount=value, account=symp_haupt, external_identifier=f"Alte Barkasse {line[COL_NR]}"))
                    value = get_value(line, COL_GELBEKASSE)
                    if value:
                        entries.append(TransactionEntry(amount=value, account=symp_gelb, external_identifier=f"Alte Barkasse {line[COL_NR]}"))
                    value = get_value(line, COL_BANK) + get_value(line, COL_BANK_NEW)
                    if value:
                        entries.append(TransactionEntry(amount=value, account=symp_bank, external_identifier=f"Alte Barkasse {line[COL_NR]}"))
                    if len(entries) != 1:
                        print("spcial line:", line)
                    ek_rest = sum([e.amount for e in entries])
                    NewTransaction.objects.filter(note=tx_note, date=tx_date, transactionentry__amount=ek_rest).delete()
                    if len(einzelrechnungen) > 0:
                        entries += [TransactionEntry(amount=get_value(line, COL_BANK), account=Accounts().SYMPOSION_EK,
                                                     type=get_tx_type(line[COL_GEGEN].lower().strip(),
                                                                      line[COL_WAS].lower().strip()),
                                                     note=f"{line[COL_GEGEN]}: {line[COL_WAS]}",
                                                     external_identifier=f"Alte Barkasse {line[COL_NR]}") for line in einzelrechnungen]
                        einzelrechnungen = []
                    else:
                        entries += [TransactionEntry(amount=ek_rest, account=Accounts().SYMPOSION_EK, type=tx_type,
                                                     external_identifier=f"Alte Barkasse {line[COL_NR]}")]
                    try:
                        trans = NewTransaction.create(date=tx_date, note=tx_note, entries=entries)
                    except ValidationError as e:
                        print(e, ek_rest, [(e.amount, e.account) for e in entries], line)
                    old_id = line[COL_NR]
                    if old_id:
                        for receipt in receipts:
                            if receipt.startswith(f"RN_{old_id}"):
                                Receipt.objects.create(transaction=trans, receipt="transactions/receipts/" + receipt)

        # Check sum of parsed transactions
        END_HAUPT = get_single_value(last_line[COL_HAUPTKASSE + 2])
        END_GELB = get_single_value(last_line[COL_GELBEKASSE + 2])
        END_KONTO = get_single_value(last_line[COL_BANK + 2]) + 0 if COL_BANK_NEW is None else get_single_value(last_line[COL_BANK_NEW + 2])
        first_date = TransactionEntry.objects.filter(id__gt=last_transaction.pk).order_by("transaction__date").values("transaction__date").first()
        print(first_date)
        print(first_date["transaction__date"])
        tx_date = first_date["transaction__date"]
        haupt_start = symp_haupt.get_balance(transaction__date__lt=tx_date)
        gelb_start = symp_gelb.get_balance(transaction__date__lt=tx_date)
        konto_start = symp_bank.get_balance(transaction__date__lt=tx_date)
        print("Hauptkasse soll vs ist", START_HAUPT, " vs ", haupt_start)
        print("Gelbekasse soll vs ist", START_GELB, " vs ", gelb_start)
        print("Bank soll vs ist", START_KONTO, " vs ", konto_start)

        haupt_end = symp_haupt.get_balance(id__gt=last_transaction.pk)
        geld_end = symp_gelb.get_balance(id__gt=last_transaction.pk)
        bank_end = symp_bank.get_balance(id__gt=last_transaction.pk)
        print("Hauptkasse db ", haupt_end, " vs csv ", END_HAUPT - START_HAUPT)
        print("Gelbekasse db ", geld_end, " vs csv ", END_GELB - START_GELB)
        print("Bank db ", bank_end, " vs csv ", END_KONTO - START_KONTO)

        if START_HAUPT != haupt_start or START_GELB != gelb_start or START_KONTO != konto_start:
            raise ValidationError("Anfangskassenstand falsch. Alle Transaktionen werden nicht gespeichert!")
        if (END_HAUPT - START_HAUPT) != haupt_end or (END_GELB - START_GELB) != geld_end or (END_KONTO - START_KONTO) != bank_end:
            raise ValidationError("Endkassenstand falsch. Alle Transaktionen werden nicht gespeichert!")
