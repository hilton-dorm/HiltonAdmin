import re

from django.core.management import CommandError
from django.core.management.base import BaseCommand
from django.db import transaction
from django.utils import timezone
from post_office import mail

import config
from finances_v2.models import Accounts, Budget
from main.models import Event
from nordigen_app.models import NordigenCredential


def get_mail():
    credentials = NordigenCredential.objects.filter(account=Accounts().NETWORK_BANK).first()
    if credentials:
        return credentials.email
    return config.EMAIL["EMAIL_ACCOUNT_VORSTAND"]


class Command(BaseCommand):
    help = 'Notifies the owner of the Networking Account if there are no semester start events in the next 6 months'

    @transaction.atomic()
    def handle(self, *args, **options):
        if Budget.objects.filter(start__type=Event.TYPE_SEMESTERSTART, start__time__gt=timezone.now()).exists():
            return "Budgets for the next semester already exist. We don't create new ones."

        current_last_semester = Event.objects.filter(type=Event.TYPE_SEMESTERSTART, time__lte=timezone.now().replace(hour=23, minute=59)).order_by('-time')[:2]
        if len(current_last_semester) == 0:
            raise CommandError("No semester in the past")
        current_semester = current_last_semester[0]
        if len(current_last_semester) == 1:
            self.stdout.write('No previous semester')
            return
        last_semester = current_last_semester[1]
        if Budget.objects.filter(start=current_semester).exists():
            # Fürs aktuelle Semester gibt es schon Budgets, also nichts machen
            return
        # Fürs aktuelle Semester gibt es nichts, also letztes Semester kopieren
        new_budgets = []
        year = current_semester.time.year
        if current_semester.time.month <= 6:
            name_postfix = f"SS {year}"
        else:
            name_postfix = f"WS {year}/{year + 1 - 2000}"
        name_regex = re.compile('(SoSe|SS|WS) 20[0-9]{2}(/[0-9]{2})?')
        for budget in Budget.objects.filter(start=last_semester):
            new_name = re.sub(name_regex, name_postfix, budget.name)
            new_budgets.append(Budget.objects.create(start=current_semester, amount=budget.amount, name=new_name))
        mail.send(
            get_mail(),
            config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
            context={
                'budgets': new_budgets,
            },
            template='semester_budgets_created',
        )
