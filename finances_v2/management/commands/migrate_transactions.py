from datetime import date

from django.core.management.base import BaseCommand
from django.db import transaction

from association.models import Membership
from finances.models import Transaction as OldTransaction, SubAccount
from finances_v2.models import Transaction as NewTransaction, TransactionEntry, Account as NewAccount, TransactionType, \
    Accounts, TransactionTypes, Account, Receipt
from workinggroups.models import WorkingGroup


def add_documents_to_last_transaction(transaction: OldTransaction):
    if transaction.receipt_1:
        Receipt.objects.create(transaction=NewTransaction.objects.last(), receipt=transaction.receipt_1)
    if transaction.receipt_2:
        Receipt.objects.create(transaction=NewTransaction.objects.last(), receipt=transaction.receipt_2)
    if transaction.receipt_3:
        Receipt.objects.create(transaction=NewTransaction.objects.last(), receipt=transaction.receipt_3)
    if transaction.receipt_4:
        Receipt.objects.create(transaction=NewTransaction.objects.last(), receipt=transaction.receipt_4)
    if transaction.receipt_5:
        Receipt.objects.create(transaction=NewTransaction.objects.last(), receipt=transaction.receipt_5)


class Command(BaseCommand):
    help = 'Computes statistics for this month if not already done'

    def add_arguments(self, parser):
        parser.add_argument('-c', '--clear', help='Removes all existing entries',
                            action='store_true')

    @transaction.atomic()
    def handle(self, *args, **options):
        clear = options['clear']
        answer = input("Are you sure that you want to migrate all transactions from finances to finances_v2? "
                       "If they are already there, they will be duplicated. "
                       "If --clear is passed, all existing transactions in finances_v2 will be deleted!\n[yes|No] ")
        if answer.lower() != "yes":
            return "Answer was not 'yes'"
        print("You can stop this command via CTRL+C")
        if clear:
            NewTransaction.objects.all().delete()
        # Account.objects.all().delete()

        nrs = SubAccount.objects.get(title="Network  Reserves Special")
        noc = SubAccount.objects.get(title="Network Outsite Capital")

        type_net_fee = TransactionTypes().NETWORK_FEE
        type_registration = TransactionTypes().REGISTRATION_FEE
        type_net_requirements = TransactionTypes().NETWORK_REQUIREMENTS_NOT_FULFILLED
        type_net_manual_check = TransactionTypes().MANUAL_TRANSFER_RECEIPT_CHECK_FEE
        type_waived_receivables = TransactionTypes().WAIVED_RECEIVABLES
        type_house_fee = TransactionTypes().HOUSE_FEE
        type_vereionskosten = TransactionType.objects.get_or_create(name="Vereinskosten")[0]
        type_bank = TransactionType.objects.get_or_create(name="Bankgebühren")[0]
        type_werkstatt = TransactionType.objects.get_or_create(name="Werkstatt")[0]
        type_hausfest = TransactionType.objects.get_or_create(name="Hausfeste")[0]
        type_aktivengrillen = TransactionType.objects.get_or_create(name="Aktivengrillen")[0]
        type_fitness = TransactionType.objects.get_or_create(name="Fitness AG")[0]
        type_festplatten = TransactionType.objects.get_or_create(name="Festplatten")[0]
        type_server = TransactionType.objects.get_or_create(name="Server")[0]
        type_netzwerktechnik = TransactionType.objects.get_or_create(name="Netzwerktechnik")[0]
        net_ek = Accounts().NETWORK_EK
        net_fk = Accounts().NETWORK_FK
        net_bank = Accounts().NETWORK_BANK
        net_ford = Accounts().NETWORK_RECEIVABLES_TENANTS
        net_group = WorkingGroup.objects.filter(name__icontains="network").first()
        net_ford_haus = NewAccount.objects.get_or_create(title="NetzAG Forderungen gegenüber Haus", type=Account.TYPE_RECEIVABLES, working_group=net_group)[0]
        net_ford_bar = NewAccount.objects.get_or_create(
            title="NetzAG Forderungen gegenüber Symposion",
            type=Account.TYPE_RECEIVABLES,
            working_group=net_group)[0]
        for network_fee in OldTransaction.objects.filter(subaccount__title__icontains="Network Receivables", person__isnull=False).select_related("person"):
            amount = network_fee.amount.amount
            if amount > 0:
                if amount == 4:
                    tx_type = type_net_fee
                elif amount == 2:
                    tx_type = type_net_manual_check
                elif amount == 5:
                    tx_type = type_registration
                elif amount == 10:
                    tx_type = type_net_requirements
                else:
                    print("Unknown net receivable with amout:", amount)
                NewTransaction.create(date=network_fee.date, person=network_fee.person, note=f"{network_fee.note}",
                                      entries=[TransactionEntry(amount=amount, account=net_ek, type=tx_type),
                                               TransactionEntry(amount=amount, account=net_ford, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}")])
            else:
                if "Erlassen von Forderungen" == network_fee.note:
                    NewTransaction.create(date=network_fee.date, person=network_fee.person, note=network_fee.note,
                                          entries=[TransactionEntry(amount=amount, account=net_ek, type=type_waived_receivables),
                                                   TransactionEntry(amount=amount, account=net_ford)])
                    add_documents_to_last_transaction(network_fee)
                elif amount != 0:
                    if network_fee.date == date(2020, 9, 30) and network_fee.person.user.pk == 502:  # User Victoria Kuznetsova
                        print("Fehlerhafte Buchung gefunden")
                        NewTransaction.create(date=network_fee.date, person=network_fee.person,
                                              note=f"Fehlerhafte Buchung. Es wurde im alten System -5 Euro auf Network Receivables gebucht, aber das Geld sonst nirgendwo abgebucht. Das kommt dem erlassen von Forderungen gleich",
                                              entries=[TransactionEntry(amount=amount, account=net_ek, type=type_waived_receivables),
                                                       TransactionEntry(amount=amount, account=net_ford, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}")])
                        add_documents_to_last_transaction(network_fee)
                    else:
                        NewTransaction.create(date=network_fee.date, person=network_fee.person, note=f"{network_fee.note}",
                                              entries=[TransactionEntry(amount=amount, account=net_fk),
                                                       TransactionEntry(amount=amount, account=net_ford, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}")])
        for network_fee in OldTransaction.objects.filter(subaccount__title__icontains="House Receivables", person__isnull=False).select_related("person"):
            amount = network_fee.amount.amount
            if amount > 0:
                if amount == 2:
                    tx_type = type_house_fee
                elif amount == 5:
                    tx_type = type_registration
                else:
                    print("Unknown net receivable with amout:", amount)
                NewTransaction.create(date=network_fee.date, person=network_fee.person, note=f"{network_fee.note}",
                                      entries=[TransactionEntry(amount=amount, account=Accounts().HOUSE_EK, type=tx_type),
                                               TransactionEntry(amount=amount, account=Accounts().HOUSE_RECEIVABLES_TENANTS, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}")])
            elif amount != 0:
                if "Erlassen von Forderungen" == network_fee.note:
                    NewTransaction.create(date=network_fee.date, person=network_fee.person, note=network_fee.note,
                                          entries=[TransactionEntry(amount=amount, account=Accounts().HOUSE_EK, type=type_waived_receivables),
                                                   TransactionEntry(amount=amount, account=Accounts().HOUSE_RECEIVABLES_TENANTS)])
                    add_documents_to_last_transaction(network_fee)
                else:
                    NewTransaction.create(date=network_fee.date, person=network_fee.person, note=f"{network_fee.note}",
                                          entries=[TransactionEntry(amount=amount, account=net_fk, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}"),
                                                   TransactionEntry(amount=-amount, account=Accounts().NETWORK_LIABILITIES_TO_HOUSE),
                                                   TransactionEntry(amount=amount, account=Accounts().HOUSE_RECEIVABLES_TENANTS),
                                                   TransactionEntry(amount=-amount, account=Accounts().HOUSE_RECEIVABLES_FROM_NETWORK)
                                                   ])
            else:
                print("Transaction mit amount = 0", network_fee)
        for network_fee in OldTransaction.objects.filter(subaccount__title__icontains="Network Outsite Capital",
                                                         person__isnull=False).select_related("person"):
            amount = network_fee.amount.amount
            if amount > 0:
                if network_fee.id == 33909:
                    print("Umbuchung gefunden (gut)")
                    NewTransaction.create(date=network_fee.date, person=network_fee.person, note=f"{network_fee.note}",
                                          entries=[TransactionEntry(amount=amount, account=net_fk, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}"),
                                                   TransactionEntry(amount=-amount, account=Accounts().NETWORK_LIABILITIES_TO_HOUSE),
                                                   TransactionEntry(amount=-amount, account=Accounts().HOUSE_EK),
                                                   TransactionEntry(amount=-amount, account=Accounts().HOUSE_RECEIVABLES_FROM_NETWORK)
                                                   ])
                else:
                    NewTransaction.create(date=network_fee.date, person=network_fee.person, note=f"{network_fee.note}",
                                          entries=[TransactionEntry(amount=amount, account=net_fk, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}"),
                                                   TransactionEntry(amount=amount, account=net_bank)])
            elif "Auszahlung" in network_fee.note:
                NewTransaction.create(date=network_fee.date, person=network_fee.person,
                                      note=f"{network_fee.note}",
                                      entries=[TransactionEntry(amount=amount, account=net_fk, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}"),
                                               TransactionEntry(amount=amount, account=net_bank)])
            elif network_fee.note:
                print("Special Network Outside Capital: ", network_fee, network_fee.amount, network_fee.note)
                if amount != 0:
                    NewTransaction.create(date=network_fee.date, person=network_fee.person,
                                          note=f"{network_fee.note}",
                                          entries=[TransactionEntry(amount=amount, account=net_fk, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}"),
                                                   TransactionEntry(amount=amount, account=net_bank)])
            add_documents_to_last_transaction(network_fee)
        for tx in TransactionEntry.objects.filter(account=Accounts().NETWORK_FK).select_related("transaction"):
            if not OldTransaction.objects.filter(amount=tx.amount, person=tx.person, date=tx.transaction.date, subaccount=noc).exists():
                print("FEHLER: Transaction existiert jetzt aber nicht früher: ", tx.transaction_id, tx.transaction, tx.amount, tx.amount, tx.person)
        for network_fee in OldTransaction.objects.filter(subaccount=nrs):  # Network reserves special
            amount = network_fee.amount.amount
            if amount < 0:
                if "Mitglieds" in network_fee.note:
                    NewTransaction.create(date=network_fee.date, person=network_fee.person, note=f"{network_fee.note}",
                                          entries=[TransactionEntry(amount=amount, account=Accounts().NETWORK_LIABILITIES_TO_HOUSE, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}"),
                                                   TransactionEntry(amount=amount, account=Accounts().NETWORK_BANK)])
                    add_documents_to_last_transaction(network_fee)
                else:
                    if network_fee.id == 33911:
                        print("Entsprechende gegen Umbuchung gefunden (gut)")
                    else:
                        # Das Konto wurde gebucht, weil mit den NetzAG Konto sachen für das Haus bezahlt wurden
                        NewTransaction.create(date=network_fee.date, person=network_fee.person,
                                              note=f"{network_fee.note}",
                                              entries=[TransactionEntry(amount=amount, account=Accounts().NETWORK_LIABILITIES_TO_HOUSE, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}"),
                                                       TransactionEntry(amount=amount, account=Accounts().NETWORK_BANK),
                                                       TransactionEntry(amount=amount, account=Accounts().HOUSE_RECEIVABLES_FROM_NETWORK),
                                                       TransactionEntry(amount=amount, account=Accounts().HOUSE_EK)
                                                       ])
                        add_documents_to_last_transaction(network_fee)
            elif amount > 0 and network_fee.person is None:
                NewTransaction.create(date=network_fee.date, note=f"{network_fee.note}",
                                      entries=[TransactionEntry(amount=amount, account=Accounts().NETWORK_LIABILITIES_TO_HOUSE, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}"),
                                               TransactionEntry(amount=amount, account=Accounts().NETWORK_BANK),
                                               TransactionEntry(amount=amount, account=Accounts().HOUSE_RECEIVABLES_FROM_NETWORK),
                                               TransactionEntry(amount=amount, account=Accounts().HOUSE_EK)
                                               ])
                add_documents_to_last_transaction(network_fee)
        for network_fee in OldTransaction.objects.filter(subaccount__title__icontains="House Liquidity"):
            amount = network_fee.amount.amount
            if network_fee.id == 2922:
                print("Interne Umbuchung auf House Reserves")
                continue
            if "Mitglied" in network_fee.note and not "Buchungsfehler" in network_fee.note:
                NewTransaction.create(date=network_fee.date, person=network_fee.person, note=f"{network_fee.note}",
                                      entries=[TransactionEntry(amount=-amount, account=Accounts().HOUSE_RECEIVABLES_FROM_NETWORK),
                                               TransactionEntry(amount=amount, account=Accounts().HOUSE_BANK, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}")])
                add_documents_to_last_transaction(network_fee)
            else:
                if "Bankgebühren" in network_fee.note or "Rechnungsabschluss" in network_fee.note:
                    tx_type = type_bank
                elif "Werkstatt" in network_fee.note:
                    tx_type = type_werkstatt
                elif "Vereins" in network_fee.note:
                    tx_type = type_vereionskosten
                elif "fest" in network_fee.note:
                    tx_type = type_hausfest
                elif "fitness" in network_fee.note.lower():
                    tx_type = type_fitness
                elif "Aktiven" in network_fee.note or "Mee" in network_fee.note:
                    tx_type = type_aktivengrillen
                else:
                    tx_type = None
                NewTransaction.create(date=network_fee.date, person=network_fee.person, note=f"{network_fee.note}",
                                      entries=[TransactionEntry(amount=amount, account=Accounts().HOUSE_EK, type=tx_type),
                                               TransactionEntry(amount=amount, account=Accounts().HOUSE_BANK, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}")])
                add_documents_to_last_transaction(network_fee)
        for network_fee in OldTransaction.objects.filter(subaccount__title__icontains="House Reserves", amount__lt=0):
            amount = network_fee.amount.amount
            tx_type = type_werkstatt if "Werkstatt" in network_fee.note else None
            NewTransaction.create(date=network_fee.date, note=f"{network_fee.note}",
                                  entries=[TransactionEntry(amount=amount, account=Accounts().HOUSE_EK, type=tx_type),
                                           TransactionEntry(amount=amount, account=Accounts().HOUSE_BANK, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}")])
            add_documents_to_last_transaction(network_fee)
        for network_fee in OldTransaction.objects.filter(subaccount__title__icontains="Network Liquidity"):
            amount = network_fee.amount.amount
            if not TransactionEntry.objects.filter(transaction__date=network_fee.date, person=network_fee.person,
                                                   amount=-amount, account=Accounts().NETWORK_FK).exists():
                # Schauen ob Forderungen erstellt wurden
                filter_add = {'amount__gt': 0, 'amount__lte': -amount} if amount < 0 else {'amount__lt': 0, 'amount__gte': -amount}
                if "Haus" in network_fee.note:
                    filter_add['note__icontains'] = "Haus"
                if "Bar" in network_fee.note:
                    filter_add['note__icontains'] = "Bar"
                old_trans = OldTransaction.objects.filter(
                    date=network_fee.date,
                    subaccount__title__icontains="Network Receivables",
                    person__isnull=True,
                    **filter_add)

                if "Kontoführungsgebühr" not in network_fee.note and old_trans.exists():
                    transactions = []
                    rest_amount = amount
                    for tx in old_trans:
                        if "Bar" in tx.note:
                            transactions += [TransactionEntry(amount=tx.amount.amount, account=net_ford_bar,
                                                              note=f"{tx.note}. From #{tx}", external_identifier=f"From Alte Finanzverwaltung #{tx.id}")]
                        elif "Haus" in tx.note:
                            transactions += [TransactionEntry(amount=tx.amount.amount, account=net_ford_haus,
                                                              note=f"{tx.note}. From #{tx}", external_identifier=f"From Alte Finanzverwaltung #{tx.id}")]
                        else:
                            print("ERROR: Forderung unbekannt: ", tx, tx.date, tx.note)
                        rest_amount += tx.amount.amount
                    print("Rest amount: ", network_fee.date, network_fee.note, amount, rest_amount)
                    if rest_amount:
                        transactions += [TransactionEntry(amount=rest_amount, account=Accounts().NETWORK_EK)]
                    transactions += [TransactionEntry(amount=amount, account=Accounts().NETWORK_BANK,
                                                      external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}")]
                    NewTransaction.create(date=network_fee.date, note=f"{network_fee.note}",
                                          entries=transactions, tx_type=type_vereionskosten)
                    add_documents_to_last_transaction(network_fee)
                else:
                    if "Festplatte" in network_fee.note:
                        tx_type = type_festplatten
                    elif "Server" in network_fee.note:
                        tx_type = type_server
                    elif "Switch" in network_fee.note or "AC" in network_fee.note or "AP" in network_fee.note or "abel" in network_fee.note:
                        tx_type = type_netzwerktechnik
                    else:
                        tx_type = None
                    NewTransaction.create(date=network_fee.date, person=network_fee.person, note=f"{network_fee.note}",
                                          entries=[TransactionEntry(amount=amount, account=Accounts().NETWORK_EK, type=tx_type),
                                                   TransactionEntry(amount=amount, account=Accounts().NETWORK_BANK, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}")])
                    add_documents_to_last_transaction(network_fee)
        for network_fee in OldTransaction.objects.filter(subaccount__title__icontains="Network Receivables", person__isnull=True):
            amount = network_fee.amount.amount
            if "Bar" in network_fee.note:
                if not TransactionEntry.objects.filter(transaction__date=network_fee.date, amount=amount, account=net_ford_bar).exists():
                    print("Forderung existiert noch nicht!: ", network_fee.date, network_fee.note)
                    NewTransaction.create(date=network_fee.date, note=f"{network_fee.note}",
                                          entries=[TransactionEntry(amount=amount, account=Accounts().NETWORK_EK),
                                                   TransactionEntry(amount=amount, account=net_ford_bar, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}")])
                    add_documents_to_last_transaction(network_fee)
            elif "Haus" in network_fee.note:
                if not TransactionEntry.objects.filter(transaction__date=network_fee.date, amount=amount, account=net_ford_haus).exists():
                    print("Forderung existiert noch nicht!: ", network_fee.date, network_fee.note)
                    NewTransaction.create(date=network_fee.date, note=f"{network_fee.note}",
                                          entries=[TransactionEntry(amount=amount, account=Accounts().NETWORK_EK),
                                                   TransactionEntry(amount=amount, account=net_ford_haus, external_identifier=f"From Alte Finanzverwaltung #{network_fee.id}")])
                    add_documents_to_last_transaction(network_fee)
            else:
                print("Not matches tx on Network Receivables", network_fee.note, network_fee.amount, network_fee.date)

        for m in Membership.objects.all():
            if m.get_receivables_house_v1() != m.get_receivables_house():
                print("Receivables House Mismatch ", m, m.get_receivables_house_v1(), " vs ", m.get_receivables_house())
            if m.get_receivables_network_v1() != m.get_receivables_network():
                print("Receivables Network Mismatch ", m, m.get_receivables_network_v1(), " vs ", m.get_receivables_network())
            if m.get_balance_v1() != m.get_balance():
                print("Balance Mismatch ", m, m.get_balance_v1(), " vs ", m.get_balance())
