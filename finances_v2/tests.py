from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.management import call_command
from django.test import TestCase
from django.utils import timezone
from freezegun.api import freeze_time
from post_office.models import EmailTemplate

from finances_v2.models import Account, TransactionEntry, Accounts, Transaction, TransactionTypes, Budget
from main.models import Person, Event
from workinggroups.models import WorkingGroup


class FinancesV2TestCase(TestCase):

    def accountSetUp(self):
        # Accessing the properties creates the object and caches it. We delete the cache here, otherwise we get objects
        # from a previous run that are already deleted
        Accounts().NETWORK_FK
        Accounts().NETWORK_EK
        Accounts().NETWORK_BANK
        Accounts().NETWORK_RECEIVABLES_TENANTS
        Accounts().NETWORK_LIABILITIES_TO_HOUSE
        Accounts().HOUSE_EK
        Accounts().HOUSE_BANK
        Accounts().HOUSE_RECEIVABLES_FROM_NETWORK
        Accounts().HOUSE_RECEIVABLES_TENANTS
        del Accounts().NETWORK_FK
        del Accounts().NETWORK_EK
        del Accounts().NETWORK_BANK
        del Accounts().NETWORK_RECEIVABLES_TENANTS
        del Accounts().NETWORK_LIABILITIES_TO_HOUSE
        del Accounts().HOUSE_EK
        del Accounts().HOUSE_BANK
        del Accounts().HOUSE_RECEIVABLES_FROM_NETWORK
        del Accounts().HOUSE_RECEIVABLES_TENANTS
        TransactionTypes().NETWORK_FEE
        del TransactionTypes().NETWORK_FEE
        TransactionTypes().HOUSE_FEE
        del TransactionTypes().HOUSE_FEE
        TransactionTypes().WAIVED_RECEIVABLES
        del TransactionTypes().WAIVED_RECEIVABLES
        TransactionTypes().REGISTRATION_FEE
        del TransactionTypes().REGISTRATION_FEE
        TransactionTypes().MANUAL_TRANSFER_RECEIPT_CHECK_FEE
        del TransactionTypes().MANUAL_TRANSFER_RECEIPT_CHECK_FEE
        TransactionTypes().NETWORK_REQUIREMENTS_NOT_FULFILLED
        del TransactionTypes().NETWORK_REQUIREMENTS_NOT_FULFILLED

    def setUp(self):
        self.accountSetUp()
        self.network_group = WorkingGroup.objects.create(name="Network", email="network@test.de", description="test")
        self.house_group = WorkingGroup.objects.create(name="House", email="house@test.de", description="test")
        user = User.objects.create(
            username="tester",
            first_name="test",
            last_name="tester",
            email="test@tester.de")
        self.person = Person.objects.create(user=user)

    def test_dont_change_account_type(self):
        acc = Account.objects.create(title="test", type=Account.TYPE_CASH, working_group=self.network_group)
        acc.type = Account.TYPE_BANK
        with self.assertRaises(ValidationError):
            acc.save()

    def test_dont_create_single_transaction_entry(self):
        with self.assertRaises(ValidationError):
            TransactionEntry.objects.create(amount=5, account=Accounts().HOUSE_BANK)

    def test_balanced_transactions(self):
        with self.assertRaises(ValidationError):  # amount = 0 forbidden
            Transaction.create([TransactionEntry(amount=0)])
        Transaction.create([TransactionEntry(amount=2, account=Accounts().HOUSE_EK),
                            TransactionEntry(amount=2, account=Accounts().HOUSE_BANK)])
        with self.assertRaises(ValidationError):  # not balanced (only positive active)
            Transaction.create([TransactionEntry(amount=2, account=Accounts().HOUSE_RECEIVABLES_FROM_NETWORK),
                                TransactionEntry(amount=2, account=Accounts().HOUSE_BANK)])
        with self.assertRaises(ValidationError):  # not balanced (only positive passive)
            Transaction.create([TransactionEntry(amount=2, account=Accounts().NETWORK_FK),
                                TransactionEntry(amount=2, account=Accounts().NETWORK_EK)])
        # transaction from active -> passive
        Transaction.create([TransactionEntry(amount=2, account=Accounts().NETWORK_BANK),
                            TransactionEntry(amount=2, account=Accounts().NETWORK_EK)])
        # transaction from active -> active
        Transaction.create([TransactionEntry(amount=2, account=Accounts().HOUSE_BANK),
                            TransactionEntry(amount=-2, account=Accounts().HOUSE_RECEIVABLES_FROM_NETWORK)])
        # transaction from passive -> passive
        Transaction.create([TransactionEntry(amount=2, account=Accounts().NETWORK_EK),
                            TransactionEntry(amount=-2, account=Accounts().NETWORK_FK)])

    def test_dont_change_transactions(self):
        Transaction.create([TransactionEntry(amount=2, account=Accounts().NETWORK_EK),
                            TransactionEntry(amount=-2, account=Accounts().NETWORK_FK)])
        Transaction.create([TransactionEntry(amount=4, account=Accounts().NETWORK_EK),
                            TransactionEntry(amount=-4, account=Accounts().NETWORK_FK)])
        self.assertEqual(TransactionEntry.objects.count(), 4)
        obj: TransactionEntry = TransactionEntry.objects.all().first()
        with self.assertRaises(ValidationError):
            obj.amount = 4
            obj.save()
        obj = TransactionEntry.objects.all().first()
        with self.assertRaises(ValidationError):
            obj.account = Accounts().HOUSE_EK
            obj.save()
        obj = TransactionEntry.objects.all().first()
        with self.assertRaises(ValidationError):
            obj.transaction = Transaction.objects.all().last()
            obj.save()
        obj = TransactionEntry.objects.all().first()
        obj.type = TransactionTypes().NETWORK_FEE
        obj.person = self.person
        obj.save()  # changing type and person should work

    def test_balances(self):
        self.assertEqual(Accounts().NETWORK_FK.get_balance(), 0)
        self.assertEqual(Accounts().NETWORK_BANK.get_balance(), 0)
        Transaction.create([TransactionEntry(amount=2, account=Accounts().NETWORK_BANK),
                            TransactionEntry(amount=2, account=Accounts().NETWORK_FK, person=self.person)])
        self.assertEqual(Accounts().NETWORK_FK.get_balance(), 2)
        self.assertEqual(Accounts().NETWORK_FK.get_balance(person=self.person), 2)
        self.assertEqual(Accounts().NETWORK_BANK.get_balance(), 2)
        Transaction.create([TransactionEntry(amount=2, account=Accounts().NETWORK_EK),
                            TransactionEntry(amount=-2, account=Accounts().NETWORK_FK, person=self.person)])
        Transaction.create([TransactionEntry(amount=2, account=Accounts().NETWORK_EK),
                            TransactionEntry(amount=-2, account=Accounts().NETWORK_FK)])
        self.assertEqual(Accounts().NETWORK_FK.get_balance(), -2)
        self.assertEqual(Accounts().NETWORK_FK.get_balance(person=self.person), 0)
        self.assertEqual(Accounts().NETWORK_EK.get_balance(), 4)

    def test_delete(self):
        self.assertEqual(Accounts().NETWORK_FK.get_balance(), 0)
        tx = Transaction.create([TransactionEntry(amount=2, account=Accounts().NETWORK_BANK),
                                 TransactionEntry(amount=2, account=Accounts().NETWORK_FK, person=self.person)])
        self.assertEqual(Accounts().NETWORK_FK.get_balance(), 2)
        tx.delete()
        self.assertEqual(Accounts().NETWORK_FK.get_balance(), 0)

    @staticmethod
    def date(*args):
        return timezone.make_aware(timezone.datetime(*args, 20, 00))

    @freeze_time("2020-01-01", as_kwarg='frozen_time')
    def test_budgets(self, frozen_time):
        types = Event.TYPES
        for type_entry in types:
            real_type = type_entry[0]
            frozen_time.move_to("2020-01-1")
            event1 = Event.objects.create(name="Senat", time=self.date(2020, 1, 4), type=real_type)
            b = Budget.objects.create(name="Werkstatt", amount=1, start=event1)
            self.assertFalse(b.is_currently_active())
            frozen_time.move_to("2020-01-5")
            self.assertTrue(b.is_currently_active())
            self.assertEqual(b.end_date(), None)
            if real_type == Event.TYPE_SENATSSITZUNG:
                # Budgets von Senatssitzungen laufen erst auf der übernächsten Sitzung ab
                Event.objects.create(name="Senat", time=self.date(2020, 2, 1), type=real_type)
                self.assertTrue(b.is_currently_active())
                self.assertEqual(b.end_date(), None)
            event2 = Event.objects.create(name="Senat", time=self.date(2020, 3, 1), type=real_type)
            self.assertEqual(b.end_date(), event2.time)
            self.assertTrue(b.is_currently_active())
            frozen_time.move_to("2020-03-5")
            self.assertFalse(b.is_currently_active())

    @freeze_time("2020-01-01", as_kwarg='frozen_time')
    def test_semester_budgets(self, frozen_time):
        EmailTemplate.objects.create(name="semester_budgets_created")
        SEM = Event.TYPE_SEMESTERSTART
        ws19 = Event.objects.create(name="Start WS 19/20", time=self.date(2019, 10, 1), type=SEM)
        Budget.objects.create(start=ws19, name="Werkstatt WS 2019/20", amount=100)
        call_command('create_recurring_semester_budgets')
        self.assertEqual(Budget.objects.count(), 1)
        ss20 = Event.objects.create(name="Start SS 20", time=self.date(2020, 4, 1), type=SEM)
        call_command('create_recurring_semester_budgets')
        self.assertEqual(Budget.objects.count(), 1)
        frozen_time.move_to("2020-04-01")
        call_command('create_recurring_semester_budgets')
        self.assertEqual(Budget.objects.count(), 2)
        self.assertEqual(Budget.objects.order_by('id').last().name, "Werkstatt SS 2020")
        call_command('create_recurring_semester_budgets')
        self.assertEqual(Budget.objects.count(), 2)
