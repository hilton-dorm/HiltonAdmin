import functools
from datetime import timedelta
from typing import List
from decimal import Decimal

from auditlog.registry import auditlog
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.db import models, transaction
from django.db.models import Sum, QuerySet, DateTimeField, Q
from django.utils import timezone
from private_storage.fields import PrivateFileField

from main.models import Person, Event
from nordigen_app.utils import is_nordigen_external_identifier
from workinggroups.models import WorkingGroup, KeyHolder


class Budget(models.Model):
    name = models.CharField(max_length=255)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    start = models.ForeignKey(Event, on_delete=models.deletion.PROTECT)

    class Meta:
        ordering = ('-start__time',)

    def __str__(self):
        return self.name

    def end_date(self):
        offset = 1 if self.start.type == Event.TYPE_SENATSSITZUNG else 0
        try:
            end_event = Event.objects.filter(type=self.start.type, time__gt=self.start.time).order_by("time")[offset]
        except IndexError:
            return None
        if end_event:
            return end_event.time

    def is_currently_active(self):
        now = timezone.now()
        if now < self.start.time:
            return False
        end = self.end_date()
        return True if end is None else now < end


class AccountNames:
    HOUSE_EK = "Haus Eigenkapital"
    HOUSE_FK = "Haus Fremdkapital"
    HOUSE_BANK = "Haus Bank"
    HOUSE_RECEIVABLES_TENANTS = "Haus Forderungen Bewohner"
    HOUSE_RECEIVABLES_FROM_NETWORK = "Haus Forderungen gegenüber NetzAG"
    NETWORK_EK = "NetzAG Eigenkapital"
    NETWORK_FK = "NetzAG Fremdkapital"
    NETWORK_BANK = "NetzAG Bank"
    NETWORK_RECEIVABLES_TENANTS = "NetzAG Forderungen Bewohner"
    NETWORK_LIABILITIES_TO_HOUSE = "NetzAG Verbindlichkeiten gegenüber Haus"
    MONEY_TRANSIT = "Geldtransit"
    SYMPOSION_EK = "Symposion Eigenkapital"


class TransactionTypeNames:
    NETWORK_FEE = "Network Fee"
    HOUSE_FEE = "House Fee"
    REGISTRATION_FEE = "Registration Fee"
    MANUAL_TRANSFER_RECEIPT_CHECK_FEE = "Manual Transfer Receipt Check Fee"
    NETWORK_REQUIREMENTS_NOT_FULFILLED = "Network Requirements Not Fulfilled"
    WAIVED_RECEIVABLES = "Waived Receivables"
    BANK_FEE = "Bankgebühren"
    BLOCK_INTERNET_ACCESS_FINE = "Block Internet Access Fine"


def with_balance(queryset: QuerySet):
    return queryset.aggregate(balance=Sum("amount", default=0))


def get_balance(queryset: QuerySet):
    return with_balance(queryset)['balance']


class Account(models.Model):
    TYPE_BANK = '0'
    TYPE_CASH = '1'
    TYPE_RECEIVABLES = '2'
    TYPE_EQUITY = '3'
    TYPE_DEBT_CAPITAL = '4'
    TYPE_LIABILITIES = '5'
    ACCOUNT_TYPES = (
        (TYPE_BANK, 'Bank'),
        (TYPE_CASH, 'Bargeld'),
        (TYPE_RECEIVABLES, 'Forderungen'),
        (TYPE_EQUITY, 'Eigenkapital'),
        (TYPE_DEBT_CAPITAL, 'Fremdkapital'),
        (TYPE_LIABILITIES, 'Verbindlichkeiten'),
    )
    ACTIVE_ACCOUNTS = (TYPE_BANK, TYPE_CASH, TYPE_RECEIVABLES)
    PASSIVE_ACCOUNTS = (TYPE_EQUITY, TYPE_DEBT_CAPITAL, TYPE_LIABILITIES)
    title = models.CharField(max_length=255, blank=False, null=False)
    description = models.TextField(blank=True)
    working_group = models.ForeignKey(WorkingGroup, on_delete=models.CASCADE, related_name='+', null=True, blank=True)
    type = models.CharField(max_length=1, choices=ACCOUNT_TYPES)

    def __str__(self):
        return self.title.replace("Verbindlichkeiten", "Verb.").replace("Forderungen", "Ford.").replace(
            "gegenüber", "ggü.")  # f"{self.title} ({Account.ACCOUNT_TYPES[int(self.type)][1]})"

    def save(self, **kwargs):
        if self.type not in Account.ACTIVE_ACCOUNTS + Account.PASSIVE_ACCOUNTS:
            raise ValidationError("Unknown account type. Must be one of " + str(Account.ACTIVE_ACCOUNTS + Account.PASSIVE_ACCOUNTS))
        if self.pk and self.type != Account.objects.get(id=self.pk).type:
            raise ValidationError("Changing the type of an account is forbidden!")
        super().save(**kwargs)

    def get_balance(self, **filters):
        return get_balance(self.entries.filter(**filters))


class Accounts:
    # Singleton:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(Accounts, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    @functools.cached_property
    def HOUSE_EK(self):
        group = WorkingGroup.objects.filter(name__icontains="haussprecher").first()
        return Account.objects.get_or_create(title=AccountNames.HOUSE_EK, type=Account.TYPE_EQUITY, working_group=group)[0]

    @functools.cached_property
    def HOUSE_RECEIVABLES_TENANTS(self):
        group = WorkingGroup.objects.filter(name__icontains="haussprecher").first()
        return Account.objects.get_or_create(title=AccountNames.HOUSE_RECEIVABLES_TENANTS, type=Account.TYPE_RECEIVABLES, working_group=group)[0]

    @functools.cached_property
    def HOUSE_RECEIVABLES_FROM_NETWORK(self):
        group = WorkingGroup.objects.filter(name__icontains="haussprecher").first()
        return Account.objects.get_or_create(title=AccountNames.HOUSE_RECEIVABLES_FROM_NETWORK, type=Account.TYPE_RECEIVABLES, working_group=group)[0]

    @functools.cached_property
    def HOUSE_BANK(self):
        group = WorkingGroup.objects.filter(name__icontains="haussprecher").first()
        return Account.objects.get_or_create(title=AccountNames.HOUSE_BANK, type=Account.TYPE_BANK, working_group=group)[0]

    @functools.cached_property
    def NETWORK_EK(self):
        net_group = WorkingGroup.objects.filter(name__icontains="network").first()
        return Account.objects.get_or_create(title=AccountNames.NETWORK_EK, type=Account.TYPE_EQUITY, working_group=net_group)[0]

    @functools.cached_property
    def NETWORK_FK(self):
        net_group = WorkingGroup.objects.filter(name__icontains="network").first()
        return Account.objects.get_or_create(title=AccountNames.NETWORK_FK, type=Account.TYPE_DEBT_CAPITAL, working_group=net_group)[0]

    @functools.cached_property
    def NETWORK_RECEIVABLES_TENANTS(self):
        net_group = WorkingGroup.objects.filter(name__icontains="network").first()
        return Account.objects.get_or_create(title=AccountNames.NETWORK_RECEIVABLES_TENANTS, type=Account.TYPE_RECEIVABLES, working_group=net_group)[0]

    @functools.cached_property
    def NETWORK_BANK(self):
        net_group = WorkingGroup.objects.filter(name__icontains="network").first()
        return Account.objects.get_or_create(title=AccountNames.NETWORK_BANK, type=Account.TYPE_BANK, working_group=net_group)[0]

    @functools.cached_property
    def NETWORK_LIABILITIES_TO_HOUSE(self):
        net_group = WorkingGroup.objects.filter(name__icontains="network").first()
        return Account.objects.get_or_create(title=AccountNames.NETWORK_LIABILITIES_TO_HOUSE, type=Account.TYPE_LIABILITIES, working_group=net_group)[0]

    @functools.cached_property
    def SYMPOSION_EK(self):
        net_group = WorkingGroup.objects.filter(name__icontains="symposion").first()
        return Account.objects.get_or_create(title=AccountNames.SYMPOSION_EK, type=Account.TYPE_EQUITY, working_group=net_group)[0]


class TransactionType(models.Model):
    name = models.CharField(max_length=255, blank=True, null=False)

    def __str__(self):
        return self.name

    class Meta:
        ordering = ["name"]


class TransactionTypes:
    # Singleton:
    _instance = None

    def __new__(cls, *args, **kwargs):
        if not cls._instance:
            cls._instance = super(TransactionTypes, cls).__new__(cls, *args, **kwargs)
        return cls._instance

    @functools.cached_property
    def NETWORK_FEE(self):
        return TransactionType.objects.get_or_create(name=TransactionTypeNames.NETWORK_FEE)[0]

    @functools.cached_property
    def HOUSE_FEE(self):
        return TransactionType.objects.get_or_create(name=TransactionTypeNames.HOUSE_FEE)[0]

    @functools.cached_property
    def REGISTRATION_FEE(self):
        return TransactionType.objects.get_or_create(name=TransactionTypeNames.REGISTRATION_FEE)[0]

    @functools.cached_property
    def MANUAL_TRANSFER_RECEIPT_CHECK_FEE(self):
        return TransactionType.objects.get_or_create(name=TransactionTypeNames.MANUAL_TRANSFER_RECEIPT_CHECK_FEE)[0]

    @functools.cached_property
    def BLOCK_INTERNET_ACCESS_FINE(self):
        return TransactionType.objects.get_or_create(name=TransactionTypeNames.BLOCK_INTERNET_ACCESS_FINE)[0]

    @functools.cached_property
    def NETWORK_REQUIREMENTS_NOT_FULFILLED(self):
        return TransactionType.objects.get_or_create(name=TransactionTypeNames.NETWORK_REQUIREMENTS_NOT_FULFILLED)[0]

    @functools.cached_property
    def WAIVED_RECEIVABLES(self):
        return TransactionType.objects.get_or_create(name=TransactionTypeNames.WAIVED_RECEIVABLES)[0]

    @functools.cached_property
    def BANK_FEE(self):
        return TransactionType.objects.get_or_create(name=TransactionTypeNames.BANK_FEE)[0]


class TransactionManager(models.Manager):
    def get_queryset(self):
        # We always need them in the __str__ method
        return super().get_queryset().prefetch_related('transactionentry_set').prefetch_related('transactionentry_set__account')


class Transaction(models.Model):
    date = models.DateField(blank=False, default=timezone.now)  # Nimmt das Datum der aktuellen Zeitzone
    note = models.CharField(max_length=255, default="", blank=True, null=False)
    created_at = models.DateTimeField(auto_now_add=True)

    objects = TransactionManager()

    @staticmethod
    @transaction.atomic
    def create(entries: List['TransactionEntry'], date=None, note="", tx_type: TransactionType = None, person: Person = None):
        tx = Transaction(note=note)
        if date is not None:
            tx.date = date
        tx.save()
        for entry in entries:
            if tx_type:
                entry.type = tx_type
            if person:
                entry.person = person
            assert entry.transaction_id is None
            entry.transaction = tx
        TransactionEntry.objects.bulk_create(entries)
        return tx

    def __str__(self):
        return f"{self.date}: " + ", ".join([f"{t.amount} {t.account}" for t in self.transactionentry_set.all()])

    def can_delete(self):
        return self.created_at > timezone.now() - timedelta(days=60)


class SimpleTransaction(Transaction):
    class Meta:
        proxy = True


class Receipt(models.Model):
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    receipt = PrivateFileField(upload_to='transactions/receipts')

    def __str__(self):
        return ""


def validate_TransactionEntrySet(entries: List['TransactionEntry']):
    if len(entries) <= 1:
        raise ValidationError("A transaction must consist of at least two entries")
    transaction_id = entries[0].transaction_id
    amount = 0
    external_ids = set()
    for entry in entries:
        if entry.transaction_id != transaction_id:
            raise ValidationError("All transactions entries must belong to the same transaction")
        if entry.amount == 0:
            raise ValidationError("The amount of a transaction entry must be non zero")
        # print("amount is ", entry.amount)
        amount += Decimal(entry.amount) * (1 if entry.account.type in Account.ACTIVE_ACCOUNTS else -1)
        if is_nordigen_external_identifier(entry.external_identifier):
            if TransactionEntry.objects.filter(Q(external_identifier=entry.external_identifier) & ~Q(pk=entry.pk)
                                               ).exists() or entry.external_identifier in external_ids:
                raise ValidationError(f"External nordigen identifier {entry.external_identifier} already used in another Transaction Entry")
            external_ids.add(entry.external_identifier)
    if amount != 0:
        error = ", ".join([f"{entry.account} {entry.amount}" for entry in entries])
        raise ValidationError(f"The transaction must be balanced (sum is {amount}): {error}")


class TransactionEntryManager(models.Manager):

    def delete(self):
        raise AssertionError("Not allowed")

    def update(self, **kwargs):
        raise AssertionError("Not allowed")

    def bulk_update(self, **kwargs):
        raise AssertionError("Not allowed")

    def bulk_create(self, objs: List['TransactionEntry']):
        validate_TransactionEntrySet(objs)
        super().bulk_create(objs)

    def with_balance(self):
        return with_balance(self)

    def get_balance(self):
        return get_balance(self)


class TransactionEntry(models.Model):
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE)
    account = models.ForeignKey(Account, related_name='entries', on_delete=models.PROTECT)
    type = models.ForeignKey(TransactionType, on_delete=models.SET_NULL, null=True, blank=True, default=None)
    budget = models.ForeignKey(Budget, on_delete=models.SET_NULL, null=True, blank=True, default=None)
    person = models.ForeignKey(Person, on_delete=models.DO_NOTHING, null=True, blank=True)
    amount = models.DecimalField(max_digits=10, decimal_places=2)
    note = models.CharField(max_length=255, default="", blank=True, null=False)
    external_identifier = models.CharField(max_length=50, blank=True, null=True, default=None)

    objects = TransactionEntryManager()

    def __str__(self):
        return f"#{self.id}"

    def clean(self):
        super().clean()
        if self.pk:
            current = TransactionEntry.objects.get(id=self.pk)
            if self.amount != current.amount:
                raise ValidationError("Changing the amount is forbidden!")
            if self.account_id != current.account_id:
                raise ValidationError("Changing the account is forbidden!")
            if self.transaction_id != current.transaction_id:
                raise ValidationError("Changing the transaction is forbidden!")
            if self.external_identifier != current.external_identifier and is_nordigen_external_identifier(self.external_identifier):
                if TransactionEntry.objects.filter(Q(external_identifier=self.external_identifier) & ~Q(pk=self.pk)
                                                   & ~Q(transaction_id=self.transaction_id)).exists():
                    raise ValidationError(f"External nordigen identifier {self.external_identifier} already used in another Transaction Entry")

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if not self.pk:
            raise ValidationError("You can only use save to update a model!")
        self.clean()
        super().save(force_update=True, using=using, update_fields=update_fields)

    def delete(self, using=None, keep_parents=False):
        raise AssertionError("Deleting a object is forbidden!")


class BankAccountCSV(models.Model):
    file = models.FileField(upload_to='account/csv')
    last_update = DateTimeField(default=timezone.now)

    def __str__(self):
        return 'CSV transaction file'

    class Meta:
        verbose_name_plural = "bank account csv files"


auditlog.register(Budget)
auditlog.register(Account)
auditlog.register(TransactionType)
auditlog.register(Transaction)
auditlog.register(TransactionEntry)
auditlog.register(BankAccountCSV)
