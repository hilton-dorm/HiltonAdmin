import datetime
from decimal import Decimal

import nested_admin
from auditlog.admin import LogEntryAdmin
from auditlog.mixins import LogEntryAdminMixin
from auditlog.models import LogEntry
from django.contrib import admin
from django.contrib.admin import SimpleListFilter, display, EmptyFieldListFilter
from django.contrib.contenttypes.models import ContentType
from django.core.management import call_command
from django.db import connection
from django.db.models import Q, Sum, QuerySet, F, Count
from django.urls import reverse
from django.utils import timezone
from django.utils.html import format_html
from django.utils.safestring import mark_safe
from djmoney.money import Money
from post_office import mail
from rangefilter.filters import DateRangeFilterBuilder, DateRangeFilter, NumericRangeFilterBuilder
from totalsum.admin import TotalsumAdmin

import config
from common.util.start_new_thread import start_new_thread
from finances.utils import match_tx_reference, match_tx_person
from main.models import Event, Person
from .forms import TransactionEntryFormSet, SimplyTransactionHeaderForm
from .models import Account, Transaction, TransactionEntry, TransactionType, Receipt, Budget, BankAccountCSV, Accounts, \
    SimpleTransaction


# Register your models here.

class RangeFilter(DateRangeFilter):
    def queryset(self, request, queryset):
        # Code copy pasted from DateRangeFilter
        if self.form.is_valid():
            validated_data = dict(self.form.cleaned_data.items())
            if validated_data:
                # We don't want to filter here, but we need the data in the TransactionTypeFilter
                setattr(queryset, "__date_from", validated_data.get(self.lookup_kwarg_gte, None))
                setattr(queryset, "__date_to", validated_data.get(self.lookup_kwarg_lte, None))
        return queryset


class TransactionTypeFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'Transaction Type'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'transaction_type'

    def lookups(self, request, model_admin):
        return TransactionType.objects.all().values_list('id', 'name')

    def queryset(self, request, queryset: QuerySet):
        filters = Q()
        if self.value():
            filters &= Q(entries__type=self.value())
        # Attributes are set in the RangeFilter
        if getattr(queryset, "__date_from", None) is not None:
            filters &= Q(entries__transaction__date__gte=getattr(queryset, "__date_from"))
        if getattr(queryset, "__date_to", None) is not None:
            filters &= Q(entries__transaction__date__lte=getattr(queryset, "__date_to"))
        return queryset.annotate(balance=Sum("entries__amount", filter=filters))


@admin.register(Account)
class AccountAdmin(nested_admin.NestedModelAdmin):
    model = Account
    ordering = ["working_group", "type", "title"]
    list_display = ('name', 'link_to_tx', 'balance',)
    # We use the "title" as field in the Range slider since we only want the dates and no actual filtering
    list_filter = ('type', 'working_group', ("title", RangeFilter), TransactionTypeFilter,)

    def get_readonly_fields(self, request, obj=None):
        if obj:  # editing an existing object
            return self.readonly_fields + ('working_group', 'type')
        return self.readonly_fields

    # In the DateRangeFilter this function gets called
    def get_rangefilter_title_title(self, request, field_path):
        return "By Transaction Range"

    @display(ordering="title")
    def name(self, obj: Account):
        return str(obj)

    @display(description="TXs")
    def link_to_tx(self, obj: Account):
        link = reverse("admin:finances_v2_transactionentry_changelist")
        return format_html('<b><a href="{}?account__id__exact={}">TXs</a></b>', link, obj.id)

    @display(ordering="balance")
    def balance(self, obj):
        return Money(amount=obj.balance, currency='EUR') if obj.balance is not None else None


class TransactionEntryInline(nested_admin.NestedTabularInline):
    model = TransactionEntry
    autocomplete_fields = ('person',)
    formset = TransactionEntryFormSet
    can_delete = False
    fields = ('type', 'account', 'amount', "budget", 'person', 'note', 'external_identifier')

    # def get_queryset(self, request):
    #    return super(TransactionEntryInline, self).get_queryset(request).select_related("account", "type", "budget", "person__user")
    def get_readonly_fields(self, request, obj=None):
        if obj:  # editing an existing object
            return ['account', 'amount']
        return self.readonly_fields

    def has_add_permission(self, request, obj=None):
        return False if obj else True


class SimpleTransactionEntryInline(nested_admin.NestedTabularInline):
    model = TransactionEntry
    min_num = 1
    fields = ('type', 'amount', "budget", 'note')


class ReceiptInline(nested_admin.NestedTabularInline):
    model = Receipt


@admin.register(Transaction)
class TransactionAdmin(nested_admin.NestedModelAdmin):
    model = Transaction
    search_fields = ['note', 'transactionentry__person__user__first_name', 'transactionentry__person__user__last_name']
    list_display = ("date", "note",)
    list_filter = [("date", DateRangeFilterBuilder("By Date"))]
    ordering = ["-date"]
    # form = TransactionForm
    inlines = [TransactionEntryInline, ReceiptInline]
    can_delete = False

    class Media:
        css = {
            "all": ["css/table_rotate_headers.css"],
        }

    # def get_queryset(self, request):
    #    return super(TransactionAdmin, self).get_queryset(request).prefetch_related("transactionentry_set")
    def save_related(self, request, form, formsets, change):
        form.save_m2m()
        print("formsets", formsets)
        formsets[1].save()
        entries = formsets[0].save(commit=False)
        for entry in entries:
            if entry.id:
                entry.save()
        if not all([entry.id for entry in entries]):
            TransactionEntry.objects.bulk_create(entries)

    def get_formset_kwargs(self, request, obj, inline, prefix):
        index = 0
        initial = []
        while True:
            entry = request.GET.getlist(f"{prefix}-{index}")
            if not entry:
                if index < inline.extra:
                    index += 1
                    initial.append(None)
                    continue
                break
            initial.append(dict(item.split('=') for item in entry))
            index += 1

        result = super().get_formset_kwargs(request, obj, inline, prefix)
        result["initial"] = initial
        return result

    def has_delete_permission(self, request, obj: Transaction = None):  # note the obj=None
        return obj and obj.can_delete()


@admin.register(SimpleTransaction)
class SimpleTransactionAdmin(TransactionAdmin):
    model = SimpleTransaction
    form = SimplyTransactionHeaderForm
    inlines = [SimpleTransactionEntryInline, ReceiptInline]

    def save_related(self, request, form: SimplyTransactionHeaderForm, formsets, change):
        assert change == False
        form.save_m2m()
        formsets[1].save()  # receipts
        entries: [TransactionEntry] = formsets[0].save(commit=False)  # transaction entries

        for entry in entries:
            entry.account = form.cleaned_data["group_account"]
            entry.amount = (-1 if form.cleaned_data["type"] == '-' else 1) * abs(entry.amount)
        entries.append(TransactionEntry(account=form.cleaned_data["transaction_account"], amount=sum(
            [Decimal(entry.amount) for entry in entries]), transaction=entries[0].transaction))
        TransactionEntry.objects.bulk_create(entries)

    def has_delete_permission(self, request, obj=None): return False

    def has_change_permission(self, request, obj=None): return False

    def has_view_permission(self, request, obj=None): return False


class ReceiptFilter(SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = 'Has Receipt'

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'receipt'

    def lookups(self, request, model_admin):
        return (
            ('1', "Has Receipt"),
            ('0', "Has no Receipt"),
        )

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value() == "0":
            return queryset.filter(receipt_count__exact=0)
        elif self.value() == "1":
            return queryset.filter(receipt_count__gt=0)


@admin.register(TransactionEntry)
class TransactionEntryAdmin(TotalsumAdmin):
    search_fields = [
        'note',
        'amount',
        'external_identifier',
        'account__title',
        'type__name',
        'person__user__first_name',
        'person__user__last_name',
        'transaction__note',
        'transaction__date']
    list_display = ("link_to_tx", "tx_note", "person", "date", "amount", "type", "account", "note", "receipt")
    totalsum_list = ('amount',)
    date_hierarchy = 'transaction__date'
    list_filter = ['account',
                   ("transaction__date", DateRangeFilterBuilder("By Transaction Date")),
                   ReceiptFilter,
                   ('amount', NumericRangeFilterBuilder(title="By amount")),
                   ('external_identifier', EmptyFieldListFilter),
                   'type', ]
    list_per_page = 100
    can_delete = False

    class Media:
        css = {
            'all': ('css/amount-text-right-align.css',)
        }

    def get_queryset(self, request):
        query = super(TransactionEntryAdmin, self).get_queryset(request).select_related("person__user", "transaction",
                                                                                        "account", "type")
        return query.annotate(receipt_count=Count("transaction__receipt"))

    @display(description="TX", ordering="transaction__id")
    def link_to_tx(self, obj: TransactionEntry):
        link = reverse("admin:finances_v2_transaction_change", args=[obj.transaction_id])
        return format_html('<b><a href="{}">{}</a></b>', link, obj.transaction_id)

    @display(description="TX Note", ordering="transaction__note")
    def tx_note(self, obj: TransactionEntry):
        return obj.transaction.note

    @display(ordering="transaction__date")
    def date(self, obj):
        return obj.transaction.date

    @display(boolean=True, ordering="receipt_count")
    def receipt(self, obj):
        return obj.receipt_count > 0

    def has_delete_permission(self, request, obj: TransactionEntry = None):
        return obj and obj.transaction.can_delete()

    def has_add_permission(self, request, obj=None):
        return False


class TransactionEntryInlineDisplay(nested_admin.NestedTabularInline):
    model = TransactionEntry
    fields = ('transaction', 'account', 'amount', 'note', "transaction_note")
    readonly_fields = ('transaction', 'account', 'amount', 'note', 'transaction_note')
    can_delete = False
    extra = 0

    def transaction_note(self, obj: TransactionEntry):
        return obj.transaction.note

    def has_add_permission(self, request, obj=None):
        return False

    # Remove object name, see https://stackoverflow.com/a/43349858/10162645
    class Media:
        css = {
            'all': ('css/admin_remove_object_text.css',)
        }


TWO_PLACES = Decimal('0.01')


@admin.register(Budget)
class BudgetAdmin(nested_admin.NestedModelAdmin):
    search_fields = ['name']
    list_display = ("name", "start", "end", "amount", "left", "used", "active")
    inlines = [TransactionEntryInlineDisplay]
    can_delete = False

    class Media:
        css = {
            'all': ('css/amount-text-right-align.css',)
        }

    def get_queryset(self, request):
        return super(BudgetAdmin, self).get_queryset(request).annotate(
            sum=Sum("transactionentry__amount", default=0),
            used=-F('sum'),
            left=F('amount') - F('used')
        )

    def formfield_for_foreignkey(self, db_field, request, **kwargs):
        if db_field.name == "start":
            kwargs["queryset"] = Event.objects.filter(type__isnull=False).order_by('-time')
        return super().formfield_for_foreignkey(db_field, request, **kwargs)

    @display(ordering="used")
    def used(self, obj: Budget):
        return obj.used.quantize(TWO_PLACES)

    @display(ordering="left")
    def left(self, obj: Budget):
        return obj.left.quantize(TWO_PLACES)

    @display()
    def end(self, obj: Budget):
        end_date = obj.end_date()
        return end_date.astimezone(timezone.get_current_timezone()).date() if end_date else None

    @display(boolean=True)
    def active(self, obj: Budget):
        return obj.is_currently_active()


@admin.register(TransactionType)
class TransactionTypeAdmin(admin.ModelAdmin):
    pass


@start_new_thread
def process_csv_file(modeladmin, request, queryset):
    for csv_file in queryset:
        csv_file.file.open(mode="rb")
        content = csv_file.file.read().decode("UTF-8")
        content = content.split("\n")
        content = content[1:-1]

        for line in content:
            csv_parts = line.split(";")
            tx_date = csv_parts[5]
            tx_date = datetime.datetime.strptime(tx_date, "%d.%m.%Y").date()

            tx_person = csv_parts[6].strip()
            tx_ref = csv_parts[10].strip()
            tx_amount = float(csv_parts[11].replace(",", "."))

            if tx_date < timezone.localdate() - timezone.timedelta(days=180):
                continue

            user = match_tx_reference(tx_ref)
            if not user:
                if tx_amount < 0 or int(tx_amount) != tx_amount:
                    continue  # Muss positiv und ganzzahlig sein
                if tx_amount % 6 != 0 and (tx_amount - 10) % 6 != 0:
                    continue  # Muss ein Vielfaches von 6 oder von 10 (Anmeldegebühr) + Vielfaches von 6 sein
                user = match_tx_person(tx_person)
            print("Found user:", user)
            if user:
                base_query = Accounts().NETWORK_BANK.entries.filter(person=user.person, transaction__date=tx_date,
                                                                    amount=tx_amount)
                count = base_query.count()
                print("Found tx:", count)
                if count == 0:
                    Transaction.create(person=user.person, date=tx_date,
                                       entries=[TransactionEntry(amount=tx_amount, account=Accounts().NETWORK_FK),
                                                TransactionEntry(amount=tx_amount, account=Accounts().NETWORK_BANK)])
                elif count > 1:
                    # Falls eine Person wirklich zweimal am Tag die gleiche Summer überwiesen hat, wird das manuell eingetragen
                    transactions = base_query.exclude(transaction__note__icontains="Doppelte Buchung")
                    if transactions.count() > 1:
                        mail.send(
                            'netzag-kasse@hilton.rwth-aachen.de',
                            config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                            subject=f'Überweisungsfehler - Doppelte Buchungen - {user.person} am {tx_date} über {tx_amount}€',
                            message=f"Die doppelten Buchungen bitte mit 'Doppelte Buchung' in der Notiz markieren. Buchungen: {', '.join(map(str, transactions.values_list('transaction__id', flat=True)))}",
                        )
            else:
                mail.send(
                    'netzag-kasse@hilton.rwth-aachen.de',
                    config.EMAIL['EMAIL_ACCOUNT_NOREPLY'],
                    subject='Überweisungsfehler - falscher username - ' + tx_ref,
                    message=line,
                )

        csv_file.file.close()
        csv_file.last_update = timezone.now()
        csv_file.save()

        # Rebalance accounts
        call_command('createinvoices')

        # see https://stackoverflow.com/a/28913218/10162645
        connection.close()


process_csv_file.short_description = "Process csv file"


@admin.register(BankAccountCSV)
class BankAccountCSVAdmin(admin.ModelAdmin):
    actions = [process_csv_file]


class DeletedTransaction(LogEntry):
    class Meta:
        proxy = True


@admin.register(DeletedTransaction)
class DeletedTransactionsAdmin(LogEntryAdmin):
    list_display = [
        "deleted",
        "object_repr",
        "user_url",
    ]
    list_filter = []
    fieldsets = [
        (None, {"fields": ["deleted_by", "deleted"]}),
        ("Deleted Transaction", {"fields": ["note", "transaction_id", "date", "entries"]})
    ]

    @admin.display(description="Deleted By")
    def deleted_by(self, obj: LogEntry):
        return self.user_url(obj)

    @admin.display(description="Deleted")
    def deleted(self, obj: LogEntry):
        return self.created(obj)

    def note(self, obj: LogEntry):
        return obj.changes_dict.get("note", [""])[0]

    def transaction_id(self, obj: LogEntry):
        return obj.object_id

    def date(self, obj: LogEntry):
        return obj.changes_dict.get("date")[0]

    def get_queryset(self, request):
        return super().get_queryset(request).filter(action=LogEntry.Action.DELETE,
                                                    content_type=ContentType.objects.get_for_model(Transaction))

    @admin.display(description="Entries")
    def entries(self, obj: LogEntry):
        entries = LogEntry.objects.filter(content_type=ContentType.objects.get_for_model(TransactionEntry),
                                          changes__transaction__0=f"{obj.object_id}")
        msg = []
        msg.append("<table>")
        msg.append(self._format_header("Amount", "Account", "Person", "Note", "External Identifier"))
        for entry in entries:
            changes: dict = entry.changes_dict
            person_id = changes.get("person", [None])[0]
            person = Person.objects.get(id=person_id).full_name() if person_id else None
            msg.append(self._format_line(changes["amount"][0], Account.objects.get(id=changes["account"][0]).title,
                                         person, changes["note"][0],
                                         changes.get("external_identifier", [""])[0]))
        msg.append("</table>")

        return mark_safe("".join(msg))
