"""hiltonadmin URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path

from hiltonadmin import settings
from main import views as main
import private_storage.urls

admin.site.site_header = 'Administration - Student Dorm "Turmstraße 1"'

urlpatterns = [
    re_path(r'^admin/', admin.site.urls),
    re_path('nested_admin/', include('nested_admin.urls'), name="nested_admin"),
    path('association/', include('association.urls')),
    path('networking/', include('networking.urls')),
    path('game_rental/', include('game_rental.urls')),
    path('nordigen/', include('nordigen_app.urls')),
    path('user/', include('main.urls')),
    path('workinggroups/', include('workinggroups.urls')),
    path('statistics/', include('hilton_statistics.urls')),
    path('tinymce/', include('tinymce.urls')),
    path('finances/', include('finances_v2.urls')),
    path('laundry_room/', include('laundry_room.urls')),

    re_path(r'^$', main.profile, name='profile'),
    re_path('^private-media/', include(private_storage.urls)),
    re_path(r'^', include('django_telegrambot.urls')),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

if settings.DEBUG:
    import debug_toolbar

    urlpatterns = [
        path('__debug__/', include(debug_toolbar.urls)),
    ] + urlpatterns
