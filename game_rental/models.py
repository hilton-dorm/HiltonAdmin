from django.db import models
from django.utils import timezone
from private_storage.fields import PrivateImageField, PrivateFileField

from association.models import SocialService
from main.models import Person, Event


class Game(models.Model):
    active = models.BooleanField(default=True, help_text="Inactive game are not shown and can not be rented")
    name = models.CharField(max_length=256)
    description = models.TextField()
    image = PrivateImageField(upload_to='game_rental/images')
    content = models.TextField(blank=True)
    min_players = models.IntegerField(blank=True, null=True)
    max_players = models.IntegerField(blank=True, null=True)
    min_avg_time = models.IntegerField(blank=True, null=True)
    max_avg_time = models.IntegerField(blank=True, null=True)
    instructions_1 = PrivateFileField(upload_to='game_rental/instructions', null=True, blank=True)
    instructions_2 = PrivateFileField(upload_to='game_rental/instructions', null=True, blank=True)

    def num_players_text(self):
        if self.min_players and self.max_players:
            if self.min_players == self.max_players:
                return f"{self.min_players}"
            return f"{self.min_players} - {self.max_players}"
        if self.min_players:
            return f"{self.min_players} +"
        return "Players count unknown"

    def avg_time_text(self):
        if self.min_avg_time and self.max_avg_time:
            return f"{self.min_avg_time} - {self.max_avg_time} min"
        if self.min_avg_time:
            return f"{self.min_avg_time} min"
        return "Average time unknown"

    def is_currently_rented_out(self):
        return self.gamerental_set.filter(rented_at__lte=timezone.now(), took_back_at__isnull=True).exists()

    def __str__(self):
        return self.name


class Category(models.Model):
    name = models.CharField(max_length=100)
    games = models.ManyToManyField(Game)

    def __str__(self):
        return self.name


class GameRental(models.Model):
    person = models.ForeignKey(Person, on_delete=models.CASCADE)
    start_date = models.DateField()
    end_date = models.DateField()
    rented_at = models.DateTimeField(null=True, blank=True, help_text="When were the games handed over")
    took_back_at = models.DateTimeField(null=True, blank=True, help_text="When were the games returned")
    rented_from = models.ForeignKey(Person, related_name="rented_from",
                                    on_delete=models.SET_NULL, null=True, blank=True)
    took_back_from = models.ForeignKey(Person, related_name="took_back_from",
                                       on_delete=models.SET_NULL, null=True, blank=True)

    games = models.ManyToManyField(Game)
    note = models.TextField(default="", blank=True)
    STATE_PENDING = 'P'
    STATE_ACCEPTED = 'A'
    STATE_DENIED = 'D'
    STATE_CHOICES = (
        (STATE_PENDING, 'Pending'),
        (STATE_ACCEPTED, 'Accepted'),
        (STATE_DENIED, 'Denied'),
    )
    state = models.CharField(max_length=1, choices=STATE_CHOICES, default=STATE_PENDING)

    def __str__(self):
        state = self.get_state_display()
        if self.took_back_from:
            state = "Returned"
        elif self.rented_from:
            state = "Rented"
        return f"[{state}] {self.person} from {self.start_date} until {self.end_date}"

    @staticmethod
    def is_game_free(game, start_date, end_date):
        return GameRental.objects.filter(accepted=True, start_date__lte=end_date, end_date__gte=start_date,
                                         games__exact=game).exists()

    def conflicting_events(self):
        return [{"name": e.name, "time": e.time} for e in Event.objects.filter(time__date__gte=self.start_date, time__date__lte=self.end_date)] +\
               [{"name": e.name, "time": e.time} for e in SocialService.objects.filter(
                   time__date__gte=self.start_date, time__date__lte=self.end_date, is_new_tenant_bar=True)]
