from django.urls import path

from game_rental import views

urlpatterns = [
    path('games', views.games, name='games'),
]
