import nested_admin.nested
from django.contrib import admin
from nested_admin.nested import NestedModelAdmin

from game_rental.models import Game, Category, GameRental


class CategoryInline(nested_admin.nested.NestedTabularInline):
    model = Category.games.through
    autocomplete_fields = ('category',)
    can_delete = False
    verbose_name = "Category"
    verbose_name_plural = "Categories"
    extra = 0  # Otherwise 3 empty relations are shows


@admin.register(Game)
class GameAdmin(NestedModelAdmin):
    search_fields = ("name",)
    inlines = [CategoryInline]


class GameInline(nested_admin.nested.NestedTabularInline):
    model = Category.games.through
    autocomplete_fields = ('game',)
    can_delete = False
    verbose_name = "Game"
    verbose_name_plural = "Games"
    extra = 0  # Otherwise 3 empty relations are shows


@admin.register(Category)
class CategoryAdmin(NestedModelAdmin):
    inlines = [GameInline]
    search_fields = ("name",)
    fields = ("name",)


class GameInlineForRental(GameInline):
    model = GameRental.games.through
    list_display = ("get_name",)

    def get_name(self, obj):
        return obj.game.content

    get_name.admin_order_field = 'author'  # Allows column order sorting
    get_name.short_description = 'Author Name'  # Renames column head


@admin.register(GameRental)
class GameRentalAdmin(NestedModelAdmin):
    inlines = [GameInlineForRental]
    fields = ("person", "state", "start_date", "end_date", "rented_at", "rented_from",
              "took_back_at", "took_back_from", "note")
    autocomplete_fields = ("person", "rented_from", "took_back_from")
    search_fields = ['person__user__first_name', 'person__user__last_name']
    list_filter = ['state']
