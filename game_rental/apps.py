from django.apps import AppConfig


class GameRentalConfig(AppConfig):
    name = 'game_rental'
