import datetime

from django.contrib.auth.decorators import login_required, user_passes_test
from django.contrib import messages
from django.db.models import ExpressionWrapper, Q, BooleanField
from django.forms import model_to_dict, forms, DateField, CharField, BooleanField as FormBooleanField
from django.http import HttpResponse, JsonResponse, HttpResponseRedirect
from django.shortcuts import redirect
from django.template import loader
from django.urls import reverse_lazy, reverse
from django.utils.timezone import now
import json

from post_office import mail

from association.models import Membership, is_member_association
from common.util.json import dump_json
from game_rental.models import Game, Category, GameRental
import config
from workinggroups.models import WorkingGroup


@login_required
@user_passes_test(is_member_association, login_url=reverse_lazy('no_permission'))
def games(request):
    class AcceptTermsForm(forms.Form):
        accepted = FormBooleanField()

    if request.method == 'POST':
        form = AcceptTermsForm(request.POST)
        if form.is_valid() and form.cleaned_data['accepted']:
            membership = Membership.get_current_membership_for(request.user.person)
            membership.agreed_k14ag_terms = True
            membership.save()
            return redirect("games")

    class GameActionForm(forms.Form):
        id = CharField()
        note = CharField(required=False)
        email_message = CharField(required=False)
        action = CharField()

    if request.method == 'POST':
        form = GameActionForm(request.POST)
        if form.is_valid():
            rental = GameRental.objects.get(id=form.cleaned_data['id'])
            action = form.cleaned_data['action']
            rental.note = form.cleaned_data['note']
            if rental.state == GameRental.STATE_PENDING and action in ["deny", "accept"]:
                rental.state = GameRental.STATE_ACCEPTED if action == "accept" else GameRental.STATE_DENIED
                mail.send(
                    rental.person.user.email,
                    WorkingGroup.objects.get(name__icontains="k14").email,
                    subject=f"Your game rental request was {rental.get_state_display()}",
                    message=form.cleaned_data['email_message']
                )
            elif not rental.rented_at and action == "rent_out":
                rental.rented_at = now()
                if rental.start_date > rental.rented_at.date():
                    rental.start_date = rental.rented_at.date()
                rental.rented_from = request.user.person
            elif not rental.took_back_at and action == "took_back":
                rental.took_back_at = now()
                if rental.end_date > rental.took_back_at.date():
                    rental.end_date = rental.took_back_at.date()
                rental.took_back_from = request.user.person
            rental.save()
            return HttpResponseRedirect(reverse("games") + "?was_admin=true")

    class RequestForm(forms.Form):
        start_date = DateField()
        end_date = DateField()
        games = CharField()

    if request.method == 'POST':
        form = RequestForm(request.POST)
        if form.is_valid():
            # Terms akzeptiert
            start_date = form.cleaned_data["start_date"]
            end_date = form.cleaned_data["end_date"]
            if start_date < now().date():
                messages.error("Start date is in the past")
            elif end_date > start_date + datetime.timedelta(days=14):
                messages.error("You can only rent games for 14 days max")
            else:
                game_ids = form.cleaned_data["games"].split(",")
                rental = GameRental(person=request.user.person, start_date=start_date,
                                    end_date=end_date)
                rental.save()  # we need an id for the next line
                rental.games.set(game_ids)
                mail.send(
                    WorkingGroup.objects.get(name__icontains="k14").email,
                    config.EMAIL["EMAIL_ACCOUNT_NOREPLY"],
                    template='new_game_rental_request',
                    context={'request': rental},
                )
                messages.success(request, "Your game request was created.")
                return redirect("games")

    template = loader.get_template('game_rental/games.html')

    games = []
    for game in Game.objects.filter(active=True):
        games.append(model_to_dict(game))
        games[-1]["image"] = game.image.url
        games[-1]["num_players"] = game.num_players_text()
        games[-1]["avg_time"] = game.avg_time_text()
        games[-1]["instructions_1"] = game.instructions_1.url if game.instructions_1 else None
        games[-1]["instructions_2"] = game.instructions_2.url if game.instructions_2 else None
        games[-1]["selected"] = False
        games[-1]["full_description"] = len(game.description) < 200
        games[-1]["times"] = list(game.gamerental_set.filter(
            Q(state=GameRental.STATE_ACCEPTED) & (Q(end_date__gt=now()) | Q(took_back_from__isnull=True)))
            .values("start_date", "end_date")
            .annotate(you=ExpressionWrapper(Q(person_id=request.user.person.id),
                                            output_field=BooleanField()),
                      given_back=ExpressionWrapper(Q(took_back_from__isnull=False),
                                                   output_field=BooleanField())))
        games[-1]["categories"] = list(game.category_set.values_list("name", flat=True))

    context = {
        'is_admin': WorkingGroup.objects.filter(name__icontains="k14").first().is_member(request.user.person),
        'games': dump_json(games),
        'categories': json.dumps(list(Category.objects.all().values_list("name", flat=True))),
        'old_rentals': request.user.person.gamerental_set.filter(
            Q(end_date__lt=now()) | Q(took_back_from__isnull=False)),
        'requests': request.user.person.gamerental_set.filter(end_date__gte=now(), took_back_from__isnull=True),
        'admin_requests': GameRental.objects.filter(took_back_from__isnull=True).exclude(state=GameRental.STATE_DENIED).order_by('-start_date'),
        'was_admin': 'was_admin' in request.GET,
        'email': WorkingGroup.objects.get(name__icontains="k14").email,
        'accepted_terms': Membership.get_current_membership_for(request.user.person).agreed_k14ag_terms
    }
    return HttpResponse(template.render(context, request))
