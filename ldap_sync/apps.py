from django.apps import AppConfig


class NetworkingConfig(AppConfig):
    name = 'ldap_sync'
