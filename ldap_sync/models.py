from __future__ import unicode_literals

import ldapdb.models
from ldapdb.models.fields import CharField, ListField

import config


class LdapPerson(ldapdb.models.Model):
    """
    Class that represents the LDAP user entry.
    Changes on this model will result in changes in the ldap database.
    """
    # LDAP meta-data
    base_dn = config.LDAP['ORGANISATION_PEOPLE'] + ',' + config.LDAP['BASE_DN']
    object_classes = ['organizationalPerson', 'person', 'inetOrgPerson', 'ownCloud']

    # inetOrgPerson
    username = CharField(db_column='cn', primary_key=True)
    password = CharField(db_column='userPassword')
    first_name = CharField(db_column='givenName')
    last_name = CharField(db_column='sn')
    full_name = CharField(db_column='displayName')
    email = CharField(db_column='mail')
    uuid = CharField(db_column='entryUUID')

    # ownCloud
    cloud_quota = CharField(db_column='ownCloudQuota')

    def __str__(self):
        return self.username

    def __unicode__(self):
        return self.full_name


class LdapGroup(ldapdb.models.Model):
    base_dn = config.LDAP['BASE_DN']
    object_classes = ['top', 'groupOfNames']

    # groupOfNames
    name = CharField(db_column='cn', max_length=200, primary_key=True)
    description = CharField(db_column='description')
    members = ListField(db_column='member', default=[])

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.name
