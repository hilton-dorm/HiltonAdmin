from django.contrib.auth.models import User, Group
from django.core.management.base import BaseCommand

import config
from ldap_sync.models import LdapGroup


class Command(BaseCommand):
    help = 'Updates all people within ldap'

    def handle(self, *args, **options):
        base_dn = config.LDAP['ORGANISATION_PEOPLE'] + ',' + config.LDAP['BASE_DN']

        for group in Group.objects.all():
            usernames = group.user_set.values_list('username', flat=True)
            usernames = ['cn=' + user + ',' + base_dn for user in usernames]
            if len(usernames) == 0:
                continue

            ldap_group: LdapGroup = LdapGroup.objects.filter(name=group.name).first()
            working_group_name: str = group.workinggroup_set.first().name
            index = working_group_name.find("(")
            if index != -1:
                working_group_name = working_group_name[:index].strip()
            if ldap_group:
                # Clears and adds all users that belong to the group
                ldap_group.members.clear()
                ldap_group.members = usernames
                ldap_group.description = working_group_name
                ldap_group.save()
            else:
                LdapGroup.objects.create(name=group.name, members=usernames, description=working_group_name)
