from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from ldap_sync.models import LdapPerson


class Command(BaseCommand):
    help = 'Updates all people within ldap'

    def add_arguments(self, parser):
        parser.add_argument('-u', '--username', type=str, help='Only sync a specific person', )

    def handle(self, *args, **options):
        username = options['username']

        if username:
            users = User.objects.filter(username=username)
        else:
            users = User.objects.all()

        for user in users:
            # Searches for the corresponding ldap entry
            try:
                ldap_person, created = LdapPerson.objects.get_or_create(
                    username=user.username, defaults={'last_name': user.last_name})
            except Exception as e:
                print("Error ", e, " User: ", user)
                continue

            # Set the full_name attribute manually because the default user model does not have such attribute
            setattr(ldap_person, 'full_name', user.get_full_name())

            # Attributes in this list will be synced. Values from the user object will get adopted.
            attributes_user = [
                'first_name',
                'last_name',
                'email'
            ]
            attributes_person = [
                'cloud_quota'
            ]

            # Iterate over the user attributes
            for attribute in attributes_user:
                value = str(getattr(user, attribute))
                setattr(ldap_person, attribute, value)

            # Iterate over the person attributes
            for attribute in attributes_person:
                value = str(getattr(user.person, attribute))
                setattr(ldap_person, attribute, value)

            try:
                ldap_person.save()
            except Exception as e:
                print("Error while saving: ", e, " User: ", user)
                continue
