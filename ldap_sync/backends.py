from django.contrib.auth import backends, get_user_model
import ldap

import config
from ldap3 import (
    HASHED_SALTED_SHA
)
from ldap3.utils.hashed import hashed


class AuthenticationBackend(backends.ModelBackend):
    """
    Custom authentication Backend for login using email,phone,username
    with password
    """

    def authenticate(self, request, username=None, password=None, **kwargs):
        usermodel = get_user_model()

        try:
            user = usermodel._default_manager.get_by_natural_key(username)
        except usermodel.DoesNotExist:
            pass
        else:
            if user.check_password(password) and self.user_can_authenticate(user):
                # User is authenticated at this point. We intercept the clear text password.
                # For the sake of simplicity the LDAP password is updated and authenticate() is called again
                try:
                    conn = ldap.initialize(config.LDAP["NAME"])
                    conn.simple_bind_s(config.LDAP["USER"], config.LDAP["PASSWORD"])

                    # change the password
                    tdn = 'cn=' + username + ',' + config.LDAP['ORGANISATION_PEOPLE'] + ',' + config.LDAP['BASE_DN']
                    hashed_password = hashed(HASHED_SALTED_SHA, password)
                    mod_attrs = [(ldap.MOD_REPLACE, 'userPassword', str.encode(hashed_password)), ]
                    conn.modify_s(tdn, mod_attrs)
                except Exception:
                    pass
        return super().authenticate(request, username=username, password=password)
