from django.contrib import admin

from ldap_sync.models import LdapPerson


class LdapPersonAdmin(admin.ModelAdmin):
    exclude = ['dn', 'objectClass']
    list_display = ['first_name', 'last_name', 'email', 'cloud_quota']


"""
class LdapGroupField(forms.ModelMultipleChoiceField):
    def clean(self, value):
        # TODO and WARNING validation is disabled
        return value


class LdapGroupForm(forms.ModelForm):
    members = LdapGroupField(queryset=LdapPerson.objects.all(),
                             widget=FilteredSelectMultiple('Members', is_stacked=False), required=True,
                             to_field_name='dn')

    class Meta:
        model = LdapGroup

    def clean_members(self):
        data = self.cleaned_data['members']
        if not data:
            raise ValidationError(_('Enter a list of values.'), code='list')
        return data


class LdapGroupAdmin(object):
    form = LdapGroupForm
    exclude = ['dn', 'objectClass']
    list_display = ['name']
    search_fields = ['name']

admin.site.register(LdapGroup, LdapGroupAdmin)
"""
admin.site.register(LdapPerson, LdapPersonAdmin)
